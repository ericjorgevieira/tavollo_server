<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SwaggerGenerate extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'swagger:generate';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Generate Swagger Docs and put them in /public';

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    */
    public function handle()
    {
        $swagger = \Swagger\scan(app_path());
        $swagger->saveAs(public_path('swagger.json'));
    }
}
