<?php

namespace App\Drivers;

use App\Models\Bill;
use App\Models\Order;
use App\Models\OrderProductGroupItem;
use App\Models\Product;
use App\Models\ProductGroupItem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DriverRaffinato
{
    protected static $establishment;

    const TYPE_PRODUCT = 1;
    const TYPE_GROUPITEM = 3;
    const TYPE_SUBITEM = 4;
    const TYPE_DEFAULTPORTION = 5;

    public static $types = [
      self::TYPE_PRODUCT => 'Product',
      self::TYPE_GROUPITEM => 'ProductGroupItem',
      self::TYPE_SUBITEM => 'ProductSubItem',
      self::TYPE_DEFAULTPORTION => 'ProductDefaultPortion',
    ];

    /**
     * Processa o carregamento de dados do extrato de uma determinada conta.
     *
     * @param [type] $billId [description]
     *
     * @return [type] [description]
     */
    public static function loadExtract($billId)
    {
        $bill = Bill::where(['id' => $billId])->get()->first();
        if ($bill && ($bill->status == Bill::STATUS_OPENED || $bill->status == Bill::STATUS_CLOSING)) {
            $bill->has_sync = 0;
            $bill->save();

            $table = $bill->spot;
            self::$establishment = $table->establishment;
            $isConsumptionCard = $table->is_consumption_card;
            Log::info('Realizando requisição de consulta de extrato para bill_id: '.$bill->id);

            try {
                $url = self::$establishment->config->bill_extract_url;
                if ($url) {
                    $isConsumptionCard = ($table->is_consumption_card == 1) ? 'true' : 'false';

                    $url = str_replace('{external_id}', intval($table->external_id), $url);
                    $url = str_replace('{is_consumption_card}', $isConsumptionCard, $url);
                    if (filter_var($url, FILTER_VALIDATE_URL) !== false) {
                        $client = new \GuzzleHttp\Client();
                        $response = $client->get($url);
                        if ($response->getStatusCode() == 200) {
                            $json = json_decode($response->getBody()->getContents(), JSON_OBJECT_AS_ARRAY);
                            if (isset($json['error'])) {
                                $bill->external_info = 'Erro do extrato da Integração: '.$json['error'];
                                $bill->save();
                            } else {
                                if (self::checkIfOrdersSync($bill, $json['result'][0])) {
                                    self::syncBillData($bill, $json['result'][0]);
                                } else {
                                    Log::info('Não houve necessidade de sincronização de contas.');
                                }
                            }
                        } else {
                            Log::error('ERRO '.$response->getStatusCode());
                        }
                    }
                }
            } catch (\Exception $e) {
                Log::error($e->getMessage());
            }
        }
    }

    /**
     * Envia os dados para confirmar pagamento de conta na Integração.
     *
     * @var [type]
     */
    public static function sendPaymentConfirmation(Bill $bill, $paidValue)
    {
        Log::info('Realizando requisição de confirmação de pagamento para bill_id: '.$bill->id);

        try {
            $idvenda = $bill->external_id;
            $parcela = 1;
            $url = self::$establishment->config->confirm_payment_url;

            // Log::info('Url de requisição:');
            // Log::info($url);
            if (filter_var($url, FILTER_VALIDATE_URL) !== false) {
                $paramsUrl = explode('&', $url);
                $token = explode('?token=', $paramsUrl[0]);
                $url = $token[0];
                $token = $token[1];
                $subdomain = explode('=', $paramsUrl[1]);
                $subdomain = $subdomain[1];

                $client = new \GuzzleHttp\Client(['base_uri' => $url]);
                $formParams = [
                    'token' => $token,
                    'subdominio' => $subdomain,
                    'idvenda' => intval($idvenda),
                    'pagamentos' => [
                        'valor' => $paidValue,
                        'parcela' => $parcela,
                    ],
                ];

                $response = $client->request('GET', '', ['headers' => ['Content-Type' => 'application/x-www-form-urlencoded'],
          'form_params' => $formParams, ]);

                if ($response->getStatusCode() == 200) {
                    return true;
                } else {
                    Log::error('ERRO '.$response->getStatusCode());

                    return false;
                }
            } else {
                Log::info('Não há url definida.');

                return false;
            }
        } catch (\Exception $e) {
            Log::error('ERRO '.$response->getStatusCode().' : '.$e->getMessage());

            return true;
        }
    }

    /**
     * Sincroniza os dados de valores da conta.
     *
     * @param Bill   $bill     [description]
     * @param [type] $response [description]
     *
     * @return [type] [description]
     */
    protected static function syncBillData(Bill $bill, $response)
    {
        $bill->external_id = $response['idvenda'];
        $bill->amount_paid = $response['valorpago'];
        $bill->remaining_value = $response['valorrestante'];
        $bill->discount = $response['valordesconto'];
        $bill->save();

        self::processOrders($bill, $response);
    }

    /**
     * Verifica se possui divergência na conta do extrato com a conta do Tavollo.
     *
     * @param Bill   $bill     [description]
     * @param [type] $response [description]
     *
     * @return [type] [description]
     */
    protected static function checkIfOrdersSync(Bill $bill, $response)
    {
        $hasSameCount = false;
        $hasSameValue = false;
        $hasSameProductsWithExternalIdCount = false;

        //Se external_id da conta estiver preenchido e for diferente do idvenda
        if ($bill->external_id && $bill->external_id != $response['idvenda']) {
            $bill->external_info = 'Id Venda da integração não bate com o External Id desta comanda.';
            $bill->save();

            return false;
        }

        //Caso idvenda esteja em outra comanda do estabelecimento, retorna false
        if (!$bill->external_id || empty($bill->external_id)) {
            if (Bill::where(['establishment_id' => self::$establishment->id, 'external_id' => $response['idvenda']])->count() > 0) {
                $bill->external_info = 'Id Venda da integração consta em outra comanda.';
                $bill->save();

                return false;
            }
        }

        //Verifica se conta e extrato possuem mesma quantidade de pedidos
        $orders = $bill->orders()->whereIn('status', ['finished', 'preparation'])->get(['id', 'product_id', 'cache_price', 'external_id']);
        $items = $response['itens'];
        $countItems = 0;

        foreach ($response['itens'] as $i) {
            if ($i['idtiporegistro'] == self::TYPE_PRODUCT) {
                ++$countItems;
            }
        }
        Log::info('Total de pedidos da conta '.$bill->id.' no Tavollo: '.count($orders));
        Log::info('Total de pedidos da conta '.$bill->id.' no extrato: '.$countItems);
        if (count($orders) == $countItems) {
            $hasSameCount = true;
        }
        //Verifica se os pedidos sem identificadorintegracao estão em mesma quantidade
        $countOrdersWithExternalId = array_filter($orders->toArray(), function ($order) {
            return $order['external_id'] != '' && $order['external_id'] != null;
        });

        $countItemsWithoutIdentificador = array_filter($items, function ($item) {
            return $item['identificadorintegracao'] != '';
        });
        $hasSameProductsWithExternalIdCount = count($items) > 0 && count($countOrdersWithExternalId) == count($countItemsWithoutIdentificador);

        //Verifica se valor da conta e valor do extrato estão diferentes
        $total = 0;
        foreach ($orders as $order) {
            // $total += ($order->updateCachePrice());
            if ($order->product && $order->product->calculate_baksheesh === 1) {
                $total += $order->cache_price;
            }
        }
        $total = $total * 100 / 100;
        Log::info('Valor da conta '.$bill->id.' no Tavollo: '.$total);
        Log::info('Valor da conta '.$bill->id.' no extrato: '.$response['valortotal']);
        if ($response['valortotal'] > 0 && abs(($total - $response['valortotal']) / $response['valortotal']) < 0.00001) {
            $hasSameValue = true;
        }
        //Caso haja divergencia, retorna true para processar sincronização
        if ($hasSameCount && $hasSameValue && $hasSameProductsWithExternalIdCount) {
            Log::info('Conta de id '.$bill->id.' possui mesmo valor e quantidade de itens que a integração.');
            $bill->has_sync = 1;
            $bill->save();

            return false;
        }
        $stringHasSameValue = ($hasSameValue) ? 'false' : 'true';
        $stringHasSameCount = ($hasSameCount) ? 'false' : 'true';
        $stringHasSameProductsWithEID = ($hasSameProductsWithExternalIdCount) ? 'false' : 'true';
        Log::info('Conta de id '.$bill->id.' possui divergência de valor = '.$stringHasSameValue.
       ' e de quantidade de itens = '.$stringHasSameCount.' e de quantidade de itens com external_id = '.$stringHasSameProductsWithEID);

        return true;
    }

    protected static function clearItensWithoutExternalId(Bill $bill)
    {
        Log::info('Limpando orders da conta '.$bill->id.' sem o external_id.');
        //Apaga todos os pedidos sem external_id que estiverem fechados
        $result = DB::table('orders')->whereNull('external_id')->where(['status' => 'finished', 'bill_id' => $bill->id])->delete();
    }

    protected static function clearItemsWithExternalId(Bill $bill, $items)
    {
        Log::info('Limpando orders da conta '.$bill->id.' que possuem external_id');
        //Verifica se todos os pedidos com external_id continuam no extrato
        $arrExternalIds = array_filter($items, function ($item) {
            return $item['idtiporegistro'] == self::TYPE_PRODUCT && $item['identificadorintegracao'] != '';
        });

        //Verifica se os pedidos da conta estão no extrato, caso não estejam, marca como cancelado
        $orders = $bill->orders()->whereIn('status', ['finished', 'preparation', 'trash'])->get();
        foreach ($orders as $o) {
            if (!array_search($o->id, array_column($arrExternalIds, 'identificadorintegracao'))
              && !array_search($o->external_id, array_column($arrExternalIds, 'idvendaitem'))) {
                $o->status = 'trash';
                $o->save();
                Log::info('Pedido '.$o->id.' não consta no extrato da conta '.$bill->id);
            }
        }
    }

    /**
     * Processa a sincronização de todos os pedidos divergentes.
     *
     * @param Bill   $bill     [description]
     * @param [type] $response [description]
     *
     * @return [type] [description]
     */
    protected static function processOrders(Bill $bill, $response)
    {
        $actualOrderId = null;
        // self::clearItensWithoutExternalId($bill);
        self::clearItemsWithExternalId($bill, $response['itens']);

        if (isset($response['itens']) && $response['itens']) {
            foreach ($response['itens'] as $k => $responseItem) {
                $responseItemType = $responseItem['idtiporegistro'];
                $responseIdItemSale = $responseItem['idvendaitem'];
                $responseItemExternalProductId = $responseItem['idproduto'];
                $responseItemExternalId = $responseItem['identificadorintegracao'];
                $responseItemQuant = $responseItem['quantidade'];
                $responseItemPrice = $responseItem['valortotal'];

                if ($responseItemType == self::TYPE_PRODUCT) {
                    $product = Product::where(['code' => $responseItemExternalProductId,
                'establishment_id' => self::$establishment->id, 'status' => 'enabled', ])->first();

                    if ($product) {
                        //Resgata o pedido pelo id
                        //verificar se produto pertence ao estabelecimento da conta
                        $order = Order::where(['external_id' => $responseIdItemSale, 'product_id' => $product->id])->orWhere(['id' => $responseItemExternalId])->first();
                        // $order = $bill->getOrderByExternalId($responseItemExternalId);

                        //Se pedido existir (foi lançado do Tavollo para o Raffinato)
                        if ($order) {
                            //Força o pedido para a conta consultada
                            $order->bill_id = $bill->id;
                            //Apaga todos os itens do pedido atual
                            $order->items()->delete();
                            $order->quant = $responseItemQuant;
                            $order->cache_price = $responseItemPrice;
                            $order->external_id = $responseIdItemSale;
                            $order->status = 'finished';
                            $order->save();
                            Log::info('Status de pedido '.$order->id.' alterado via atualização de extrato.');
                        }
                        //Caso pedido não tenha sido inserido
                        else {
                            //Caso pedido realmente não exista, insere na conta
                            $order = new Order();
                            $order->bill_id = $bill->id;
                            $order->product_id = $product->id;
                            $order->quant = $responseItemQuant;
                            $order->cache_price = $responseItemPrice;
                            $order->status = 'finished';
                            $order->external_id = $responseIdItemSale;
                            $order->by_admin = 1;
                            $order->save();
                            Log::info('Pedido inserido: '.$order->id.' via atualização de extrato.');
                        }
                        //Salva o cache do pedido para a próxima iteração
                        $actualOrderId = $order->id;
                    } else {
                        Log::info('Produto de código externo '.$responseItemExternalProductId.' para estabelecimento '.self::$establishment->id.' não consta na base do Tavollo.');
                    }
                } elseif ($responseItemType == self::TYPE_GROUPITEM) {
                    $productItem = ProductGroupItem::where(['external_id' => $responseItemExternalProductId,
                'establishment_id' => self::$establishment->id, ])->get()->first();
                    if ($productItem) {
                        //Insere no actualOrder
                        $orderItem = new OrderProductGroupItem();
                        $orderItem->order_id = $actualOrderId;
                        $orderItem->product_group_id = $productItem->product_group_id;
                        $orderItem->product_group_item_id = $productItem->id;
                        $orderItem->quant = $responseItemQuant;
                        $orderItem->value = $responseItemPrice;
                        $orderItem->external_id = $responseIdItemSale;
                        $orderItem->save();
                    }
                }
            }
        }
        $bill->has_sync = 1;
        $bill->save();
    }
}
