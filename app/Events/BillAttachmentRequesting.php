<?php

namespace App\Events;

use App\Models\BillAttachmentRequest;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BillAttachmentRequesting implements ShouldBroadcast
{
    use SerializesModels;

    public $request;

    protected $md5;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(BillAttachmentRequest $request, User $user)
    {
        $this->request = $request;
        $this->user = $user;
        $this->md5 = md5($this->request->bill->id);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('bill-attachment-request.' . $this->md5);
    }
}
