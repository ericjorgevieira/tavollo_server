<?php

namespace App\Events;

use App\Models\BillAttachmentRequest;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class HasAttached implements ShouldBroadcast
{
    use SerializesModels;

    public $request;

    public $user;

    protected $md5;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, $response = false)
    {
        $this->user = $user;
        $this->request = $response;
        $this->md5 = md5($user->id);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('has-attached.' . $this->md5);
    }
}
