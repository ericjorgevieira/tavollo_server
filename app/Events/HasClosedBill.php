<?php

namespace App\Events;

use App\Models\Bill;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class HasClosedBill implements ShouldBroadcast
{
    use SerializesModels;

    public $bill;

    protected $md5;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Bill $bill)
    {
        $this->bill = $bill;
        $this->md5 = md5($this->bill->id);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('has-closed-bill.' . $this->md5);
    }
}
