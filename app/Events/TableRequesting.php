<?php

namespace App\Events;

use App\Models\TableRequest;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TableRequesting implements ShouldBroadcast
{
    use SerializesModels;

    public $request;

    protected $md5;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(TableRequest $request)
    {
        $this->request = $request;
        $this->md5 = md5($this->request->establishment->id);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('table-request.' . $this->md5);
    }
}
