<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
    AuthorizationException::class,
    HttpException::class,
    ModelNotFoundException::class,
    ValidationException::class,
  ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof AuthorizationException) {
            return response()->view('errors.authorization', [], 500);
        }

        if ($e instanceof HttpException) {
            return response()->view('errors.authorization', [], 401);
        }

        if ($e instanceof ModelNotFoundException) {
            return response()->view('errors.authorization', [], 404);
        }

        if ($e instanceof FatalThrowableError) {
            Log::info('Limite de requisições por minuto atingido.');

            return response()->content = ['error' => true, 'message' => 'Fatal Error. Check logs.'];
        }

        if ($e instanceof FatalErrorException) {
            Log::info('Limite de requisições por minuto atingido.');

            return response()->content = ['error' => true, 'message' => 'Fatal Error. Check logs.'];
        }

        if ($e instanceof \Illuminate\Session\TokenMismatchException) {
            return redirect('/')->withErrors(['token_error' => 'Descupe, sua sessão expirou. Tente novamente.']);
        }

        return parent::render($request, $e);
    }

    /**
     * Convert a validation exception into a JSON response.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function invalidJson($request, ValidationException $exception)
    {
        return response()->json($exception->errors(), $exception->status);
    }
}
