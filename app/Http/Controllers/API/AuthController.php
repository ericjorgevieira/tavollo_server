<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\API\APIRepository;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $repository;

    protected $user;

    public function __construct(APIRepository $repository)
    {
        $this->repository = $repository;

        $this->middleware(function ($request, $next) {
            if ($request->attributes->get('establishment')) {
                $this->repository->setEstablishment($request->attributes->get('establishment'));
                $this->user = $request->attributes->get('user');
            }

            return $next($request);
        });
    }

    public function index(Request $request)
    {
        return view('errors.apiauthok');
    }

    /**
     * Processa autenticação de usuários no Manager App.
     *
     * @return void
     */
    public function auth(Request $request)
    {
        return $this->repository->authUser($request);
    }

    /**
     * Listagem de Garçons.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function waiters(Request $request, $waiter = null)
    {
        $status = $request->input('status');

        return $this->repository->listWaiters($waiter, $status);
    }
}
