<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\API\APIRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BillsController extends Controller
{
    protected $repository;

    protected $user;

    public function __construct(APIRepository $repository)
    {
        $this->repository = $repository;

        $this->middleware(function ($request, $next) {
            if ($request->attributes->get('establishment')) {
                $this->repository->setEstablishment($request->attributes->get('establishment'));
                $this->user = $request->attributes->get('user');
            }

            return $next($request);
        });
    }

    /**
     * Listagem de Contas.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function bills(Request $request, $bill = null)
    {
        $status = $request->input('status');

        return $this->repository->listBills($bill, $status);
    }

    /**
     * Cria uma Conta em um determinado Spot.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function createBill(Request $request)
    {
        $table = $request->input('table');
        $username = $request->input('username');
        $isConsumptionCard = $request->input('is_consumption_card');
        $alias = $request->input('alias');

        if ($isConsumptionCard == null) {
            $isConsumptionCard = false;
        }
        // Log::info('Requisição para criação de conta feita por: '.$this->user->id.' - '.$this->user->name.' - '.$this->user->email);

        return $this->repository->createBill($username, $table, $isConsumptionCard, $alias);
    }

    /**
     * Migra os pedidos de uma Conta para Outra.
     *
     * @param Request $request [description]
     * @param [type]  $bill    [description]
     * @param [type]  $newBill [description]
     *
     * @return [type] [description]
     */
    public function migrateBill(Request $request, $bill, $newBill)
    {
        $orders = $request->input('orders');

        return $this->repository->migrateBill($bill, $newBill, $orders);
    }

    /**
     * Atualiza uma Conta.
     *
     * @param Request $request [description]
     * @param [type]  $bill    [description]
     *
     * @return [type] [description]
     */
    public function updateBill(Request $request, $bill)
    {
        $status = $request->input('status');
        $has_sync = $request->input('has_sync');
        if ($status == null) {
            $status = 'opened';
        }
        if ($has_sync == null) {
            $has_sync = false;
        }
        // Log::info('Requisição para atualização de conta feita por: '.$this->user->id.' - '.$this->user->name.' - '.$this->user->email);

        return $this->repository->updateBill($bill, $status, $has_sync);
    }
}
