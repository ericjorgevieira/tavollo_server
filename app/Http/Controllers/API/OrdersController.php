<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Repositories\API\APIRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class OrdersController extends Controller
{
    protected $repository;

    protected $user;

    public function __construct(APIRepository $repository)
    {
        $this->repository = $repository;

        $this->middleware(function ($request, $next) {
            if ($request->attributes->get('establishment')) {
                $this->repository->setEstablishment($request->attributes->get('establishment'));
                $this->user = $request->attributes->get('user');
            }

            return $next($request);
        });
    }

    /**
     * Listagem de Pedidos.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function orders(Request $request, $bill = null)
    {
        $status = $request->input('status');

        return $this->repository->listOrders($bill, $status);
    }

    /**
     * Cria um Pedido em determinada Conta.
     *
     * @param Request $request [description]
     * @param [type]  $bill    [description]
     *
     * @return [type] [description]
     */
    public function createOrder(Request $request, $bill)
    {
        $productId = $request->input('product_id');
        $code = $request->input('code');
        $quant = ($request->input('quant')) ? $request->input('quant') : 1;
        $observation = ($request->input('observation')) ? $request->input('observation') : '';
        $status = ($request->input('status')) ? $request->input('status') : 'opened';
        $waiter_code = $request->input('waiter_code');
        $is_defined_price = $request->input('is_defined_price', false);
        $defined_price = $request->input('defined_price');
        $external_id = $request->input('external_id');
        $weight = $request->input('weight');
        if ($weight == 0) {
            $weight = null;
        }
        // var_dump($is_defined_price, $defined_price, $code);exit;
        // Log::info('Requisição para criação de pedido feita por: '.$this->user->id.' - '.$this->user->name.' - '.$this->user->email);

        return $this->repository->createOrder($bill, $waiter_code, $productId, $code, $quant, $observation, $weight, null, $status, $is_defined_price, $defined_price, $external_id);
    }

    /**
     * Atualiza um Pedido em determinada Conta.
     *
     * @param Request $request [description]
     * @param [type]  $order   [description]
     *
     * @return [type] [description]
     */
    public function updateOrder(Request $request, $order)
    {
        if ($order) {
            $order = Order::where(['id' => $order])->withTrashed()->first();
        }
        $status = $request->input('status');
        $external_id = $request->input('external_id');
        $external_info = $request->input('external_info');
        if ($status == null) {
            $status = Order::STATUS_OPENED;
        }
        // Log::info('Requisição para atualização de pedido feita por: '.$this->user->id.' - '.$this->user->name.' - '.$this->user->email.'. Parâmetro: status = '.$status);

        return $this->repository->updateOrder($order, $status, $external_id, $external_info);
    }

    /**
     * Atualiza um Pedido em determinada Conta.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function updateManyOrders(Request $request)
    {
        $orders = $request->input('orders');
        if (is_string($orders)) {
            $orders = json_decode($orders, JSON_OBJECT_AS_ARRAY);
        }
        // Log::info('Requisição para atualização de vários pedidos feita por: '.$this->user->id.' - '.$this->user->name.' - '.$this->user->email);

        return $this->repository->updateManyOrders($orders);
    }
}
