<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\PreOrder;
use App\Repositories\API\APIPreOrderRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PreOrdersController extends Controller
{
    protected $repository;

    protected $user;

    public function __construct(APIPreOrderRepository $repository)
    {
        $this->repository = $repository;

        $this->middleware(function ($request, $next) {
            if ($request->attributes->get('establishment')) {
                $this->repository->setEstablishment($request->attributes->get('establishment'));
                $this->user = $request->attributes->get('user');
            }

            return $next($request);
        });
    }

    /**
     * Listagem de Pedidos.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function preOrders(Request $request)
    {
        $status = $request->input('status');

        return $this->repository->listPreOrders($status);
    }

    /**
     * Atualiza um Pedido.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function updatePreOrder(Request $request, $preorder)
    {
        if ($preorder) {
            $preorder = PreOrder::where(['id' => $preorder])->withTrashed()->first();
        }
        $external_id = $request->input('external_id');
        $status = $request->input('status');
        if ($status == null) {
            $status = PreOrder::STATUS_OPENED;
        }
        Log::info('Requisição para atualização de pedido pre pago feita por: '.$this->user->id.' - '.$this->user->name.' - '.$this->user->email.'. Parâmetro: status = '.$status);

        return $this->repository->updatePreOrder($preorder, $status, $external_id);
    }

    /**
     * Atualiza vários pedidos.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function updateManyPreOrders(Request $request)
    {
        $preorders = $request->input('preorders');
        if (is_string($preorders)) {
            $preorders = json_decode($preorders, JSON_OBJECT_AS_ARRAY);
        }
        Log::info('Requisição para atualização de vários pedidos feita por: '.$this->user->id.' - '.$this->user->name.' - '.$this->user->email);

        return $this->repository->updateManyPreOrders($preorders);
    }
}
