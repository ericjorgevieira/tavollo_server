<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\API\APIRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProductsController extends Controller
{
    protected $repository;

    protected $user;

    public function __construct(APIRepository $repository)
    {
        $this->repository = $repository;

        $this->middleware(function ($request, $next) {
            if ($request->attributes->get('establishment')) {
                $this->repository->setEstablishment($request->attributes->get('establishment'));
                $this->user = $request->attributes->get('user');
            }

            return $next($request);
        });
    }

    /**
     * Listagem de Produtos.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function products(Request $request, $product = null)
    {
        $status = $request->input('status');

        return $this->repository->listProducts($product, $status);
    }

    /**
     * Insere/Atualiza um Produto.
     *
     * @param Request $request [description]
     * @param [type]  $product [description]
     *
     * @return [type] [description]
     */
    public function updateProduct(Request $request, $product = null)
    {
        // Log::info('Requisição para atualização de produto feita por: '.$this->user->id.' - '.$this->user->name.' - '.$this->user->email);
        return $this->repository->updateProduct($request, $product);
    }

    /**
     * Insere/atualiza vários produtos.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function updateManyProducts(Request $request)
    {
        $products = $request->input('products');
        if (is_string($products)) {
            $products = json_decode($products, JSON_OBJECT_AS_ARRAY);
        }
        // Log::info('Requisição para inserção/atualização de vários produtos feita por: '.$this->user->id.' - '.$this->user->name.' - '.$this->user->email);

        return $this->repository->insertManyProducts($products);
    }
}
