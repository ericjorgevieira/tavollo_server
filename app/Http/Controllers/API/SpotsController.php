<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Table;
use App\Repositories\API\APIRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SpotsController extends Controller
{
    protected $repository;

    protected $user;

    public function __construct(APIRepository $repository)
    {
        $this->repository = $repository;

        $this->middleware(function ($request, $next) {
            if ($request->attributes->get('establishment')) {
                $this->repository->setEstablishment($request->attributes->get('establishment'));
                $this->user = $request->attributes->get('user');
            }

            return $next($request);
        });
    }

    public function spotRequests(Request $request)
    {
        $status = $request->input('status');
        if (!$status) {
            $status = 'opened';
        }

        return $this->repository->tableRequests($status);
    }

    public function updateSpotRequest(Request $request, $tableRequest)
    {
        $status = $request->input('status');

        return $this->repository->updateTableRequest($tableRequest, $status);
    }

    public function listSpots(Request $request)
    {
        $status = $request->input('status');
        if (!$status) {
            $status = 'enabled';
        }

        return $this->repository->listTables($status);
    }

    public function getSpot(Request $request)
    {
        $tableCode = $request->input('table_number');
        $isConsumptionCard = $request->input('is_consumption_card');
        if ($isConsumptionCard == null) {
            $isConsumptionCard = false;
        }
        // Log::info('Requisição para resgatar spot feita por: '.$this->user->id.' - '.$this->user->name.' - '.$this->user->email);

        return $this->repository->getTable($tableCode, null, $isConsumptionCard);
    }

    /**
     * Atualiza os dados de um Spot.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function updateSpot(Request $request, $tableId = null)
    {
        $number = $request->input('number');
        $name = $request->input('name');
        $alias = $request->input('alias');
        $status = $request->input('status');
        $external_id = $request->input('external_id');
        $is_consumption_card = $request->input('is_consuption_card');
        // Log::info('Requisição para inserção/atualização de spot feita por: '.$this->user->id.' - '.$this->user->name.' - '.$this->user->email);

        return $this->repository->updateTable($tableId, $number, $name, $alias, $is_consumption_card, $status, $external_id);
    }

    /**
     * Atualiza vários spots.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function updateManySpots(Request $request)
    {
        $tables = $request->get('tables');
        if (is_string($tables)) {
            $tables = json_decode($tables, JSON_OBJECT_AS_ARRAY);
        }
        Log::info('Requisição para inserção/atualização de vários spots feita por: '.$this->user->id.' - '.$this->user->name.' - '.$this->user->email);

        return $this->repository->updateManyTables($tables);
    }

    public function setSpotAlias(Request $request, Table $table)
    {
        $alias = $request->input('alias');

        return $this->repository->setTableAlias($table, $alias);
    }
}
