<?php

namespace App\Http\Controllers\Auth;

use App\Models\Establishment;
use App\Models\EstablishmentConfig;
use App\Http\Controllers\Controller;
use App\Models\Partnership;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm($partnershipEmail = null)
    {
        $partnershipToken = null;
        $partnership = null;
        if ($partnershipEmail) {
            $user = User::where(['email' => $partnershipEmail, 'profile' => 'partnership', 'status' => 'enabled'])->firstOrFail();
            if ($user->partnership()) {
                return redirect('/register?partnership_token='.$user->partnership->partnership_token);
            }
        } else {
            $partnershipToken = request()->get('partnership_token');
            $partnership = Partnership::where(['partnership_token' => $partnershipToken, 'status' => 'enabled'])->first();
        }

        if (!$partnershipToken || !$partnership) {
            return view('errors.authorization');
        } else {
            return view('auth.register', [
            'partnership' => $partnership,
          ]);
        }
    }

    /**
     * Handle a registration request for the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        DB::beginTransaction();
        $partnership = Partnership::find($request->partnership_id);
        try {
            $this->validator($request->all())->validate();
            //
            // event(new Registered($user = $this->create($request->all())));
            //
            //Add establishment
            $establishment = new Establishment();
            $establishment->name = $request->name;
            $establishment->fantasy_name = $request->fantasy_name;
            $establishment->cnpj = $request->cnpj;
            $establishment->ie = $request->ie;
            $establishment->phone = $request->phone;
            $establishment->email = $request->email;
            $establishment->address = $request->address;
            $establishment->district = $request->district;
            $establishment->city = $request->city;
            $establishment->state = $request->state;
            $establishment->postal_code = $request->postal_code;
            $establishment->plan = 'test';
            $establishment->status = Establishment::STATUS_DISABLED;

            $establishment->save();

            $establishment->slug = str_slug($request->name.'-'.$establishment->id);
            $establishment->save();

            //Add establishment config
            $config = new EstablishmentConfig();
            $config->establishment_id = $establishment->id;
            $config->enable_advanced_functions = $request->enable_advanced_functions;
            if ($request->product == Establishment::PRODUCT_ONLY_MENU) {
                $config->allow_bills = false;
                $config->allow_pre_bills = true;
                $config->disable_orders = true;
            }
            if ($request->product == Establishment::PRODUCT_MENU_AND_ALLOW_BILLS) {
                $config->allow_bills = true;
                $config->allow_pre_bills = true;
                $config->disable_orders = true;
            }
            if ($request->product == Establishment::PRODUCT_FULL) {
                $config->allow_bills = true;
                $config->allow_pre_bills = true;
                $config->disable_orders = false;
            }

            $config->disable_location = false;
            $config->display_location = false;
            $config->save();

            //Add establishment user
            $user = new User();
            $user->is_establishment = 1;
            $user->establishment_id = $establishment->id;
            $user->name = $request->username;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->status = User::STATUS_ENABLED;
            $user->profile = User::PROFILE_MANAGER;
            $user->save();

            //Add partnership relationship
            $partnership->establishments()->attach($establishment->id);
            DB::commit();

            return redirect('/')->with('success', 'Sua conta foi cadastrada com sucesso! Aguarde a ativação do nosso suporte.');
        } catch (\Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            if (isset($e->validator)) {
                $message = $e->validator->errors()->first();
            }

            return redirect('/register?partnership_token='.$partnership->partnership_token)
              ->withInput($request->input())->with('warning', $message);
        }
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('web');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'partnership_token' => 'required',
            'partnership_id' => 'required',
            'username' => 'required|max:255',
            'fantasy_name' => 'required|max:255',
            'name' => 'required|max:255',
            'cnpj' => 'required|max:255|unique:establishments',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }
}
