<?php

namespace App\Http\Controllers\Master;

use App\Models\Establishment;
use App\Http\Requests;
use App\Repositories\EstablishmentRepository;
use App\Models\User;
use App\Models\Bill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class BillsController extends Controller
{
    protected $user;

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('master.auth');

        $this->user = Auth::guard('master')->user();

        if ($this->user->partnership()) {
            echo "404 NOT FOUND.";
            exit;
        }
    }

    public function index(Request $request)
    {
        $bills = Bill::where('user_id', '!=', 0)
        ->orderBy('status', 'asc')
        ->orderBy('created_at', 'desc')
        ->limit(100)
        ->get();
        return view('master.bills', [
            'bills' => $bills
        ]);
    }
}
