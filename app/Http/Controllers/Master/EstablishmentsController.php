<?php

namespace App\Http\Controllers\Master;

use App\Models\ApiToken;
use App\Models\Bill;
use App\Models\Establishment;
use App\Models\EstablishmentConfig;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\Order;
use App\Models\OrderProductGroupItem;
use App\Models\Partnership;
use App\Models\PreOrder;
use App\Models\PreOrderProduct;
use App\Models\PreOrderProductGroupItem;
use App\Models\ProductCategory;
use App\Models\User;
use App\Models\Waiter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class EstablishmentsController extends Controller
{
    protected $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('master.auth');

        $this->user = Auth::guard('master')->user();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $establishments = null;
        if ($this->user->partnership()) {
            $establishments = $this->user->partnership->establishments()->orderBy('status', 'ASC')->orderBy('created_at', 'DESC')->get();
        } else {
            $establishments = Establishment::orderBy('status', 'ASC')->orderBy('created_at', 'DESC')->get();
        }

        return view('master.establishments', [
            'establishments' => $establishments,
        ]);
    }

    public function establishment(Request $request, Establishment $establishment)
    {
        return view('master.establishment', [
            'establishment' => $establishment,
            'products' => $establishment->products,
            'bills' => $establishment->getBills(Bill::STATUS_CLOSED),
            'preOrders' => $establishment->getPreOrders(),
            'averageTicket' => $establishment->getAverageTicket(),
            'sales' => $establishment->getTotalSales(),
            'lastMonthBills' => $establishment->getBills(Bill::STATUS_CLOSED, 30),
            'lastMonthSales' => $establishment->getTotalSales(true, 30),
            'averageServiceTime' => $establishment->getAverageServiceTime(),
            'users' => $establishment->users,
        ]);
    }

    /**
     * Processa cadastro de novo Establishment.
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        Log::notice('[MASTER] Usuário '.$this->user->name.' - '.$this->user->id.' criando novo estabelecimento: '.$request->name);
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|unique:establishments,email',
            'cnpj' => 'required|unique:establishments,cnpj',
        ]);

        if ($validator->fails()) {
            return redirect('/establishments/create')
                ->withInput()
                ->withErrors($validator);
        }

        $establishment = new Establishment();
        $establishment->name = $request->name;
        $establishment->fantasy_name = $request->fantasy_name;
        $establishment->cnpj = $request->cnpj;
        $establishment->phone = $request->phone;
        $establishment->email = $request->email;
        $establishment->address = $request->address;
        $establishment->district = $request->district;
        $establishment->city = $request->city;
        $establishment->state = $request->state;
        $establishment->postal_code = $request->postal_code;
        $establishment->plan = $request->plan;
        $establishment->payment_date = ($request->payment_date) ? Carbon::createFromFormat('d/m/Y', $request->payment_date) : null;
        $establishment->status = Establishment::STATUS_DISABLED;

        $establishment->save();

        $establishment->slug = str_slug($request->name.'-'.$establishment->id);
        $establishment->save();

        $config = new EstablishmentConfig();
        $config->establishment_id = $establishment->id;
        $config->enable_advanced_functions = $request->enable_advanced_functions;
        $config->allow_bills = $request->allow_bills;
        $config->allow_pre_bills = $request->allow_pre_bills;
        $config->disable_location = $request->disable_location;
        $config->display_location = $request->display_location;
        $config->disable_orders = ($establishment->hasPaymentMethod()) ? $request->disable_orders : true;
        $config->save();

        return redirect('/establishments/'.$establishment->id)
            ->with('success', ['Estabelecimento criado com sucesso!']);
    }

    /**
     * Processa edição de Establishment.
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Establishment $establishment)
    {
        Log::notice('[MASTER] Usuário '.$this->user->name.' - '.$this->user->id.' atualizando dados do estabelecimento: '.$establishment->id);
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|unique:establishments,email,'.$establishment->id,
            'cnpj' => 'required|unique:establishments,cnpj,'.$establishment->id,
        ]);

        if ($validator->fails()) {
            return redirect('/establishments/'.$establishment->id.'/edit')
                ->withInput()
                ->withErrors($validator);
        }
        $establishment->name = $request->name;
        $establishment->slug = $request->slug;
        $establishment->fantasy_name = $request->fantasy_name;
        $establishment->cnpj = $request->cnpj;
        $establishment->phone = $request->phone;
        $establishment->email = $request->email;
        $establishment->address = $request->address;
        $establishment->district = $request->district;
        $establishment->city = $request->city;
        $establishment->state = $request->state;
        $establishment->plan = $request->plan;
        $establishment->payment_date = ($request->payment_date) ? Carbon::createFromFormat('d/m/Y', $request->payment_date) : null;
        $establishment->postal_code = $request->postal_code;

        $config = $establishment->config;
        if (!$config) {
            $config = new EstablishmentConfig();
            $config->establishment_id = $establishment->id;
        }
        $config->enable_advanced_functions = $request->enable_advanced_functions;
        $config->allow_bills = $request->allow_bills;
        $config->allow_pre_bills = $request->allow_pre_bills;
        $config->disable_location = $request->disable_location;
        $config->display_location = $request->display_location;
        $config->disable_orders = ($establishment->hasPaymentMethod()) ? $request->disable_orders : true;

        $config->save();
        $establishment->save();

        Log::notice('[MASTER] Novo setup:  '.$config);

        return redirect('/establishments/'.$establishment->id.'/edit')
            ->with('success', ['Estabelecimento alterado com sucesso!']);
    }

    /**
     * Exibe formulário de cadastro de Produto.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        if ($this->user->partnership()) {
            return redirect('/establishments');
        }

        return view('master.establishments-create', [
            'estados' => Establishment::estados(),
        ]);
    }

    /**
     * Exibe formulário de edição de Produto.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Establishment $establishment)
    {
        if ($this->user->partnership()) {
            return redirect('/establishments');
        }

        return view('master.establishments-edit', [
            'estados' => Establishment::estados(),
            'establishment' => $establishment,
        ]);
    }

    /**
     * Habilita/Desabilita exibição de Establishment.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function disable(Establishment $establishment)
    {
        Log::notice('[MASTER] Usuário '.$this->user->name.' - '.$this->user->id.' definindo novo status do estabelecimento: '.$establishment->id);
        if ($establishment->status == Establishment::STATUS_DISABLED) {
            $establishment->status = Establishment::STATUS_ENABLED;
            $establishment->save();

            User::where('is_establishment', 1)->where('establishment_id', $establishment->id)->update(['status' => User::STATUS_ENABLED]);

            return redirect('/establishments')
                ->with('success', ['Estabelecimento habilitado com sucesso!']);
        } else {
            $establishment->status = Establishment::STATUS_DISABLED;
            $establishment->save();

            User::where('is_establishment', 1)->where('establishment_id', $establishment->id)->update(['status' => User::STATUS_DISABLED]);
            ApiToken::where('establishment_id', $establishment->id)->update(['expires_at' => Carbon::now()]);

            return redirect('/establishments')
                ->with('success', ['Estabelecimento desabilitado com sucesso!']);
        }
        Log::notice('[MASTER] Status: '.$establishment->status);
    }

    /**
     * Limpa registro de contas abertas e vendas.
     *
     * @param Establishment $establishment [description]
     *
     * @return [type] [description]
     */
    public function clear(Establishment $establishment)
    {
        Log::notice('[MASTER] Usuário '.$this->user->name.' - '.$this->user->id.' apagando registro de comandas e vendas do estabelecimento: '.$establishment->id);
        $tables = $establishment->tables;
        $establishment->billRatings()->delete();
        foreach ($tables as $t) {
            Bill::where('table_id', $t->id)->where('status', Bill::STATUS_CLOSED)
            ->orWhere('status', Bill::STATUS_TRASH)->delete();
        }
        $preorders = PreOrder::where('establishment_id', $establishment->id)->get();
        foreach ($preorders as $p) {
            $preOrderProducts = PreOrderProduct::where('pre_order_id', $p->id)->get();
            foreach ($preOrderProducts as $preOrderProduct) {
                $preOrderProduct->items()->delete();
                $preOrderProduct->delete();
            }
            $p->delete();
        }

        return redirect('/establishments/'.$establishment->id)
            ->with('success', ['Contas apagadas com sucesso!']);
    }

    /**
     * Limpa registro de produtos.
     *
     * @param Establishment $establishment [description]
     *
     * @return [type] [description]
     */
    public function clearProducts(Establishment $establishment)
    {
        Log::notice('[MASTER] Usuário '.$this->user->name.' - '.$this->user->id.' apagando registro de produtos do estabelecimento: '.$establishment->id);
        $products = $establishment->products;
        foreach ($products as $p) {
            foreach ($p->groups as $g) {
                OrderProductGroupItem::where('product_group_id', $g->id)->forceDelete();
                PreOrderProductGroupItem::where('product_group_id', $g->id)->forceDelete();
                $g->items()->forceDelete();
            }
            PreOrderProduct::where('product_id', $p->id)->forceDelete();
            Order::where('product_id', $p->id)->forceDelete();
            $p->groups()->forceDelete();
            $p->forceDelete();
        }
        ProductCategory::where('establishment_id', $establishment->id)->forceDelete();
        Menu::where('establishment_id', $establishment->id)->forceDelete();

        return redirect('/establishments/'.$establishment->id)
            ->with('success', ['Produtos, Categorias e Menus apagados com sucesso!']);
    }

    /**
     * Limpa registro de mesas e cartões consumo.
     *
     * @param Establishment $establishment [description]
     *
     * @return [type] [description]
     */
    public function clearTables(Establishment $establishment)
    {
        Log::notice('[MASTER] Usuário '.$this->user->name.' - '.$this->user->id.' apagando registro de mesas e cartões do estabelecimento: '.$establishment->id);
        $tables = $establishment->tables;
        $establishment->billRatings()->delete();
        foreach ($tables as $t) {
            Bill::where('table_id', $t->id)->where('status', Bill::STATUS_CLOSED)
            ->orWhere('status', Bill::STATUS_TRASH)->delete();
            $t->forceDelete();
        }

        return redirect('/establishments/'.$establishment->id)
            ->with('success', ['Mesas e cartões apagados com sucesso!']);
    }

    /**
     * Remove Establishment.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     *
     * @throws \Exception
     */
    public function destroy(Establishment $establishment)
    {
        $establishment->delete();

        return redirect('/establishments')
            ->with('success', 'Estabelecimento removido.');
    }

    public function generateTokens(Request $request, Establishment $establishment)
    {
        $tables = $establishment->tables;
        foreach ($tables as $t) {
            if (!$t->hasActiveBill()) {
                $t->generateNewCode();
            }
        }

        return redirect('/establishments/'.$establishment->id)
            ->with('success', ['Tokens gerados com sucesso!']);
    }

    public function loginAs(Request $request, Establishment $establishment)
    {
        Log::notice('[MASTER] Usuário '.$this->user->name.' - '.$this->user->id.' logando em estabelecimento: '.$establishment->id);
        $user = User::where(['establishment_id' => $establishment->id, 'is_establishment' => 1, 'profile' => 'manager'])->first();
        if ($user) {
            Auth::guard('web')->login($user, true);

            return redirect(config('app.url'));
        } else {
            return redirect('/establishments')->with('error', 'Estabelecimento não possui usuário cadastrado!');
        }
    }

    /**
     * Exibe formulário de cadastro de Usuário.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createUser(Request $request, Establishment $establishment)
    {
        return view('master.users-create', [
            'establishment' => $establishment,
        ]);
    }

    /**
     * Processa cadastro de novo Establishment.
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeUser(Request $request, Establishment $establishment)
    {
        Log::notice('[MASTER] Usuário '.$this->user->name.' - '.$this->user->id.' criando novo usuário no estabelecimento: '.$establishment->id);
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|unique:users,email',
            'new_password' => 'required|max:16|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('/establishments/'.$request->establishment_id)
            ->withInput()
            ->withErrors($validator);
        }

        $user = new User();
        $user->is_establishment = 1;
        $user->establishment_id = $request->establishment_id;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->new_password);
        $user->status = User::STATUS_ENABLED;
        $user->profile = User::PROFILE_MANAGER;
        $user->save();

        return redirect('/establishments/'.$user->establishment_id)
        ->with('success', ['Usuário cadastrado com sucesso!']);
    }

    /**
     * Exibe formulário de edição de Produto.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editUser(Request $request, Establishment $establishment, User $user)
    {
        if ($user->establishment_id == '') {
            return redirect('/establishments');
        }

        return view('master.users-edit', [
            'user' => $user,
        ]);
    }

    /**
     * Processa edição de Establishment.
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateUser(Request $request, Establishment $establishment, User $user)
    {
        Log::notice('[MASTER] Usuário '.$this->user->name.' - '.$this->user->id.' editando usuário do estabelecimento: '.$establishment->id);
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|unique:users,email,'.$user->id,
            'new_password' => 'max:16|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('/users/'.$user->id.'/edit')
            ->withInput()
            ->withErrors($validator);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        if (!empty($request->new_password)) {
            $user->password = Hash::make($request->new_password);
        }
        $user->save();

        return redirect('/establishments/'.$establishment->id.'/users/'.$user->id.'/edit')
        ->with('success', ['Usuário alterado com sucesso!']);
    }

    /**
     * Processa remoção de usuário.
     *
     * @param Request $request [description]
     * @param User    $user    [description]
     *
     * @return [type] [description]
     */
    public function deleteUser(Request $request, Establishment $establishment, User $user)
    {
        Log::notice('[MASTER] Usuário '.$this->user->name.' - '.$this->user->id.' removendo usuário do estabelecimento: '.$establishment->id);
        if ($user->partnership()) {
            $partnership = Partnership::where(['user_id' => $user->id])->first();
            $partnership->delete();
        }
        if ($user->waiter()) {
            $waiter = Waiter::where(['user_id' => $user->id])->first();
            $waiter->delete();
        }
        $user->delete();

        return redirect('/establishments/'.$establishment->id)
      ->with('success', ['Usuário deletado com sucesso!']);
    }

     /**
    * Habilita/Desabilita exibição de User
    * @param User $user
    * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
    */
    public function enableUser(User $user)
    {
        if ($user->status == 'disabled') {
            $user->status = 'enabled';
            $user->save();
            return redirect('/establishments/' . $user->establishment_id)
            ->with('success', ['Usuário habilitado com sucesso!']);
        } else {
            $user->status = 'disabled';
            $user->save();
            return redirect('/establishments/' . $user->establishment_id)
            ->with('success', ['Usuário desabilitado com sucesso!']);
        }
    }
}
