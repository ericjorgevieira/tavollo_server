<?php

namespace App\Http\Controllers\Master;

use App\Models\Bill;
use App\Models\Establishment;
use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use App\Models\User;
use Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    protected $bills;

    protected $orders;

    protected $products;

    protected $establishment;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('master.auth');

        $this->user = Auth::guard('master')->user();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.home');
    }

    public function dashboard()
    {
        $establishments = null;
        if ($this->user->partnership()) {
            $establishments = $this->user->partnership->establishments()->orderBy('status', 'ASC')->orderBy('created_at', 'DESC')->get();
        } else {
            $establishments = Establishment::orderBy('status', 'ASC')->get();
        }

        $totalSales = Cache::remember('master_home_establishments_totalsales_'.$this->user->id, 30, function () use ($establishments) {
            $totalSales = 0;
            foreach ($establishments as $e) {
                $totalSales += $e->getTotalSales(false);
            }

            return $totalSales;
        });
        $users = Cache::remember('master_home_establishments_users_'.$this->user->id, 30, function () {
            return User::where('is_establishment', '0')->where('is_admin', '0')->where('profile', 'customer')->get();
        });

        $bills = Cache::remember('master_home_establishments_bills_'.$this->user->id, 30, function () {
            return Bill::where('status', Bill::STATUS_CLOSED)->get();
        });

        return response([
          'establishments' => $establishments,
          'users' => $users,
          'bills' => $bills,
          'total' => number_format($totalSales, 2, ',', '.'),
      ]);
    }

    public function categories()
    {
        return view('master.categories', [
            'categories' => ProductCategory::where('status', 'enabled')->get(),
            'trash' => false,
        ]);
    }

    public function categoriesTrash()
    {
        return view('master.categories', [
            'categories' => ProductCategory::where('status', 'disabled')->get(),
            'trash' => true,
        ]);
    }

    public function categoriesCreate(Request $request)
    {
        return view('master.category-create');
    }

    public function categoriesStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/categories/create')
                ->withInput()
                ->withErrors($validator);
        }

        $ProductCategory = new ProductCategory();
        $ProductCategory->name = $request->name;
        $ProductCategory->slug = str_slug($request->name);
        $ProductCategory->status = 'disabled';
        if ($request->status == 'enabled') {
            $ProductCategory->status = 'enabled';
        }
        $ProductCategory->save();

        return redirect('/categories')
            ->with('success', ['Categoria criada com sucesso!']);
    }

    public function categoriesEdit(Request $request, $category_id)
    {
        $productCategory = ProductCategory::find($category_id);

        return view('master.category-edit', [
            'category' => $productCategory,
        ]);
    }

    public function categoriesUpdate(Request $request, $category_id)
    {
        $productCategory = ProductCategory::find($category_id);
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/categories/'.$productCategory->id.'/edit')
                ->withInput()
                ->withErrors($validator);
        }

        $productCategory->name = $request->name;
        $productCategory->slug = str_slug($request->name);
        $productCategory->status = 'disabled';
        if ($request->status == 'enabled') {
            $productCategory->status = 'enabled';
        }
        $productCategory->save();

        return redirect('/categories/'.$productCategory->id.'/edit')
            ->with('success', ['Categoria alterada com sucesso!']);
    }

    public function categoriesEnable(Request $request, $category_id)
    {
        $productCategory = ProductCategory::find($category_id);
        if ($productCategory->status == 'disabled') {
            $productCategory->status = 'enabled';
            $productCategory->save();

            return redirect('/categories/trash')
                ->with('success', ['Categoria habilitada com sucesso!']);
        } else {
            $productCategory->status = 'disabled';
            $productCategory->save();

            return redirect('/categories')
                ->with('success', ['Categoria desabilitada com sucesso!']);
        }
    }

    public function destroy(Request $request, $category_id)
    {
        $productCategory = ProductCategory::find($category_id);
        $productCategory->delete();

        return redirect('/categories')
            ->with('success', 'Categoria removida.');
    }
}
