<?php

namespace App\Http\Controllers\Master;

use App\Models\Establishment;
use App\Models\Integration;
use App\Http\Requests;
use App\Repositories\Integration\IntegrationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class IntegrationsController extends Controller
{
    protected $user;

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('master.auth');

        $this->user = Auth::guard('master')->user();

        if ($this->user->partnership()) {
            echo "404 NOT FOUND.";
            exit;
        }
    }

    public function listIntegrations(Request $request)
    {
        return view('master.integrations', [
            'integrations' => Integration::orderBy('status', 'ASC')->get()
        ]);
    }

    /**
    * Processa cadastro de nova Integração
    * @param Request $request
    * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'company' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/integrations/create')
            ->withInput()
            ->withErrors($validator);
        }

        $i = new Integration();
        $i->name = $request->name;
        $i->slug = str_slug($request->name);
        $i->email = $request->email;
        $i->company = $request->company;
        $i->description = $request->description;
        $i->phone = $request->phone;
        $i->city = $request->city;
        $i->state = $request->state;
        $i->status = Integration::STATUS_ENABLED;
        $i->save();

        return redirect('/integrations')
        ->with('success', ['Integração cadastrada com sucesso!']);
    }

    /**
    * Processa edição de Integração
    * @param Request $request
    * @param Integration $integration
    * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
    */
    public function update(Request $request, Integration $integration)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'company' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return redirect('/integrations/' . $integration->id . '/edit')
            ->withInput()
            ->withErrors($validator);
        }

        $integration->name = $request->name;
        $integration->slug = str_slug($request->name);
        $integration->email = $request->email;
        $integration->company = $request->company;
        $integration->description = $request->description;
        $integration->phone = $request->phone;
        $integration->city = $request->city;
        $integration->state = $request->state;
        $integration->save();

        return redirect('/integrations/' . $integration->id . '/edit')
        ->with('success', ['Integração alterada com sucesso!']);
    }

    /**
    * Exibe formulário de cadastro de Usuário
    * @param Request $request
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function create(Request $request, Establishment $establishment)
    {
        return view('master.integrations-create', [
            'estados' => Establishment::estados()
        ]);
    }

    /**
    * Exibe formulário de edição de Integração
    * @param Integration $integration
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function edit(Integration $integration)
    {
        return view('master.integrations-edit', [
            'integration' => $integration,
            'estados' => Establishment::estados()
        ]);
    }

    /**
    * Habilita/Desabilita exibição de User
    * @param User $user
    * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
    */
    public function enable(User $user)
    {
    }

    /**
    * Remove User
    * @param User $user
    * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
    * @throws \Exception
    */
    public function destroy(User $user)
    {
    }
}
