<?php

namespace App\Http\Controllers\Master;

use App\Models\Establishment;
use App\Models\Marketplace;
use App\Models\User;
use App\Models\ApiToken;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Storage;
use Image;

class MarketplacesController extends Controller
{
    protected $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('master.auth');

        $this->user = Auth::guard('master')->user();

        if ($this->user->partnership()) {
            echo "404 NOT FOUND.";
            exit;
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.marketplaces', [
            'marketplaces' => Marketplace::orderBy('status', 'ASC')->orderBy('created_at', 'DESC')->get()
        ]);
    }

    /**
     * Processa cadastro de novo Marketplace
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048',
            'logo' => 'image|mimes:jpeg,png,jpg|max:2048',
            'background' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);

        if ($validator->fails()) {
            return redirect('/marketplaces/create')
                ->withInput()
                ->withErrors($validator);
        }
        $slug = str_slug($request->name);
        if (!$this->validateSlug($slug)) {
            return redirect('/marketplaces/create')
              ->withInput()
              ->withErrors(null);
        }
        $marketplace = new Marketplace();
        $marketplace->name = $request->name;
        $marketplace->slug = $slug;
        $marketplace->description = $request->description;
        $marketplace->status = $request->status;

        if ($request->file('image')) {
            $path = Storage::putFile('public/marketplaces_images', $request->file('image'));
            $image = Image::make(Storage::get($path));
            $image->resize(1368, 720)->stream('jpg', 30);
            Storage::put($path, $image);
            $env = \Config::get('app.env');
            $urlProduction = $env == 'homologation' ? 'https://test.admin.tavollo.com' : 'https://admin.tavollo.com';
            $marketplace->image = ($env == 'local') ? 'http://admin.tavollo.localhost:90' . Storage::url($path) : $urlProduction . Storage::url($path);
        }

        if ($request->file('logo')) {
            $path = Storage::putFile('public/marketplaces_logos', $request->file('logo'));
            $image = Image::make(Storage::get($path));
            $image->resize(668, 400)->stream('png', 30);
            Storage::put($path, $image);
            $env = \Config::get('app.env');
            $urlProduction = $env == 'homologation' ? 'https://test.admin.tavollo.com' : 'https://admin.tavollo.com';
            $marketplace->logo = ($env == 'local') ? 'http://admin.tavollo.localhost:90' . Storage::url($path) : $urlProduction . Storage::url($path);
        }

        if ($request->file('background')) {
            $path = Storage::putFile('public/marketplaces_backgrounds', $request->file('logo'));
            $image = Image::make(Storage::get($path));
            $image->resize(600, 1212)->stream('jpg', 70);
            Storage::put($path, $image);
            $env = \Config::get('app.env');
            $urlProduction = $env == 'homologation' ? 'https://test.admin.tavollo.com' : 'https://admin.tavollo.com';
            $marketplace->background = ($env == 'local') ? 'http://admin.tavollo.localhost:90' . Storage::url($path) : $urlProduction . Storage::url($path);
        }

        $config = array(
          'primary_color' => $request->primary_color,
          'secondary_color' => $request->secondary_color,
          'title_color' => $request->title_color,
          'text_color' => $request->text_color
        );

        $marketplace->saveThemeConfig($config);

        $marketplace->save();

        return redirect('/marketplaces')
            ->with('success', ['Marketplace criado com sucesso!']);
    }

    /**
     * Processa edição de Marketplace
     * @param Request $request
     * @param Marketplace $marketplace
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Marketplace $marketplace)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048',
            'logo' => 'image|mimes:jpeg,png,jpg|max:2048',
            'background' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);

        if ($validator->fails()) {
            return redirect('/marketplaces/' . $marketplace->id . '/edit')
                ->withInput()
                ->withErrors($validator);
        }
        $slug = str_slug($request->name);
        if (!$this->validateSlug($slug, $marketplace)) {
            return redirect('/marketplaces/' . $marketplace->id . '/edit')
              ->withInput()
              ->withErrors(null);
        }
        $marketplace->name = $request->name;
        $marketplace->slug = $slug;
        $marketplace->description = $request->description;
        $marketplace->location_lat = $request->location_lat;
        $marketplace->location_long = $request->location_long;
        $marketplace->status = $request->status;
        $marketplace->save();

        if ($request->file('image')) {
            $path = Storage::putFile('public/marketplaces_images', $request->file('image'));
            $image = Image::make(Storage::get($path));
            $image->resize(1368, 720)->stream('jpg', 30);
            Storage::put($path, $image);
            $env = \Config::get('app.env');
            $urlProduction = $env == 'homologation' ? 'https://test.admin.tavollo.com' : 'https://admin.tavollo.com';
            $marketplace->image = ($env == 'local') ? 'http://admin.tavollo.localhost:90' . Storage::url($path) : $urlProduction . Storage::url($path);
        }

        if ($request->file('logo')) {
            $path = Storage::putFile('public/marketplaces_logos', $request->file('logo'));
            $image = Image::make(Storage::get($path));
            $image->resize(668, 400)->stream('jpg', 30);
            Storage::put($path, $image);
            $env = \Config::get('app.env');
            $urlProduction = $env == 'homologation' ? 'https://test.admin.tavollo.com' : 'https://admin.tavollo.com';
            $marketplace->logo = ($env == 'local') ? 'http://admin.tavollo.localhost:90' . Storage::url($path) : $urlProduction . Storage::url($path);
        }

        if ($request->file('background')) {
            $path = Storage::putFile('public/marketplaces_backgrounds', $request->file('background'));
            $image = Image::make(Storage::get($path));
            $image->resize(600, 1212)->stream('jpg', 30);
            Storage::put($path, $image);
            $env = \Config::get('app.env');
            $urlProduction = $env == 'homologation' ? 'https://test.admin.tavollo.com' : 'https://admin.tavollo.com';
            $marketplace->background = ($env == 'local') ? 'http://admin.tavollo.localhost:90' . Storage::url($path) : $urlProduction . Storage::url($path);
        }

        if ($request->establishments && count($request->establishments) > 0) {
            $marketplace->establishments()->sync($request->establishments);
        }

        $config = array(
          'primary_color' => $request->primary_color,
          'secondary_color' => $request->secondary_color,
          'title_color' => $request->title_color,
          'text_color' => $request->text_color
        );

        $marketplace->saveThemeConfig($config);
        return redirect('/marketplaces/' . $marketplace->id . '/edit')
            ->with('success', ['Marketplace alterado com sucesso!']);
    }

    /**
     * Exibe formulário de cadastro de Produto
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('master.marketplaces-create');
    }

    /**
     * Exibe formulário de edição de Marketplace
     * @param Marketplace $marketplace
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Marketplace $marketplace)
    {
        return view('master.marketplaces-edit', [
          'marketplace' => $marketplace,
          'marketplaceEstablishments' => $marketplace->establishments->pluck('id')->toArray(),
          'establishments' => Establishment::where(['status' => Establishment::STATUS_ENABLED])->get()
        ]);
    }

    /**
     * Habilita/Desabilita exibição de Marketplace
     * @param Marketplace $marketplace
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function disable(Marketplace $marketplace)
    {
        if ($marketplace->status == Marketplace::STATUS_DISABLED) {
            $marketplace->status = Marketplace::STATUS_ENABLED;
            $marketplace->save();

            return redirect('/marketplaces/' . $marketplace->id)
                ->with('success', ['Marketplace habilitado com sucesso!']);
        } else {
            $marketplace->status = Marketplace::STATUS_DISABLED;
            $marketplace->save();

            return redirect('/marketplaces/' . $marketplace->id)
                ->with('success', ['Marketplace desabilitado com sucesso!']);
        }
    }

    /**
     * Remove Marketplace
     * @param Marketplace $marketplace
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Marketplace $marketplace)
    {
        $marketplace->delete();
        return redirect('/marketplaces')
            ->with('success', 'Marketplace removido.');
    }

    public function validateSlug($slug, Marketplace $marketplace = null)
    {
        $mk = Marketplace::where(['slug' => $slug])->first();
        if ($mk) {
            if ($marketplace && $mk->id == $marketplace->id) {
                return true;
            }
            return false;
        }
        return true;
    }
}
