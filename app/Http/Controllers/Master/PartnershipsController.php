<?php

namespace App\Http\Controllers\Master;

use App\Models\Establishment;
use App\Models\User;
use App\Models\ApiToken;
use App\Models\Menu;
use App\Models\Partnership;
use App\Models\PartnershipEstablishment;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class PartnershipsController extends Controller
{
    protected $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('master.auth');

        $this->user = Auth::guard('master')->user();

        if ($this->user->partnership()) {
            echo "404 NOT FOUND.";
            exit;
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.partnerships', [
            'partnerships' => Partnership::orderBy('status', 'ASC')->orderBy('created_at', 'DESC')->get()
        ]);
    }

    public function partnership(Request $request, Partnership $partnership)
    {
        return view('master.partnership', [
            'partnership' => $partnership,
            'partnershipEstablishments' => $partnership->establishments->pluck('id')->toArray(),
            'establishments' => Establishment::where(['status' => Establishment::STATUS_ENABLED])->get()
        ]);
    }

    /**
     * Processa cadastro de novo Partnership
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|unique:users,email',
            'password' => 'required|max:16|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('/partnerships/create')
                ->withInput()
                ->withErrors($validator);
        }

        $partnership = new Partnership();
        $partnership->populate($request);

        if (!$partnership) {
            return redirect('/partnerships/')
              ->with('error', ['Erro ao criar parceiro. Provavelmente o usuário já existe na base, então utilize outro e-mail.']);
        }

        return redirect('/partnerships/' . $partnership->id . '/edit')
            ->with('success', ['Parceiro criado com sucesso!']);
    }

    /**
     * Processa edição de Partnership
     * @param Request $request
     * @param Partnership $establishment
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Partnership $partnership)
    {
        $arrValidation = [
            'name' => 'required|max:255',
            'email' => 'required|unique:users,email,' . $partnership->user->id
        ];

        if ($request->password && $request->password != '') {
            $arrValidation['password'] = 'required|max:16|confirmed';
        }
        $validator = Validator::make($request->all(), $arrValidation);

        if ($validator->fails()) {
            return redirect('/partnerships/' . $partnership->id . '/edit')
                ->withInput()
                ->withErrors($validator);
        }

        $partnership->populate($request);

        if (!$partnership) {
            return redirect('/partnerships/' . $partnership->id . '/edit')
              ->with('error', ['Erro ao criar parceiro.']);
        }

        // if ($request->establishments && count($request->establishments) > 0) {
        $partnership->establishments()->sync($request->establishments);
        // }

        return redirect('/partnerships/' . $partnership->id . '/edit')
            ->with('success', ['Parceiro alterado com sucesso!']);
    }

    /**
     * Exibe formulário de cadastro de Partnership
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('master.partnerships-create', [
            'estados' => Establishment::estados()
        ]);
    }

    /**
     * Exibe formulário de edição de Partnership
     * @param Partnership $partnership
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Partnership $partnership)
    {
        return view('master.partnerships-edit', [
            'estados' => Establishment::estados(),
            'partnership' => $partnership,
            'partnershipEstablishments' => $partnership->establishments->pluck('id')->toArray(),
            'establishments' => Establishment::where(['status' => Establishment::STATUS_ENABLED])->get()
        ]);
    }

    /**
     * Habilita/Desabilita exibição de Partnership
     * @param Partnership $partnership
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function disable(Partnership $partnership)
    {
        if ($partnership->status == Partnership::STATUS_DISABLED) {
            $partnership->status = Partnership::STATUS_ENABLED;
            $partnership->save();

            $partnership->user->update(['is_admin' => 1]);

            return redirect('/partnerships')
                ->with('success', ['Parceiro habilitado com sucesso!']);
        } else {
            $partnership->status = Partnership::STATUS_DISABLED;
            $partnership->save();

            $partnership->user->update(['is_admin' => 0]);

            return redirect('/partnerships')
                ->with('success', ['Parceiro desabilitado com sucesso!']);
        }
    }

    /**
     * Remove Partnership
     * @param Partnership $establishment
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Partnership $partnership)
    {
        $partnership->delete();
        return redirect('/partnerships')
            ->with('success', 'Parceiro removido.');
    }
}
