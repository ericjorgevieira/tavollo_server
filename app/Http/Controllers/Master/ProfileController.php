<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    protected $repository;

    protected $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $repository)
    {
        $this->middleware('master.auth');

        $this->repository = $repository;

        $this->user = Auth::guard('master')->user();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.profile', [
            'profile' => $this->user,
            'action' => 'user',
        ]);
    }

    public function partnership()
    {
        if ($this->user->partnership()) {
            return view('master.profile', [
                'profile' => $this->user,
                'partnership' => $this->user->partnership,
                'action' => 'partnership',
            ]);
        }

        return response('Você não possui permissão.', 401);
    }

    public function update(Request $request)
    {
        $user = $this->user;

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|unique:users,email,'.$user->id,
        ]);

        if ($validator->fails()) {
            return redirect('/profile')
                ->withInput()
                ->withErrors($validator);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return redirect('/profile')
            ->with('success', ['Perfil alterado com sucesso!']);
    }

    public function password()
    {
        return view('master.profile', [
            'profile' => $this->user,
            'action' => 'password',
        ]);
    }

    public function passwordUpdate(Request $request)
    {
        $user = $this->user;

        $validator = Validator::make($request->all(), [
            'password' => 'required|max:16|hash:'.$user->password,
            'new_password' => 'required|max:16|different:password|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('/profile/password')
                ->withInput()
                ->withErrors($validator);
        }

        $user->password = Hash::make($request->new_password);
        $user->save();

        return redirect('/profile/password')
            ->with('success', ['Senha alterada com sucesso!']);
    }

    public function config()
    {
    }

    public function configUpdate()
    {
    }
}
