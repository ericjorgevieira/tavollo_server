<?php

namespace App\Http\Controllers\Master;

use App\Http\Requests;
use App\Models\User;
use App\Models\Rating;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RatingsController extends Controller
{
    protected $user;

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('master.auth');

        $this->user = Auth::guard('master')->user();

        if ($this->user->partnership()) {
            echo "404 NOT FOUND.";
            exit;
        }
    }

    public function listRatings(Request $request)
    {
        $ratings = Rating::with(['user'])->orderBy('created_at', 'desc') ->get();

        $ratingTotal = $this->getRatingTotal();

        return view('master.ratings', [
            'ratings' => $ratings,
            'ratingTotal' => $ratingTotal['count'],
            'ratingAverage' => $ratingTotal['average']
        ]);
    }

    private function getRatingTotal()
    {
        $total = 0;
        $count = 0;
        $media = 0;
        
        $ratings = Rating::get();

        $ratings->map(function($r) use (&$total, &$count){
            $count++;
            $total += $r->rating;
        });

        if($count > 0){
            $media = $total / $count;
        }

        return ['average' => round($media, 2), 'count' => $count];
    }

}
