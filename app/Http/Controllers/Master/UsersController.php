<?php

namespace App\Http\Controllers\Master;

use App\Models\Establishment;
use App\Http\Requests;
use App\Repositories\EstablishmentRepository;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    protected $user;

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('master.auth');

        $this->user = Auth::guard('master')->user();

        if ($this->user->partnership()) {
            echo "404 NOT FOUND.";
            exit;
        }
    }

    public function listUsers(Request $request)
    {
        $users = User::where('is_admin', '0')
        ->where('profile', 'customer')
        ->where('is_establishment', '0')
        ->orderBy('created_at', 'desc')
        ->get(['photo', 'name', 'email', 'facebook_id', 'updated_at', 'created_at']);

        return view('master.users', [
            'users' => $users
        ]);
    }
   
}
