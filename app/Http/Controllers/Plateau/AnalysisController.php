<?php

namespace App\Http\Controllers\Plateau;

use App\Http\Controllers\Controller;
use App\Repositories\AnalysisRepository;
use Illuminate\Support\Facades\Auth;

class AnalysisController extends Controller
{
    protected $analysisRepository;

    protected $establishment;

    protected $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AnalysisRepository $analysisRepository)
    {
        $this->analysisRepository = $analysisRepository;

        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->establishment = Auth::user()->establishment;
            $this->analysisRepository->setEstablishment($this->establishment);

            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('plateau.analysis', [
          'action' => 'index',
        ]);
    }

    public function loadIndex()
    {
        return response([
            'averageService' => $this->analysisRepository->averageService(),
            'totalUsers' => $this->analysisRepository->totalUsers(),
            'bestSellerProducts' => $this->analysisRepository->bestSellerProducts(),
            'topUsers' => $this->analysisRepository->topUsers(),
            'totalBills' => $this->analysisRepository->totalBills(),
            'consumption' => $this->analysisRepository->getConsumption(),
            'preorderAccesses' => $this->establishment->accesses()->where('type', '=', 'preorder')->count(),
            'billAccesses' => $this->establishment->accesses()->where('type', '=', 'bill')->count(),
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function service()
    {
        return view('plateau.analysis', [
            'action' => 'service',
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function products()
    {
        return view('plateau.analysis', [
            'action' => 'products',
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function users()
    {
        return view('plateau.analysis', [
            'action' => 'users',
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function waiters()
    {
        return view('plateau.analysis', [
            'action' => 'waiters',
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function evaluation()
    {
        return view('plateau.analysis', [
            'action' => 'evaluation',
        ]);
    }
}
