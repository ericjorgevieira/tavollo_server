<?php

namespace App\Http\Controllers\Plateau;

use App\Models\Bill;
use App\Http\Controllers\Controller;
use App\Repositories\BillRepository;
use App\Viewers\BillViewer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BillsController extends Controller
{
    protected $repository;

    protected $establishment;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BillRepository $billRepository)
    {
        $this->middleware(function ($request, $next) {
            $this->establishment = Auth::user()->establishment;
            $this->repository->setEstablishment($this->establishment);

            return $next($request);
        });

        $this->repository = $billRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('plateau.bills', [
            'url' => '/bills/list',
        ]);
    }

    public function listBills(Request $request)
    {
        return app(BillViewer::class)->listForTable($this->establishment, $request->input('limit'),
            $request->input('status'), $request->input('period'));
    }

    public function updateBill(Request $request, Bill $bill)
    {
        return $this->repository->updateBill($bill, $request->input('status'));
    }
}
