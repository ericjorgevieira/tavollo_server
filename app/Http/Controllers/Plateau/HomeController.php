<?php

namespace App\Http\Controllers\Plateau;

use App\Models\Bill;
use App\Models\Table;
use App\Models\Order;
use App\Models\PreOrder;
use App\Models\TableRequest;
use App\Http\Requests;
use App\Repositories\BillRepository;
use App\Repositories\OrderRepository;
use App\Repositories\ProductRepository;
use App\Repositories\PreOrderRepository;
use App\Repositories\TableRepository;
use App\Repositories\SectorRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    protected $billRepository;

    protected $orderRepository;

    protected $preOrderRepository;

    protected $productRepository;

    protected $tableRepository;

    protected $sectorRepository;

    protected $establishment;

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct(
        BillRepository $billRepository,
        OrderRepository $orderRepository,
        PreOrderRepository $preOrderRepository,
        ProductRepository $productRepository,
        TableRepository $tableRepository,
        SectorRepository $sectorRepository
    ) {
        $this->billRepository = $billRepository;
        $this->orderRepository = $orderRepository;
        $this->preOrderRepository = $preOrderRepository;
        $this->productRepository = $productRepository;
        $this->tableRepository = $tableRepository;
        $this->sectorRepository = $sectorRepository;


        $this->middleware(function ($request, $next) {
            $this->establishment = Auth::user()->establishment;

            $this->sectorRepository->setEstablishment($this->establishment);

            $this->billRepository->setEstablishment($this->establishment);

            $this->preOrderRepository->setEstablishment($this->establishment);

            $this->orderRepository->setEstablishment($this->establishment);

            $this->productRepository->setEstablishment($this->establishment);

            $this->tableRepository->setEstablishment($this->establishment);

            return $next($request);
        });
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $tables = $this->establishment->tables()->where('status', 'enabled')->orderBy('number', 'ASC')->get();
        $arrTables = array();
        foreach ($tables as $t) {
            if (!$t->hasActiveBill()) {
                $arrTables[] = $t;
            }
        }
        return view('plateau.home', [
      'productsToOrder' => $this->productRepository->forEstablishment(),
      'tables' => $arrTables
    ]);
    }

    /**
     * List all request tokens
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getRequestTokens(Request $request)
    {
        $tableRequests = TableRequest::with(['user'])
      ->where(['establishment_id' => $this->establishment->id, 'status' => 'opened'])
      ->get();
        return response()->json($tableRequests);
    }

    /**
     * Finaliza as requisições de token
     * @param  Request      $request      [description]
     * @param  TableRequest $tableRequest [description]
     * @return [type]                     [description]
     */
    public function closeRequestToken(Request $request)
    {
        $tableRequest = TableRequest::find($request->table_request_id);
        $tableRequest->status = 'closed';
        $tableRequest->save();
        return response()->json(['success' => true]);
    }

    /**
    * List all active Sectors
    * @param  Request $request [description]
    * @return [type]           [description]
    */
    public function sectors(Request $request)
    {
        return $this->sectorRepository->listSectors();
    }

    /**
    * List all active Products
    * @param  Request $request [description]
    * @return [type]           [description]
    */
    public function products(Request $request)
    {
        return $this->productRepository->listProductsForModal();
    }

    /**
    * Lista os pedidos ativos do estabelecimento
    * @param Request $request
    * @return \Illuminate\Http\Response
    */
    public function preOrders(Request $request, $status = 'opened')
    {
        return $this->preOrderRepository->listPreOrders($status);
    }

    /**
    * Lista os pedidos ativos do estabelecimento
    * @param Request $request
    * @return \Illuminate\Http\Response
    */
    public function orders(Request $request, $status = 'opened', $sector_id = null)
    {
        if ($sector_id == '0' && empty($sector_id)) {
            $sector_id = null;
        }
        return $this->orderRepository->listOrders($status, $sector_id);
    }

    /**
    * Lista as contas do estabelecimento
    * @param Request $request
    * @return \Illuminate\Http\Response
    */
    public function bills(Request $request, $status = 'opened')
    {
        return $this->billRepository->listBills($status);
    }

    /**
    * Retorna os detalhes de uma conta específica
    * @param Request $request
    * @param $bill_id
    * @return \Illuminate\Http\Response
    */
    public function bill(Request $request, $bill_id)
    {
        return $this->billRepository->getBill($bill_id);
    }

    /**
     * Cria uma nova conta sem usuário
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function createBill(Request $request)
    {
        $table_id = $request->input('table_id');
        $user_name = $request->input('user_name');
        if ($user_name == null || $user_name == '') {
            $user_name = 'Sem Nome';
        }
        Log::notice('[ADMIN] Usuário ' . Auth::user()->name . ' - ' . Auth::user()->id . ' criando uma conta via Dashboard com id de mesa ' . $table_id);
        return $this->billRepository->createBill($user_name, $table_id);
    }

    /**
     * Migra os pedidos de uma conta e a encerra
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function migrateBill(Request $request)
    {
        $originBill = $request->input('origin_bill');
        $destinyBill = $request->input('destiny_bill');
        Log::notice('[ADMIN] Usuário ' . Auth::user()->name . ' - ' . Auth::user()->id . ' migrando uma conta de originId: ' . $originBill . ' para destinyId: ' . $destinyBill);
        return $this->billRepository->migrateBill($originBill, $destinyBill);
    }

    /**
    * Muda o nome da mesa relacionada à conta
    * @param Request $request
    * @param $bill_id
    * @return mixed
    */
    public function changeTableName(Request $request, $bill_id)
    {
        $name = $request->input('name');
        return $this->billRepository->changeTableName($bill_id, $name);
    }

    /**
    * Muda a mesa da conta
    * @param Request $request
    * @param $bill_id
    * @param $table_id
    * @return mixed
    */
    public function changeBillTable(Request $request, $bill_id)
    {
        $table_id = $request->input('table');
        return $this->billRepository->changeBillTable($bill_id, $table_id);
    }

    /**
    * Adiciona um pedido à uma determinada conta
    * @param Request $request
    * @param $bill_id
    * @param $product_id
    * @return \Illuminate\Http\Response
    */
    public function addOrder(Request $request, Bill $bill, $product_id)
    {
        $quant = $request->input('quant');
        $obs = $request->input('observation');
        $options = $request->input('options');
        $weight = $request->input('weight');
        Log::notice('[ADMIN] Usuário ' . Auth::user()->name . ' - ' . Auth::user()->id . ' adicionando pedido à conta: ' . $bill->id . ' com produto: ' . $product_id);
        return $this->billRepository->addOrderToBill($bill, $product_id, $quant, $obs, $weight, $options);
    }

    /**
    * Adiciona o peso a um determinado pedido
    * @param Request $request
    * @param Order $order
    * @return \Illuminate\Http\Response
    */
    public function addWeight(Request $request, Order $order)
    {
        $is_weight = $request->input('is_weight');
        $weight = $request->input('weight');
        return $this->orderRepository->addWeightToOrder($order, $is_weight, $weight);
    }

    /**
    * Marca um pedido pré-pago como em preparo
    * @param Request $request
    * @param Order $order
    * @return \Illuminate\Http\Response
    */
    public function preOrderPreparation(Request $request, PreOrder $preOrder)
    {
        Log::notice('[ADMIN] Usuário ' . Auth::user()->name . ' - ' . Auth::user()->id . ' iniciou preparo do pedido pré-pago no dashboard: ' . $preOrder->id);
        return $this->preOrderRepository->preparePreOrder($preOrder);
    }

    /**
    * Marca um pedido como concluído
    * @param Request $request
    * @param Order $order
    * @return \Illuminate\Http\Response
    */
    public function orderClose(Request $request, Order $order)
    {
        Log::notice('[ADMIN] Usuário ' . Auth::user()->name . ' - ' . Auth::user()->id . ' confirmou no dashboard o pedido: ' . $order->id);
        return $this->orderRepository->confirmOrder($order);
    }

    /**
    * Marca um pedido pré-pago como concluído
    * @param Request $request
    * @param Order $order
    * @return \Illuminate\Http\Response
    */
    public function preOrderClose(Request $request, PreOrder $preOrder)
    {
        Log::notice('[ADMIN] Usuário ' . Auth::user()->name . ' - ' . Auth::user()->id . ' confirmou no dashboard o pedido pré-pago: ' . $preOrder->id);
        return $this->preOrderRepository->confirmPreOrder($preOrder);
    }

    /**
    * Cancela um pedido
    * @param Request $request
    * @param Order $order
    * @return \Illuminate\Http\Response
    */
    public function cancelOrder(Request $request, Order $order)
    {
        Log::notice('[ADMIN] Usuário ' . Auth::user()->name . ' - ' . Auth::user()->id . ' cancelou no dashboard o pedido: ' . $order->id);
        if ($order->status == Order::STATUS_DENY) {
            Log::notice('[ADMIN] Pedido ' . $order->id . ' possuia status: deny');
        }
        return $this->orderRepository->cancelOrder($order);
    }

    /**
    * Encerra uma conta
    * @param Request $request
    * @param Bill $bill
    * @return \Illuminate\Http\Response
    */
    public function billClose(Request $request, Bill $bill)
    {
        Log::notice('[ADMIN] Usuário ' . Auth::user()->name . ' - ' . Auth::user()->id . ' encerrou uma conta via dashboard: ' . $bill->id);
        return $this->billRepository->closeBill($bill);
    }

    /**
    * Resgata o código TOKEN de uma mesa
    * @param  Request $request [description]
    * @param  Table   $table   [description]
    * @return [type]           [description]
    */
    public function tableCode(Request $request, Table $table)
    {
        return $this->tableRepository->getTableCode($table);
    }
}
