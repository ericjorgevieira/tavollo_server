<?php

namespace App\Http\Controllers\Plateau;

use App\Models\EstablishmentConfig;
use App\Models\EstablishmentIntegration;
use App\Models\Integration;
use App\Repositories\EstablishmentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ApiToken;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class IntegrationsController extends Controller
{

  protected $repository;

  protected $establishment;

  protected $user;

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct(EstablishmentRepository $repository)
  {
    $this->middleware(function ($request, $next) {
      $this->user = Auth::user();
      $this->establishment = Auth::user()->establishment;

      return $next($request);
    });

    $this->repository = $repository;
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $establishmentIntegration = $this->establishment->integration;
    if(!$this->establishment->integration){
      $establishmentIntegration = new EstablishmentIntegration();
      $establishmentIntegration->establishment_id = $this->establishment->id;
      $establishmentIntegration->status = EstablishmentIntegration::STATUS_ENABLED;
      $establishmentIntegration->integration_id = 0;
      $establishmentIntegration->is_active = 0;
      $establishmentIntegration->save();
    }

    return view('plateau.profile',[
      'profile' => $this->user,
      'action' => 'integration',
      'establishment' => $this->establishment,
      'establishmentIntegration' => $establishmentIntegration,
      'integrations' => Integration::where('status', Integration::STATUS_ENABLED)->orderBy('name', 'ASC')->get()
    ]);
  }

  /**
   * Atualiza os dados de Integração
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function update(Request $request)
  {
    $integration = $this->establishment->integration;
    $validator = Validator::make($request->all(), [
      'integration_id' => 'required',
    ]);

    if($this->establishment->hasOpenedBill()){
      return redirect('/profile/integration')
      ->with('warning', ['Existem contas abertas. Por favor, encerre todas para efetuar alterações.']);
    }

    if ($validator->fails()) {
      return redirect('/profile/integration')
      ->withInput()
      ->withErrors($validator);
    }

    $integration->integration_id = $request->integration_id;
    $integration->credential_user = $request->credential_user;
    $integration->credential_password = $request->credential_password;
    $integration->api_url = $request->api_url;
    $integration->api_token = $request->api_token;
    $integration->is_active = ($request->is_active) ? 1 : 0;
    $integration->save();

    return redirect('/profile/integration')
    ->with('success', ['Dados de Integração salvos com sucesso!']);
  }

  public function test(Request $request)
  {
    $integration = $this->establishment->integration->getIntegration();
    if(!$integration){
      return redirect('/profile/integration')
      ->with('error', ['A integração está desativada.']);
    }
    if($integration->checkConectivity(null, 'POST')){
      return redirect('/profile/integration')
      ->with('success', ['Conexão estabelecida com sucesso!']);
    }else{
      return redirect('/profile/integration')
      ->with('error', ['Houve um problema com a conexão. Verifique os dados e entre em contato com o suporte.']);
    }
  }

}
