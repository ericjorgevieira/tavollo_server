<?php

namespace App\Http\Controllers\Plateau;

use App\Http\Controllers\Controller;
use App\Models\Island;
use App\Repositories\IslandRepository;
use App\Viewers\IslandViewer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IslandsController extends Controller
{
    protected $repository;

    protected $establishment;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(IslandRepository $repository)
    {
        $this->repository = $repository;

        $this->middleware(function ($request, $next) {
            $this->establishment = Auth::user()->establishment;
            $this->repository->setEstablishment($this->establishment);

            return $next($request);
        });
    }

    /**
     * Lista de ilhas.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('plateau.profile', [
            'action' => 'islands',
            'url' => 'profile/islands/list',
        ]);
    }

    public function listIslands()
    {
        return app(IslandViewer::class)->listForTable($this->establishment);
    }

    /**
     * Insere uma Mesa no registro.
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        return $this->repository->store($request);
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Island $island)
    {
        if ($this->authorize('update', $island)) {
            return $this->repository->update($request, $island);
        }

        return redirect('/profile/islands')
            ->withErrors(['Você não pode realizar esta ação.']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('plateau.profile', [
            'action' => 'islands-create',
            'waiters' => $this->establishment->waiters()->where('status', 'enabled')->get(),
            'listTables' => $this->repository->tables(),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Island $island)
    {
        if ($this->authorize('update', $island)) {
            return view('plateau.profile', [
                'action' => 'islands-edit',
                'island' => $island,
                'waiters' => $this->establishment->waiters()->where('status', 'enabled')->get(),
                'listTables' => $this->repository->tables(),
                'islandTables' => $this->repository->tablesToArray($island),
            ]);
        }

        return redirect('/profile/islands')
            ->withErrors(['Você não pode realizar esta ação.']);
    }

    /**
     * Habilita/Desabilita exibição de Island.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function enable(Island $island)
    {
        if ($this->authorize('update', $island)) {
            return $this->repository->enable($island);
        }

        return redirect('/profile/islands')
      ->withErrors(['Você não pode realizar esta ação.']);
    }

    /**
     * Remove uma Ilha.
     */
    public function destroy(Island $island)
    {
        if ($this->authorize('destroy', $island)) {
            $island->delete();

            return redirect('/profile/islands')
                ->with('success', ['Island removido.']);
        }

        return redirect('/profile/islands')
            ->withErrors(['Você não pode realizar esta ação.']);
    }
}
