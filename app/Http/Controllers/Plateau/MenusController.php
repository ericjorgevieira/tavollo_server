<?php

namespace App\Http\Controllers\Plateau;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Repositories\MenuRepository;
use App\Viewers\MenuViewer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class MenusController extends Controller
{
    protected $repository;

    protected $establishment;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MenuRepository $menuRepository)
    {
        $this->repository = $menuRepository;

        $this->middleware(function ($request, $next) {
            $this->establishment = Auth::user()->establishment;
            $this->repository->setEstablishment($this->establishment);

            return $next($request);
        });
    }

    /**
     * Lista de cardápios.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('plateau.menus.menus', [
            'url' => '/menus/list',
        ]);
    }

    public function listMenus()
    {
        return app(MenuViewer::class)->listForTable($this->establishment);
    }

    /**
     * Insere uma Mesa no registro.
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        return $this->repository->store($request);
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Menu $menu)
    {
        if ($this->authorize('update', $menu)) {
            return $this->repository->update($request, $menu);
        } else {
            return redirect('/menus/'.$menu->id.'/edit')
            ->withErrors(['Você não pode realizar esta ação.']);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('plateau.menus.menus-create', [
            'listProducts' => $this->repository->productsByCategories(),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Menu $menu)
    {
        if ($this->authorize('update', $menu)) {
            return view('plateau.menus.menus-edit', [
                'menu' => $menu,
                'listProducts' => $this->repository->productsByCategories(),
                'menuProducts' => $menu->products()->pluck('id')->all(),
            ]);
        }

        return redirect('/menus')
        ->withErrors(['Você não pode realizar esta ação.']);
    }

    public function uploadPhoto(Request $request)
    {
        return $this->repository->uploadTmpPhoto($request);
    }

    /**
     * Habilita/Desabilita exibição de Menu.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function enable(Menu $menu)
    {
        if ($this->authorize('update', $menu)) {
            return $this->repository->enable($menu);
        }
    }

    /**
     * Remove uma Mesa.
     */
    public function destroy(Menu $menu)
    {
        if ($this->authorize('destroy', $menu)) {
            $menu->delete();

            return redirect('/menus')
            ->with('success', ['Menu removido.']);
        }

        return redirect('/tables')
        ->withErrors(['Você não pode realizar esta ação.']);
    }

    /**
     * Ordena com Jquery UI a posição dos cardápios.
     *
     * @return Response
     */
    public function order(Request $request)
    {
        $response = new Response();

        $menus = [];
        parse_str($request->order, $menus);
        foreach ($menus['menu'] as $order => $id) {
            $menu = Menu::find($id);
            if ($menu->establishment->id == $this->establishment->id &&
            $menu->status == 'enabled'
        ) {
                $menu->position = $order;
                $menu->save();
            }
        }

        $response->setStatusCode(200);

        return $response;
    }
}
