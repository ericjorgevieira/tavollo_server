<?php

namespace App\Http\Controllers\Plateau;

use App\Http\Controllers\Controller;
use App\Repositories\PreOrderRepository;
use App\Viewers\PreOrderViewer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PreOrdersController extends Controller
{
    protected $repository;

    protected $establishment;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PreOrderRepository $repository)
    {
        $this->middleware(function ($request, $next) {
            $this->establishment = Auth::user()->establishment;
            $this->repository->setEstablishment($this->establishment);

            return $next($request);
        });

        $this->repository = $repository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('plateau.preorders', [
            'url' => '/pre-orders/list',
        ]);
    }

    public function listPreOrders(Request $request)
    {
        return app(PreOrderViewer::class)->listForTable($this->establishment, $request->input('limit'),
            $request->input('status'), $request->input('period'));
    }

    public function getPreOrder(Request $request, $id)
    {
        return $this->repository->getPreOrder($id);
    }
}
