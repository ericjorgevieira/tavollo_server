<?php

namespace App\Http\Controllers\Plateau;

use App\Models\EstablishmentConfig;
use App\Repositories\PrinterRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class PrinterController extends Controller
{
    protected $profileRepository;

    protected $establishment;

    protected $user;

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct(PrinterRepository $repository)
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->establishment = Auth::user()->establishment;

            return $next($request);
        });

        $this->repository = $repository;
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('plateau.profile', [
          'profile' => $this->user,
          'config' => $this->establishment->config,
          'action' => 'printer'
        ]);
    }

    public function update(Request $request)
    {
        Log::notice('[ADMIN] Usuário ' . Auth::user()->name . ' - ' . Auth::user()->id . ' alterando configurações de impressão estabelecimento: ' . $this->establishment->id);
        return $this->repository->updatePrinter($request, $this->establishment);
    }
}
