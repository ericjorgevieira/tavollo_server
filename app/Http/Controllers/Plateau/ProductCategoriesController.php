<?php

namespace App\Http\Controllers\Plateau;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Repositories\ProductCategoryRepository;
use App\Viewers\ProductCategoryViewer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductCategoriesController extends Controller
{
    protected $repository;

    protected $establishment;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ProductCategoryRepository $repository)
    {
        $this->repository = $repository;

        $this->middleware(function ($request, $next) {
            $this->establishment = Auth::user()->establishment;
            $this->repository->setEstablishment($this->establishment);

            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('plateau.categories.categories', [
      'url' => '/categories/list',
    ]);
    }

    public function listCategories()
    {
        return app(ProductCategoryViewer::class)->listForTable($this->establishment);
    }

    /**
     * Processa cadastro de novo Product.
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        return $this->repository->store($request);
    }

    /**
     * Processa edição de Product.
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, ProductCategory $category)
    {
        if ($this->authorize('update', $category)) {
            return $this->repository->update($request, $category);
        }

        return redirect('/categories/'.$category->id.'/edit')
    ->withErrors(['Você não pode realizar esta ação.']);
    }

    /**
     * Exibe formulário de cadastro de Produto.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('plateau.categories.categories-create', [
      'categories' => $this->repository->forEstablishment(),
    ]);
    }

    /**
     * Exibe formulário de edição de Produto.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(ProductCategory $category)
    {
        if ($this->authorize('update', $category)) {
            return view('plateau.categories.categories-edit', [
        'category' => $category,
        'categories' => $this->repository->forEstablishment(),
      ]);
        }

        return redirect('/categories')
    ->withErrors(['Você não pode realizar esta ação.']);
    }

    /**
     * Habilita/Desabilita exibição de ProductCategory.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function enable(ProductCategory $category)
    {
        if ($this->authorize('update', $category)) {
            return $this->repository->enable($category);
        }
    }

    /**
     * Remove Product.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     *
     * @throws \Exception
     */
    public function destroy(Product $product)
    {
        if ($this->authorize('destroy', $product)) {
            $product->delete();

            return redirect('/products')
      ->with('success', 'Produto removido.');
        }

        return redirect('/products')
    ->withErrors(['Você não pode realizar esta ação.']);
    }
}
