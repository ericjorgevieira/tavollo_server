<?php

namespace App\Http\Controllers\Plateau;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductGroup;
use App\Models\ProductGroupItem;
use App\Repositories\ProductRepository;
use App\Viewers\ProductViewer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{
    protected $repository;

    protected $establishment;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;

        $this->middleware(function ($request, $next) {
            $this->establishment = Auth::user()->establishment;
            $this->repository->setEstablishment($this->establishment);

            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->establishment->productCategories()->where('status', 'enabled')->get()->toJson();

        return view('plateau.products.products', [
            'url' => '/products/list',
            'categories' => $categories,
        ]);
    }

    public function listProducts(Request $request)
    {
        return app(ProductViewer::class)->listForTable($this->establishment, $request->input('limit'),
            $request->input('status'), $request->input('category'));
    }

    /**
     * Exibe formulário de cadastro de Produto.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        if ($this->establishment->config->enable_advanced_functions == 1) {
            return view('plateau.products.products-create', [
              'products' => $this->repository->forEstablishment(),
              'productCategories' => $this->repository->listCategories(),
            ]);
        } else {
            return view('plateau.products.products-info');
        }
    }

    /**
     * Processa cadastro de novo Product.
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if ($this->establishment->config->enable_advanced_functions == 1) {
            return $this->repository->store($request);
        }
    }

    /**
     * Processa edição de Product.
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Product $product)
    {
        if ($this->authorize('update', $product)) {
            return $this->repository->update($request, $product);
        }

        return redirect('/products/'.$product->id.'/edit')->withErrors(['Você não pode realizar esta ação.']);
    }

    /**
     * Exibe formulário de edição de Produto.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Product $product)
    {
        if ($this->authorize('update', $product)) {
            return view('plateau.products.products-edit', [
                'product' => $product,
                'products' => $this->repository->forEstablishment(),
                'productCategories' => $this->repository->listCategories(),
            ]);
        }

        return redirect('/products')->withErrors(['Você não pode realizar esta ação.']);
    }

    public function uploadPhoto(Request $request)
    {
        return $this->repository->uploadTmpPhoto($request);
    }

    /**
     * Habilita/Desabilita exibição de Product.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function enable(Product $product)
    {
        if ($this->authorize('update', $product)) {
            return $this->repository->enable($product);
        }
    }

    public function updateCategory(Product $product, ProductCategory $category)
    {
        if ($this->authorize('update', $product)) {
            return $this->repository->updateCategory($product, $category);
        }
    }

    /**
     * Remove Product.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     *
     * @throws \Exception
     */
    public function destroy(Product $product)
    {
        if ($this->authorize('destroy', $product)) {
            return $this->repository->destroy($product);
        }
    }

    /**
     * Lista as observações do produto.
     */
    public function listObservations(Product $product)
    {
        if ($this->authorize('update', $product)) {
            return $this->repository->listObservations($product);
        }
    }

    /**
     * Salva uma observação para o produto.
     */
    public function saveObservation(Request $request, Product $product)
    {
        if ($this->authorize('update', $product)) {
            return $this->repository->saveObservation($product, $request->input('observation'));
        }
    }

    /**
     * Deleta uma observação do produto.
     */
    public function deleteObservation(Request $request, Product $product)
    {
        if ($this->authorize('update', $product)) {
            return $this->repository->deleteObservation($product, $request->input('key'));
        }
    }

    /**
     * Lista todos os grupos de itens.
     *
     * @param Request $request [description]
     * @param Product $product [description]
     *
     * @return [type] [description]
     */
    public function listGroups(Request $request, Product $product)
    {
        if ($this->authorize('listgroups', $product)) {
            return response(['groups' => $product->groups]);
        }
    }

    /**
     * Lista todos os itens de um grupo.
     *
     * @param Request      $request [description]
     * @param Product      $product [description]
     * @param ProductGroup $group   [description]
     *
     * @return [type] [description]
     */
    public function listGroupItems(Request $request, Product $product, ProductGroup $group)
    {
        if ($this->authorize('listgroupitems', $product)) {
            return response(['items' => $group->items]);
        }
    }

    /**
     * Persiste um grupo de itens.
     *
     * @param Request $request [description]
     * @param Product $product [description]
     * @param [type]  $group   [description]
     *
     * @return [type] [description]
     */
    public function saveGroup(Request $request, Product $product, ProductGroup $group = null)
    {
        if ($this->authorize('listgroups', $product)) {
            return $this->repository->saveGroup($request, $product, $group);
        }
    }

    /**
     * Remove um agrupamento.
     */
    public function deleteGroup(Request $request, Product $product, ProductGroup $group)
    {
        if ($this->authorize('listgroups', $product)) {
            return $this->repository->deleteGroup($group);
        }
    }

    /**
     * Persiste um item em grupo.
     *
     * @param Request      $request [description]
     * @param Product      $product [description]
     * @param ProductGroup $group   [description]
     * @param [type]       $item    [description]
     *
     * @return [type] [description]
     */
    public function saveItem(Request $request, Product $product, ProductGroup $group, ProductGroupItem $item = null)
    {
        if ($this->authorize('listgroups', $product)) {
            return $this->repository->saveItem($request, $group, $item);
        }
    }

    /**
     * Remove um item de um agrupamento.
     */
    public function deleteItem(Request $request, Product $product, ProductGroup $group, ProductGroupItem $item)
    {
        if ($this->authorize('listgroups', $product)) {
            return $this->repository->deleteItem($item);
        }
    }

    /**
     * Tela de Importação .csv.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function showImport(Request $request)
    {
        return view('plateau.products.products-import');
    }

    /**
     * Processa a Importação.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function import(Request $request)
    {
        return $this->repository->importProducts($request);
    }

    /**
     * Processa a Importação via integração.
     *
     * @return [type] [description]
     */
    public function importIntegration()
    {
        if ($this->establishment->integration) {
            return $this->repository->importProductsFromIntegration();
        }

        return redirect('/products/import')
    ->withErrors(['Você não pode realizar esta ação.']);
    }
}
