<?php

namespace App\Http\Controllers\Plateau;

use App\Http\Controllers\Controller;
use App\Models\LogActivity;
use App\Repositories\EstablishmentRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use QRCode;

class ProfileController extends Controller
{
    protected $userRepository;

    protected $establishmetnRepository;

    protected $establishment;

    protected $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository, EstablishmentRepository $establishmentRepository)
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            $this->establishment = Auth::user()->establishment;

            return $next($request);
        });

        $this->userRepository = $userRepository;
        $this->establishmentRepository = $establishmentRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('plateau.profile', [
          'profile' => $this->user,
          'action' => 'user',
        ]);
    }

    public function faq()
    {
        return view('plateau.profile', ['action' => 'faq']);
    }

    public function update(Request $request)
    {
        return $this->userRepository->updateProfile($request, $this->user);
    }

    public function password()
    {
        return view('plateau.profile', [
          'profile' => $this->user,
          'action' => 'password',
        ]);
    }

    public function passwordUpdate(Request $request)
    {
        return $this->userRepository->updatePassword($request, $this->user);
    }

    public function establishment()
    {
        return view('plateau.profile', [
          'establishment' => $this->establishment,
          'action' => 'establishment',
          'estados' => \App\Models\Establishment::estados(),
        ]);
    }

    public function generateEstablishmentQRCode()
    {
        $url = \Config::get('app.env') == 'homologation' ? 'https://testapp.tavollo.com/#/establishments/' :
        'https://app.tavollo.com/#/establishments/';
        $stream = QRCode::text($url.$this->establishment->slug)->setSize(10)->png();
        $filename = 'tavollo-qrcode-establishment-'.$this->establishment->slug.'.png';

        return response($stream, 200, [
          'Content-Type' => 'image/png',
          'Content-Disposition' => 'attachment; filename="'.$filename.'"',
        ]);
    }

    public function establishmentUpdate(Request $request)
    {
        return $this->establishmentRepository->updateEstablishment($request, $this->establishment);
    }

    public function config()
    {
        return view('plateau.profile', [
          'establishment' => $this->establishment,
          'config' => $this->establishmentRepository->getEstablishmentConfig($this->establishment),
          'action' => 'config',
        ]);
    }

    public function configUpdate(Request $request)
    {
        Log::notice('[ADMIN] Usuário '.Auth::user()->name.' - '.Auth::user()->id.' alterando configurações do estabelecimento: '.$this->establishment->id);

        return $this->establishmentRepository->updateEstablishmentConfig($request, $this->establishment);
    }

    public function api()
    {
        $token = Auth::user()->establishment->api_token;
        $sequence = ($token) ? $token->sequence : '';

        return view('plateau.profile', [
          'token' => $sequence,
          'action' => 'api',
        ]);
    }

    public function generateApiKey(Request $request)
    {
        Log::notice('[ADMIN] Usuário '.Auth::user()->name.' - '.Auth::user()->id.' gerando chave de API para estabelecimento: '.$this->establishment->id);
        Auth::user()->generateApiToken();

        return redirect('/profile/api');
    }

    public function about()
    {
        return view('plateau.about');
    }

    public function payment(Request $request)
    {
        $enable_payment = $this->establishment->config->enable_payment;
        $cieloAccount = $this->establishment->getPaymentMethod();

        return view('plateau.profile', [
          'establishment' => $this->establishment,
          'account' => $cieloAccount,
          'enable_payment' => $enable_payment,
          'action' => 'payment',
        ]);
    }

    public function savePayment(Request $request)
    {
        Log::notice('[ADMIN] Usuário '.Auth::user()->name.' - '.Auth::user()->id.' alterando configurações de pagamento do estabelecimento: '.$this->establishment->id);
        if ($request->enable_payment) {
            $validator = Validator::make($request->all(), [
            'merchant_id' => 'required',
            'merchant_key' => 'required',
          ]);

            if ($validator->fails()) {
                return redirect('/profile/payment')
              ->withInput()
              ->withErrors($validator);
            }
        }

        $config = $this->establishment->config;
        if ($request->merchant_id && $request->merchant_key) {
            $account = [
            'MerchantId' => $request->merchant_id,
            'MerchantKey' => $request->merchant_key,
          ];
            $config->cielo_account = json_encode($account);
        } else {
            $config->cielo_account = null;
        }
        $config->enable_payment = ($request->enable_payment) ? 1 : 0;
        $config->bill_extract_url = trim($request->bill_extract_url);

        $config->save();

        return redirect('/profile/payment')->with('success', ['Configurações alteradas com sucesso!']);
    }

    public function activities(Request $request)
    {
        $logs = LogActivity::with('user')->where(['establishment_id' => $this->establishment->id])->orderBy('created_at', 'desc')->get();

        return view('plateau.profile', [
          'logs' => $logs,
          'action' => 'activities',
        ]);
    }
}
