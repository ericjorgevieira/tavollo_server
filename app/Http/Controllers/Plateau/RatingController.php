<?php

namespace App\Http\Controllers\Plateau;

use App\Http\Controllers\Controller;
use App\Repositories\EstablishmentRepository;
use App\Repositories\RatingRepository;
use App\Viewers\RatingViewer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RatingController extends Controller
{
    protected $repository;

    protected $establishment;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RatingRepository $repository)
    {
        $this->repository = $repository;

        $this->middleware(function ($request, $next) {
            $this->establishment = Auth::user()->establishment;
            $this->repository->setEstablishment($this->establishment);

            return $next($request);
        });
    }

    /**
     * Lista de avaliações.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('plateau.analysis', [
          'action' => 'ratings',
          'url' => '/analysis/ratings/list',
          'ratingTotal' => $this->establishment->getRatingTotal(),
        ]);
    }

    public function listRatings()
    {
        return app(RatingViewer::class)->listForTable($this->establishment);
    }

    public function config()
    {
        $establishmentRepository = app(EstablishmentRepository::class);
        $config = $establishmentRepository->getEstablishmentConfig($this->establishment);

        return view('plateau.analysis', [
            'establishment' => $this->establishment,
            'config' => $config,
            'predefinedAnswers' => json_decode($config->predefined_answers, true),
            'action' => 'ratings-config',
        ]);
    }

    public function configSave(Request $request)
    {
        return $this->repository->configSave($request);
    }

    public function enable()
    {
    }

    public function delete()
    {
    }
}
