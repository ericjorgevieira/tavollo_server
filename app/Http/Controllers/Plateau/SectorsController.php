<?php

namespace App\Http\Controllers\Plateau;

use App\Http\Controllers\Controller;
use App\Repositories\SectorRepository;
use App\Models\Sector;
use App\Viewers\SectorViewer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SectorsController extends Controller
{
    protected $repository;

    protected $establishment;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SectorRepository $repository)
    {
        $this->repository = $repository;

        $this->middleware(function ($request, $next) {
            $this->establishment = Auth::user()->establishment;
            $this->repository->setEstablishment($this->establishment);

            return $next($request);
        });
    }

    /**
     * Lista de setores.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('plateau.profile', [
          'action' => 'sectors',
          'url' => '/profile/sectors/list',
        ]);
    }

    public function listSectors()
    {
        return app(SectorViewer::class)->listForTable($this->establishment);
    }

    /**
     * Insere um Setor no registro.
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        return $this->repository->store($request);
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Sector $sector)
    {
        if ($this->authorize('update', $sector)) {
            return $this->repository->update($request, $sector);
        } else {
            return redirect('/profile/sectors/'.$sector->id.'/edit')
      ->withErrors(['Você não pode realizar esta ação.']);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('plateau.profile', [
          'action' => 'sectors-create',
          'listProducts' => $this->repository->productsByCategories(),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Sector $sector)
    {
        if ($this->authorize('edit', $sector)) {
            return view('plateau.profile', [
              'action' => 'sectors-edit',
              'sector' => $sector,
              'listProducts' => $this->repository->productsByCategories($sector->id),
              'sectorProducts' => $sector->products()->pluck('id')->all(),
            ]);
        } else {
            return redirect('/profile/sectors')
              ->withErrors(['Você não pode realizar esta ação.']);
        }
    }

    /**
     * Habilita/Desabilita exibição de Setor.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function enable(Sector $sector)
    {
        if ($this->authorize('update', $sector)) {
            return $this->repository->enable($sector);
        }

        return redirect('/profile/sectors')
    ->withErrors(['Você não pode realizar esta ação.']);
    }

    /**
     * Remove um Setor.
     */
    public function destroy(Sector $sector)
    {
        $sector->delete();

        return redirect('/profile/sectors')
    ->with('success', ['Setor removido.']);
    }
}
