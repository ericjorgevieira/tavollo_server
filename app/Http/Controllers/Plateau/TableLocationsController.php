<?php

namespace App\Http\Controllers\Plateau;

use App\Http\Controllers\Controller;
use App\Repositories\TableLocationRepository;
use App\Models\TableLocation;
use App\Viewers\TableLocationViewer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TableLocationsController extends Controller
{
    protected $repository;

    protected $establishment;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TableLocationRepository $locations)
    {
        $this->repository = $locations;

        $this->middleware(function ($request, $next) {
            $this->establishment = Auth::user()->establishment;
            $this->repository->setEstablishment($this->establishment);

            return $next($request);
        });
    }

    /**
     * Carrega template inicial.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('plateau.locations.locations', [
            'url' => '/locations/list',
        ]);
    }

    public function listLocations()
    {
        return app(TableLocationViewer::class)->listForTable($this->establishment);
    }

    /**
     * Retorna lista de Locais.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function getList(Request $request)
    {
        return $this->repository->listLocations();
    }

    /**
     * Insere um Local no registro.
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        return $this->repository->store($request);
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, TableLocation $location)
    {
        if ($this->authorize('update', $location)) {
            return $this->repository->update($request, $location);
        }

        return redirect('/locations/'.$location->id.'/edit')
        ->withErrors(['Você não pode realizar esta ação.']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('plateau.locations.locations-create');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(TableLocation $location)
    {
        if ($this->authorize('update', $location)) {
            return view('plateau.locations.locations-edit', [
                'location' => $location,
            ]);
        }

        return redirect('/locations')
        ->withErrors(['Você não pode realizar esta ação.']);
    }

    /**
     * Habilita/Desabilita exibição de Local.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function enable(TableLocation $location)
    {
        if ($this->authorize('update', $location)) {
            return $this->repository->enable($location);
        }
    }

    /**
     * Remove um Local.
     */
    public function destroy(TableLocation $location)
    {
        if ($this->authorize('destroy', $location)) {
            $location->delete();

            return redirect('/locations')
            ->with('success', 'Local removido.');
        }

        return redirect('/locations')
        ->withErrors(['Você não pode realizar esta ação.']);
    }
}
