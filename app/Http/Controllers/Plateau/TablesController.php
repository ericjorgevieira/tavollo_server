<?php

namespace App\Http\Controllers\Plateau;

use App\Http\Controllers\Controller;
use App\Models\Table;
use App\Repositories\TableRepository;
use App\Viewers\TableViewer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use QRCode;
use Storage;

class TablesController extends Controller
{
    protected $repository;

    protected $establishment;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TableRepository $tables)
    {
        $this->repository = $tables;

        $this->middleware(function ($request, $next) {
            $this->establishment = Auth::user()->establishment;
            $this->repository->setEstablishment($this->establishment);

            return $next($request);
        });
    }

    /**
     * Carrega template inicial.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->slugify();

        return view('plateau.spots.tables', [
            'url' => '/tables/list',
        ]);
    }

    public function listTables()
    {
        return app(TableViewer::class)->listForTable($this->establishment);
    }

    /**
     * Retorna lista de Mesas.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function getList(Request $request)
    {
        return $this->repository->listTables();
    }

    /**
     * Gera novos tokens para todas as Mesas.
     */
    public function generate()
    {
        $this->repository->generateTokens();

        return redirect('/tables')
        ->with('success', ['TOKENS gerados com sucesso!']);
    }

    /**
     * Gera QR code de todos os spots ativos.
     */
    public function getAllQrCodes(Request $request)
    {
        $spots = $this->establishment->tables()->where(['status' => Table::STATUS_ENABLED])->get();

        $url = \Config::get('app.env') == 'homologation' ? 'https://testapp.tavollo.com/#/t/' :
              'https://app.tavollo.com/#/t/';

        //zipar com o ZipArchive a pasta e processar download
        $zip = new \ZipArchive();
        $zipName = '/tmp/spots-'.$this->establishment->id.'.zip';
        $directory = 'tmp_qr_codes/'.$this->establishment->id;

        Storage::makeDirectory('/public/'.$directory);

        if ($zip->open($zipName, \ZipArchive::CREATE) === true) {
            //iterar sobre os spots ativos do estabelecimento
            foreach ($spots as $s) {
                //gerar url para qr code de cada spot
                $qrCodeUrl = $this->establishment->slug ? $url.$this->establishment->slug.'/'.$s->code : $url.$s->code;

                //gerar png de cada qr code
                $filename = 'tavollo-qrcode-'.$s->id.'-'.str_slug($s->name).'.png';
                $path = 'storage/'.$directory.'/'.$filename;

                QRCode::text($qrCodeUrl)->setOutfile(public_path($path))->setSize(10)->png();

                $zip->addFile($path, $filename);
            }

            $zip->close();

            //apagar pasta com pngs
            Storage::deleteDirectory('/public/'.$directory);

            return response()->download($zipName);
        }
        abort(404);
    }

    /**
     * Gera um novo QR Code com o token.
     *
     * @param Request $request [description]
     * @param Table   $table   [description]
     *
     * @return [type] [description]
     */
    public function getQrCode(Request $request, Table $table)
    {
        if ($table->establishment_id == $this->establishment->id) {
            $url = \Config::get('app.env') == 'homologation' ? 'https://testapp.tavollo.com/#/t/' :
              'https://app.tavollo.com/#/t/';

            $qrCodeUrl = $this->establishment->slug ? $url.$this->establishment->slug.'/'.$table->code : $url.$table->code;
            $stream = QRCode::text($qrCodeUrl)->setSize(10)->png();
            $filename = 'tavollo-qrcode-'.$table->id.'-'.str_slug($table->name).'.png';

            return response($stream, 200, [
              'Content-Type' => 'image/png',
              'Content-Disposition' => 'attachment; filename="'.$filename.'"',
            ]);
        }
        abort(404);
    }

    /**
     * Insere um Spot no registro.
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        return $this->repository->insert($request);
    }

    /**
     * Insere uma sequência de Spots no registro.
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeSequence(Request $request)
    {
        return $this->repository->insertSequence($request);
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Table $table)
    {
        if ($this->authorize('update', $table)) {
            return $this->repository->update($request, $table);
        }

        return redirect('/tables/'.$table->id.'/edit')
        ->withErrors(['Você não pode realizar esta ação.']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        if ($this->establishment->config->enable_advanced_functions == 1) {
            return view('plateau.spots.tables-create');
        }

        return redirect('/tables');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createSequence(Request $request)
    {
        if ($this->establishment->config->enable_advanced_functions == 1) {
            return view('plateau.spots.tables-sequence');
        }

        return redirect('/tables');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Table $table)
    {
        if ($this->authorize('update', $table)) {
            return view('plateau.spots.tables-edit', [
                'table' => $table,
            ]);
        }

        return redirect('/tables')
        ->withErrors(['Você não pode realizar esta ação.']);
    }

    /**
     * Gera novo código para Table.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function generateCode(Table $table)
    {
        if ($this->authorize('update', $table)) {
            $table->generateNewCode();

            return ['success' => true, 'message' => 'Token gerado com sucesso! '.$table->code];
        }

        return ['error' => true, 'message' => 'Não foi possível gerar novo Token.'];
    }

    /**
     * Habilita/Desabilita exibição de Table se a mesma não estiver sendo utilizada.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function enable(Table $table)
    {
        if ($this->authorize('update', $table)) {
            return $this->repository->enable($table);
        }
    }

    /**
     * Remove uma Mesa.
     */
    public function destroy(Table $table)
    {
        if ($this->authorize('destroy', $table)) {
            //Validar se a Mesa(Table) não está sendo usada
            // por uma Conta(Bill) no momento
            if ($table->hasActiveBill()) {
                return redirect('/tables')
                ->withErrors(['Mesa está sendo usada no momento e não pode ser removida']);
            }
            $table->code = '';
            $table->save();
            $table->delete();

            return redirect('/tables')
            ->with('success', 'Mesa removida.');
        }

        return redirect('/tables')
        ->withErrors(['Você não pode realizar esta ação.']);
    }

    private function slugify()
    {
        $tables = Table::where(['establishment_id' => $this->establishment->id, 'status' => Table::STATUS_ENABLED])->get();
        foreach ($tables as $t) {
            $t->slug = str_slug($t->name);
            $t->save();
        }
    }

    /**
     * Tela de Importação .csv.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function showImport(Request $request)
    {
        return view('plateau.spots.tables-import', [
        'integration' => $this->establishment->integration,
      ]);
    }

    /**
     * Processa a Importação.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function import(Request $request)
    {
        return $this->repository->importTables($request);
    }

    public function importIntegration()
    {
    }
}
