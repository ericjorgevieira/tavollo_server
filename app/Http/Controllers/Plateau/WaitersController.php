<?php

namespace App\Http\Controllers\Plateau;

use App\Http\Controllers\Controller;
use App\Repositories\WaiterRepository;
use App\Viewers\WaiterViewer;
use App\Models\Waiter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WaitersController extends Controller
{
    protected $repository;

    protected $establishment;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(WaiterRepository $repository)
    {
        $this->repository = $repository;

        $this->middleware(function ($request, $next) {
            $this->establishment = Auth::user()->establishment;
            $this->repository->setEstablishment($this->establishment);

            return $next($request);
        });
    }

    /**
     * Lista de garçons.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('plateau.profile', [
            'action' => 'waiters',
            'url' => '/profile/waiters/list',
        ]);
    }

    public function listWaiters()
    {
        return app(WaiterViewer::class)->listForTable($this->establishment);
    }

    /**
     * Insere um garçom no registro.
     *
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        return $this->repository->store($request);
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Waiter $waiter)
    {
        if ($this->authorize('update', $waiter)) {
            return $this->repository->update($request, $waiter);
        }

        return redirect('/profile/waiters')
        ->withErrors(['Você não pode realizar esta ação.']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('plateau.profile', [
            'action' => 'waiters-create',
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Waiter $waiter)
    {
        if ($this->authorize('update', $waiter)) {
            return view('plateau.profile', [
                'action' => 'waiters-edit',
                'waiter' => $waiter,
            ]);
        }

        return redirect('/profile/waiters')
        ->withErrors(['Você não pode realizar esta ação.']);
    }

    /**
     * Habilita/Desabilita exibição de Waiter.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function enable(Waiter $waiter)
    {
        if ($this->authorize('update', $waiter)) {
            return $this->repository->enable($waiter);
        }

        return redirect('/profile/waiters')
        ->withErrors(['Você não pode realizar esta ação.']);
    }

    /**
     * Remove Waiter.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     *
     * @throws \Exception
     */
    public function destroy(Waiter $waiter)
    {
        if ($this->authorize('destroy', $waiter)) {
            return $this->repository->destroy($waiter);
        }
    }
}
