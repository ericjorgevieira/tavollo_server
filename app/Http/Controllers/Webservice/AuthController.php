<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 14/08/16
* Time: 00:06.
*/

namespace App\Http\Controllers\Webservice;

use App\Http\Controllers\Controller;
use App\Http\Requests\Webservice\StoreSessionsRequest;
use App\Models\Establishment;
use App\Repositories\BillRepository;
use App\Repositories\EstablishmentRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $userRepository;

    protected $billRepository;

    protected $establishmentRepository;

    /**
     * Controller constructor.
     */
    public function __construct(UserRepository $userRepository, BillRepository $billRepository, EstablishmentRepository $establishmentRepository)
    {
        $this->userRepository = $userRepository;
        $this->billRepository = $billRepository;
        $this->establishmentRepository = $establishmentRepository;
    }

    public function index()
    {
        return view('errors.wsauthok');
    }

    /**
     * Verifica se a mesa está disponível.
     */
    public function checkSpot(Request $request)
    {
        $user = $request->attributes->get('user');
        $establishment = null;
        if ($request->input('establishment')) {
            $establishment = Establishment::where(['id' => $request->input('establishment')])->orWhere(['slug' => $request->input('establishment')])->first();
        }

        //Resolvendo problema de cliente com QR code antigo
        // if (!$establishment) {
        //     return response(['error' => true, 'title' => 'Erro', 'message' => 'Estabelecimento não encontrado.']);
        // }

        return $this->establishmentRepository->checkSpot($establishment, $user, $request->input('code'),
            $request->input('lat'), $request->input('lng'));
    }

    public function authUser(Request $request)
    {
        return $this->userRepository->attemptLogin($request->input('user'));
    }

    /**
     * Autentica o usuário.
     */
    public function authSocial(StoreSessionsRequest $request)
    {
        return $this->userRepository->attemptSocial($request->input('user'));
    }

    /**
     * Register a new user.
     */
    public function register(Request $request)
    {
        return $this->userRepository->register($request);
    }

    /**
     * Send a mail to user redefine his password.
     */
    public function forgotPassword(Request $request)
    {
        return $this->userRepository->forgotPassword($request);
    }

    /**
     * Redefine password for user.
     */
    public function redefinePassword(Request $request, $token = null)
    {
        return $this->userRepository->redefinePassword($request);
    }

    public function establishments(Request $request)
    {
        $lat = $request->input('latitude');
        $lng = $request->input('longitude');

        return $this->establishmentRepository->listEstablishments($lat, $lng);
    }

    /**
     * Retorna o tema de um determinado estabelecimento.
     */
    protected function establishmentTheme(Request $request)
    {
        $establishment_id = $request->input('establishment_id');

        return $this->establishmentRepository->getEstablishmentTheme($establishment_id);
    }

    /**
     * Abre requisição para receber Token.
     *
     * @param Request       $request       [description]
     * @param Establishment $establishment [description]
     *
     * @return [type] [description]
     */
    public function requestToken(Request $request, Establishment $establishment)
    {
        $tableNumber = $request->input('table_number');
        $user = $request->attributes->get('user');

        return $this->establishmentRepository->requestToken($establishment, $user, $tableNumber);
    }
}
