<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 14/08/16
* Time: 00:06.
*/

namespace App\Http\Controllers\Webservice;

use App\Models\Bill;
use App\Models\Establishment;
use App\Http\Controllers\Controller;
use App\Http\Requests\Webservice\StoreSessionsRequest;
use App\Models\Order;
use App\Repositories\UserRepository;
use App\Repositories\Webservice\WSRepository;
use Illuminate\Http\Request;

class BillsController extends Controller
{
    /**
     * Repository for WS API.
     *
     * @var WSRepository
     */
    protected $repository;

    protected $userRepository;

    /**
     * Controller constructor.
     */
    public function __construct(WSRepository $repository, UserRepository $userRepository)
    {
        $this->repository = $repository;

        $this->userRepository = $userRepository;
    }

    protected function index(Request $request)
    {
        $user = $request->attributes->get('user');
        $status = $request->input('status');
        if ($status == null) {
            $status = Bill::STATUS_OPENED;
        }
        $isAttached = $request->input('isattached', false);
        $billId = $request->input('bill');

        return $this->repository->getBill($user, $status, $isAttached, $billId);
    }

    /**
     * POST /sessions.
     *
     * @return \Illuminate\Http\Response
     */
    public function openBill(StoreSessionsRequest $request)
    {
        $user = $request->attributes->get('user');

        return $this->repository->generateBill($user, $request->input('code'));
    }

    /**
     * Adiciona pedido (Order) a uma conta (Bill).
     */
    protected function add(Request $request)
    {
        $user = $request->attributes->get('user');
        $product = $request->input('product');
        $quant = $request->input('quant');
        $obs = $request->input('observation');
        $options = $request->input('options');

        return $this->repository->addOrderToBill($user, $product, $quant, $obs, $options);
    }

    protected function addOrders(Request $request)
    {
        $user = $request->attributes->get('user');
        $orders = $request->input('orders');

        return $this->repository->addOrdersToBill($user, $orders);
    }

    /**
     * Processa cancelamento de pedido (Order).
     *
     * @return \Illuminate\Http\Response
     */
    protected function cancel(Request $request)
    {
        $user = $request->attributes->get('user');
        $order = Order::find($request->input('order'));

        return $this->repository->cancelOrder($user, $order);
    }

    /**
     * Processa pedido de encerramento de conta.
     *
     * @return \Illuminate\Http\Response
     */
    protected function close(Request $request)
    {
        $user = $request->attributes->get('user');
        $cardFlag = $request->input('card_flag');
        $transactionCode = $request->input('transaction_code');
        $transactionType = $request->input('transaction_type');

        return $this->repository->requestCloseBill($user, $cardFlag, $transactionCode, $transactionType);
    }

    /**
     * Verifica se conta foi encerrada.
     *
     * @return \Illuminate\Http\Response
     */
    protected function hasClosed(Request $request)
    {
        $user = $request->attributes->get('user');

        return $this->repository->hasBillClosed($request->input('bill'), $user);
    }

    /**
     * Altera o status de acesso de um usuário anexo.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    protected function changeAttachedStatus(Request $request)
    {
        $attachedUser = $request->input('attached_user');
        $status = $request->input('status');
        if ($status == null) {
            $status = 'closed';
        }
        $bill = Bill::where(['id' => $request->input('bill'), 'status' => Bill::STATUS_OPENED])->first();

        return $this->repository->changeAttachedUserStatus($bill, $attachedUser, $status);
    }

    /**
     * Retorna o status de acesso de um usuário anexo.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    protected function getAttachedStatus(Request $request)
    {
        $user = $request->attributes->get('user');
        $bill = Bill::where(['id' => $request->input('bill')])->first();

        return $this->repository->getAttachedUserStatus($user, $bill);
    }

    protected function alias(Request $request)
    {
        $user = $request->attributes->get('user');

        return $this->repository->setTableAlias($user, $request->input('alias'));
    }

    /**
     * Adiciona requisição de anexação de Usuário a conta aberta.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    protected function requestAttachment(Request $request)
    {
        $bill = Bill::where(['id' => $request->input('bill_id'), 'status' => Bill::STATUS_OPENED])->first();
        $user = $request->attributes->get('user');

        return $this->repository->requestBillAttachment($bill, $user);
    }

    /**
     * Lista todas as requisições feitas para anexar usuários a conta.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    protected function listAttachmentRequests(Request $request)
    {
        $user = $request->attributes->get('user');

        return $this->repository->listBillAttachmentRequests($user);
    }

    /**
     * Processa o anexamento de um usuário a uma conta.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    protected function processAttach(Request $request)
    {
        $attachmentRequest = $request->input('attachment_request_id');
        $attached = $request->input('attached');

        return $this->repository->processAttach($attachmentRequest, $attached);
    }

    /**
     * Retorna lista de locais para definir no alias.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    protected function locations(Request $request, $establishment_id = null)
    {
        $user = $request->attributes->get('user');
        $establishment = null;
        if ($establishment_id) {
            $establishment = Establishment::find($establishment_id);
        } else {
            $bill = $user->getActiveBill('opened', false, false);
            if ($bill) {
                $table = $bill->spot;
                if ($table) {
                    $establishment = $table->establishment;
                }
            }
        }

        return $this->repository->listLocations($establishment);
    }

    /**
     * Process bill rating.
     */
    protected function rating(Request $request)
    {
        $bill = Bill::find($request->input('bill_id'));
        $user = $request->attributes->get('user');

        return $this->userRepository->ratingBill($bill, $user, $request->input('rating'),
            $request->input('answers'), $request->input('comment'));
    }
}
