<?php
/**
 * Created by PhpStorm.
 * User: ericoliveira
 * Date: 14/08/16
 * Time: 00:06
 */

namespace App\Http\Controllers\Webservice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DefaultController extends Controller
{

    /**
     * GET /
     *
     * @return \Illuminate\Http\Response
     */
    protected function index()
    {
        return response(['version' => '1.0']);
    }

    /**
     * GET /token
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    protected function show(Request $request)
    {
        return response($request->attributes->get('user'));
    }

}
