<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 14/08/16
* Time: 00:06.
*/

namespace App\Http\Controllers\Webservice;

use App\Http\Controllers\Controller;
use App\Models\Marketplace;
use App\Repositories\EstablishmentRepository;
use App\Repositories\Webservice\WSRepository;
use Illuminate\Http\Request;

class MarketplaceController extends Controller
{
    /**
     * Repository for WS API.
     *
     * @var WSRepository
     */
    protected $repository;

    protected $establishmentRepository;

    /**
     * Controller constructor.
     */
    public function __construct(WSRepository $repository, EstablishmentRepository $establishmentRepository)
    {
        $this->repository = $repository;
        $this->establishmentRepository = $establishmentRepository;
    }

    public function index()
    {
        return view('errors.wsauthok');
    }

    public function marketplace(Request $request, $slug)
    {
        $lat = $request->input('latitude');
        $lng = $request->input('longitude');
        $marketplace = Marketplace::where(['slug' => $slug, 'status' => Marketplace::STATUS_ENABLED])->first();
        if ($marketplace) {
            return $this->establishmentRepository->listEstablishments($lat, $lng, $marketplace);
        } else {
            return view('errors.custom');
        }
    }
}
