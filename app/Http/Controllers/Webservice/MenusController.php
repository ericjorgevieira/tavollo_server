<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 14/08/16
* Time: 00:06.
*/

namespace App\Http\Controllers\Webservice;

use App\Http\Controllers\Controller;
use App\Models\Establishment;
use App\Repositories\EstablishmentRepository;
use App\Repositories\MenuRepository;
use Illuminate\Http\Request;

class MenusController extends Controller
{
    /**
     * Repository for WS API.
     *
     * @var WSRepository
     */
    protected $repository;

    protected $establishmentRepository;

    /**
     * Controller constructor.
     */
    public function __construct(MenuRepository $repository, EstablishmentRepository $establishmentRepository)
    {
        $this->repository = $repository;

        $this->establishmentRepository = $establishmentRepository;

        $this->middleware(function ($request, $next) {
            $user = $request->attributes->get('user');
            $bill = ($user) ? $user->getActiveBill('opened', false, false) : null;
            if ($bill && $user) {
                $this->repository->setEstablishment($bill->spot->establishment);
                $this->establishmentRepository->setEstablishment($bill->spot->establishment);
            }

            return $next($request);
        });
    }

    protected function index(Request $request)
    {
        $establishment_id = $request->input('establishment_id');

        return $this->repository->getMenus($establishment_id);
    }

    protected function categories(Request $request)
    {
        $establishment_id = $request->input('establishment_id');
        $menu_id = $request->input('menu');

        return $this->repository->getCategories($establishment_id, $menu_id);
    }

    protected function product(Request $request)
    {
        $establishment_id = $request->input('establishment_id');
        $product_id = $request->input('product');

        return $this->establishmentRepository->getProduct($product_id, $establishment_id);
    }

    /** PRE ORDER **/
    protected function listMenuPreOrder(Request $request, $slug = null)
    {
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $establishment_id = $request->input('establishment_id');
        if ($slug) {
            if ($this->enabledToPreOrder(null, $slug)) {
                return $this->repository->getMenus(null, $slug, $latitude, $longitude);
            }
        }
        if ($this->enabledToPreOrder($establishment_id)) {
            return $this->repository->getMenus($establishment_id, null, $latitude, $longitude);
        }
    }

    protected function listCategoriesPreOrder(Request $request)
    {
        $establishment_id = $request->input('establishment_id');
        $menu_id = $request->input('menu');
        if ($this->enabledToPreOrder($establishment_id)) {
            return $this->repository->getCategories($establishment_id, $menu_id);
        } else {
            return false;
        }
    }

    protected function getProductPreOrder(Request $request)
    {
        $establishment_id = $request->input('establishment_id');
        $product_id = $request->input('product');
        if ($this->enabledToPreOrder($establishment_id)) {
            return $this->establishmentRepository->getProduct($product_id, $establishment_id);
        } else {
            return false;
        }
    }

    /**
     * Confere se estabelecimento possui pre order ativado.
     *
     * @param [type] $establishment_id [description]
     *
     * @return [type] [description]
     */
    private function enabledToPreOrder($establishment_id = null, $slug = null)
    {
        $establishment = null;
        if ($slug) {
            $establishment = Establishment::where(['slug' => $slug])->first();
        } else {
            $establishment = Establishment::find($establishment_id);
        }
        if ($establishment) {
            $config = $establishment->config;
            if ($config && ($config->allow_pre_bills || $config->disable_orders)) {
                return true;
            }
        }

        return false;
    }

    protected function searchProducts(Request $request)
    {
        $establishment = Establishment::find($request->input('establishment_id'));

        return $establishment->searchProducts($request->input('query_string'));
    }
}
