<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 14/08/16
* Time: 00:06.
*/

namespace App\Http\Controllers\Webservice;

use App\Http\Controllers\Controller;
use App\Models\PreOrder;
use App\Repositories\Webservice\WSPreOrderRepository;
use Illuminate\Http\Request;

class PreOrderController extends Controller
{
    /**
     * Repository for WS API.
     *
     * @var WSPreOrderRepository
     */
    protected $repository;

    /**
     * Controller constructor.
     */
    public function __construct(WSPreOrderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Resgata os dados do PreOrder.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    protected function getActive(Request $request)
    {
        $user = $request->attributes->get('user');

        return $this->repository->getActive($user);
    }

    /**
     * Resgata todos os PreOrders.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    protected function list(Request $request)
    {
        $user = $request->attributes->get('user');

        return $this->repository->listActive($user);
    }

    /**
     * Adiciona pedido (PreOrder).
     */
    protected function add(Request $request)
    {
        $user = $request->attributes->get('user');
        $products = $request->input('products');
        $establishment_id = $request->input('establishment_id');
        $cardFlag = $request->input('card_flag');
        $transactionCode = $request->input('transaction_code');
        $merchantOrderId = $request->input('merchant_order_id');
        $transactionType = $request->input('transaction_type');
        $alias = $request->input('alias');
        $payment_id = $request->input('cielo_payment_id');

        return $this->repository->addPreOrder(
            $user,
            $establishment_id,
            $products,
            $cardFlag,
            $transactionCode,
            $merchantOrderId,
            $transactionType,
            $alias,
            $payment_id
        );
    }

    /**
     * Confirma a entrega de um pedido (PreOrder).
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    protected function confirm(Request $request)
    {
        $user = $request->attributes->get('user');
        $preorder_id = $request->input('preorder_id');

        return $this->repository->confirmPreOrder($preorder_id, $user);
    }

    /**
     * Cancela a entrega de um pedido (PreOrder).
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    protected function cancel(Request $request)
    {
        $user = $request->attributes->get('user');
        $preorder_id = $request->input('preorder_id');

        return $this->repository->cancelPreOrder($preorder_id, $user);
    }

    /**
     * Verifica se Pedido foi encerradp.
     *
     * @return \Illuminate\Http\Response
     */
    protected function hasClosed(Request $request, PreOrder $order)
    {
        $user = $request->attributes->get('user');

        return $this->repository->hasPreOrderClosed($order, $user);
    }
}
