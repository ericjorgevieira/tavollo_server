<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 14/08/16
* Time: 00:06.
*/

namespace App\Http\Controllers\Webservice;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Repository for WS API.
     *
     * @var WSRepository
     */
    protected $repository;

    /**
     * Controller constructor.
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return view('errors.wsauthok');
    }

    public function updateProfile(Request $request)
    {
        $user = $request->attributes->get('user');
        $name = $request->input('name');
        $email = $request->input('email');
        $cpf = $request->input('cpf');

        return $this->repository->updateProfileWebapp($user, $name, $email, $cpf);
    }

    public function rating(Request $request)
    {
        $user = $request->attributes->get('user');

        return $this->repository->rating($user, $request->input('rating'), $request->input('comment'));
    }
}
