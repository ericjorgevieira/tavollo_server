<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\Cors::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            // \App\Http\Middleware\EncryptCookies::class,
            // \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            // \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            // \App\Http\Middleware\VerifyCsrfToken::class,
        ],

        'webservice' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\WebserviceIsApp::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
            \App\Http\Middleware\APICors::class,
            \App\Http\Middleware\APIUserAuthentication::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'cors' => \App\Http\Middleware\Cors::class,
        'auth' => \App\Http\Middleware\Authenticate::class,
        'master.auth' => \App\Http\Middleware\MasterAuthenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'master.guest' => \App\Http\Middleware\RedirectIfMasterAuthenticated::class,
        'throttle' => \App\Http\Middleware\ThrottleRequests::class,
        'is.establishment' => \App\Http\Middleware\IsEstablishment::class,
        'is.admin' => \App\Http\Middleware\IsAdmin::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'webservice.user' => \App\Http\Middleware\WebserviceUserAuthentication::class,
        'webservice.basic' => \App\Http\Middleware\WebserviceBasicAuthentication::class,
        'api.user' => \App\Http\Middleware\APIUserAuthentication::class,
        'api.basic' => \App\Http\Middleware\APIBasicAuthentication::class,
        'api.cors' => \App\Http\Middleware\APICors::class,
    ];
}
