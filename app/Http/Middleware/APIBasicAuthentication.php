<?php

namespace App\Http\Middleware;

use Closure;

class APIBasicAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param string|null              $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->getUser() == 'tavollo') {
            return $next($request);
        }

        return abort(401);
    }
}
