<?php

namespace App\Http\Middleware;

use App\Models\ApiToken;
use App\Models\Establishment;
use App\Models\User;
use App\Models\UserToken;
use Closure;
use Illuminate\Http\Request;

class APIUserAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $sequence = $request->get('_token');
        /** @var ApiToken $token */
        $token = ApiToken::query()->where('sequence', $sequence)->first();

        if (!$token) {
            /** @var UserToken $token */
            $token = UserToken::query()->where('sequence', $sequence)->first();
            if (!$token || ($token->user->profile != User::PROFILE_WAITER && $token->user->profile != User::PROFILE_MANAGER) || $token->hasExpired()) {
                return abort(401);
            }
        } elseif ($token->hasExpired()) {
            return abort(401);
        }

        $user = $token->user;
        $establishment = $user->establishment;
        if ($establishment && $establishment->status == Establishment::STATUS_DISABLED) {
            return abort(401);
        }
        $request->attributes->add(['user' => $user, 'token' => $token, 'establishment' => $establishment]);

        return $next($request);
    }
}
