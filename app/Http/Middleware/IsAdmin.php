<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::guard('master')->user();
        if ($user->is_admin == 0) {
            Auth::guard('master')->logout();
            return redirect('/')->with('warning', 'Você não possui permissão para acessar.');
        }
        if ($user->partnership() && $user->partnership->status == 'disabled') {
            Auth::guard('master')->logout();
            return redirect('/')->with('warning', 'Sua conta foi desabilitada. Contate o suporte.');
        }

        return $next($request);
    }
}
