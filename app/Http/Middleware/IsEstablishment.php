<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use App\Models\Establishment;
use Illuminate\Support\Facades\Auth;

class IsEstablishment
{
  /**
  * Handle an incoming request.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \Closure  $next
  * @param  string|null  $guard
  * @return mixed
  */
  public function handle($request, Closure $next)
  {
    if (Auth::user()->status == User::STATUS_DISABLED) {
      Auth::logout();
      return redirect('/')->with('warning', 'Você não possui permissão para acessar.');
    }
    if (Auth::user()->is_establishment == 0) {
      Auth::logout();
      return redirect('/')->with('warning', 'Você não possui permissão para acessar.');
    }

    if (Auth::user()->profile == User::PROFILE_CUSTOMER) {
      Auth::logout();
      return redirect('/')->with('warning', 'Você não possui permissão para acessar.');
    }

    if (Auth::user()->profile == User::PROFILE_WAITER) {
      Auth::logout();
      return redirect('/')->with('warning', 'Você não possui permissão para acessar.');
    }

    if (Auth::user()->establishment->status == Establishment::STATUS_DISABLED) {
      Auth::logout();
      return redirect('/')->with('warning', 'O estabelecimento está desabilitado. Entre em contato com o suporte.');
    }

    return $next($request);
  }
}
