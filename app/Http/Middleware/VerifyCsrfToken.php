<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Illuminate\Support\Str;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'http://ws.tavollo.localhost:90/*',
        'https://ws.tavollo.com/*',
        'http://api.tavollo.localhost:90/*',
        'https://api.tavollo.com/*',
        'http://api.tavollo.com/*',
        'https://test.ws.tavollo.com/*',
        'https://test.api.tavollo.com/*',
        'https://test.api.tavollo.com/*',
        'https://test.ws.tavollo.com/*',
    ];

    /* Add the CSRF token to the response cookies.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Symfony\Component\HttpFoundation\Response  $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    // protected function addCookieToResponse($request, $response)
    // {
    //     $config = config('session');
    //
    //     $response->headers->setCookie(
    //         new Cookie(
    //             'XSRF-TOKEN',
    //             $request->session()->token(),
    //             $this->availableAt(60 * $config['lifetime']),
    //             $config['path'],
    //             $config['domain'],
    //             $config['secure'],
    //             false,
    //             false,
    //             $config['same_site'] ?? null
    //         )
    //     );
    //
    //     return $response;
    // }

    /**
     * Determine if the request has a URI that should pass through CSRF verification.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    protected function inExceptArray($request)
    {
        foreach ($this->except as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            if (Str::is($except, $request->url())) {
                return true;
            }

            if ($request->is($except)) {
                return true;
            }
        }

        return false;
    }
}
