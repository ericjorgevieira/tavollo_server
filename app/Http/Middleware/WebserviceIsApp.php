<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class WebserviceIsApp
{
    const AUTHORIZATION_TOKEN = 'Bearer 771h0jij0d1h9dh9127d7u19hd1n98g9gdh0182eh0128g98ghd1';

    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $authorization = $request->header('Authorization');

        if (!$authorization || $authorization != self::AUTHORIZATION_TOKEN) {
            return abort(401);
        }

        return $next($request);
    }
}
