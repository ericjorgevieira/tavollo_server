<?php

namespace App\Http\Middleware;

use App\Models\UserToken;
use Closure;
use Illuminate\Http\Request;

class WebserviceUserAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var UserToken $token */
        $sequence = $request->get('_token');
        $token = UserToken::query()->where('sequence', $sequence)->first();
        if (! $token || $token->hasExpired()) {
            return abort(401);
        }

        $user = $token->user;

        $request->attributes->add(['user' => $user, 'token' => $token]);

        return $next($request);
    }
}
