<?php
/**
 * Created by PhpStorm.
 * User: ericoliveira
 * Date: 14/08/16
 * Time: 00:59
 */

namespace app\Http\Requests\Webservice;


use App\Http\Requests\Request;

class StoreSessionsRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'user' => 'required'
        ];
    }
}
