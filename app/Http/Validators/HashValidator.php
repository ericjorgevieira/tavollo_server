<?php
/**
 * Created by PhpStorm.
 * User: ericoliveira
 * Date: 19/09/16
 * Time: 18:12
 */

namespace App\Http\Validators;

use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;

class HashValidator extends Validator
{
    public function validateHash($attribute, $value, $parameters)
    {
        return Hash::check($value, $parameters[0]);
    }
}