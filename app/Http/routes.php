<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Get router object
$app = $this->app->router;


// Register routes' files
$routes = [
  'webservice',
  'api',
  'master',
  'web',
];

// Load routes' files
foreach ($routes as $route) {
  include base_path('routes/' . $route . '.php');
}
