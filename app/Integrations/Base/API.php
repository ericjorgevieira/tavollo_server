<?php

namespace App\Models\Integrations\Base;

abstract class API{

  protected $api_url;

  protected $api_token;

  protected $establishment;

  protected $credential;

  abstract protected function checkConectivity();

  abstract protected function getSpots();

  abstract protected function getProducts();

  abstract protected function getProductCategories();

  abstract protected function getBills();

  abstract protected function openBill();

  abstract protected function closeBill();

  abstract protected function addOrder();

  abstract protected function deleteOrder();


}
