<?php

namespace App\Models\Integrations;

use App\Models\Integrations\Base\API;
use App\Models\Product;
use App\Models\Order;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use App\Miscelaneous\Utils;
use Carbon\Carbon;


class Bematech extends API
{

  const STATUS_SUCCESS = 200;
  const STATUS_SERVER_ERROR = 500;
  const STATUS_NOT_FOUND = 404;
  const STATUS_FORBIDDEN = 401;

  const INTEGRATION_CODE = 47;
  const INTEGRATION_KEY = 'OzptUTUP';

  const URL_SUFFIX = '/IntegracaoPedidosOnline';

  protected $client;

  public function __construct($establishment, $establishmentCode, $apiUrl, $token)
  {
    $this->establishment = $establishment;
    $this->api_url = $apiUrl;
    $this->api_token = $token;
    $this->credential = $establishmentCode;
    $this->client = new Client(['base_uri' => $this->api_url, 'headers' => $this->getHeaders()]);
  }

  private function getHeaders()
  {
    $data = Carbon::now('America/Sao_Paulo');
    $dataToken = $data->format('YmdHisO');
    $strToken = md5(self::INTEGRATION_CODE . '|' . self::INTEGRATION_KEY . '|' . $dataToken);
    $basic = 'Basic ' . base64_encode($dataToken . ':' . $strToken);
    return array(
      'Authorization' => $basic
    );
  }

  private function prepareRequest($array)
  {
      return array('json' => array('parametros' => $array));
  }

  public function checkConectivity($url = null, $type = 'GET')
  {
    $code = null;
    try{
      if($url){
        $code = $this->client->request($type, $url)->getStatusCode();
      }else{
        $response = $this->client->request(
          $type,
          self::URL_SUFFIX . '/LojaService.svc/StatusEstabelecimento',
          $this->prepareRequest(array(
            'CodigoEstabelecimento' => strval($this->credential),
            'CodigoIntegracao' => self::INTEGRATION_CODE
          ))
        );
        if($response->getStatusCode() == self::STATUS_SUCCESS){
          $body = json_decode($response->getBody()->getContents());
          if($body->StatusEstabelecimentoResult->Online == true){
            return true;
          }
        }
      }
    }catch(RequestException $e){
      $code = self::STATUS_SERVER_ERROR;
    }
    return false;
  }

  protected function getProducts()
  {
    // if($this->checkConectivity()){
      $res = $this->client->post(
        self::URL_SUFFIX . '/CadastroService.svc/ObterCardapio',
        $this->prepareRequest(array(
            'CodigoEstabelecimento' => $this->credential,
            'CodigoIntegracao' => self::INTEGRATION_CODE
          ))
      );
      $content = json_decode($res->getBody()->getContents());
      var_dump($content->ObterCardapioResult->Produtos);exit;
      return array_shift($content->ObterCardapioResult->Produtos);
    // }
    return false;
  }

  public function importProducts()
  {
    $products = $this->getProducts();
    if($products != false){
      $arrProducts = array();
      foreach($products as $p){
        $objProduct = Product::where(['code' => $p->id, 'establishment_id' => $this->establishment->id])->first();
        if(!$objProduct){
          $objProduct = new Product();
          $objProduct->code = $p->id;
        }
        $objProduct->name = Utils::normalize($p->nomereduzido);
        $objProduct->establishment_id = $this->establishment->id;
        $objProduct->price = $p->valor;
        $objProduct->status = Product::STATUS_DISABLED;
        $objProduct->save();
        $arrProducts[] = $objProduct;
      }
      return $arrProducts;
    }
    return false;
  }

  public function getProductCategories()
  {

  }

  public function getBills()
  {

  }

  public function openBill()
  {

  }

  public function closeBill()
  {

  }

  public function getSpots(){

  }

  public function addOrder()
  {

  }

  private function sendOrderToConsumptionCard()
  {

  }

  private function sendOrderToTable()
  {

  }

  public function deleteOrder()
  {

  }

  private function isProductAvailable()
  {

  }

}
