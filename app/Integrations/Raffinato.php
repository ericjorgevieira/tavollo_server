<?php

namespace App\Models\Integrations;

use App\Models\Integrations\Base\API;
use App\Models\Product;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use App\Miscelaneous\Utils;

class Raffinato extends API
{
    const STATUS_SUCCESS = 200;
    const STATUS_SERVER_ERROR = 500;
    const STATUS_NOT_FOUND = 404;
    const STATUS_FORBIDDEN = 401;

    protected $client;

    public function __construct($establishment, $apiUrl, $token)
    {
        $this->establishment = $establishment;
        $this->api_url = $apiUrl;
        $this->api_token = $token;
        $this->client = new Client(['base_uri' => $this->api_url]);
    }

    public function checkConectivity($url = null, $type = 'GET')
    {
        $code = null;
        try {
            if ($url) {
                $code = $this->client->request($type, $url)->getStatusCode();
            } else {
                $code = $this->client->request($type, '/')->getStatusCode();
            }
        } catch (RequestException $e) {
            $code = self::STATUS_SERVER_ERROR;
        }
        if ($code == self::STATUS_SUCCESS) {
            return true;
        }
        return false;
    }

    public function getSpots()
    {
        if ($this->checkConectivity()) {
            $res = $this->client->get('api/integracao/mesa');
            $res2 = $this->client->get('api/integracao/cartaoconsumo');
        }
    }

    public function importSpots()
    {
    }

    public function getProducts()
    {
        if ($this->checkConectivity()) {
            $res = $this->client->get('api/integracao/produto');
            $content = json_decode($res->getBody()->getContents());
            return array_shift($content->result);
        }
        return false;
    }

    public function importProducts()
    {
        $products = $this->getProducts();
        if ($products != false) {
            $arrProducts = array();
            foreach ($products as $p) {
                $objProduct = Product::where(['code' => $p->id, 'establishment_id' => $this->establishment->id])->first();
                if (!$objProduct) {
                    $objProduct = new Product();
                    $objProduct->code = $p->id;
                    $objProduct->name = Utils::normalize($p->nomereduzido);
                    $objProduct->establishment_id = $this->establishment->id;
                    $objProduct->price = $p->valor;
                }
                $objProduct->status = Product::STATUS_DISABLED;
                $objProduct->save();
                $arrProducts[] = $objProduct;
            }
            return $arrProducts;
        }
        return false;
    }

    public function getProductCategories()
    {
    }

    public function getBills()
    {
    }

    public function openBill()
    {
    }

    public function closeBill()
    {
    }

    public function addOrder()
    {
    }

    public function deleteOrder()
    {
    }
}
