<?php

namespace App\Listeners;

use App\Events\BillAttachmentRequesting;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class BillAttachmentRequestingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SomeEvent  $event
     * @return void
     */
    public function handle(BillAttachmentRequesting $event)
    {
        $establishment = $event->request->establishment;
    }
}
