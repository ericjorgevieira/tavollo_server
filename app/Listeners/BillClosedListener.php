<?php

namespace App\Listeners;

use App\Events\BillClosed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class BillClosedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BillClosed  $event
     * @return void
     */
    public function handle(BillClosed $event)
    {
        $bill = $event->bill;
    }
}
