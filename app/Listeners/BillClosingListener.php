<?php

namespace App\Listeners;

use App\Events\BillClosing;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class BillClosingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BillClosing  $event
     * @return void
     */
    public function handle(BillClosing $event)
    {
        $bill = $event->bill;
    }
}
