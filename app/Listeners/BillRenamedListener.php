<?php

namespace App\Listeners;

use App\Events\BillCreated;
use App\Events\BillRenamed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class BillRenamedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BillRenamed  $event
     * @return void
     */
    public function handle(BillRenamed $event)
    {
        $bill = $event->bill;
    }
}
