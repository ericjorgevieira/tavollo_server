<?php

namespace App\Listeners;

use App\Events\HasBillChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class HasBillChangedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  HasBillChanged  $event
     * @return void
     */
    public function handle(HasBillChanged $event)
    {
        $bill = $event->bill;
    }
}
