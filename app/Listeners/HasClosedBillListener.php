<?php

namespace App\Listeners;

use App\Events\HasClosedBill;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class HasClosedBillListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  HasClosedBill  $event
     * @return void
     */
    public function handle(HasClosedBill $event)
    {
        $bill = $event->bill;
    }
}
