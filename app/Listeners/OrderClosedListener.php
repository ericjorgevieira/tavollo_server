<?php

namespace App\Listeners;

use App\Events\OrderClosed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderClosedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderClosed  $event
     * @return void
     */
    public function handle(OrderClosed $event)
    {
        $order = $event->order;
    }
}
