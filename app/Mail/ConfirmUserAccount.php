<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class ConfirmUserAccount extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $token = bcrypt(Carbon::now());
        $token = str_replace("/", "", $token);
        $this->user->remember_token = $token;
        $this->user->save();

        $link = config('app.url') . '/api/activate';

        return $this->view('mails.confirmUserAccount')
          ->subject('Confirme seu Cadastro')
          ->with([
            'link' => $link,
            'user' => $this->user,
            'token' => $this->user->remember_token,
            'email' => $this->user->email_address
          ]);
    }
}
