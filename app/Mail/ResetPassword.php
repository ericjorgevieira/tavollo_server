<?php

namespace App\Mail;

use App\Models\User;
use App\Models\PasswordReset;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $token = bcrypt(Carbon::now());
        $token = str_replace("/", "", $token);
        $passwordToken = PasswordReset::where(['email' => $this->user->email])->first();
        $passwordToken = ($passwordToken) ? $passwordToken : new PasswordReset();
        $passwordToken->email = $this->user->email;
        $passwordToken->token = $token;
        $passwordToken->created_at = Carbon::now();
        $passwordToken->save();

        $link = \Config::get('app.app_url') . '/#/redefine-password/' . $this->user->email . '/' . $token;

        return $this->view('mails.resetPassword')
          ->subject('Redefinição de Senha')
          ->with([
            'link' => $link,
            'user' => $this->user,
            'token' => $passwordToken->token,
            'email' => $passwordToken->email
          ]);
    }
}
