<?php
/**
 * Created by PhpStorm.
 * User: ericoliveira
 * Date: 17/07/16
 * Time: 18:34.
 */

namespace App\Miscelaneous;

use App\Models\Table;

class TableCode
{
    /**
     * @param string $prefix
     * @param bool   $entropy
     */
    public function __construct($prefix = '', $entropy = false)
    {
        $this->uuid = uniqid($prefix, $entropy);
    }

    /**
     * Limit the UUID by a number of characters.
     *
     * @param $length
     * @param int $start
     *
     * @return $this
     */
    public function limit($length, $start = 0)
    {
        $this->uuid = substr($this->uuid, $start, $length);

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->uuid;
    }

    /**
     * Gera um código único para identificar Table.
     *
     * @param $length
     *
     * @return string
     */
    public static function getCode($length)
    {
        $code = substr(uniqid('', false), 9, $length);
        $count = Table::where('code', $code)->count();
        if ($count > 0) {
            while ($count != 0) {
                $code = substr(uniqid('', false), 9, $length);
                $count = Table::where('code', $code)->withTrashed()->count();
            }
        }

        return $code;
    }
}
