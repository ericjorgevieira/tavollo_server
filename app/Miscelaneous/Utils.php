<?php

namespace App\Miscelaneous;

class Utils
{
    public static function normalize($value, $formatText = true)
    {
        
        if (false === mb_check_encoding($value, 'UTF-8')) {
            return $formatText === true ? ucfirst(strtolower(utf8_encode($value))) : utf8_encode($value);
        } else {
            return $formatText === true ? ucfirst(strtolower($value)) : $value;
        }
    }
}
