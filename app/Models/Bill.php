<?php

namespace App\Models;

use App\Events\BillClosed;
use App\Events\BillCreated;
use App\Events\HasBillChanged;
use App\Events\HasClosedBill;
use App\Events\OrderCreated;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Bill extends Model
{
    const STATUS_OPENED = 'opened';
    const STATUS_CLOSING = 'closing';
    const STATUS_CLOSED = 'closed';
    const STATUS_DISABLED = 'disabled';
    const STATUS_TRASH = 'trash';

    public static $statusList = [
    self::STATUS_OPENED => 'Aberta',
    self::STATUS_CLOSING => 'Encerrando',
    self::STATUS_CLOSED => 'Encerrada',
    self::STATUS_DISABLED => 'Bloqueada',
    self::STATUS_TRASH => 'Lixo',
  ];

    protected $dates = [
    'created_at',
    'opened_at',
    'closed_at',
    'updated_at',
  ];

    public function table()
    {
        return $this->belongsTo(Table::class);
    }

    public function spot()
    {
        return $this->belongsTo(Table::class, 'table_id');
    }

    public function establishment()
    {
        return $this->table()->getRelated('establishment');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function attachedUsers()
    {
        return $this->belongsToMany(User::class, 'bill_attached_users');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function island()
    {
        $table = $this->spot;

        return $table->getIslandToday();
    }

    public function rating()
    {
        return $this->hasOne(BillRating::class);
    }

    public function waiter()
    {
        $island = $this->island();
        if ($island) {
            //Retorna o garçom da ilha
            return $island->waiter;
        } else {
            //Pega o garçom padrão do estabelecimento
            $establishment_id = $this->spot->establishment_id;
            $waiter = Waiter::where(['establishment_id' => $establishment_id, 'is_default' => true, 'status' => Waiter::STATUS_ENABLED])->first();
            if ($waiter) {
                return $waiter;
            }
        }

        return false;
    }

    /**
     * Retorna o valor total da conta (Bill).
     *
     * @return float
     */
    public static function getTotalValue(Bill $bill, $format = false)
    {
        $total = 0.0;
        $totalBaksheesh = 0.0;

        $table = $bill->spot;
        $establishment = ($table && $table instanceof Table) ? $table->establishment : null;
        $config = ($establishment) ? $establishment->config : null;
        $hasBaksheesh = ($config && $config->has_baksheesh) ? true : false;
        $baksheeshPercentage = ($config) ? $config->baksheesh_percentage : 0;

        $orders = $bill->orders()->where('status', '!=', Order::STATUS_TRASH)
          ->where('status', '!=', Order::STATUS_DENY)->get(['id', 'product_id', 'cache_price']);
        foreach ($orders as $order) {
            // $total += ($order->updateCachePrice());
            if ($order->product) {
                $total += $order->cache_price;
                if ($hasBaksheesh && $order->cache_price > 0 && $order->product->calculate_baksheesh && $baksheeshPercentage) {
                    $totalBaksheesh += ($baksheeshPercentage / 100) * $order->cache_price;
                }
            }
        }

        $total = $total * 100 / 100;
        $bill->cache_total = $total;
        $bill->baksheesh_value = $totalBaksheesh;
        $bill->save();

        if ($format) {
            return number_format($total, 2, ',', '.');
        }

        return $total;
    }

    public function getCreatedAtAttribute($value)
    {
        if ($value != '') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);

            return $date->format('d/m/Y H:i:s');
        }

        return $value;
    }

    public function getUpdatedAtAttribute($value)
    {
        if ($value != '') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);

            return $date->format('d/m/Y H:i:s');
        }

        return $value;
    }

    public function getOpenedAtAttribute($value)
    {
        if ($value != '') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);

            return $date->format('d/m/Y H:i:s');
        }

        return $value;
    }

    public function getClosedAtAttribute($value)
    {
        if ($value != '' && $value != '0000-00-00 00:00:00' && $value != '-0001-11-30 00:00:00') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);

            return $date->format('d/m/Y H:i:s');
        }

        return $value;
    }

    public function duration()
    {
        $createdAt = Carbon::createFromFormat('d/m/Y H:i:s', $this->created_at);

        if ($this->status == self::STATUS_CLOSED && $this->closed_at != '0000-00-00 00:00:00') {
            $closedAt = Carbon::createFromFormat('d/m/Y H:i:s', $this->closed_at);
        } else {
            $closedAt = Carbon::now();
        }
        $diff = $createdAt->diff($closedAt);

        return $diff->h.'h '.$diff->i.'m';
    }

    /**
     * Processa adição de produtos automáticos na comanda.
     *
     * @return void
     */
    protected function addAutomaticOrders()
    {
        $automaticProducts = $this->spot->establishment->products()->where(['enable_automatic_order' => true])->get();
        foreach ($automaticProducts as $product) {
            $this->addOrder($product);
        }
    }

    /**
     * Abre uma nova conta com os dados do Spot e do Usuário.
     *
     * @param Table  $table    [description]
     * @param [type] $user     [description]
     * @param [type] $raw_user [description]
     *
     * @return [type] [description]
     */
    public static function openBill(Table $table, User $user = null, $raw_user = null)
    {
        //Verifica se Spot já possui conta temporária aberta
        $bill = $table->getActiveBill();
        if ($bill && $user && $bill->temp_user == 1) {
            $bill->temp_user = 0;
            $bill->raw_user = '';
            $bill->establishment_id = $table->establishent_id;
        } else {
            //Cria nova Bill
            $bill = new Bill();
            $bill->opened_at = Carbon::now();
            $bill->closed_at = Carbon::now();
            $bill->status = self::STATUS_OPENED;
            $bill->establishment_id = $table->establishent_id;
            $bill->table_id = $table->id;
        }
        $bill->user_id = ($user) ? $user->id : 0;

        if (!$user) {
            $bill->temp_user = true;
            $bill->raw_user = serialize($raw_user);
        }
        $bill->save();
        $bill->addAutomaticOrders();
        broadcast(new BillCreated($bill));
        Log::info('Abrindo nova conta de id: '.$bill->id);

        return $bill;
    }

    /**
     * Encerra uma conta.
     *
     * @return [type] [description]
     */
    public function close()
    {
        if ($this->status != self::STATUS_CLOSED) {
            Order::where(['bill_id' => $this->id])->whereIn('status', [Order::STATUS_PREPARATION, Order::STATUS_OPENED])->update(['status' => Order::STATUS_FINISHED]);
            BillAttachedUser::where(['bill_id' => $this->id])->update(['status' => Bill::STATUS_CLOSED]);
            $this->status = self::STATUS_CLOSED;
            $this->closed_at = Carbon::now();
            $table = $this->spot;
            if ($table) {
                $table->alias = null;
                $table->save();
            }
            //Apaga todos os pedidos cancelados da conta
            DB::table('orders')->where(['status' => Order::STATUS_DENY, 'bill_id' => $this->id])->delete();
            Log::info('Conta encerrada de id: '.$this->id);

            $this->save();
            broadcast(new HasClosedBill($this));
            broadcast(new BillClosed($this));
        } else {
            Log::info('Conta já havia sido encerrada antes. Id: '.$this->id);
        }
    }

    /**
     * Verifica se existe conta (Bill) ativa.
     *
     * @param string $status
     *
     * @return bool
     */
    public static function checkBill(User $user, $status = self::STATUS_OPENED)
    {
        $bill = Bill::where(['user_id' => $user->id, 'status' => $status])->first();
        if ($bill) {
            return $bill;
        }

        return false;
    }

    /**
     * Migra pedidos de uma conta para outra.
     *
     * @return [type] [description]
     */
    public static function migrate(Bill $origin, Bill $destiny, $orders = [])
    {
        //Pega todos os pedidos da conta original e muda o id pra conta nova
        if ($orders && count($orders) > 0) {
            Order::where(['bill_id' => $origin->id])->whereIn('id', $orders)->update(['bill_id' => $destiny->id]);
        } else {
            Order::where(['bill_id' => $origin->id])->update(['bill_id' => $destiny->id]);
        }

        Log::info('Migrando contas de id:'.$origin->id.' para id:'.$destiny->id);

        //Encerra a conta original e desativa a mesa
        $originTable = $origin->table_id;
        // $origin->table_id = 0;
        $origin->close();
        $origin->save();

        broadcast(new BillClosed($origin));
        broadcast(new BillCreated($destiny));
        broadcast(new HasBillChanged($destiny));

        return $destiny;
    }

    /**
     * Adiciona pedido à Conta.
     *
     * @param Product $product [description]
     * @param [type]  $quant   [description]
     * @param [type]  $obs     [description]
     * @param [type]  $options [description]
     */
    public function addOrder(Product $product, $quant = 1, $obs = '', $weight = null, $options = null, $status = null,
        Waiter $waiter = null, $by_admin = true, $by_waiter = false, User $user = null, $is_defined_price = false,
        $defined_price = 0.0, $external_id = null) 
        {
        $product->updateIfPromoPrice();
        $order = new Order();
        $order->product_id = $product->id;
        $order->bill_id = $this->id;
        $order->quant = $quant;
        $order->external_id = $external_id;
        $order->is_promo = ($product->is_promo_price == 1) ? true : false;
        if ($is_defined_price) {
            $order->is_defined_price = true;
            $order->defined_price = $defined_price;
            $order->cache_price = $defined_price;
            // Log::info('Definindo valor do produto '.$product->id.' em um pedido para a conta '.$this->id.' manualmente. Valor novo: '.$defined_price);
        } else {
            $order->is_defined_price = false;
            $order->cache_price = $product->price;
        }
        $order->observation = ($obs) ? $obs : '';
        $order->quant = ($quant) ? $quant : 1;
        if (!empty($weight) && $weight > 0) {
            $order->is_weight_price = 1;
            $order->order_weight = $weight;
            $order->quant = 1;
        } else {
            $order->is_weight_price = 0;
        }
        //Caso não defina Garçom, pega o garçøm padrão da conta
        if (!$waiter) {
            $waiter = $this->waiter();
        }
        $order->waiter_id = ($waiter) ? $waiter->id : null;
        $order->by_admin = $by_admin;
        $order->by_waiter = $by_waiter;
        $order->user_id = ($user) ? $user->id : null;
        $order->save();

        $order->setStatus($status);
        if ($options) {
            $order->addOptions($options);
        }
        $order->updateCachePrice();

        Log::info('Pedido '.$order->id.' adicionado na conta '.$this->id.' com sucesso');

        // Dispara o evento para atualizar lista de pedidos
        broadcast(new OrderCreated($order));
        broadcast(new HasBillChanged($this));

        return $order;
    }

    /**
     * Verifica se conta possui mais de uma hora aberta sem pedidos e encerra.
     *
     * @return bool [description]
     */
    public function hasExpired()
    {
        $openedAt = Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['opened_at']);
        $now = Carbon::now();
        if ($openedAt->diffInHours($now) > 1 && count($this->orders) == 0) {
            $this->close();
        }
    }

    public function toPublicArray()
    {
        // $this->hasExpired();

        $table = $this->spot;
        $establishment = $table->establishment()->get()->first();
        $establishment->theme = $establishment->getThemeConfig();
        $config = $establishment->config()->get()->first();
        $total = self::getTotalValue($this);

        //Verifica se estabelecimento possui cobrança de gorjeta e soma com o valor total
        $baksheesh = ($config &&
          $config->has_baksheesh == 1) ? $this->baksheesh_value : 0;

        $couvert = ($config && $config->has_couvert == 1) ? $config->couvert_price : 0;

        $waiter = $this->waiter();

        $duration = $this->duration();

        $orders = $this->orders()->with(['product', 'items.item', 'items.group', 'waiter', 'user'])->withTrashed()
          ->where('status', '!=', Order::STATUS_DENY)->orderBy('created_at', 'DESC')->get();

        $denyOrders = $this->orders()->with(['product', 'items.item', 'items.group', 'waiter', 'user'])->withTrashed()
          ->where('status', '=', Order::STATUS_DENY)->orderBy('created_at', 'DESC')->get();

        $orders = Order::getOrdersWithGroups($orders);

        $hasPaid = ($this->card_flag && $this->transaction_code && $this->transaction_type && 
            ($this->status == self::STATUS_CLOSING || $this->status == self::STATUS_CLOSED)) ? true : false;

        unset($establishment->created_at);
        unset($establishment->updated_at);
        unset($establishment->deleted_at);
        unset($establishment->postal_code);
        unset($establishment->status);
        unset($establishment->plan);
        unset($establishment->remember_token);
        unset($establishment->theme_config);
        unset($establishment->slug);
        unset($establishment->street);
        unset($establishment->address);
        unset($establishment->city);
        unset($establishment->district);
        unset($establishment->cnpj);
        unset($establishment->fantasy_name);
        unset($establishment->cel);
        unset($establishment->email);
        unset($config->external_products_cache);

        $establishment->config = $config;

        $hasRating = $this->rating ? true : false;

        $arr = [
          'id' => $this->id,
          'hash' => md5($this->id),
          'created_at' => $this->created_at,
          'opened_at' => $this->opened_at,
          'updated_at' => $this->updated_at,
          'status' => $this->status,
          'table_id' => $this->table_id,
          'table' => $table,
          'establishment' => $establishment,
          'user' => $this->user,
          'waiter' => $waiter,
          'orders' => $orders,
          'denyOrders' => $denyOrders,
          'total' => $total + $baksheesh,
          'alias' => ($table->alias) ? $table->alias : null,
          'request_alias' => 0,
          'duration' => $duration,
          'has_baksheesh' => ($config) ? $config->has_baksheesh : 0,
          'baksheesh_percentage' => $config->baksheesh_percentage,
          'baksheesh' => $baksheesh,
          'has_couvert' => ($config) ? $config->has_couvert : 0,
          'couvert_price' => $couvert,
          'enable_advanced_functions' => ($config) ? $config->enable_advanced_functions : 0,
          'enable_payment' => ($establishment->hasPaymentMethod()) ? $config->enable_payment : 0,
          'temp_user' => $this->temp_user,
          'raw_user' => unserialize($this->raw_user),
          'external_id' => $this->external_id,
          'amount_paid' => $this->amount_paid,
          'remaining_value' => $this->remaining_value,
          'attached_users' => $this->getAttachedUsers(),
          'has_sync' => $this->has_sync,
          'has_paid' => $hasPaid,
          'has_rating' => $hasRating,
        ];
        if($hasPaid){
            $arr['transaction_code'] = $this->transaction_code;
            $arr['card_flag'] = $this->card_flag;
        }
        //Libera os dados da cielo para processar pagamentos
        if ($config && $config->enable_payment == 1) {
            $account = $establishment->getPaymentMethod();
            $arr['cielo_merchantId'] = $account['MerchantId'];
            $arr['cielo_merchantKey'] = $account['MerchantKey'];
        }

        return $arr;
    }

    public function getAttachedUsers()
    {
        $attached = BillAttachedUser::with('user')->where(['bill_id' => $this->id])->get();

        return $attached;
    }

    public function changeAttachedPermission(User $user, $status = 'opened')
    {
        Log::info('Alterando status de anexo de conta de id '.$this->id.' para usuário '.$user->id.': '.$status);
        $attached = BillAttachedUser::where(['bill_id' => $this->id, 'user_id' => $user->id])->first();
        $attached->status = $status;
        if ($status == 'closed') {
            $attached->closed_at = Carbon::now();
        }
        $attached->save();
    }

    public function attachedHasPermission(User $user)
    {
        $attached = BillAttachedUser::where(['bill_id' => $this->id, 'user_id' => $user->id])->first();
        if ($attached->status == BillAttachedUser::STATUS_OPENED) {
            return true;
        }

        return false;
    }

    public function getOrderByExternalId($externalId)
    {
        if ($externalId) {
            $order = $this->orders()->where(['bill_id' => $this->id, 'external_id' => $externalId])->get();

            return $order;
        }

        return null;
    }

    public function getStatusMessage()
    {
        return Bill::$statusList[$this->status];
    }
}
