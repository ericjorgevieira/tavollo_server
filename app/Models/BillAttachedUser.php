<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillAttachedUser extends Model
{

  protected $table = "bill_attached_users";

  protected $primaryKey = "bill_id";

  protected $dates = [
    'created_at',
    'opened_at',
    'closed_at',
    'updated_at'
  ];

  const STATUS_OPENED = 'opened';
  const STATUS_CLOSING= 'closing';
  const STATUS_CLOSED = 'closed';

  public static $statusList = array(
    self::STATUS_OPENED => 'Aberto',
    self::STATUS_CLOSING => 'Fechando',
    self::STATUS_CLOSED => 'Fechado'
  );

  public function bill(){
      return $this->belongsTo(Bill::class);
  }

  public function user(){
      return $this->belongsTo(User::class);
    }
}
