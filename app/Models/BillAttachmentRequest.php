<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Events\BillAttachmentRequesting;

class BillAttachmentRequest extends Model
{

  const STATUS_OPENED = 'opened';
  const STATUS_CLOSED = 'closed';

  public static $statusList = array(
    self::STATUS_OPENED => 'Aberto',
    self::STATUS_CLOSED => 'Fechado'
  );

  public function bill(){
    return $this->belongsTo(Bill::class);
  }

  public function user(){
    return $this->belongsTo(User::class);
  }

  public static function openRequest($bill, $user){
    $request = new BillAttachmentRequest();
    $request->bill_id = $bill->id;
    $request->user_id = $user->id;
    $request->status = self::STATUS_OPENED;
    $request->ip = '';
    $request->save();
    broadcast(new BillAttachmentRequesting($request));
    return $request;
  }
}
