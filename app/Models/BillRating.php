<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillRating extends Model
{
    public function establishment()
    {
        return $this->belongsTo(Establishment::class);
    }

    public function bill()
    {
        return $this->belongsTo(Bill::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
