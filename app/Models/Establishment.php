<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Establishment extends Model
{
    use SoftDeletes;

    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';
    const STATUS_TRASH = 'trash';

    const PLAN_TEST = 'test';
    const PLAN_MASTER = 'master';

    const THEME_LIGHT = 'light';
    const THEME_DARK = 'dark';

    const PRODUCT_ONLY_MENU = 1;
    const PRODUCT_MENU_AND_ALLOW_BILLS = 2;
    const PRODUCT_FULL = 3;

    public static $statusList = [
        self::STATUS_ENABLED => 'Ativado',
        self::STATUS_DISABLED => 'Desativado',
        self::STATUS_TRASH => 'Lixo',
    ];

    public static $plansList = [
        self::PLAN_TEST => 'Teste',
        self::PLAN_MASTER => 'Definitivo',
    ];

    public static $productsList = [
      self::PRODUCT_ONLY_MENU => 'Cardápio Digital',
      self::PRODUCT_MENU_AND_ALLOW_BILLS => 'Cardápio Digital + Pedido na Mesa/Cartão',
      self::PRODUCT_FULL => 'Cardápio Digital + Pedido na Mesa/Cartão + Pedido Pré-Pago',
    ];

    public static $themesList = [
      self::THEME_LIGHT => 'Claro',
      self::THEME_DARK => 'Escuro',
    ];

    protected $fillable = ['name', 'cnpj'];

    protected $dates = ['payment_date'];

    public function tables()
    {
        return $this->hasMany(Table::class);
    }

    public function menus()
    {
        return $this->hasMany(Menu::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function productCategories()
    {
        return $this->hasMany(ProductCategory::class);
    }

    public function locations()
    {
        return $this->hasMany(TableLocation::class);
    }

    public function waiters()
    {
        return $this->hasMany(Waiter::class);
    }

    public function sectors()
    {
        return $this->hasMany(Sector::class);
    }

    public function islands()
    {
        return $this->hasMany(Island::class);
    }

    public function billRatings()
    {
        return $this->hasMany(BillRating::class);
    }

    public function preOrderRatings()
    {
        return $this->hasMany(PreOrderRating::class);
    }

    public function config()
    {
        return $this->hasOne(EstablishmentConfig::class);
    }

    public function api_token()
    {
        return $this->hasOne(ApiToken::class);
    }

    public function integration()
    {
        return $this->hasOne(EstablishmentIntegration::class);
    }

    public function accesses()
    {
        return $this->hasMany(EstablishmentAccess::class);
    }

    public function externalProducts()
    {
        return json_decode($this->config->external_products_cache, JSON_OBJECT_AS_ARRAY);
    }

    public static function estados()
    {
        return [
            'AC' => 'Acre',
            'AL' => 'Alagoas',
            'AM' => 'Amazonas',
            'AP' => 'Amapá',
            'BA' => 'Bahia',
            'CE' => 'Ceará',
            'DF' => 'Distrito Federal',
            'ES' => 'Espírito Santo',
            'GO' => 'Goiás',
            'MA' => 'Maranhão',
            'MT' => 'Mato Grosso',
            'MS' => 'Mato Grosso do Sul',
            'MG' => 'Minas Gerais',
            'PA' => 'Pará',
            'PB' => 'Paraíba',
            'PR' => 'Paraná',
            'PE' => 'Pernambuco',
            'PI' => 'Piauí',
            'RJ' => 'Rio de Janeiro',
            'RN' => 'Rio Grande do Norte',
            'RO' => 'Rondônia',
            'RS' => 'Rio Grande do Sul',
            'RR' => 'Roraima',
            'SC' => 'Santa Catarina',
            'SE' => 'Sergipe',
            'SP' => 'São Paulo',
            'TO' => 'Tocantins',
        ];
    }

    public function getCreatedAtAttribute($value)
    {
        if ($value != '') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);

            return $date->format('d/m/Y h:i:s');
        }

        return $value;
    }

    public function getUpdatedAtAttribute($value)
    {
        if ($value != '') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);

            return $date->format('d/m/Y H:i:s');
        }

        return $value;
    }

    /**
     * Retorna o total de vendas feitas.
     *
     * @return string
     */
    public function getTotalSales($format = true, $period = null)
    {
        $total = 0.0;

        $ordersQuery = Order::query()
          ->select(DB::raw('orders.id, orders.product_id, orders.cache_price, products.calculate_baksheesh as calculate_baksheesh'))
          ->join('bills', function ($join) {
              $join->on('orders.bill_id', 'bills.id');
          })
          ->join('tables', function ($join) {
              $join->on('tables.id', 'bills.table_id');
          })
          ->join('products', function ($join) {
              $join->on('orders.product_id', 'products.id');
          })
          ->where('tables.establishment_id', '=', $this->id)
          ->where('orders.status', '!=', Order::STATUS_DENY)
          ->where('orders.status', '!=', Order::STATUS_TRASH);

        $preOrdersQuery = PreOrder::where(['establishment_id' => $this->id])->whereIn('status', [PreOrder::STATUS_FINISHED, PreOrder::STATUS_DELIVERED]);

        if ($period) {
            $date = Carbon::now()->subDays($period);
            $ordersQuery->where('orders.created_at', '>=', $date);
            $preOrdersQuery->where('created_at', '>=', $date);
        }

        $orders = $ordersQuery->get();
        $preOrders = $preOrdersQuery->get();

        foreach ($orders as $order) {
            $total += $order->cache_price;
        }

        foreach ($preOrders as $preOrder) {
            $total += $preOrder->cache_total;
        }

        $total = floor($total * 100) / 100;

        if ($format) {
            return number_format($total, 2, ',', '.');
        }

        return $total;
    }

    /**
     * Retorna o ticket médio do estabelecimento.
     *
     * @param bool $format [description]
     *
     * @return [type] [description]
     */
    public function getAverageTicket($format = true)
    {
        $bills = $this->getBills();
        $preOrders = $this->getPreOrders();
        $countBills = count($bills);
        $countPreOrders = count($preOrders);
        $countTotal = $countBills + $countPreOrders;
        $sales = $this->getTotalSales(false);
        $ticket = 0;
        if ($countTotal > 0) {
            $ticket = $sales / $countTotal;
        }
        if ($format) {
            return number_format($ticket, 2, ',', '.');
        }

        return $ticket;
    }

    /**
     * Retorna o tempo médio de atendimento.
     */
    public function getAverageServiceTime()
    {
        //Pega todas as contas abertas e encerradas no estabelecimento
        $bills = $this->getBills(Bill::STATUS_CLOSED);

        $arrTime = [];

        foreach ($bills as $b) {
            $openedAt = new Carbon($b->getAttributes()['opened_at']);
            $closedAt = new Carbon($b->getAttributes()['closed_at']);
            $diff = $openedAt->diffInMinutes($closedAt);
            $arrTime[] = $diff;
        }
        $mediaTime = (count($arrTime)) ? (array_sum($arrTime)) / count($arrTime) : 0;
        $hours = intval($mediaTime / 60);
        $minutes = ($mediaTime % 60);

        return [
            'hours' => $hours,
            'minutes' => $minutes,
        ];
    }

    /**
     * Retorna lista de todos as Contas (Bills) do Estabelecimento.
     *
     * @return array
     */
    public function getBills($status = null, $period = null)
    {
        $billsQuery = Bill::query()
          ->select(DB::raw('bills.*'))
          ->join('tables', function ($join) {
              $join->on('tables.id', 'bills.table_id');
          })
          ->where('tables.establishment_id', '=', $this->id);
        if ($status) {
            $billsQuery->where('bills.status', '=', $status);
        }
        if ($period) {
            $date = Carbon::now()->subDays($period);
            $billsQuery->where('bills.closed_at', '>=', $date);
        }
        $bills = $billsQuery->get();

        return $bills;
    }

    /**
     * Retorna lista de todos os pedidos pré-pagos.
     */
    public function getPreOrders($status = null)
    {
        $preOrdersQuery = PreOrder::where(['establishment_id' => $this->id]);

        if ($status) {
            $preOrdersQuery->where('status', $status);
        }
        $preOrders = $preOrdersQuery->get();

        return $preOrders;
    }

    /**
     * Verifica se existe conta aberta no estabelecimento.
     *
     * @return bool [description]
     */
    public function hasOpenedBill()
    {
        $tables = $this->tables;
        foreach ($tables as $t) {
            if ($t->hasActiveBill()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if is test establishment.
     *
     * @return bool [description]
     */
    public function isTest()
    {
        return $this->plan == self::PLAN_TEST;
    }

    /**
     * Check if has payment method.
     *
     * @return bool [description]
     */
    public function hasPaymentMethod()
    {
        return ($this->config && $this->config->cielo_account) ? true : false;
    }

    /**
     * Return payment method.
     *
     * @return [type] [description]
     */
    public function getPaymentMethod()
    {
        return ($this->config && $this->config->cielo_account) ? json_decode($this->config->cielo_account, true) : null;
    }

    /**
     * Return theme config.
     *
     * @return [type] [description]
     */
    public function getThemeConfig()
    {
        $arrTheme = $this->theme_config ? json_decode($this->theme_config, true) : [];
        $arrTheme['image'] = $this->image;
        $arrTheme['logo'] = $this->logo;
        $arrTheme['background'] = $this->background;
        $arrTheme['theme'] = $this->theme;

        return $arrTheme;
    }

    /**
     * Save theme config.
     *
     * @param [type] $config [description]
     *
     * @return [type] [description]
     */
    public function saveThemeConfig($config)
    {
        $this->theme_config = json_encode($config);
        $this->save();
    }

    /**
     * Check if establishment is near latitude and longitude.
     *
     * @param [type] $lat      [description]
     * @param [type] $lng      [description]
     * @param [type] $distance Distância em metros. 1000 = 100 metros
     *
     * @return [type] [description]
     */
    public function checkIfIsNear($lat, $lng, $distance = 5000)
    {
        $isLocationDisabled = ($this->config->disable_location == 1) ? true : false;
        $displayLocation = ($this->config->display_location == 1) ? true : false;
        if (!$isLocationDisabled && $displayLocation && $lat && $lng) {
            $eLat = $this->config->location_lat;
            $eLng = $this->config->location_long;
            $R = 6378137; // km
            $dLat = deg2rad($eLat - $lat);
            $dLon = deg2rad($eLng - $lng);
            $a = sin($dLat / 2) * sin($dLat / 2) + cos(deg2rad($eLat)) * cos(deg2rad($lat)) * sin($dLon / 2) * sin($dLon / 2);
            $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
            $d = $R * $c;

            return $d < $distance;
        }

        return $isLocationDisabled;
    }

    public function searchProducts($queryString)
    {
        $products = $this->products()->where('name', 'LIKE', "%{$queryString}%")->where('status', '=', 'enabled')->get();
        $productsFiltered = $products->filter(function ($p) {
            return $p->menus && count($p->menus) > 0;
        });

        return $productsFiltered;
    }

    public function getRatingTotal()
    {
        $total = 0;
        $count = 0;
        $media = 0;

        $this->billRatings->map(function ($r) use (&$total, &$count) {
            ++$count;
            $total += $r->rating;
        });

        $this->preOrderRatings->map(function ($r) use (&$total, &$count) {
            ++$count;
            $total += $r->rating;
        });

        if ($count > 0) {
            $media = $total / $count;
        }

        return ['average' => round($media, 2), 'count' => $count];
    }
}
