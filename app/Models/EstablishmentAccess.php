<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class EstablishmentAccess extends Model
{
    public function establishment()
    {
        return $this->belongsTo(Establishment::class);
    }

    public static function registerAccess($establishment_id, $type = 'preorder', $utm_source = null)
    {
        $access = new EstablishmentAccess();
        $access->establishment_id = $establishment_id;
        $access->type = $type;
        $access->utm_source = $utm_source;
        $access->ip_address = Request::capture()->ip();
        $access->save();

        return $access;
    }
}
