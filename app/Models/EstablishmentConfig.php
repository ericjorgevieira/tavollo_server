<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class EstablishmentConfig extends Model
{
    public $timestamps = false;

    public $primaryKey = 'establishment_id';

    public static $days = array(
    'monday' => 'Segunda',
    'tuesday' => 'Terça',
    'wednesday' => 'Quarta',
    'thursday' => 'Quinta',
    'friday' => 'Sexta',
    'saturday' => 'Sábado',
    'sunday' => 'Domingo'
  );

    public function establishment()
    {
        return $this->belongsTo(Establishment::class);
    }

    /**
    * Verifica se já existe Spot com mesmo número
    * @param  [type]  $number [description]
    * @return boolean         [description]
    */
    public function isTableLimited($number, $isConsumptionCard = false, $table_id = null)
    {
        //Verifica se Spot existe
        $table = Table::where(['establishment_id' => $this->establishment_id, 'number' => $number,
     'is_consumption_card' => $isConsumptionCard, 'status' => Table::STATUS_ENABLED])->first();
        //Se existir e limitação estiver ativa
        if ($table && $this->limit_table == 1) {
            //Verifica se o Spot possui conta ativa OU está desativado
            if ($table_id && $table->id == $table_id) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
    * Define os dias da semana em que o estabelecimento estará disponível
    * @param array $arrDays    [description]
    */
    public function setAvailableDays($arrDays = null)
    {
        if (!$arrDays) {
            $arrDays = array();
        }
        $this->available_days = json_encode(array_keys($arrDays));
    }

    /**
    * Retorna os dias da semana em que o estabelecimento estará disponível
    * @return [type] [description]
    */
    public function getAvailableDays()
    {
        if (!json_decode($this->available_days)) {
            return array();
        }
        return json_decode($this->available_days);
    }

    public function isAvailable()
    {
        $today = Carbon::now();
        $now = $today->format('H:i:s');
        $day = strtolower($today->format('l'));
        if (in_array($day, $this->getAvailableDays())) {
            $firstTurnBegin = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d') . ' ' . $this->first_turn_begin);
            $firstTurnEnd = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d') . ' ' . $this->first_turn_end);
            $secondTurnBegin = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d') . ' ' . $this->second_turn_begin);
            $secondTurnEnd = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d') . ' ' . $this->second_turn_end);
            if ($today->between($firstTurnBegin, $firstTurnEnd) || $today->between($secondTurnBegin, $secondTurnEnd)) {
                return true;
            }
        }
        return false;
    }
}
