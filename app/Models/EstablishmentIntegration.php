<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\StatusEnableTrait;
use App\Models\Integrations\Raffinato;
use App\Models\Integrations\Bematech;

class EstablishmentIntegration extends Model
{

    use SoftDeletes, StatusEnableTrait;

    public $primaryKey = 'establishment_id';

    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';
    const STATUS_TRASH = 'trash';

    public static $statusList = array(
        self::STATUS_ENABLED => 'Ativado',
        self::STATUS_DISABLED => 'Desativado',
        self::STATUS_TRASH => 'Deletado'
    );

    const INTEGRATION_RAFFINATO = 'raffinato';
    const INTEGRATION_BEMATECH = 'bematech';
    const INTEGRATION_MISTERCHEF = 'misterchef';

    public function establishment()
    {
        return $this->belongsTo(Establishment::class);
    }

    protected function integration()
    {
        return $this->belongsTo(Integration::class);
    }

    public function getIntegration()
    {
        if($this->is_active){
          $slug = $this->integration->slug;
          switch($slug){
              case self::INTEGRATION_BEMATECH:
                  return new Bematech($this->establishment, $this->credential_user, $this->api_url, $this->api_token);
                  break;
              case self::INTEGRATION_MISTERCHEF:
                  return new Bematech($this->establishment, $this->credential_user, $this->api_url, $this->api_token);
                  break;
              case self::INTEGRATION_RAFFINATO:
                  default:
                  return new Raffinato($this->establishment, $this->api_url, $this->api_token);
                  break;
          }
        }
        return false;
    }
}
