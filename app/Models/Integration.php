<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\StatusEnableTrait;

class Integration extends Model
{

  use SoftDeletes, StatusEnableTrait;

  const STATUS_ENABLED = 'enabled';
  const STATUS_DISABLED = 'disabled';
  const STATUS_TRASH = 'trash';

  public static $statusList = array(
      self::STATUS_ENABLED => 'Ativado',
      self::STATUS_DISABLED => 'Desativado',
      self::STATUS_TRASH => 'Deletado'
  );

  public function establishmentIntegrations(){
    return $this->hasMany(EstablishmentIntegration::class);
  }
}
