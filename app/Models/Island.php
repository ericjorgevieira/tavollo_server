<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\StatusEnableTrait;
use Carbon\Carbon;

class Island extends Model
{
    use SoftDeletes, StatusEnableTrait;

    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';
    const STATUS_TRASH = 'trash';

    public static $statusList = array(
      self::STATUS_ENABLED => 'Ativado',
      self::STATUS_DISABLED => 'Desativado',
      self::STATUS_TRASH => 'Deletado'
  );

    public static $days = array(
        'monday' => 'Segunda',
        'tuesday' => 'Terça',
        'wednesday' => 'Quarta',
        'thursday' => 'Quinta',
        'friday' => 'Sexta',
        'saturday' => 'Sábado',
        'sunday' => 'Domingo'
    );

    protected $dates = ['deleted_at'];

    public function tables()
    {
        return $this->belongsToMany(Table::class);
    }

    public function establishment()
    {
        return $this->belongsTo(Establishment::class);
    }

    public function waiter()
    {
        return $this->belongsTo(Waiter::class);
    }

    public function getTablesToList()
    {
        $tables = $this->tables()->orderBy('number', 'asc')->get();
        $arrTables = array();
        foreach ($tables as $p) {
            $arrTables[] = array(
                'link' => "/menus/" . $p->id . "/edit/",
                'name' => "<b>" . $p->name . "</b>"
            );
        }
        return $arrTables;
    }

    public function getWaiterToList()
    {
        $waiter = $this->waiter;
        if ($waiter) {
            return '<a href="/profile/waiters/' . $waiter->id . '/edit">' . $waiter->name . '</a>';
        }
        return null;
    }

    /**
    * Define os dias da semana em que o produto estará listado
    * @param array $arrDays    [description]
    */
    public function setAvailableDays($arrDays)
    {
        if ($arrDays) {
            $this->available_days = serialize(array_keys($arrDays));
        } else {
            $this->available_days = serialize(array());
        }
    }

    /**
    * Retorna os dias da semana em que o produto estará listado
    * @return [type] [description]
    */
    public function getAvailableDays()
    {
        if (!unserialize($this->available_days)) {
            return array();
        }
        return unserialize($this->available_days);
    }

    public function setRawTableNumbers()
    {
        $arrNumbers = array();
        foreach ($this->tables as $t) {
            $arrNumbers[] = $t->number;
        }
        $this->raw_table_numbers = serialize($arrNumbers);
        $this->save();
        $this->syncTableNumbers();
    }

    public function getRawTableNumbers()
    {
        return unserialize($this->raw_table_numbers);
    }

    /**
    * Sincroniza todos os Spots com números já existentes atrelados à Ilha
    * @return [type] [description]
    */
    public function syncTableNumbers()
    {
        $this->tables()->detach();
        $numbers = $this->getRawTableNumbers();
        $tables = Table::where(['establishment_id' => $this->establishment_id, 'status' => Table::STATUS_ENABLED])->whereIn('number', $numbers)->get();
        $arrTables = array();
        foreach ($tables as $t) {
            $arrTables[] = $t->id;
        }
        $this->tables()->sync($arrTables);
    }

    /**
    * Retorna Ilha ativa que possui número definido
    * @param  [type] $number           [description]
    * @param  [type] $establishment_id [description]
    * @return [type]                   [description]
    */
    public static function withTableNumberNow($number, $establishment_id)
    {
        $islands = Island::where(['status' => ISLAND::STATUS_ENABLED, 'establishment_id' => $establishment_id])->get();
        foreach ($islands as $i) {
            if ($i->isAvailableNow() && $i->hasTableNumber($number)) {
                $i->syncTableNumbers();
                return $i;
            }
        }
        return false;
    }

    /**
    * Verifica se Ilha está disponível em determinados dias
    * @param  [type]  $days [description]
    * @return boolean       [description]
    */
    public function hasActiveDays($days)
    {
        $availableDays = $this->getAvailableDays();
        foreach ($days as $d) {
            if (in_array(strtolower($d), $availableDays)) {
                return true;
            }
        }
        return false;
    }

    /**
    * Verifica se Ilha possui número de Spot associado
    * @param  [type]  $number [description]
    * @return boolean         [description]
    */
    public function hasTableNumber($number)
    {
        if (in_array(intval($number), $this->getRawTableNumbers())) {
            return true;
        }
        return false;
    }

    /**
    * Verifica se Ilha está disponível agora
    * @return boolean [description]
    */
    public function isAvailableNow()
    {
        if ($this->status == self::STATUS_ENABLED) {
            $today = Carbon::now();
            $yesterday = Carbon::now()->subDay();
            $now = $today->format('H:i:s');

            //Verifica se o dia de hoje está marcado na Ilha
            if (in_array(strtolower($today->format('l')), $this->getAvailableDays())) {
                $openedAt = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d') . ' ' . $this->opened_at);
                $closedAt = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d') . ' ' . $this->closed_at);

                //Verifica se o horário de início começa hoje e termina amanhã
                if (intval($openedAt->format('H')) > intval($closedAt->format('H'))) {
                    $closedAt->addDay();
                }
                //Verifica se o dia de ontem também está marcado, para saber se horário não está entre início e fim de ontem
                elseif (in_array(strtolower($yesterday->format('l')), $this->getAvailableDays()) && !$today->between($openedAt, $closedAt)) {
                    $openedAt->subDay();
                }

                if ($today->between($openedAt, $closedAt)) {
                    return true;
                }
            }
            //Verifica se o dia de ontem está marcado na Ilha
            elseif (in_array(strtolower($yesterday->format('l')), $this->getAvailableDays())) {
                $openedAt = Carbon::createFromFormat('Y-m-d H:i:s', $yesterday->format('Y-m-d') . ' ' . $this->opened_at);
                $closedAt = Carbon::createFromFormat('Y-m-d H:i:s', $yesterday->format('Y-m-d') . ' ' . $this->closed_at);

                if (intval($openedAt->format('H')) > intval($closedAt->format('H'))) {
                    $closedAt->addDay();
                }

                if ($today->between($openedAt, $closedAt)) {
                    return true;
                }
            }
        }
        return false;
    }
}
