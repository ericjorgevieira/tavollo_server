<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IslandTable extends Model
{
    protected $table = "island_table";

    protected $primaryKey = "island_id";

    public function island(){
      return $this->belongsTo(Island::class);
    }

    public function table(){
      return $this->belongsTo(Table::class);
    }
}
