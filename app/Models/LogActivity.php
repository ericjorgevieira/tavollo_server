<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class LogActivity extends Model
{
    const ACTION_INSERT = 'insert';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';

    protected $dates = [
        'created_at',
    ];

    protected $table = 'logs';

    public $timestamps = true;

    public function establishment()
    {
        return $this->belongsTo(Establishment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function registerActivity($action, $description, $establishment_id = null)
    {
        $log = new LogActivity();
        $log->establishment_id = $establishment_id;
        $log->user_id = Auth::user()->id;
        $log->action = $action;
        $log->description = $description;
        $log->save();
    }
}
