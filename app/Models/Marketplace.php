<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Marketplace extends Model
{
    use SoftDeletes;

    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';

    public static $statusList = array(
        self::STATUS_ENABLED => 'Ativado',
        self::STATUS_DISABLED => 'Desativado'
    );

    public function establishments()
    {
        return $this->belongsToMany(Establishment::class, 'marketplace_establishments');
    }

    public function getThemeConfig()
    {
        $arrTheme = $this->theme_config ? json_decode($this->theme_config, true) : array();
        $arrTheme['image'] = $this->image;
        $arrTheme['logo'] = $this->logo;
        $arrTheme['background'] = $this->background;
        return $arrTheme;
    }

    public function saveThemeConfig($config)
    {
        $this->theme_config = json_encode($config);
        $this->save();
    }
}
