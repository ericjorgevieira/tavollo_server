<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarketplaceEstablishment extends Model
{
    protected $table = "marketplace_establishments";

    public function marketplace()
    {
        return $this->hasOne(Marketplace::class);
    }

    public function establishment()
    {
        return $this->hasOne(Establishment::class);
    }
}
