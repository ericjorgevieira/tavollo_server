<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Cache;
use Storage;
use Image;

class Menu extends Model
{
    use SoftDeletes;
    //
    protected $fillable = ['name'];

    public function establishment(){
        return $this->belongsTo(Establishment::class);
    }

    public function products(){
        return $this->belongsToMany(Product::class);
    }

    public function getProductsToList()
    {
      $products = $this->products;
      $arrProducts = array();
      foreach($products as $p){
          $arrProducts[] = array(
              'link' => "/products/" . $p->id . "/edit/",
              'name' => "<b>" . $p->name . "</b>"
          );
      }
      return $arrProducts;
    }

    public function countProducts()
    {
      return $this->products->count();
    }

    public static function clearCache($establishment_id)
    {
        Cache::forget($establishment_id . '_list_menus_for_ws');
    }

    public static function insertMenu($name, $status = 'enabled', $establishmentId)
    {
      $menu = new Menu();
      $menu->name = $name;
      $menu->slug = str_slug($name);
      $menu->establishment_id = $establishmentId;
      $menu->status = $status;
      $menu->save();
      return $menu;
    }

    /**
    * Processa upload e persistencia de foto do produto
    * @param  Request $request [description]
    * @return [type]           [description]
    */
    public function uploadPhoto(Request $request){
      $photoPath = $request->input('photo');

      if(!empty($photoPath)){
        $ext = pathinfo($photoPath, PATHINFO_EXTENSION);
        $newPath = 'public/menus/' . md5($this->establishment_id) . '/' . md5($this->id) . '.' . $ext;
        Storage::delete($newPath);
        Storage::move($photoPath, $newPath);

        $this->photo = \Config::get('app.url') . Storage::url($newPath);
        $this->save();
      }
      return true;
    }
}
