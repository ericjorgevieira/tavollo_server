<?php

namespace App\Models;

use App\Events\HasBillChanged;
use App\Events\OrderClosed;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Order extends Model
{
    const STATUS_OPENED = 'opened';
    const STATUS_PREPARATION = 'preparation';
    const STATUS_FINISHED = 'finished';
    const STATUS_CANCELED = 'canceled';
    const STATUS_TRASH = 'trash';
    const STATUS_DENY = 'deny';

    public static $statusList = [
        self::STATUS_OPENED => 'Aberto',
        self::STATUS_PREPARATION => 'Preparação',
        self::STATUS_FINISHED => 'Finalizado',
        self::STATUS_CANCELED => 'Cancelamento',
        self::STATUS_TRASH => 'Deletado',
        self::STATUS_DENY => 'Rejeitado',
    ];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function bill()
    {
        return $this->belongsTo(Bill::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function items()
    {
        return $this->hasMany(OrderProductGroupItem::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function waiter()
    {
        return $this->belongsTo(Waiter::class);
    }

    public function getCreatedAtAttribute($value)
    {
        if ($value != '') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);

            return $date->format('d/m/Y H:i:s');
        }

        return $value;
    }

    public function getUpdatedAtAttribute($value)
    {
        if ($value != '') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);

            return $date->format('d/m/Y H:i:s');
        }

        return $value;
    }

    public function getDeletedAtAttribute($value)
    {
        if ($value != '') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);

            return $date->format('d/m/Y H:i:s');
        }

        return $value;
    }

    /**
     * Altera o status do pedido.
     *
     * @return [type] [description]
     */
    public function setStatus($status = self::STATUS_OPENED)
    {
        Log::info('Definindo status de pedido :'.$this->id.' para '.$status);

        if ($status == null || !array_key_exists($status, self::$statusList)) {
            $status = self::STATUS_OPENED;
        }
        if ($this->status != $status) {
            $this->status = $status;

            if ($status == self::STATUS_TRASH || $status == self::STATUS_CANCELED || $status == self::STATUS_DENY) {
                $this->deleted_at = Carbon::now();
            } else {
                $this->deleted_at = null;
            }
            $this->save();
            // if($status == self::STATUS_OPENED){
            //     $this->updateCachePrice();
            // }
            broadcast(new OrderClosed($this));
            broadcast(new HasBillChanged($this->bill));
        } else {
            Log::info('Status do pedido :'.$this->id.' já havia sido definido como '.$status.' antes.');
        }
    }

    /**
     * Adiciona peso ao pedido para cálculo proporcional.
     *
     * @param bool   $isWeight [description]
     * @param [type] $weight   [description]
     */
    public function addWeight($isWeight = true, $weight)
    {
        $this->is_weight_price = $isWeight;
        $this->order_weight = $weight;
        $this->save();
        $this->updateCachePrice();
        broadcast(new HasBillChanged($this->bill));
    }

    /**
     * Adiciona uma opção ao pedido.
     */
    public function addOptions($groups)
    {
        $this->items()->delete();
        if ($this->product_id) {
            foreach ($groups as $g) {
                $items = $g['items'];
                foreach ($items as $i) {
                    if ($i['quant'] > 0) {
                        $productItem = ProductGroupItem::where('id', $i['item_id'])->first();
                        $group = $productItem->group;
                        if ($productItem) {
                            $orderItem = new OrderProductGroupItem();
                            $orderItem->product_group_item_id = $productItem->id;
                            $orderItem->product_group_id = $group->id;
                            $orderItem->quant = $i['quant'];
                            $orderItem->value = $productItem->price;
                            $orderItem->external_id = $productItem->external_id;
                            $orderItem->group_external_id = $group->external_id;
                            $orderItem->group_composition_id = $productItem->group_composition_id;
                            $orderItem->order_id = $this->id;
                            $orderItem->save();
                        }
                    }
                }
            }
        }
    }

    /**
     * Retorna o valor atualizado do pedido.
     *
     * @return [type] [description]
     */
    public function updateCachePrice()
    {
        if ($this->is_defined_price == 1) {
            //Pega o valor definido no pedido manualmente
            $price = $this->defined_price;
        } else {
            //Pega o valor base do produto verificando se está em promoção no momento do pedido ou não
            $price = ($this->is_promo) ? $this->product->promo_price : $this->product->price;

            //Se o pedido foi feito com base em peso, calcula o valor por peso
            $price = ($this->is_weight_price) ? $this->product->calcPriceByWeight($this->order_weight) : $price;

            //Resgata todas as opções selecionada e soma com aquelas que possuem valor e quantidade
            $price += $this->calculateGroupsByFormula();

            //Se não, calcula por quantidade
            if (!$this->is_weight_price) {
                $price = $price * $this->quant;
            }
        }

        Log::info('Atualizando cache_price de pedido :'.$this->id.' para R$ '.$price);
        $this->cache_price = $price;
        $this->save();

        return $this->cache_price;
    }

    /**
     * Calcula a soma dos itens obrigatórios através das fórmulas dos grupos.
     *
     * @return [type] [description]
     */
    protected function calculateGroupsByFormula()
    {
        $items = $this->items;
        $value = 0;
        if ($items) {
            $arrGroups = [];
            foreach ($items as $orderItem) {
                $productGroup = $orderItem->group;
                $arrGroups[$productGroup->id]['formula'] = $productGroup->formula;
                $arrGroups[$productGroup->id]['items'][] = $orderItem;
            }
            foreach ($arrGroups as $g) {
                $value += ProductGroup::calculateByFormula($g['formula'], $g['items']);
            }
        }

        return $value;
    }

    /**
     * Verifica se há pedido externo já inserido na base.
     *
     * @param [type] $external_id      [description]
     * @param [type] $establishment_id [description]
     *
     * @return bool [description]
     */
    public static function hasExternalId($external_id, $establishment_id)
    {
        $order = Order::where(['external_id' => $external_id])->get()->first();
        if ($order) {
            $bill = $order->bill;
            $table = $bill->spot;
            if ($table->establishment_id == $establishment_id) {
                return $order;
            }
        }

        return false;
    }

    /**
     * Prepara um array de orders com agrupamento de itens obrigatórios.
     *
     * @param [type] $orders [description]
     *
     * @return [type] [description]
     */
    public static function getOrdersWithGroups($orders)
    {
        $orders = $orders->toArray();
        //Agrupamento de items
        foreach ($orders as $k => $o) {
            $items = $o['items'];
            $groups = [];
            foreach ($items as $l => $item) {
                $groupId = $item['product_group_id'];
                if (!isset($groups[$groupId])) {
                    $groups[$groupId] = $item['group'];
                    $groups[$groupId]['items'] = [];
                }
                unset($item['group']);
                $groups[$groupId]['items'][] = $item;
            }
            $orders[$k]['product']['groups'] = $groups;
            unset($orders[$k]['items']);
        }

        return $orders;
    }

    /**
     * Prepara um array de orders com agrupamento de itens obrigatórios.
     *
     * @param [type] $orders [description]
     *
     * @return [type] [description]
     */
    public static function getOrdersWithGroupsToApi($orders)
    {
        $orders = $orders->toArray();
        // //Agrupamento de items
        foreach ($orders as $k => $o) {
            $items = $o['items'];
            foreach ($items as $l => $item) {
                $item['group_external_id'] = $item['group']['external_id'];
                $item['group_composition_id'] = $item['item']['group_composition_id'];
                $item['external_id'] = $item['item']['external_id'];
                $item['name'] = $item['item']['name'];
                $item['description'] = $item['item']['description'];
                $item['price'] = $item['value'];
                $item['value'] = $item['value'] * $item['quant'];
                $item['formula'] = $item['group']['formula'];
                $item['original_price'] = $item['item']['price'];
                unset($item['item']);
                // unset($item['value']);
                unset($item['group']);
                $items[$l] = $item;
            }
            $orders[$k]['product']['items'] = $items;
            unset($orders[$k]['items']);
            //   unset($orders[$k]['items']);
        }

        return $orders;
    }
}
