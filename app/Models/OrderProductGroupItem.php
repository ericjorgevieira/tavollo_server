<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProductGroupItem extends Model
{

  protected $hidden = ['group_external_id'];

  public $timestamps = false;

  public function order()
  {
    return $this->belongsTo(Order::class);
  }

  public function item()
  {
    return $this->belongsTo(ProductGroupItem::class, 'product_group_item_id');
  }

  public function group()
  {
    return $this->belongsTo(ProductGroup::class, 'product_group_id');
  }

}
