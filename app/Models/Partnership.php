<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\StatusEnableTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class Partnership extends Model
{
    use SoftDeletes, StatusEnableTrait;

    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';

    public static $statusList = array(
        self::STATUS_ENABLED => 'Ativado',
        self::STATUS_DISABLED => 'Desativado'
    );

    protected $dates = ['deleted_at'];

    public function establishments()
    {
        return $this->belongsToMany(Establishment::class, 'partnerships_establishments');
    }

    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }

    public function populate(Request $request)
    {
        \DB::beginTransaction();

        try {
            //Salva os dados do Parceiro
            $this->name = $request->name;
            $this->company_name = $request->company_name;
            $this->phone = $request->phone;
            $this->document = $request->document;
            $this->city = $request->city;
            $this->uf = $request->state;
            $this->status = ($request->status == self::STATUS_ENABLED) ? self::STATUS_ENABLED : self::STATUS_DISABLED;
            $this->save();
            if (!$this->partnership_token) {
                $this->partnership_token = bcrypt($this->id . '-' . $this->name);
                $this->save();
            }

            $user = null;
            if (!$this->user() || !$this->user_id) {
                $user = User::where(['email' => $request->email, 'profile' => User::PROFILE_PARTNERSHIP])->first();
                if (!$user) {
                    $user = new User();
                    $user->email = $request->email;
                    $user->email_account = $request->email;
                    $user->is_admin = 1;
                    $user->save();

                    $this->user_id = $user->id;
                    $this->save();
                } else {
                    throw new \Exception('Usuário já existe na base! Use outro e-mail.');
                }
            } else {
                $user = $this->user;
            }

            //Salva usuário do Parceiro
            $user->name = $request->name;
            if ($request->password && $request->password != '') {
                $user->password = Hash::make($request->password);
            }
            $user->phone = $request->phone;
            $user->city = $request->city;
            $user->state = $request->state;
            $user->status = ($request->status == self::STATUS_ENABLED) ? self::STATUS_ENABLED : self::STATUS_DISABLED;
            $user->profile = User::PROFILE_PARTNERSHIP;
            $user->save();
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            return false;
        }
    }
}
