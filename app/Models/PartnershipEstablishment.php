<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnershipEstablishment extends Model
{
    protected $table = "partnerships_establishments";

    public function partnership()
    {
        return $this->hasOne(Partnership::class);
    }

    public function establishment()
    {
        return $this->hasOne(Establishment::class);
    }
}
