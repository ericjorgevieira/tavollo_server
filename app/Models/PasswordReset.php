<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class PasswordReset extends Model
{
  public $timestamps = false;
  public $primaryKey = 'email';
  public $incrementing = false;

}
