<?php

namespace App\Models;

use App\Events\PreOrderClosed;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class PreOrder extends Model
{
    const STATUS_OPENED = 'opened';
    const STATUS_PREPARATION = 'preparation';
    const STATUS_FINISHED = 'finished';
    const STATUS_DELIVERED = 'delivered';
    const STATUS_CANCELED = 'canceled';
    const STATUS_TRASH = 'trash';
    const STATUS_DENY = 'deny';

    public static $statusList = [
        self::STATUS_OPENED => 'Aberto',
        self::STATUS_PREPARATION => 'Preparação',
        self::STATUS_FINISHED => 'Finalizado',
        self::STATUS_DELIVERED => 'Entregue',
        self::STATUS_CANCELED => 'Cancelamento',
        self::STATUS_TRASH => 'Deletado',
        self::STATUS_DENY => 'Rejeitado',
    ];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $appends = ['hash'];

    public function products()
    {
        return $this->hasMany(PreOrderProduct::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function establishment()
    {
        return $this->belongsTo(Establishment::class);
    }

    public function rating()
    {
        return $this->hasOne(PreOrderRating::class);
    }

    public function getHashAttribute()
    {
        return md5($this->establishment_id);
    }

    public function getCreatedAtAttribute($value)
    {
        if ($value != '') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);

            return $date->format('d/m/Y H:i:s');
        }

        return $value;
    }

    public function getUpdatedAtAttribute($value)
    {
        if ($value != '') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);

            return $date->format('d/m/Y H:i:s');
        }

        return $value;
    }

    public function getDeletedAtAttribute($value)
    {
        if ($value != '') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);

            return $date->format('d/m/Y H:i:s');
        }

        return $value;
    }

    /**
     * Altera o status do pedido.
     *
     * @return [type] [description]
     */
    public function setStatus($status = self::STATUS_OPENED, $external_info = null)
    {
        Log::info('Definindo status de pedido pré-pago:'.$this->id.' para '.$status);

        if ($status == null || !array_key_exists($status, self::$statusList)) {
            $status = self::STATUS_OPENED;
        }
        if ($this->status != $status) {
            $this->status = $status;

            if ($status == self::STATUS_TRASH || $status == self::STATUS_CANCELED || $status == self::STATUS_DENY) {
                $this->deleted_at = Carbon::now();
            } else {
                $this->deleted_at = null;
            }
            if ($external_info) {
                $this->external_info = $external_info;
            }
            $this->save();
            // if($status == self::STATUS_OPENED){
            //     $this->updateCachePrice();
            // }
            broadcast(new PreOrderClosed($this));
        } else {
            Log::info('Status do pedido :'.$this->id.' já havia sido definido como '.$status.' antes.');
        }
    }

    /**
     * Adiciona um produto ao PreOrder.
     *
     * @param Product $product [description]
     * @param int     $quant   [description]
     * @param string  $obs     [description]
     * @param array   $options [description]
     */
    public function addProduct(Product $product, $quant = 1, $obs = '', $options = [])
    {
        $preOrderProduct = new PreOrderProduct();
        $preOrderProduct->pre_order_id = $this->id;
        $preOrderProduct->product_id = $product->id;
        $preOrderProduct->quant = $quant;
        $preOrderProduct->observation = $obs;
        $preOrderProduct->cache_price = ($product->is_promo_price) ? $product->promo_price : $product->price;
        $preOrderProduct->is_promo = ($product->is_promo_price) ? true : false;
        $preOrderProduct->save();

        $preOrderProduct->addOptions($options);
    }

    /**
     * Retorna o valor atualizado do pedido.
     *
     * @return [type] [description]
     */
    public function updateCacheTotal()
    {
        $total = 0.0;
        if ($this->is_defined_total == 1) {
            //Pega o valor definido no pedido manualmente
            $price = $this->defined_total;
        } else {
            $products = $this->products;

            foreach ($products as $product) {
                if ($product->is_defined_price) {
                    $productPrice = $product->defined_price;
                } else {
                    $productPrice = ($product->is_promo) ? $product->product->promo_price : $product->product->price;
                }

                //Resgata todas as opções selecionada e soma com aquelas que possuem valor e quantidade
                $productPrice += $product->calculateGroupsByFormula();

                //Multiplica pelo total de produtos
                $productPrice = $productPrice * $product->quant;

                //Soma ao valor total do pedido
                $total += $productPrice;
            }
        }

        Log::info('Atualizando total de pre order :'.$this->id.' para R$ '.$total);
        $this->cache_total = $total;
        $this->save();

        return $this->cache_total;
    }

    /**
     * Verifica se há pedido externo já inserido na base.
     *
     * @param [type] $external_id      [description]
     * @param [type] $establishment_id [description]
     *
     * @return bool [description]
     */
    public static function hasExternalId($external_id, $establishment_id)
    {
        $order = Order::where(['external_id' => $external_id])->get()->first();
        if ($order) {
            $bill = $order->bill;
            $table = $bill->spot;
            if ($table->establishment_id == $establishment_id) {
                return $order;
            }
        }

        return false;
    }

    /**
     * Prepara um array de orders com agrupamento de itens obrigatórios.
     *
     * @param [type] $orders [description]
     *
     * @return [type] [description]
     */
    public static function getPreOrdersWithGroups($orders)
    {
        $orders = $orders->toArray();
        //Agrupamento de items
        foreach ($orders as $k => $o) {
            $products = $o['products'];
            foreach ($products as $pk => $p) {
                $items = $p['items'];
                $groups = [];
                foreach ($items as $l => $item) {
                    $groupId = $item['product_group_id'];
                    if (!isset($groups[$groupId])) {
                        $groups[$groupId] = $item['group'];
                        $groups[$groupId]['items'] = [];
                    }
                    unset($item['group']);
                    $groups[$groupId]['items'][] = $item;
                }
                $orders[$k]['products'][$pk]['groups'] = $groups;
                unset($orders[$k]['products'][$pk]['items']);
            }
        }

        return $orders;
    }

    /**
     * Prepara um array de orders com agrupamento de itens obrigatórios.
     *
     * @param [type] $orders [description]
     *
     * @return [type] [description]
     */
    public static function getPreOrdersWithGroupsToApi($orders, $establishment_id)
    {
        $orders = $orders->toArray();
        //Solicitado pela Raffinato
        $waiter = Waiter::where(['establishment_id' => $establishment_id, 'is_default' => true, 'status' => Waiter::STATUS_ENABLED])->first();

        // //Agrupamento de items
        foreach ($orders as $k => $o) {
            $products = $o['products'];
            foreach ($products as $pk => $p) {
                $items = $p['items'];
                foreach ($items as $l => $item) {
                    $item['group_external_id'] = $item['group']['external_id'];
                    $item['group_composition_id'] = $item['item']['group_composition_id'];
                    $item['external_id'] = $item['item']['external_id'];
                    $item['name'] = $item['item']['name'];
                    $item['description'] = $item['item']['description'];
                    $item['price'] = $item['value'];
                    $item['value'] = $item['value'] * $item['quant'];
                    $item['formula'] = $item['group']['formula'];
                    $item['original_price'] = $item['item']['price'];
                    unset($item['item']);
                    // unset($item['value']);
                    unset($item['group']);
                    $items[$l] = $item;
                }
                $orders[$k]['products'][$pk]['items'] = $items;
                // unset($orders[$k]['products'][$pk]['items']);
            }
            //   unset($orders[$k]['items']);
            if ($waiter) {
                $orders[$k]['waiter'] = $waiter;
            }
        }

        return $orders;
    }
}
