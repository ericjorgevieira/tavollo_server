<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PreOrderProduct extends Model
{

  // protected $hidden = [];

  public $timestamps = false;

  public function preOrder()
  {
    return $this->belongsTo(PreOrder::class);
  }

  public function product()
  {
    return $this->belongsTo(Product::class);
  }

  public function items(){
      return $this->hasMany(PreOrderProductGroupItem::class);
  }

  /**
  * Adiciona uma opção ao produto do pedido
  * @param [type]  $options [description]
  * @param integer $quant   [description]
  */
  public function addOptions($groups)
  {
    $this->items()->delete();
    if($this->product_id){
      foreach($groups as $g){
        $items = $g['items'];
        foreach($items as $i){
          if($i['quant'] > 0){
            $productItem = ProductGroupItem::where('id', $i['item_id'])->first();
            $group = $productItem->group;
            if($productItem){
              $orderItem = new PreOrderProductGroupItem();
              $orderItem->product_group_item_id = $productItem->id;
              $orderItem->product_group_id = $group->id;
              $orderItem->quant = $i['quant'];
              $orderItem->value = $productItem->price;
              $orderItem->external_id = $productItem->external_id;
              $orderItem->group_external_id = $group->external_id;
              $orderItem->group_composition_id = $productItem->group_composition_id;
              $orderItem->pre_order_product_id = $this->id;
              $orderItem->save();
            }
          }
        }
      }
    }
  }

  /**
   * Calcula a soma dos itens obrigatórios através das fórmulas dos grupos
   * @return [type] [description]
   */
  public function calculateGroupsByFormula()
  {
    $items = $this->items;
    $value = 0;
    if($items){
      $arrGroups = [];
      foreach($items as $orderItem){
        $productGroup = $orderItem->group;
        $arrGroups[$productGroup->id]['formula'] = $productGroup->formula;
        $arrGroups[$productGroup->id]['items'][] = $orderItem;
      }
      foreach($arrGroups as $g){
        $value += ProductGroup::calculateByFormula($g['formula'], $g['items']);
      }
    }
    return $value;
  }

}
