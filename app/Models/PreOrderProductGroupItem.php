<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PreOrderProductGroupItem extends Model
{

  protected $hidden = ['group_external_id'];

  public $timestamps = false;

  public function preOrderProduct()
  {
    return $this->belongsTo(PreOrderProduct::class);
  }

  public function item()
  {
    return $this->belongsTo(ProductGroupItem::class, 'product_group_item_id');
  }

  public function group()
  {
    return $this->belongsTo(ProductGroup::class, 'product_group_id');
  }

}
