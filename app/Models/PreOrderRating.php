<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PreOrderRating extends Model
{
    public function establishment()
    {
        return $this->belongsTo(Establishment::class);
    }

    public function preOrder()
    {
        return $this->belongsTo(PreOrder::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
