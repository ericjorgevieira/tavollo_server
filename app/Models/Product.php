<?php

namespace App\Models;

use App\Traits\StatusEnableTrait;
use Cache;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Storage;

class Product extends Model
{
    use StatusEnableTrait;
    use SoftDeletes;

    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';

    public static $statusList = [
        self::STATUS_ENABLED => 'Ativado',
        self::STATUS_DISABLED => 'Desativado',
    ];

    const TURN_ALL = 'all';

    const TURN_FIRST = 'first';

    const TURN_SECOND = 'second';

    public static $turns = [
        self::TURN_ALL => 'Todos os Turnos',
        self::TURN_FIRST => '1&ordm; Turno',
        self::TURN_SECOND => '2&ordm; Turno',
    ];

    public static $days = [
    'monday' => 'Segunda',
    'tuesday' => 'Terça',
    'wednesday' => 'Quarta',
    'thursday' => 'Quinta',
    'friday' => 'Sexta',
    'saturday' => 'Sábado',
    'sunday' => 'Domingo',
  ];

    public static $hours = [
        '00:00:00' => '00:00', '01:00:00' => '01:00', '02:00:00' => '02:00',
        '03:00:00' => '03:00', '04:00:00' => '04:00', '05:00:00' => '05:00',
        '06:00:00' => '06:00', '07:00:00' => '07:00', '08:00:00' => '08:00',
        '09:00:00' => '09:00', '10:00:00' => '10:00', '11:00:00' => '11:00',
        '12:00:00' => '12:00', '13:00:00' => '13:00', '14:00:00' => '14:00',
        '15:00:00' => '15:00', '16:00:00' => '16:00', '17:00:00' => '17:00',
        '18:00:00' => '18:00', '19:00:00' => '19:00', '20:00:00' => '20:00',
        '21:00:00' => '21:00', '22:00:00' => '22:00', '23:00:00' => '23:00',
        '23:59:59' => '23:59',
    ];

    protected $fillable = ['name', 'default_observations'];

    protected $casts = [
        'default_observations' => 'array',
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function productCategory()
    {
        return $this->belongsTo(ProductCategory::class);
    }

    public function menus()
    {
        return $this->belongsToMany(Menu::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function sector()
    {
        return $this->belongsTo(Sector::class);
    }

    public function establishment()
    {
        return $this->belongsTo(Establishment::class);
    }

    public function groups()
    {
        return $this->hasMany(ProductGroup::class);
    }

    public function parent()
    {
        if ($this->parent_id != '') {
            return Product::find($this->parent_id);
        }

        return false;
    }

    public function updateIfPromoPrice()
    {
        if ($this->enable_promo_schedule == 1) {
            if ($this->isPromoPriceNow()) {
                $this->is_promo_price = 1;
            } else {
                $this->is_promo_price = 0;
            }
            $this->save();
        }
    }

    /**
     * Verifica se produto está em período promocional.
     *
     * @return bool [description]
     */
    public function isPromoPriceNow()
    {
        $today = Carbon::now();
        $now = $today->format('H:i:s');
        $day = strtolower($today->format('l'));
        if ($this->enable_promo_schedule == 1 && $this->promo_price_begin_at && $this->promo_price_end_at) {
            if ($this->promo_price_begin_at != $this->promo_price_end_at) {
                $beginAt = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d').' '.$this->promo_price_begin_at);
                $endAt = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d').' '.$this->promo_price_end_at);
                if ($this->status == self::STATUS_ENABLED) {
                    if (in_array($day, $this->getPromoPriceAvailableDays()) && $today->between($beginAt, $endAt)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Calcula o valor do produto pelo peso.
     *
     * @param $weight
     *
     * @return float
     */
    public function calcPriceByWeight($weight)
    {
        $productWeight = $this->weight;
        $productPrice = ($this->is_promo_price == 1) ? $this->promo_price : $this->price;
        $price = ($weight * $productPrice) / $productWeight;

        return $price;
    }

    /**
     * Define os dias da semana em que o produto estará com preço promocional.
     *
     * @param array $arrDays [description]
     */
    public function setPromoPriceAvailableDays($arrDays)
    {
        if ($arrDays) {
            $this->promo_price_available_days = serialize(array_keys($arrDays));
        }
    }

    /**
     * Retorna os dias da semana em que o produto estará com preço promocional.
     *
     * @return [type] [description]
     */
    public function getPromoPriceAvailableDays()
    {
        if (!unserialize($this->promo_price_available_days)) {
            return [];
        }

        return unserialize($this->promo_price_available_days);
    }

    /**
     * Define os dias da semana em que o produto estará listado.
     *
     * @param array $arrDays [description]
     */
    public function setAvailableDays($arrDays)
    {
        if ($arrDays) {
            $this->available_days = serialize(array_keys($arrDays));
        }
    }

    /**
     * Retorna os dias da semana em que o produto estará listado.
     *
     * @return [type] [description]
     */
    public function getAvailableDays()
    {
        if (!unserialize($this->available_days)) {
            return [];
        }

        return unserialize($this->available_days);
    }

    public function isAvailable()
    {
        $today = Carbon::now();
        $now = $today->format('H:i:s');
        $day = strtolower($today->format('l'));
        if ($this->status == 'enabled') {
            if (in_array($day, $this->getAvailableDays())) {
                //Verifica se produto está em período promocional e atualiza
                $this->updateIfPromoPrice();

                $firstTurnBegin = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d').' '.$this->first_turn_begin);
                $firstTurnEnd = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d').' '.$this->first_turn_end);
                $secondTurnBegin = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d').' '.$this->second_turn_begin);
                $secondTurnEnd = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d').' '.$this->second_turn_end);

                switch ($this->available_turn) {
          case self::TURN_FIRST:
          if ($today->between($firstTurnBegin, $firstTurnEnd)) {
              return true;
          }
          break;

          case self::TURN_SECOND:
          if ($today->between($secondTurnBegin, $secondTurnEnd)) {
              return true;
          }
          break;

          case self::TURN_ALL:
          default:
          if ($today->between($firstTurnBegin, $firstTurnEnd) ||
          $today->between($secondTurnBegin, $secondTurnEnd)) {
              return true;
          }
          break;
        }
            }
        }

        return false;
    }

    /**
     * Popula os dados do Produto a serem persistidos.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function populate(Request $request, $establishment_id)
    {
        $this->name = ucfirst($request->name);
        $this->code = $request->code;
        $this->weight = $request->weight;
        $this->product_category_id = $request->product_category_id;
        $this->description = $request->description;
        $this->preparation_time = $request->preparation_time;

        $price = str_replace(',', '.', str_replace('.', '', $request->price));
        $promo_price = str_replace(',', '.', str_replace('.', '', $request->promo_price));
        $this->price = $price;
        $this->promo_price = $promo_price;

        $this->promo_price_begin_at = ($request->promo_price_begin_at != '') ? $request->promo_price_begin_at.':00' : '00:00:00';
        $this->promo_price_end_at = ($request->promo_price_end_at != '') ? $request->promo_price_end_at.':00' : '23:59:59';
        $this->setPromoPriceAvailableDays($request->promo_price_available_days);

        $this->parent_id = $request->parent_id;
        $this->status = 'disabled';

        $this->setAvailableDays($request->available_days);

        $this->available_turn = $request->available_turn;
        $this->first_turn_begin = ($request->first_turn_begin != '') ? $request->first_turn_begin : '00:00:00';
        $this->first_turn_end = ($request->first_turn_end != '') ? $request->first_turn_end : '23:59:59';
        $this->second_turn_begin = ($request->second_turn_begin != '') ? $request->second_turn_begin : '00:00:00';
        $this->second_turn_end = ($request->second_turn_end != '') ? $request->second_turn_end : '23:59:59';

        $this->enable_promo_schedule = ($request->enable_promo_schedule == '1') ? 1 : 0;

        $this->is_promo_price = ($request->is_promo_price == '1') ? 1 : 0;

        $this->not_sale = ($request->not_sale == '1') ? 1 : 0;

        $this->calculate_baksheesh = ($request->calculate_baksheesh == '1') ? 1 : 0;

        $this->enable_automatic_order = ($request->enable_automatic_order) ? 1 : 0;

        if ($request->status == 'enabled') {
            $this->status = $request->status;
        }
        $this->establishment_id = $establishment_id;
        $this->save();

        $this->updateIfPromoPrice();

        $this->uploadPhoto($request);
    }

    /**
     * Processa upload e persistencia de foto do produto.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function uploadPhoto(Request $request)
    {
        $photoPath = $request->input('photo');

        if (!empty($photoPath)) {
            $ext = pathinfo($photoPath, PATHINFO_EXTENSION);
            $newPath = 'public/products/'.md5($this->establishment_id).'/'.md5($this->id).'.'.$ext;
            Storage::delete($newPath);
            Storage::move($photoPath, $newPath);

            $this->photo = \Config::get('app.url').Storage::url($newPath);
            $this->save();
        }

        return true;
    }

    /**
     * Adiciona observação padrão no produto.
     */
    public function addDefaultObservation($observation)
    {
    }

    /**
     * Remove observação padrão do produto.
     */
    public function removeDefaultObservation($key)
    {
    }

    /**
     * Apaga todos os grupos de itens obrigatórios não relacionados à importação.
     */
    public function clearGroupItems($groups)
    {
        if (count($groups)) {
            foreach ($groups as $g) {
                $itemsExternalIds = array_column($g['items'], 'external_id');
                $productGroup = $this->groups()->where(['external_id' => $g['external_id']])->first();
                if ($productGroup) {
                    $productGroup->items()->whereNotIn('external_id', $itemsExternalIds)->delete();
                }
            }
            $groupExternalIds = array_column($groups, 'external_id');
            $this->groups()->whereNotIn('external_id', $groupExternalIds)->delete();
        }
    }

    public static function clearCache($establishment_id)
    {
        Cache::forget($establishment_id.'_list_products_for_modal');
        Cache::forget($establishment_id.'_list_products_for_table');
        Cache::forget($establishment_id.'_list_products_for_api_');
        Cache::forget($establishment_id.'_list_products_for_api_enabled');
        Cache::forget($establishment_id.'_list_products_for_api_disabled');
        Cache::forget($establishment_id.'_list_products_for_api_trash');
    }

    public function toPublicArray()
    {
        return [
          'id' => $this->id,
          'name' => $this->name,
          'price' => $this->price,
          'is_promo_price' => $this->is_promo_price,
          'promo_price' => $this->promo_price,
          'code' => $this->code,
          'preparation_time' => $this->preparation_time,
          'photo' => $this->photo,
          'description' => $this->description,
          'weight' => $this->weight,
          'default_observations' => $this->default_observations,
        ];
    }
}
