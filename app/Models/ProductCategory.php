<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model
{

  use SoftDeletes;

    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';

    public static $statusList = array(
        self::STATUS_ENABLED => 'Ativado',
        self::STATUS_DISABLED => 'Desativado'
    );

    //
    protected $fillable = ['name'];

    public function products(){
        return $this->hasMany(Product::class);
    }

    public function establishment(){
        return $this->belongsTo(Establishment::class);
    }

    public function populate(Request $request, $establishment_id = null){
        $this->name = ucfirst($request->name);
        $this->slug = str_slug($request->name);
        $this->status = self::STATUS_DISABLED;

        if ($request->status == self::STATUS_ENABLED) {
            $this->status = $request->status;
        }

        if($establishment_id){
            $this->establishment_id = $establishment_id;
        }
        $this->save();
    }

    /**
    * Habilita ou desabilita Categoria
    * @return [type] [description]
    */
    public function enable(){
        if ($this->status == self::STATUS_DISABLED) {
            $this->status = self::STATUS_ENABLED;
        } else {
            $this->status = self::STATUS_DISABLED;
        }
        $this->save();
        return $this->status;
    }

    public static function insertCategory($name, $status = 'enabled', $establishmentId){
      $category = new ProductCategory();
      $category->name = $name;
      $category->slug = str_slug($name);
      $category->status = $status;
      $category->establishment_id = $establishmentId;
      $category->save();
      return $category;
    }
}
