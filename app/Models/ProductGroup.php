<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductGroup extends Model
{
    use SoftDeletes;

    public static $formulas = [
    'sum_all' => 0,
    'sum_and_divide' => 1,
    'sum_and_divide_price' => 2,
    'item_highest_value' => 3,
    'item_lower_value' => 4,
    'item_highest_divide' => 5,
    'item_lower_divide' => 6,
  ];

    public static $formulaKeys = [
    'sum_all',
    'sum_and_divide',
    'sum_and_divide_price',
    'item_highest_value',
    'item_lower_value',
    'item_highest_divide',
    'item_lower_divide',
  ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function items()
    {
        return $this->hasMany(ProductGroupItem::class);
    }

    public function getFormulaAttribute($value)
    {
        return ProductGroup::$formulas[$value];
    }

    public static function calculateByFormula($formula, $orderItems)
    {
        switch ($formula) {
      case self::$formulas['sum_all']:
        return self::sumAll($orderItems);
        break;
      case self::$formulas['sum_and_divide']:
        return self::sumAndDivide($orderItems);
        break;
      case self::$formulas['sum_and_divide_price']:
        return self::sumAndDividePrice($orderItems);
        break;
      case self::$formulas['item_highest_value']:
        return self::itemHighestValue($orderItems);
        break;
      case self::$formulas['item_lower_value']:
        return self::itemLowerValue($orderItems);
        break;
      case self::$formulas['item_highest_divide']:
        return self::itemHighestDivide($orderItems);
        break;
      case self::$formulas['item_lower_divide']:
        return self::itemLowerDivide($orderItems);
        break;
      default:
        return self::sumAll($orderItems);
        break;
    }
    }

    /**
     * Soma todos os valores dos itens.
     *
     * @var [type]
     */
    protected static function sumAll($items)
    {
        $total = 0;
        foreach ($items as $orderItem) {
            $quant = $orderItem->quant;
            $value = $orderItem->item->price;
            $total += ($quant * $value);
        }

        return $total;
    }

    /**
     * Soma todos os valores e divide pela quantidade.
     *
     * @var [type]
     */
    protected static function sumAndDivide($items)
    {
        $total = 0;
        $countItems = 0;
        foreach ($items as $orderItem) {
            if ($orderItem->quant > 0) {
                $countItems += $orderItem->quant;
                $total += ($orderItem->item->price * $orderItem->quant);
            }
        }
        if ($total > 0) {
            $totalValue = $total / $countItems;
            $total = floor($totalValue * 100) / 100;
            $valueItem = $total / count($items);
            foreach ($items as $orderItem) {
                $orderItem->value = $valueItem;
                $orderItem->save();
            }
        }

        return $total;
    }

    /**
     * Soma apenas valores que possuem preço e divide por quantidade.
     *
     * @var [type]
     */
    protected static function sumAndDividePrice($items)
    {
        $total = 0;
        $value = 0;
        $countItems = 0;
        foreach ($items as $orderItem) {
            if ($orderItem->item->price > 0) {
                $countItems += $orderItem->quant;
                $total += ($orderItem->item->price * $orderItem->quant);
            }
        }
        //Atualiza o valor dos itens no pedido
        if ($countItems) {
            $total = round($total / $countItems, 2);
            $value = round($total / $countItems, 2);
        }
        foreach ($items as $orderItem) {
            if ($orderItem->item->price > 0) {
                $orderItem->value = $value;
                $orderItem->save();
            }
        }

        return $total;
    }

    /**
     * Retorna o maior valor entre os items.
     *
     * @var [type]
     */
    protected static function itemHighestValue($items)
    {
        $maxValue = 0;
        //Caso tenha apenas um item, se houver quantidade define maior valor
        if (count($items) == 1) {
            foreach ($items as $orderItem) {
                if ($orderItem->quant > 0) {
                    $maxValue = $orderItem->item->price;
                }
            }
        } else {
            $maxValue = self::getMaxValue($items);
        }

        //Atualiza items
        foreach ($items as $orderItem) {
            if ($maxValue != $orderItem->item->price) {
                $orderItem->value = 0;
            } elseif ($maxValue == $orderItem->item->price) {
                $orderItem->value = $orderItem->item->price / $orderItem->quant;
            }
            $orderItem->save();
        }

        return $maxValue;
    }

    /**
     * Retorna o menor valor entre os items.
     *
     * @var [type]
     */
    protected static function itemLowerValue($items)
    {
        $minValue = 0;
        //Caso tenha apenas um item, se houver quantidade define menor valor
        if (count($items) == 1) {
            foreach ($items as $orderItem) {
                if ($orderItem->quant > 0) {
                    $minValue = $orderItem->item->price;
                }
            }
        } else {
            $minValue = self::getMinValue($items);
        }

        //Atualiza items
        foreach ($items as $orderItem) {
            if ($minValue != $orderItem->item->price) {
                $orderItem->value = 0;
            } elseif ($minValue == $orderItem->item->price) {
                $orderItem->value = $orderItem->item->price / $orderItem->quant;
            }
            $orderItem->save();
        }

        return $minValue;
    }

    /**
     * Pega o maior valor e divide por todos.
     *
     * @var [type]
     */
    protected static function itemHighestDivide($items)
    {
        $total = 0;
        $valueAdjust = 0;
        $countItems = 0;
        $maxValue = 0;
        if (count($items) == 1) {
            foreach ($items as $orderItem) {
                $maxValue = $orderItem->item->price;
                $countItems = $orderItem->quant;
                $valueAdjust = $maxValue / $countItems;
            }
        } else {
            $maxValue = self::getMaxValue($items);
            foreach ($items as $orderItem) {
                $countItems += $orderItem->quant;
            }
            if ($countItems > 0) {
                $valueAdjust = $maxValue / $countItems;
                $total = $maxValue;
                foreach ($items as $orderItem) {
                    if ($orderItem->quant > 0) {
                        $orderItem->value = $valueAdjust;
                        $orderItem->save();
                    }
                }
            }
        }

        return $total;
    }

    /**
     * Pega o menor valor e divide por todos.
     *
     * @var [type]
     */
    protected static function itemLowerDivide($items)
    {
        $total = 0;
        $valueAdjust = 0;
        $countItems = 0;
        $minValue = 0;
        if (count($items) == 1) {
            foreach ($items as $orderItem) {
                $minValue = $orderItem->item->price;
                $countItems = $orderItem->quant;
                $valueAdjust = $minValue / $countItems;
            }
        } else {
            $minValue = self::getMinValue($items);
            foreach ($items as $orderItem) {
                $countItems += $orderItem->quant;
            }
            if ($countItems > 0) {
                $valueAdjust = $minValue / $countItems;
                $total = $minValue;
                foreach ($items as $orderItem) {
                    if ($orderItem->quant > 0) {
                        $orderItem->value = $valueAdjust;
                        $orderItem->save();
                    }
                }
            }
        }

        return $total;
    }

    /**
     * Retorna o maior valor entre items.
     *
     * @param [type] $items [description]
     *
     * @return [type] [description]
     */
    protected static function getMaxValue($items)
    {
        $arrValues = [];
        $maxValue = 0;
        foreach ($items as $orderItem) {
            if ($orderItem->quant > 0) {
                $arrValues[] = $orderItem->item->price;
            }
        }
        $maxValue = max($arrValues);

        return $maxValue;
    }

    /**
     * Retorna o menor valor entre items.
     *
     * @param [type] $items [description]
     *
     * @return [type] [description]
     */
    protected static function getMinValue($items)
    {
        $minValue = 0;
        //Monta um array de todos os valores e pega o menor se houver quantidade
        $arrValues = [];
        foreach ($items as $orderItem) {
            if ($orderItem->quant > 0 && $orderItem->item->price > 0) {
                $arrValues[] = $orderItem->item->price;
            }
        }
        if (count($arrValues) > 0) {
            $minValue = min($arrValues);
        }

        return $minValue;
    }
}
