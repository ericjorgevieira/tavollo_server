<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\StatusEnableTrait;

class Sector extends Model
{
  use StatusEnableTrait;

  const STATUS_ENABLED = 'enabled';
  const STATUS_DISABLED = 'disabled';

  public static $statusList = array(
    self::STATUS_ENABLED => 'Ativado',
    self::STATUS_DISABLED => 'Desativado'
  );

  public function establishment()
  {
    return $this->belongsTo(Establishment::class);
  }

  public function products()
  {
    return $this->hasMany(Product::class);
  }

  public function getProductsToList(){
    $products = $this->products;
    $arrProducts = array();
    foreach($products as $p){
      $arrProducts[] = array(
        'link' => "/products/" . $p->id . "/edit/",
        'name' => "<b>" . $p->name . "</b>"
      );
    }
    return $arrProducts;
  }

  public function syncProducts($arrayProductsId)
  {
    $this->detachProducts();
    foreach ($arrayProductsId as $p) {
      $product = Product::find($p);
      $product->sector_id = $this->id;
      $product->save();
    }
  }

  public function detachProducts()
  {
    foreach ($this->products as $p) {
      $p->sector_id = 0;
      $p->save();
    }
  }
}
