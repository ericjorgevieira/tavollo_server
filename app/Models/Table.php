<?php

namespace App\Models;

use App\Miscelaneous\TableCode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Table extends Model
{
    use SoftDeletes;

    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';
    const STATUS_TRASH = 'trash';

    public static $statusList = [
    self::STATUS_ENABLED => 'Ativado',
    self::STATUS_DISABLED => 'Desativado',
    self::STATUS_TRASH => 'Lixo',
  ];

    /*
    * Tamanho do Token a ser gerado
    */
    protected $code_size = 4;

    protected $fillable = ['name'];

    public function establishment()
    {
        return $this->belongsTo(Establishment::class);
    }

    public function islands()
    {
        return $this->belongsToMany(Island::class);
    }

    public function bills()
    {
        return $this->hasMany(Bill::class);
    }

    /**
     * Verifica se mesa possui conta ativa.
     *
     * @return bool
     */
    public function hasActiveBill()
    {
        $billCount = Bill::where(['table_id' => $this->id])->whereIn('status', [Bill::STATUS_OPENED, Bill::STATUS_CLOSING])->count();
        if ($billCount > 0) {
            return true;
        }

        return false;
    }

    public function getPublicActiveBill()
    {
        return Bill::join('tables', 'bills.table_id', '=', 'tables.id')->join('users', 'bills.user_id', '=', 'users.id')
    ->join('establishments', 'tables.establishment_id', '=', 'establishments.id')
    ->where(['bills.table_id' => $this->id, 'bills.status' => 'opened'])->orderBy('bills.id', 'DESC')
    ->first(['bills.id', 'bills.temp_user', 'tables.name as table_name', 'users.name as user_name', 'users.google_id as google_id',
     'users.facebook_id as facebook_id', 'establishments.name as establishment_name', ]);
    }

    public function getActiveBill()
    {
        return Bill::with(['table', 'user'])->where(['table_id' => $this->id])->whereIn('status', [Bill::STATUS_OPENED, Bill::STATUS_DISABLED])->orderBy('bills.id', 'DESC')->first();
    }

    public static function getOrInsertNew($name = null, $number, $establishment_id, $isConsumptionCard = false, $alias = '', $externalId = null)
    {
        $table = Table::where(['establishment_id' => $establishment_id, 'number' => $number,
     'status' => self::STATUS_ENABLED, 'is_consumption_card' => $isConsumptionCard, 'external_id' => $externalId, ])->first();
        //Se não houver Spot ou o Spot estiver com conta ativa
        Log::info('Recuperando dados de spot com número'.$number.' no estabelecimento '.$establishment_id);
        if (!$table || $table->hasActiveBill()) {
            Log::info('Mesa não encontrada ou em uso:'.$number.' no estabelecimento '.$establishment_id);
            $table = new Table();
            $table->establishment_id = $establishment_id;
            $table->status = Table::STATUS_ENABLED;
            $table->is_consumption_card = $isConsumptionCard ? true : false;
            if (!$name) {
                if ($isConsumptionCard) {
                    $name = 'Cartão '.$number;
                } else {
                    $name = 'Mesa '.$number;
                }
            }
            $table->name = $name;
            $table->external_id = $externalId;
            $table->slug = str_slug($name);
            $table->alias = $alias;
            $table->save();
            $table->changeNumber($number, $establishment_id);
            Log::info('Nova mesa inserida de id:'.$table->id);
        }
        if ($table->code == null || $table->code == '') {
            $table->generateNewCode();
        }

        return $table;
    }

    public function changeNumber($number, $establishment_id)
    {
        $this->number = intval($number);
        $this->save();
        $island = $this->getIslandToday();
        if ($island) {
            $island->tables()->detach($this->id);
        }
        Island::withTableNumberNow($number, $establishment_id);
    }

    /**
     * Gera novo Token de identificação.
     */
    public function generateNewCode()
    {
        if (!$this->hasActiveBill()) {
            try {
                $this->code = strtolower(TableCode::getCode($this->code_size));
                $this->save();
            } catch (\Illuminate\Database\QueryException $e) {
                $errorCode = $e->errorInfo[1];
                if ($errorCode == 1062) {
                    $this->generateNewCode();
                }
            }
        }
    }

    public function getIslandToday()
    {
        foreach ($this->islands as $i) {
            if ($i->isAvailableNow()) {
                return $i;
            }
        }

        return false;
    }

    //Retorna o objeto do Spot com o Garçøm da ilha
    public function toPublicArray()
    {
        $island = $this->getIslandToday();
        $waiter = null;
        if ($island) {
            $waiter = $island->waiter;
        }

        return [
      'id' => $this->id,
      'name' => $this->name,
      'number' => $this->number,
      'code' => $this->code,
      'alias' => $this->alias,
      'description' => $this->description,
      'status' => $this->status,
      'establishment_id' => $this->establishment_id,
      'waiter' => $waiter,
    ];
    }
}
