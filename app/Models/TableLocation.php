<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Traits\StatusEnableTrait;
use Cache;

class TableLocation extends Model
{
  use StatusEnableTrait;

  const STATUS_ENABLED = 'enabled';
  const STATUS_DISABLED = 'disabled';

  public static $statusList = array(
    self::STATUS_ENABLED => 'Ativado',
    self::STATUS_DISABLED => 'Desativado'
  );

  public function establishment()
  {
    return $this->belongsTo(Establishment::class);
  }

  public function populate(Request $request, $establishment_id = null){
      $this->name = ucfirst($request->name);
      $this->slug = str_slug($request->name);
      $this->status = self::STATUS_DISABLED;

      if ($request->status == self::STATUS_ENABLED) {
          $this->status = $request->status;
      }

      if($establishment_id){
          $this->establishment_id = $establishment_id;
      }
      $this->save();
      self::clearCache($this->establishment_id);
  }

  /**
  * Habilita ou desabilita Categoria
  * @return [type] [description]
  */
  public function enable(){
      if ($this->status == self::STATUS_DISABLED) {
          $this->status = self::STATUS_ENABLED;
      } else {
          $this->status = self::STATUS_DISABLED;
      }
      $this->save();
      self::clearCache($this->establishment_id);
      return $this->status;
  }

  public static function clearCache($establishment_id){
    Cache::forget($establishment_id . '_locations_query');
  }
}
