<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Events\TableRequesting;

class TableRequest extends Model
{
  public function establishment(){
      return $this->belongsTo(Establishment::class);
  }

  public function user(){
    return $this->belongsTo(User::class);
  }

  public static function openRequest($user, $table_number, $establishment_id, $lat = '', $lng = ''){
    $request = new TableRequest();
    $request->establishment_id = $establishment_id;
    $request->table_number = $table_number;
    $request->ip = '';
    $request->location_lat = $lat;
    $request->location_long = $lng;
    $request->user_id = $user->id;
    $request->save();
    broadcast(new TableRequesting($request));
    return $request;
  }
}
