<?php

namespace App\Models;

use App\Drivers\DriverRaffinato;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;

    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';
    const STATUS_TRASH = 'trash';

    const PROFILE_CUSTOMER = 'customer';
    const PROFILE_TEST = 'test';
    const PROFILE_MANAGER = 'manager';
    const PROFILE_WAITER = 'waiter';
    const PROFILE_PARTNERSHIP = 'partnership';

    public static $statusList = [
      self::STATUS_ENABLED => 'Ativado',
      self::STATUS_DISABLED => 'Desativado',
      self::STATUS_TRASH => 'Lixo',
    ];

    public static $profileList = [
      self::PROFILE_CUSTOMER => 'Cliente',
      self::PROFILE_TEST => 'Teste',
      self::PROFILE_MANAGER => 'Administrador',
      self::PROFILE_WAITER => 'Atendente',
      self::PROFILE_PARTNERSHIP => 'Revenda',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'password', 'remember_token',
    ];

    public function establishment()
    {
        return $this->belongsTo(Establishment::class);
    }

    public function bills()
    {
        return $this->hasMany(Bill::class);
    }

    public function preOrders()
    {
        return $this->hasMany(PreOrder::class);
    }

    public function user_tokens()
    {
        return $this->hasMany(UserToken::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function waiter()
    {
        if ($this->profile == self::PROFILE_WAITER) {
            return $this->hasOne(Waiter::class);
        }

        return false;
    }

    public function partnership()
    {
        if ($this->profile == self::PROFILE_PARTNERSHIP) {
            return $this->hasOne(Partnership::class);
        }

        return null;
    }

    /** Tavollo rating */
    public function rating()
    {
        return $this->hasOne(Rating::class);
    }

    /** Opened bills ratings */
    public function billRatings()
    {
        return $this->hasMany(BillRating::class);
    }

    /** Opened preOrders ratings */
    public function preOrderRatings()
    {
        return $this->hasMany(PreOrderRating::class);
    }

    public function getCreatedAtAttribute($value)
    {
        if ($value != '') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);

            return $date->format('d/m/Y h:i:s');
        }

        return $value;
    }

    public function getUpdatedAtAttribute($value)
    {
        if ($value != '') {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $value);

            return $date->format('d/m/Y H:i:s');
        }

        return $value;
    }

    public function firstName()
    {
        $name = explode(' ', $this->name);

        return $name[0];
    }

    public function totalSpent($format = true)
    {
        $total = 0.0;
        $bills = Bill::where(['user_id' => $this->id, 'status' => 'closed'])->get();
        foreach ($bills as $b) {
            $total += Bill::getTotalValue($b);
        }
        if ($format) {
            return 'R$ '.number_format($total, 2, ',', '.');
        }

        return $total;
    }

    /**
     * Retorna a conta (Bill) ativa.
     *
     * @param string $status
     *
     * @return array|\Illuminate\Database\Eloquent\Model|static|null
     */
    public function getActiveBill($status = 'opened', $isAttached = false, $loadExtract = true)
    {
        $bill = Bill::with(['orders' => function ($query) {
            $query->where('status', '!=', 'trash');
        },
        'orders.product', 'table', 'table.establishment', ])
        ->where(['user_id' => $this->id])->whereIn('status', ['opened', 'closing', 'disabled'])->first();

        if (!$bill || $isAttached) {
            //Verifica se Usuário não está anexo em alguma conta
            $bill = Bill::with(['orders' => function ($query) {
                $query->where('status', '!=', 'trash');
            },
          'orders.product', 'table', 'table.establishment', ])
          ->join('bill_attached_users', 'bills.id', '=', 'bill_attached_users.bill_id')
          ->where(['bill_attached_users.user_id' => $this->id])->whereIn('bills.status', ['opened', 'closing', 'disabled'])->orderBy('bills.id', 'DESC')->first();
        }

        if ($bill && $bill->spot && $loadExtract) {
            //Consulta extrato da conta no PDV e retorna atualizada
            DriverRaffinato::loadExtract($bill->id);
        }

        if (!$bill) {
            Log::info('Usuário '.$this->id.' não conseguiu encontrar sua conta aberta. Attached: '.$isAttached);
        }

        return $bill;
    }

    /**
     * Verifica se usuário possui preorder aberta.
     *
     * @return [type] [description]
     */
    public function getActivePreOrder()
    {
        $preOrder = PreOrder::with(['products', 'products.product', 'establishment'])->where(['user_id' => $this->id])->where(function ($q) {
            $q->whereIn('status', ['opened', 'preparation', 'finished']);
        })->first();

        return $preOrder;
    }

    /**
     * Verifica se usuário possui preorder aberta.
     *
     * @return [type] [description]
     */
    public function listActivePreOrders()
    {
        $preOrders = PreOrder::with(['products', 'products.product', 'establishment', 'establishment.config'])->where(['user_id' => $this->id])->where(function ($q) {
            $q->whereIn('status', ['opened', 'preparation', 'finished', 'deny']);
        })->orderBy('status', 'desc')->get();

        return $preOrders;
    }

    /**
     * Generate a new token.
     *
     * @return void
     */
    public function generateToken()
    {
        $this->user_tokens()->where('expires_at', '<=', Carbon::now())->delete();

        $token = new UserToken();
        $token->sequence = Str::random(32);
        $token->expires_at = Carbon::now()->addDays(730);
        $token->user_id = $this->id;
        $token->locale = ($this->country) ? $this->country : 'br';
        $token->save();

        $this->token = $token;

        return $token;
    }

    /**
     * Generate a new API Token.
     *
     * @return [type] [description]
     */
    public function generateApiToken()
    {
        $this->establishment->api_token()->delete();

        $ip = Request::capture()->ip();

        $token = new ApiToken();
        $token->sequence = Str::random(32);
        $token->expires_at = Carbon::now()->addDays(720);
        $token->user_id = $this->id;
        $token->establishment_id = $this->establishment_id;
        $token->locale = $ip ? $ip : '000.000.000.000';
        $token->save();

        return $token;
    }

    public function isManager()
    {
        return $this->profile == self::PROFILE_MANAGER;
    }

    public static function validaCPF($cpf = null)
    {
        // Verifica se um número foi informado
        if (empty($cpf)) {
            return false;
        }

        // Elimina possivel mascara
        $cpf = preg_replace('/[^0-9]/', '', $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se nenhuma das sequências invalidas abaixo
        // foi digitada. Caso afirmativo, retorna falso
        elseif ($cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999') {
            return false;
        // Calcula os digitos verificadores para verificar se o
         // CPF é válido
        } else {
            for ($t = 9; $t < 11; ++$t) {
                for ($d = 0, $c = 0; $c < $t; ++$c) {
                    $d += $cpf[$c] * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf[$c] != $d) {
                    return false;
                }
            }

            return true;
        }
    }

    public function toPublicArray()
    {
        $hasRating = $this->rating && ($this->bills->count() > 1 || $this->preOrders->count() > 1);

        return [
          'id' => $this->id,
          'name' => $this->name,
          'email' => $this->email,
          'email_account' => $this->email_account,
          'facebook_id' => $this->facebook_id,
          'google_id' => $this->google_id,
          'phone' => $this->phone,
          'cel' => $this->cel,
          'photo' => $this->photo,
          'cpf' => $this->cpf,
          'token' => $this->token,
          'first_access' => $this->first_access,
          'has_rating' => $hasRating,
        ];
    }
}
