<?php

namespace App\Models;

use App\Traits\StatusEnableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class Waiter extends Model
{
    use SoftDeletes;
    use StatusEnableTrait;

    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';

    public static $statusList = [
        self::STATUS_ENABLED => 'Ativado',
        self::STATUS_DISABLED => 'Desativado',
    ];

    protected $dates = ['deleted_at'];

    public function establishment()
    {
        return $this->belongsTo(Establishment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function avaliations()
    {
        return $this->hasMany(WaiterAvaliation::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function populate(Request $request, $establishment_id)
    {
        $user = $this->user;
        if (!$user) {
            $user = User::where(['email' => $this->defineUsername($request->name, $establishment_id), 'profile' => User::PROFILE_WAITER])->first();
            if (!$user) {
                $user = new User();
                $user->password = Hash::make($request->new_password);
                $user->email = $this->defineUsername($request->register, $establishment_id);
                $user->is_establishment = true;
            }
        } else {
            if (!empty($request->new_password)) {
                $user->password = Hash::make($request->new_password);
            }
        }

        //Salva usuário do Garçom
        $user->name = $request->name;
        $user->establishment_id = $establishment_id;
        $user->status = ($request->status == self::STATUS_ENABLED) ? self::STATUS_ENABLED : self::STATUS_DISABLED;
        $user->profile = User::PROFILE_WAITER;
        $user->save();

        //Salva os dados do Garçom
        $this->name = $request->name;
        $this->register = $request->register;
        $this->work_begin = $request->work_begin;
        $this->work_end = $request->work_end;
        $this->establishment_id = $establishment_id;
        $this->user_id = $user->id;
        $this->code = $request->code;
        $this->is_default = ($request->is_default == '1') ? 1 : 0;
        $this->status = ($request->status == self::STATUS_ENABLED) ? self::STATUS_ENABLED : self::STATUS_DISABLED;
        $this->save();

        //Caso seja definido garçom padrão, zera os outros
        if ($request->is_default == '1') {
            $this->clearAllDefaultWaiters($establishment_id, $this->id);
        }
    }

    protected function clearAllDefaultWaiters($establishment_id, $waiter_id)
    {
        $establishment = Establishment::find($establishment_id);
        $establishment->waiters()->where('id', '!=', $waiter_id)->update(['is_default' => false]);
    }

    public function defineUsername($name, $establishment_id)
    {
        $establishment = Establishment::find($establishment_id);
        $name = explode(' ', $name);
        $establishmentName = explode(' ', $establishment->name);
        $username = str_slug($name[0]).'@'.str_slug(join($establishmentName)).'.com';

        $i = 0;
        while (User::where(['email' => $username])->withTrashed()->count() > 0) {
            ++$i;
            $username = str_slug($name[0]).'-'.$i.'@'.str_slug(join($establishmentName)).'.com';
        }

        return $username;
    }
}
