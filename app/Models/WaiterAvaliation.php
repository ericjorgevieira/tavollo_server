<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WaiterAvaliation extends Model
{
    public function waiter(){
        return $this->belongsTo(Waiter::class);
    }
}
