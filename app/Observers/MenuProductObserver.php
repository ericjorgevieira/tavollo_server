<?php

namespace App\Observers;

use App\Models\MenuProduct;

class MenuProductObserver
{

  /**
  * Listen to the MenuProduct created event.
  *
  * @param  MenuProduct  $menuProduct
  * @return void
  */
  public function created(MenuProduct $menuProduct)
  {
    //
  }

  /**
  * Listen to the MenuProduct deleting event.
  *
  * @param  MenuProduct  $menuProduct
  * @return void
  */
  public function deleting(MenuProduct $menuProduct)
  {
    $count = $menuProduct->menu->countProducts();
    if($count == 0){
      $menu = $menuProduct->menu;
      $menu->status = 'disabled';
      $menu->save();
    }
  }

}
