<?php

namespace App\Policies;

use App\Models\Bill;
use App\Models\User;

class BillPolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function enable(User $user, Bill $bill)
    {
        return $user->establishment->id === $bill->spot->establishment_id;
    }

    public function edit(User $user, Bill $bill)
    {
        return $user->establishment->id === $bill->spot->establishment_id;
    }

    public function update(User $user, Bill $bill)
    {
        return $user->establishment->id === $bill->spot->establishment_id;
    }

    public function destroy(User $user, Bill $bill)
    {
        return $user->establishment->id === $bill->spot->establishment_id;
    }
}
