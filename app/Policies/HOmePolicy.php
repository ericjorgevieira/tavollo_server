<?php

namespace App\Policies;

use App\Models\Bill;
use App\Models\Establishment;
use App\Models\Order;
use Illuminate\Auth\Access\HandlesAuthorization;

class HomePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function addOrder(Establishment $establishment, Bill $bill)
    {
        return $establishment->id === $bill->spot->establishment_id;
    }

    public function cancelOrder(Establishment $establishment, Order $order)
    {
        return $establishment->id === $order->bill->spot->establishment_id;
    }
}
