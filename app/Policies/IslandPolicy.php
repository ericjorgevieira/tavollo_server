<?php

namespace App\Policies;

use App\Models\Island;
use App\Models\User;

class IslandPolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function enable(User $user, Island $island)
    {
        return $user->establishment->id === $island->establishment_id;
    }

    public function edit(User $user, Island $island)
    {
        return $user->establishment->id === $island->establishment_id;
    }

    public function update(User $user, Island $island)
    {
        return $user->establishment->id === $island->establishment_id;
    }

    public function destroy(User $user, Island $island)
    {
        return $user->establishment->id === $island->establishment_id;
    }
}
