<?php

namespace App\Policies;

use App\Models\Menu;
use App\Models\User;

class MenuPolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function enable(User $user, Menu $menu)
    {
        return $user->establishment->id === $menu->establishment_id;
    }

    public function edit(User $user, Menu $menu)
    {
        return $user->establishment->id === $menu->establishment_id;
    }

    public function update(User $user, Menu $menu)
    {
        return $user->establishment->id === $menu->establishment_id;
    }

    public function destroy(User $user, Menu $menu)
    {
        return $user->establishment->id === $menu->establishment_id;
    }
}
