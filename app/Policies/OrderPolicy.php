<?php

namespace App\Policies;


use App\Models\Order;
use App\Models\User;

class OrderPolicy
{

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function enable(User $user, Order $order)
    {
        return $user->establishment->id === $order->bill()->table()->establishment_id;
    }

    public function edit(User $user, Order $order)
    {
        return $user->establishment->id === $order->bill()->table()->establishment_id;
    }

    public function update(User $user, Order $order)
    {
        return $user->establishment->id === $order->bill()->table()->establishment_id;
    }

    public function destroy(User $user, Order $order)
    {
        return $user->establishment->id === $order->bill()->table()->establishment_id;
    }
}
