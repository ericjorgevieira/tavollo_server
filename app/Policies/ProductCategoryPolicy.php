<?php

namespace App\Policies;

use App\Models\ProductCategory;
use App\Models\User;

class ProductCategoryPolicy
{

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function enable(User $user, ProductCategory $category)
    {
        return $user->establishment->id === $category->establishment_id;
    }

    public function edit(User $user, ProductCategory $category)
    {
        return $user->establishment->id === $category->establishment_id;
    }

    public function update(User $user, ProductCategory $category)
    {
        return $user->establishment->id === $category->establishment_id;
    }

    public function destroy(User $user, ProductCategory $category)
    {
        return $user->establishment->id === $category->establishment_id;
    }
}
