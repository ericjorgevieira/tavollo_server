<?php

namespace App\Policies;

use App\Models\Product;
use App\Models\User;

class ProductPolicy
{

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function enable(User $user, Product $product)
    {
        return $user->establishment->id === $product->establishment_id;
    }

    public function edit(User $user, Product $product)
    {
        return $user->establishment->id === $product->establishment_id;
    }

    public function update(User $user, Product $product)
    {
        return $user->establishment->id === $product->establishment_id;
    }

    public function destroy(User $user, Product $product)
    {
        return $user->establishment->id === $product->establishment_id;
    }

    public function listgroups(User $user, Product $product)
    {
        return $user->establishment->id === $product->establishment_id;
    }

    public function listgroupitems(User $user, Product $product)
    {
        return $user->establishment->id === $product->establishment_id;
    }
}
