<?php

namespace App\Policies;

use App\Models\Sector;
use App\Models\User;

class SectorPolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function enable(User $user, Sector $sector)
    {
        return $user->establishment->id === $sector->establishment_id;
    }

    public function edit(User $user, Sector $sector)
    {
        return $user->establishment->id === $sector->establishment_id;
    }

    public function update(User $user, Sector $sector)
    {
        return $user->establishment->id === $sector->establishment_id;
    }

    public function destroy(User $user, Sector $sector)
    {
        return $user->establishment->id === $sector->establishment_id;
    }
}
