<?php

namespace App\Policies;

use App\Models\TableLocation;
use App\Models\User;

class TableLocationPolicy
{

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function enable(User $user, TableLocation $location)
    {
        return $user->establishment->id === $location->establishment_id;
    }

    public function edit(User $user, TableLocation $location)
    {
        return $user->establishment->id === $location->establishment_id;
    }

    public function update(User $user, TableLocation $location)
    {
        return $user->establishment->id === $location->establishment_id;
    }

    public function destroy(User $user, TableLocation $location)
    {
        return $user->establishment->id === $location->establishment_id;
    }
}
