<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Table;

class TablePolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function enable(User $user, Table $table)
    {
        return $user->establishment_id === $table->establishment_id;
    }

    public function edit(User $user, Table $table)
    {
        return $user->establishment_id === $table->establishment_id;
    }

    public function update(User $user, Table $table)
    {
        return $user->establishment_id === $table->establishment_id;
    }

    public function destroy(User $user, Table $table)
    {
        return $user->establishment_id === $table->establishment_id;
    }
}
