<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Storage;
use Symfony\Component\HttpFoundation\Request;

class UserGuard
{
    /**
     * Create a new authentication guard.
     */
    public function __construct(Request $request = null)
    {
        $this->session = session();
        $this->request = $request;
        $this->user = null;
    }

    public function attempt(array $credentials = [])
    {
        $credentials['profile'] = User::PROFILE_CUSTOMER;
        if (Auth::attempt($credentials)) {
            $this->user = Auth::user();

            return true;
        }

        return false;
    }

    /**
     * Attempt to authenticate a user using the given credentials.
     *
     * @param bool $login
     *
     * @return bool
     */
    public function attemptSocial(array $credentials = [], $login = true)
    {
        $param = null;
        $user = null;

        if (isset($credentials['facebook_id'])) {
            $param = 'facebook_id';
        } elseif (isset($credentials['google_id'])) {
            $param = 'google_id';
        }

        $credentials['profile'] = 'customer';
        $credentials['status'] = 'enabled';

        if ($param) {
            $user = User::where($param, $credentials[$param])->first();
        }

        if ($user) {
            if (@getimagesize($credentials['picture']) !== false) {
                $user->photo = $this->uploadUserPhoto($user->id, $credentials['picture']);
            } else {
                $user->photo = \Config::get('app.env').'/no-photo.png';
            }
            $user->save();
            $this->login($user, $login);

            return true;
        } else {
            $this->newUser($credentials, $param);

            return true;
        }

        return false;
    }

    /**
     * Log a user into the application.
     *
     * @param User $user
     * @param bool $login
     */
    public function login($user, $login = true)
    {
        $this->user = $user;

        if ($login) {
            $this->session->put($this->getName(), $user->id);
        }
    }

    /**
     * Add user with new credentials.
     *
     * @param $credentials
     */
    public function newUser($credentials, $type = 'facebook_id')
    {
        $user = new User();
        $user->name = $credentials['name'];
        $user->email_account = isset($credentials['email']) ? $credentials['email'] : null;
        $user->cpf = (isset($credentials['cpf'])) ? $credentials['cpf'] : null;

        $email = '';
        if (isset($type)) {
            if (isset($credentials['email'])) {
                $email = $type.'_'.$credentials['email'];
            } else {
                $email = $credentials[$type].'@'.$type.'.com';
            }
        } else {
            $email = $credentials['email'];
        }
        $user->email = $email;

        if ($type == 'facebook_id') {
            $user->facebook_id = $credentials[$type];
        } elseif ($type == 'google_id') {
            $user->google_id = $credentials[$type];
        }

        if (@getimagesize($credentials['picture']) !== false) {
            $user->photo = $this->uploadUserPhoto($email, $credentials['picture']);
        } else {
            $user->photo = \Config::get('app.url').'/no-photo.png';
        }

        $user->country = isset($credentials['locale']) ? $credentials['locale'] : 'br';
        $user->is_establishment = 0;
        $user->is_admin = 0;
        $user->profile = 'customer';
        $user->status = 'enabled';
        $user->password = (isset($credentials['password'])) ? bcrypt($credentials['password']) : bcrypt(\Carbon\Carbon::now());

        $user->save();
        $this->login($user);

        return $user;
    }

    /**
     * Save user photo.
     *
     * @param [type] $imageUrl [description]
     *
     * @return [type] [description]
     */
    private function uploadUserPhoto($user_id, $imageUrl)
    {
        $image = file_get_contents($imageUrl);
        $path = 'public/user_profile/'.sha1($user_id).'.jpg';
        $env = \Config::get('app.env');
        $urlProduction = ($env == 'homologation') ? 'https://test.admin.tavollo.com' : 'https://admin.tavollo.com';
        $url = ($env == 'local' || $env == 'testing') ? 'http://admin.tavollo.localhost:90' : $urlProduction;
        Storage::put($path, $image);

        return $url.Storage::url($path);
    }

    /**
     * Log a user out of the application.
     *
     * @return void
     */
    public function logout()
    {
        $this->session->remove($this->getName());
        $this->user = null;
    }

    /**
     * Check if user is logged in.
     *
     * @return bool
     */
    public function check()
    {
        return !is_null($this->user());
    }

    /**
     * Get the currently authenticated user.
     *
     * @return User
     */
    public function user()
    {
        if (!is_null($this->user)) {
            return $this->user;
        }

        $id = $this->session->get($this->getName());
        $user = null;

        if (!is_null($id)) {
            $user = User::find($id)->load('profile');
        }

        return $this->user = $user;
    }

    /**
     * Alias for user.
     *
     * @return User
     */
    public function get()
    {
        return $this->user();
    }

    /**
     * Get a unique identifier for the auth session value.
     *
     * @return string
     */
    private function getName()
    {
        return 'login_'.md5(get_class($this));
    }
}
