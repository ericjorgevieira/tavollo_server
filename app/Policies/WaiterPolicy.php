<?php

namespace App\Policies;

use App\Models\Waiter;
use App\Models\User;

class WaiterPolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function enable(User $user, Waiter $waiter)
    {
        return $user->establishment->id === $waiter->establishment_id;
    }

    public function edit(User $user, Waiter $waiter)
    {
        return $user->establishment->id === $waiter->establishment_id;
    }

    public function update(User $user, Waiter $waiter)
    {
        return $user->establishment->id === $waiter->establishment_id;
    }

    public function destroy(User $user, Waiter $waiter)
    {
        return $user->establishment->id === $waiter->establishment_id;
    }
}
