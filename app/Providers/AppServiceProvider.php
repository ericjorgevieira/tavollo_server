<?php

namespace App\Providers;

use App\Http\Validators\HashValidator;
use App\Models\MenuProduct;
use App\Observers\MenuProductObserver;
use App\Repositories\ProductRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Processa a lista de Produtos no modal
        view()->composer('layouts.plateau', function ($view) {
            $productRepository = new ProductRepository();
            $productRepository->setEstablishment(Auth::user()->establishment);
            $productsToOrder = $productRepository->forEstablishment();
            $sectors = $productRepository->listSectors();

            $view->with('productsToOrder', $productsToOrder);
            $view->with('sectors', $sectors);
            $view->with('establishment', Auth::user()->establishment);
            $view->with('establishmentConfigs', Auth::user()->establishment->config);
        });

        Validator::resolver(function ($translator, $data, $rules, $messages) {
            return new HashValidator($translator, $data, $rules, $messages);
        });

        MenuProduct::observe(MenuProductObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
