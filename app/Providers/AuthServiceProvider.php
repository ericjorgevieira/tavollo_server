<?php

namespace App\Providers;

use App\Models\Bill;
use App\Models\Island;
use App\Models\Menu;
use App\Models\Order;
use App\Policies\IslandPolicy;
use App\Policies\ProfilePolicy;
use App\Policies\SectorPolicy;
use App\Policies\WaiterPolicy;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Sector;
use App\Models\Table;
use App\Models\TableLocation;
use App\Policies\MenuPolicy;
use App\Policies\OrderPolicy;
use App\Policies\ProductPolicy;
use App\Policies\ProductCategoryPolicy;
use App\Policies\TablePolicy;
use App\Policies\TableLocationPolicy;
use App\Policies\BillPolicy;
use App\Models\User;
use App\Models\Waiter;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Menu::class => MenuPolicy::class,
        Order::class => OrderPolicy::class,
        Product::class => ProductPolicy::class,
        ProductCategory::class => ProductCategoryPolicy::class,
        TableLocation::class => TableLocationPolicy::class,
        Table::class => TablePolicy::class,
        Bill::class => BillPolicy::class,
        Sector::class => SectorPolicy::class,
        Waiter::class => WaiterPolicy::class,
        Island::class => IslandPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        //
    }
}
