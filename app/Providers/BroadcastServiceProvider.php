<?php

namespace App\Providers;

use App\Models\Bill;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Broadcast;

class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Broadcast::routes();

        /*
         * Authenticate the user's personal channel...
         */
        Broadcast::channel('preorder-created.{establishmentId}', function ($user, $establishmentId) {
            return md5($user->establishment_id) === $establishmentId;
        });
        Broadcast::channel('preorder-closed.{establishmentId}', function ($user, $establishmentId) {
            return md5($user->establishment_id) === $establishmentId;
        });
        Broadcast::channel('order-created.{establishmentId}', function ($user, $establishmentId) {
            return md5($user->establishment_id) === $establishmentId;
        });
        Broadcast::channel('order-canceled.{establishmentId}', function ($user, $establishmentId) {
            return md5($user->establishment_id) === $establishmentId;
        });
        Broadcast::channel('order-closed.{establishmentId}', function ($user, $establishmentId) {
            return md5($user->establishment_id) === $establishmentId;
        });
        Broadcast::channel('bill-created.{establishmentId}', function ($user, $establishmentId) {
            return md5($user->establishment_id) === $establishmentId;
        });
        Broadcast::channel('bill-renamed.{establishmentId}', function ($user, $establishmentId) {
            return md5($user->establishment_id) === $establishmentId;
        });
        Broadcast::channel('bill-closing.{establishmentId}', function ($user, $establishmentId) {
            return md5($user->establishment_id) === $establishmentId;
        });
        Broadcast::channel('bill-closed.{establishmentId}', function ($user, $establishmentId) {
            return md5($user->establishment_id) === $establishmentId;
        });
        Broadcast::channel('has-bill-changed.{billId}', function ($user, $billId) {
            return md5($user->establishment_id) === Bill::find($billId)->table->establishment_id;
        });
        Broadcast::channel('has-bill-closed.{billId}', function ($user, $billId) {
            return md5($user->establishment_id) === Bill::find($billId)->table->establishment_id;
        });
        Broadcast::channel('bill-attachment-request.{billId}', function ($user, $billId) {
            return md5($user->establishment_id) === Bill::find($billId)->table->establishment_id;
        });
        Broadcast::channel('has-attached.{userId}', function ($user, $userId) {
            return md5($user->id) === $userId;
        });
    }
}
