<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\OrderCreated' => [
            'App\Listeners\OrderCreatedListener',
        ],
        'App\Events\PreOrderCreated' => [
            'App\Listeners\PreOrderCreatedListener',
        ],
        'App\Events\BillCreated' => [
            'App\Listeners\BillCreatedListener',
        ],
        'App\Events\BillClosing' => [
            'App\Listeners\BillClosingListener',
        ],
        'App\Events\TableRequesting' => [
            'App\Listeners\TableRequestingListener',
        ],
        'App\Events\BillAttachmentRequesting' => [
            'App\Listeners\BillAttachmentRequestingListener',
        ],
        'App\Events\HasAttached' => [
            'App\Listeners\HasAttachedListener',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
