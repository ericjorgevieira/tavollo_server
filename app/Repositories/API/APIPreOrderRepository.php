<?php

namespace App\Repositories\API;

use App\Events\PreOrderCreated;
use App\Models\PreOrder;
use App\Traits\SetEstablishmentTrait;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class APIPreOrderRepository
{
    use SetEstablishmentTrait;

    /**
     * Lista de Pedidos, com relacionamento de Produtos, Categorias e Opções.
     *
     * @param [type] $status [description]
     *
     * @return [type] [description]
     */
    public function listPreOrders($status = null)
    {
        $preOrdersQuery = PreOrder::with(['products.product', 'products.items', 'products.items.item', 'products.items.group'])
            ->leftJoin('users', 'users.id', '=', 'pre_orders.user_id')
            ->where('pre_orders.status', '!=', PreOrder::STATUS_DENY)
            ->where('pre_orders.establishment_id', '=', $this->establishment->id)
            ->orderBy('pre_orders.status', 'ASC')
            ->orderBy('pre_orders.created_at', 'DESC');

        if ($status) {
            $preOrdersQuery = $preOrdersQuery->where('pre_orders.status', $status);
            if ($status == PreOrder::STATUS_TRASH) {
                $preOrdersQuery = $preOrdersQuery->orderBy('pre_orders.deleted_at', 'DESC');
            }
        }
        $preOrders = $preOrdersQuery->get(['pre_orders.*', 'users.name as user_name', 'users.cpf as user_cpf']);
        $preOrders = PreOrder::getPreOrdersWithGroupsToApi($preOrders, $this->establishment->id);

        // Log::info('Listando pedidos via API do estabelecimento ' . $this->establishment->id . ' com parâmetro status: ' . $status);
        return response($preOrders);
    }

    /**
     * Atualiza um determinado Pedido Pre Pago.
     *
     * @return [type] [description]
     */
    public function updatePreOrder(PreOrder $preorder, $status = 'opened', $external_id = null, $external_info = null)
    {
        if ($preorder && $preorder->establishment_id == $this->establishment->id) {
            if (array_key_exists($status, PreOrder::$statusList)) {
                $preorder->setStatus($status, $external_info);
                $this->broadcastPreOrderEvents($preorder);
                Log::info('Atualizando status via API de pedido pre pago '.$preorder->id.' para:'.$status);

                return response(['success' => true, 'message' => 'Pedido pre pago '.$preorder->id.' processado com sucesso!']);
            } else {
                Log::error('Erro ao tentar atualizar status de pedido pre pago '.$preorder->id);

                return response(['error' => true, 'message' => 'Erro ao definir status do pedido pre pago '.$preorder->id.': status '.$status.' não existe.']);
            }
        } else {
            Log::error('Erro ao tentar atualizar status de pedido pre pago '.$preorder->id);

            return response(['success' => false, 'message' => 'Pedido pre pago '.$preorder->id.' não encontrado no estabelecimento']);
        }
    }

    /**
     * Atualiza o status de vários pedidos Pre Pagos em uma mesma demanda.
     *
     * @param array $arrOrders [description]
     *
     * @return [type] [description]
     */
    public function updateManyPreOrders($arrOrders = null)
    {
        $arr = [];
        if ($arrOrders) {
            try {
                foreach ($arrOrders as $k => $o) {
                    if (isset($o['id'])) {
                        if (array_key_exists($o['status'], PreOrder::$statusList)) {
                            $preorder = PreOrder::where(['id' => $o['id']])->first();
                            if ($preorder && $preorder->establishment_id == $this->establishment->id) {
                                $preorder->status = $o['status'];
                                $preorder->external_id = isset($o['external_id']) ? $o['external_id'] : null;
                                if (isset($o['external_info'])) {
                                    $preorder->external_info = $o['external_info'];
                                }
                                $preorder->save();
                                $arr[$preorder->id] = ['success' => true, 'message' => 'Pedido pré-pago '.$preorder->id.' alterado com sucesso!'];
                                Log::info('Atualizando status via API de pedido pre pago '.$preorder->id.' para:'.$o['status']);
                                $this->broadcastPreOrderEvents($preorder);
                            } else {
                                $id = isset($o['id']) ? $o['id'] : $k.' - Sem Id';
                                $arr[$k] = ['error' => true, 'message' => 'Erro ao definir status do pedido pre pago '.$id.': Pedido não encontrado.'];
                            }
                        } else {
                            //Status não existe
                            $id = isset($o['id']) ? $o['id'] : $k.' - Sem Id';
                            $status = isset($o['status']) ? $o['status'] : 'undefined';
                            $arr[$k] = ['error' => true, 'message' => 'Erro ao definir status do pedido pre pago '.$id.': status '.$status.' não existe.'];
                        }
                    } else {
                        $arr[$k] = ['error' => true, 'message' => 'Erro ao definir status do pedido pre pago '.$k.': ID do pedido não definido.'];
                    }
                }
            } catch (\Exception $e) {
                Log::error('Erro ao processar alteração de vários pedidos pre pagos no estabelecimento '.$this->establishment->id.': '.$e->getMessage());
                $arr = ['error' => true, 'message' => 'Erro ao definir status do pedido pre pago. Erro de processamento.'];
            }

            return response(['process' => 'Pedidos pré-pagos alterados: '.count($arr), 'payload' => $arr]);
        }

        return response(['error' => true, 'message' => 'Você deve enviar o campo "preorders" com um array dos pedidos pré-pagos.']);
    }

    /**
     * Dispara os eventos de socket relacionados a um pedido.
     *
     * @param [type] $order [description]
     *
     * @return [type] [description]
     */
    private function broadcastPreOrderEvents(PreOrder $order)
    {
        broadcast(new PreOrderCreated($order));
    }
}
