<?php

namespace App\Repositories\API;

use App\Drivers\DriverRaffinato;
use App\Events\BillClosing;
use App\Events\BillCreated;
use App\Events\HasBillChanged;
use App\Events\HasClosedBill;
use App\Events\OrderCreated;
use App\Miscelaneous\Utils;
use App\Models\Bill;
use App\Models\Menu;
use App\Models\MenuProduct;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductGroup;
use App\Models\ProductGroupItem;
use App\Models\Table;
use App\Models\TableRequest;
use App\Models\User;
use App\Models\Waiter;
use App\Traits\SetEstablishmentTrait;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class APIRepository
{
    use SetEstablishmentTrait;

    /**
     * Autentica usuário na API.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function authUser(Request $request)
    {
        $arrResponse = [];
        $validator = Validator::make($request->all(), [
            'login' => 'required|max:255',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $arrResponse = ['success' => false, 'messages' => $validator->errors()];
        } else {
            $login = $request->input('login');
            $password = $request->input('password');
            if (Auth::attempt(['email' => $login, 'password' => $password, 'is_establishment' => 1, 'status' => 'enabled'])) {
                // Authentication passed...
                $user = Auth::user();
                if ($user->profile != 'manager' && $user->profile != 'waiter') {
                    $arrResponse = ['success' => false, 'messages' => ['Você não possui autorização para acessar.']];
                    Log::info('Usuário sem autorização tentou acessar via API. '.$user->id);
                } else {
                    $establishment = $user->establishment;
                    $ApiToken = $user->establishment->api_token;
                    $user->generateToken();
                    $AccessToken = $user->token;
                    $arrResponse = ['success' => true,
          'payload' => ['user' => $user, 'establishment' => $establishment,
          'api_token' => $ApiToken, '_token' => $AccessToken, 'hash' => md5($establishment->id), ], ];
                    Log::info('Usuário obteve acesso via API. '.$user->id);
                }
            } else {
                $arrResponse = ['success' => false, 'messages' => ['Login e/ou Senha não conferem.']];
            }
        }

        return response($arrResponse);
    }

    /**
     * Lista todas as requisiçÕes de Token do estabelecimento.
     *
     * @param [type] $status [description]
     *
     * @return [type] [description]
     */
    public function tableRequests($status = 'opened')
    {
        $requests = TableRequest::where([
            'establishment_id' => $this->establishment->id,
            'status' => $status,
        ])->get();

        return response($requests);
    }

    /**
     * Atualiza status de request de token.
     *
     * @param [type] $requestId [description]
     * @param string $status    [description]
     *
     * @return [type] [description]
     */
    public function updateTableRequest($requestId, $status = 'opened')
    {
        $request = TableRequest::where([
            'establishment_id' => $this->establishment->id,
            'id' => $requestId,
        ])->get()->first();
        if ($request) {
            $request->status = $status;
            $request->save();
            Log::info('Atualizando request de TOKEN com id:'.$request->id);

            return response(['success' => true, 'message' => 'Requisição de Token atualizada.', 'payload' => $request]);
        }

        return response(['success' => false, 'message' => 'Erro ao atualizar Requisição de Token']);
    }

    /**
     * Lista de Contas com relacionamento de Spots e Pedidos.
     *
     * @param [type] $bill_id [description]
     * @param [type] $status  [description]
     *
     * @return [type] [description]
     */
    public function listBills($bill_id = null, $status = null)
    {
        $billsQuery = Bill::with(['table', 'user'])
        ->join('tables', 'tables.id', '=', 'bills.table_id')
        ->join('establishment_configs', 'tables.establishment_id', '=', 'establishment_configs.establishment_id')
        ->where(['tables.establishment_id' => $this->establishment->id])
        ->orderBy('bills.status', 'ASC')
        ->orderBy('tables.number', 'ASC');

        if ($status && !empty($status) && $status != '') {
            $billsQuery = $billsQuery->where('bills.status', $status);
        } else {
            $billsQuery = $billsQuery->whereIn('bills.status', [Bill::STATUS_OPENED, Bill::STATUS_CLOSING]);
        }

        if ($bill_id) {
            //Carrega os dados do PDV
            DriverRaffinato::loadExtract($bill_id);
            $billsQuery = $billsQuery->where('bills.id', $bill_id);
        }

        $billsQuery->groupBy('bills.id');
        $bills = $billsQuery->get(['bills.*', 'tables.id as table_id', 'tables.name as table_name',
        'tables.number as table_number', 'tables.code as table_code',
        'tables.is_consumption_card as table_is_consumption_card', 'tables.alias as table_alias', 'tables.external_id as table_external_id',
        'establishment_configs.has_couvert', 'establishment_configs.couvert_price',
        'tables.slug as table_slug', 'establishment_configs.has_baksheesh', 'establishment_configs.baksheesh_percentage', ]);

        if ($bill_id && count($bills) == 1) {
            $bills = $bills->first();
            //Consulta extrato da conta no PDV e retorna atualizada
            $bills = $bills->toPublicArray();
        }
        // Log::info('Listando contas via API do estabelecimento ' . $this->establishment->id
        //   . ' com parâmetros bill_id: ' . $bill_id . ' e status: ' . $status);
        return response($bills);
    }

    /**
     * Lista de Pedidos, com relacionamento de Produtos, Categorias e Opções.
     *
     * @param [type] $bill_id [description]
     * @param [type] $status  [description]
     *
     * @return [type] [description]
     */
    public function listOrders($bill_id = null, $status = null)
    {
        $ordersQuery = Order::with(['product', 'product.productCategory', 'waiter', 'items', 'items.item', 'items.group'])
        ->join('bills', 'bills.id', '=', 'orders.bill_id')
        ->leftJoin('users', 'users.id', '=', 'bills.user_id')
        ->join('tables', 'tables.id', '=', 'bills.table_id')
        ->where(['tables.establishment_id' => $this->establishment->id])
        ->orderBy('orders.status', 'ASC')
        ->orderBy('orders.created_at', 'DESC');

        if ($status) {
            $ordersQuery = $ordersQuery->where(['orders.status' => $status]);
            if ($status == Order::STATUS_TRASH) {
                $ordersQuery = $ordersQuery->orderBy('orders.deleted_at', 'DESC');
            }
        } else {
            $ordersQuery->where('orders.status', '!=', Order::STATUS_DENY);
        }
        if ($bill_id) {
            $ordersQuery = $ordersQuery->where('orders.bill_id', $bill_id);
        } else {
            $ordersQuery->whereIn('bills.status', [Bill::STATUS_OPENED, Bill::STATUS_CLOSING])->withTrashed();
        }
        $orders = $ordersQuery->get([
            'orders.*', 'users.name as user_name', 'tables.id as table_id', 'tables.name as table_name',
            'tables.number as table_number', 'tables.slug as table_slug',
            'tables.alias as table_alias', 'tables.is_consumption_card as table_is_consumption_card',
            'tables.external_id as table_external_id',
        ]);
        $orders = Order::getOrdersWithGroupsToApi($orders);
        // Log::info('Listando pedidos via API do estabelecimento ' . $this->establishment->id . ' com parâmetros bill_id: ' . $bill_id . ' e status: ' . $status);
        return response($orders);
    }

    /**
     * Lista de Produtos, com Categorias e Opções.
     *
     * @param int    $product_id [description]
     * @param string $status     [description]
     *
     * @return json [description]
     */
    public function listProducts($product_id = null, $status = 'enabled')
    {
        $productsQuery = Product::where([
      'products.establishment_id' => $this->establishment->id,
      'products.not_sale' => false,
    ])
    ->orderBy('products.status', 'ASC')
    ->orderBy('products.name', 'ASC');
        if ($status) {
            $productsQuery = $productsQuery->where('products.status', $status);
        }
        if ($product_id) {
            $productsQuery->with(['productCategory']);
            $productsQuery = $productsQuery->where('products.id', $product_id);
            $products = $productsQuery->get(['products.*']);
        } else {
            $products = $productsQuery->join('product_categories', 'products.product_category_id', '=', 'product_categories.id')
        ->get(['products.id', 'products.code', 'products.name', 'product_categories.id as category_id', 'product_categories.name as category_name',
        'products.price', 'products.description', 'products.promo_price', 'products.is_promo_price', 'products.weight', 'products.status',
        'products.photo', 'products.enable_promo_schedule',
        'products.promo_price_begin_at', 'products.promo_price_end_at', 'products.promo_price_available_days', ]);
            $arrProducts = [];
            foreach ($products as $p) {
                $p->updateIfPromoPrice();
                $arrProducts[] = $p;
            }
            $products = $arrProducts;
        }

        return response($products);
    }

    /**
     * Lista de Garçons.
     *
     * @param [type] $waiter_code [description]
     * @param string $status      [description]
     *
     * @return [type] [description]
     */
    public function listWaiters($waiter_code = null, $status = 'enabled')
    {
        $waitersQuery = Waiter::where([
      'establishment_id' => $this->establishment->id,
    ])
    ->orderBy('status', 'ASC')
    ->orderBy('name', 'ASC');

        if ($status) {
            $waitersQuery = $waitersQuery->where('status', $status);
        }
        if ($waiter_code) {
            $waitersQuery = $waitersQuery->where(['code' => $waiter_code]);
        }
        $waiters = $waitersQuery->get();

        return response($waiters);
    }

    /**
     * Cria uma nova Conta em determinado Spot e retorna a mesma.
     *
     * @param [type] $user_name    [description]
     * @param [type] $table_number [description]
     *
     * @return [type] [description]
     */
    public function createBill($user_name, $table_number, $isConsumptionCard = false, $alias = '')
    {
        //Cria um Spot temporário, que deve ser desativado após encerrar conta
        $table = Table::where(['establishment_id' => $this->establishment->id,
    'number' => $table_number, 'is_consumption_card' => $isConsumptionCard, 'status' => Table::STATUS_ENABLED, ])->first();
        $arr = [];
        if ($table) {
            if (!$table->hasActiveBill()) {
                $table->alias = $alias;
                $table->save();
                $bill = Bill::openBill($table, null, ['username' => $user_name]);
                Log::info('Criação de Bill '.$bill->id.' com o Spot:'.$table->id);
                $arr = ['success' => true, 'message' => 'Conta criada com sucesso!', 'payload' => $bill];
            } else {
                if ($isConsumptionCard) {
                    $arr = ['success' => false, 'message' => 'Erro ao criar conta. Cartão em uso/não disponível.'];
                } else {
                    $arr = ['success' => false, 'message' => 'Erro ao criar conta. Mesa desativada/não disponível.'];
                }
                Log::info('Erro de conta aberta em mesa/cartão existente e ocupada:'.$table->id);
            }
        } else {
            if ($isConsumptionCard) {
                $arr = ['success' => false, 'message' => 'Erro ao criar conta. Cartão não existe.'];
            } else {
                $arr = ['success' => false, 'message' => 'Erro ao criar conta. Mesa não existe.'];
            }
            Log::info('Erro em conta aberta via API em mesa/cartão não existente. Nome do cliente: '.$user_name.
      ' e Número da mesa: '.$table_number.'. Solicitação feita por establishment:'.$this->establishment->id);
        }

        return response($arr);
    }

    /**
     * Migra conta temporária para usuário.
     *
     * @param [type] $originBillId  [description]
     * @param [type] $destinyBillId [description]
     *
     * @return [type] [description]
     */
    public function migrateBill($originBillId, $destinyBillId, $orders = [])
    {
        //Localiza a conta original
        $originBill = $this->getEstablishmentBill($originBillId);
        //localiza a conta nova
        $destinyBill = $this->getEstablishmentBill($destinyBillId);

        if ($originBill && $destinyBill) {
            $destinyBill = Bill::migrate($originBill, $destinyBill, $orders);
            $arr = ['success' => true, 'message' => 'Pedidos migrados com sucesso!'];
        } else {
            $arr = ['success' => false, 'message' => 'Erro ao migrar pedidos. Conta(s) inexistente(s).'];
        }

        return response($arr);
    }

    /**
     * Lista todos os Spots do estabelecimento.
     *
     * @param string $status [description]
     *
     * @return [type] [description]
     */
    public function listTables($status = 'enabled')
    {
        $tables = Table::where(['establishment_id' => $this->establishment->id, 'status' => $status])
    ->orderBy('number', 'ASC')->get(['tables.id', 'tables.name', 'tables.number', 'tables.code']);

        return response($tables);
    }

    /**
     * Retorna um Spot liberado, ou cria um novo.
     *
     * @param [type] $table_code [description]
     *
     * @return [type] [description]
     */
    public function getTable($table_code, $name = null, $isConsumptionCard = false)
    {
        $table = Table::where(['establishment_id' => $this->establishment->id,
    'number' => $table_code, 'is_consumption_card' => $isConsumptionCard, 'status' => Table::STATUS_ENABLED, ])->first();

        if ($table) {
            if (!$table->hasActiveBill()) {
                Log::info('Recuperando Spot com limitação não ocupada. '.$table->id);

                return response(['success' => true, 'message' => '', 'payload' => $table->toPublicArray()]);
            } else {
                Log::info('Erro de Spot com limitação ocupada. '.$table->id);

                return response(['success' => false, 'message' => 'Mesa ocupada.']);
            }
        } else {
            Log::info('Spot de código '.$table_code.' para estabelecimento '.$this->establishment->id.' não encontrada/desativada.');

            return response(['success' => false, 'message' => 'Mesa não existe/está desativada no estabelecimento.']);
        }

        return response(['success' => false, 'message' => 'Erro ao resgatar Spot. Já existe um Spot ocupado ou está desativado.']);
    }

    /**
     * Atualiza dados do Spot.
     *
     * @param [type] $number [description]
     * @param [type] $name   [description]
     * @param [type] $alias  [description]
     *
     * @return [type] [description]
     */
    public function updateTable($tableId, $number, $name, $alias, $isConsumptionCard = false, $status = 'enabled', $external_id = null)
    {
        if ($tableId) {
            $table = Table::where(['id' => $tableId, 'establishment_id' => $this->establishment->id])->first();
        } elseif ($external_id) {
            $table = Table::where(['establishment_id' => $this->establishment->id,
      'external_id' => $external_id, 'is_consumption_card' => $isConsumptionCard, 'status' => Table::STATUS_ENABLED, ])->first();
        } else {
            $table = Table::where(['establishment_id' => $this->establishment->id,
      'number' => $number, 'is_consumption_card' => $isConsumptionCard, 'status' => Table::STATUS_ENABLED, ])->first();
        }
        //Se o Spot existir e pertencer ao estabelecimento
        if ($table && $table->establishment->id == $this->establishment->id) {
            $table->name = ($name) ? $name : null;
            $table->alias = ($this->establishment->config->request_alias == 1) ? $alias : '';
            $table->status = ($status) ? $status : '';
            $table->save();

            $bill = $table->getActiveBill();
            if ($bill) {
                $this->broadcastBillEvents($bill);
            }
            // Log::info('Atualização de dados de Spot efetuado com sucesso:'.$table->id);

            return response(['success' => true, 'message' => 'Dados do Spot alterados com sucesso!', 'payload' => $table->toPublicArray()]);
        } else {
            $table = Table::getOrInsertNew($name, $number, $this->establishment->id, $isConsumptionCard, $external_id);
            // Log::info('Novo Spot inserido de id:'.$table->id.' com número '.$number.' para estabelecimento '.$this->establishment->id);

            return response(['success' => true, 'message' => 'Mesa/Cartão criado com sucesso!', 'payload' => $table->toPublicArray()]);
        }
    }

    public function updateManyTables($arrTables = null)
    {
        ini_set('max_execution_time', 180);
        $arr = [];
        $config = $this->establishment->config;
        if ($arrTables) {
            DB::beginTransaction();
            try {
                foreach ($arrTables as $t) {
                    $t['is_consumption_card'] = (isset($t['is_consumption_card'])) ? $t['is_consumption_card'] : false;

                    if (isset($t['id']) && !empty($t['id'])) {
                        $table = Table::where(['id' => $t['id'], 'establishment_id' => $this->establishment->id])->first();
                    } elseif (isset($t['external_id']) && !empty($t['external_id'])) {
                        $table = Table::where(['establishment_id' => $this->establishment->id,
                        'external_id' => $t['external_id'], 'number' => $t['number'], 'is_consumption_card' => $t['is_consumption_card'], ])->first();
                    } else {
                        $table = Table::where(['establishment_id' => $this->establishment->id,
                        'number' => $t['number'], 'is_consumption_card' => $t['is_consumption_card'], ])->first();
                    }

                    $status = (isset($t['status'])) ? ($t['status'] == Table::STATUS_ENABLED || $t['status'] == Table::STATUS_DISABLED) ? $t['status'] : Table::STATUS_ENABLED : Table::STATUS_ENABLED;

                    //Se o Spot existir e pertencer ao estabelecimento
                    if ($table && $table->establishment_id == $this->establishment->id) {
                        $table->name = (isset($t['name'])) ? $t['name'] : null;
                        // $table->alias = ($this->establishment->config->request_alias == 1 && isset($t['alias'])) ? $t['alias'] : '';
                        $table->status = $status;
                        $table->save();
                        $bill = $table->getActiveBill();
                        if ($bill) {
                            if ($status == Table::STATUS_DISABLED && $bill->status == Bill::STATUS_OPENED) {
                                $bill->status = Bill::STATUS_DISABLED;
                                $bill->save();
                            } elseif ($status == Table::STATUS_ENABLED && $bill->status == Bill::STATUS_DISABLED) {
                                $bill->status = Bill::STATUS_OPENED;
                                $bill->save();
                            }
                            $this->broadcastBillEvents($bill);
                        }
                        $arr[$t['number']] = ['success' => true, 'message' => 'Dados do Spot alterados com sucesso!', 'payload' => $table->toPublicArray()];
                    // Log::info('Atualização de dados de Spot efetuado com sucesso:'.$table->id);
                    } else {
                        //Caso não exista, insere um novo
                        $table = new Table();
                        $table->name = (isset($t['name'])) ? $t['name'] : null;
                        // $table->alias = ($config->request_alias == 1 && isset($t['alias'])) ? $t['alias'] : '';
                        $table->status = $status;
                        $table->is_consumption_card = (isset($t['is_consumption_card'])) ? $t['is_consumption_card'] : false;
                        $table->establishment_id = $this->establishment->id;
                        $table->external_id = isset($t['external_id']) ? $t['external_id'] : null;
                        $table->number = $t['number'];
                        $table->save();
                        // $table->generateNewCode();
                        $arr[$t['number']] = ['success' => true, 'message' => 'Spot inserido com sucesso!', 'payload' => $table->toPublicArray()];
                        // Log::info('Novo Spot inserido de id:'.$table->id.' com número '.$t['number'].' para estabelecimento '.$this->establishment->id);
                    }
                }

                DB::commit();

                return response($arr);
            } catch (\Exception $e) {
                DB::rollback();
                Log::error('Erro ao importar spots em massa: '.$e->getMessage());

                return response(['success' => false, 'message' => $e->getMessage()]);
            }
        } else {
            return response(['success' => false, 'message' => 'Nenhuma mesa/cartão enviado.']);
        }
    }

    /**
     * Define apenas o alias do Spot, para definir descrição do local.
     *
     * @param Table  $table [description]
     * @param string $alias [description]
     */
    public function setTableAlias(Table $table, $alias = '')
    {
        if ($table->establishment->id == $this->establishment->id) {
            $table->alias = $alias;
            $table->save();
            Log::info('Definindo alias para Table '.$table->id.' : '.$alias);

            return response(['success' => true, 'message' => 'Local do Spot alterado com sucesso!']);
        }
        Log::info('Erro ao definir alias para Table '.$table->id);

        return response(['success' => false, 'message' => 'Erro ao alterar dados do Spot.']);
    }

    /**
     * Atualiza uma determinada Conta.
     *
     * @param [type] $bill_id [description]
     * @param string $status  [description]
     *
     * @return [type] [description]
     */
    public function updateBill($bill_id, $status = 'opened', $has_sync = false)
    {
        $bill = $this->getEstablishmentBill($bill_id);
        if ($bill) {
            $bill->has_sync = $has_sync;
            switch ($status) {
                case Bill::STATUS_CLOSED:
                $bill->close();
                // no break
                default:
                $bill->status = $status;
                $bill->save();
                $this->broadcastBillEvents($bill);
            }
            Log::info('Atualizando status da conta '.$bill_id.' para:'.$status);
            $arr = ['success' => true, 'message' => 'Status de Conta alterado com sucesso', 'payload' => $bill];
        } else {
            Log::error('Erro ao tentar encontrar conta com id:'.$bill_id.' para atualizar.');
            $arr = ['success' => false, 'message' => 'Conta não encontrada.'];
        }

        return response($arr);
    }

    /**
     * Cria um novo Pedido em determinada Conta.
     *
     * @param [type] $bill_id [description]
     *
     * @return [type] [description]
     */
    public function createOrder($bill_id, $waiter_code, $product_id, $code, $quant = 1, $observation = '',
        $weight = null, $options = null, $status = null, $is_defined_price = false, $defined_price = 0.0, $external_id = null)
    {
        Log::info('Inserindo pedido na conta '.$bill_id);
        $arr = [];
        $bill = $this->getEstablishmentBill($bill_id);
        $product = $this->getEstablishmentProduct($product_id, $code);
        //Verifica se pedido já foi inserido anteriormente
        if ($external_id) {
            $externalOrder = null;
            if (($externalOrder = Order::hasExternalId($external_id, $this->establishment->id)) !== false) {
                Log::info('Pedido de ID externo '.$external_id.' já havia sido inserido antes no estabelecimento '.$this->establishment->id);

                return response(['success' => true, 'message' => 'Pedido já foi inserido com um ID externo anteriormente.',
         'payload' => $externalOrder, 'has_bill' => true, 'has_product' => true, ]);
            } else {
                Log::info('Inserindo ID externo de valor '.$external_id.' num pedido no estabelecimento '.$this->establishment->id);
            }
        }
        if (!$product) {
            Log::info('Tentativa de inserção de pedido na conta '.$bill_id.'sem sucesso pois o produto não existe.');
            $arr = ['success' => false, 'message' => 'Produto de código '.$code.' não existe no Tavollo.', 'has_product' => false];
        } else {
            if ($bill) {
                if ($bill->status != Bill::STATUS_CLOSED) {
                    $by_waiter = true;
                    $by_admin = false;
                    if (request()->attributes->get('user')->profile == User::PROFILE_WAITER) {
                        $waiter = Waiter::where(['user_id' => request()->attributes->get('user')->id])->first();
                    } else {
                        $waiter = Waiter::where(['code' => $waiter_code, 'establishment_id' => $this->establishment->id])->first();
                    }
                    if (!$waiter) {
                        $by_waiter = false;
                        $by_admin = true;
                    }
                    $order = $bill->addOrder($product, $quant, $observation, $weight, $options,
                        $status, $waiter, $by_admin, $by_waiter, null, $is_defined_price, $defined_price, $external_id
                    );
                    if ($order) {
                        $arr = ['success' => true, 'message' => 'Pedido adicionado com sucesso!', 'payload' => $order, 'has_bill' => true, 'has_product' => true];
                    } else {
                        Log::info('Tentativa de inserção de pedido na conta '.$bill_id.'sem sucesso.');
                        $arr = ['success' => false, 'message' => 'Erro ao adicionar pedido na conta '.$bill_id];
                    }
                } else {
                    Log::info('Tentativa de inserção de pedido na conta '.$bill_id.'sem sucesso pois a conta está encerrada.');
                    $arr = ['success' => false, 'message' => 'Conta já foi encerrada.', 'payload' => $bill, 'has_bill' => false, 'has_product' => true];
                }
            } else {
                Log::info('Erro ao tentar adicionar pedido na conta '.$bill_id);
                $arr = ['success' => false, 'message' => 'Produto ou Conta não encontrado(a).'];
            }
        }

        return response($arr);
    }

    /**
     * Atualiza um determinado Pedido.
     *
     * @return [type] [description]
     */
    public function updateOrder(Order $order, $status = 'opened', $external_id = null, $external_info = null)
    {
        if ($order && $order->bill->table->establishment_id == $this->establishment->id) {
            if (array_key_exists($status, Order::$statusList)) {
                $order->setStatus($status);
                if ($external_id) {
                    $order->external_id = $external_id;
                }
                if ($external_info) {
                    $order->external_info = $external_info;
                }
                $order->save();
                $this->broadcastOrderEvents($order);
                Log::info('Atualizando status via API de pedido '.$order->id.' para:'.$status);

                return response(['success' => true, 'message' => 'Pedido '.$order->id.' processado com sucesso!']);
            } else {
                Log::info('Erro ao tentar atualizar status de pedido '.$order->id);

                return response(['error' => true, 'message' => 'Erro ao definir status do pedido '.$order->id.': status '.$status.' não existe.']);
            }
        } else {
            Log::info('Erro ao tentar atualizar status de pedido '.$order->id);

            return response(['success' => false, 'message' => 'Pedido '.$order->id.' não encontrado no estabelecimento']);
        }
    }

    /**
     * Atualiza o status de vários pedidos em uma mesma demanda.
     *
     * @param array $arrOrders [description]
     *
     * @return [type] [description]
     */
    public function updateManyOrders($arrOrders = null)
    {
        ini_set('max_execution_time', 180);
        $arr = [];
        if ($arrOrders) {
            try {
                foreach ($arrOrders as $o) {
                    if (array_key_exists($o['status'], Order::$statusList)) {
                        $order = Order::where(['id' => $o['id']])->first();
                        if ($order && $order->bill->table->establishment_id == $this->establishment->id) {
                            $order->status = $o['status'];
                            if (isset($o['external_id'])) {
                                $order->external_id = $o['external_id'];
                            }
                            if (isset($o['external_info'])) {
                                $order->external_info = $o['external_info'];
                            }
                            $order->save();
                            $arr[$order->id] = ['success' => true, 'message' => 'Pedido '.$order->id.' alterado com sucesso!'];
                            Log::info('Atualizando status via API de pedido '.$order->id.' para:'.$o['status']);
                            $this->broadcastOrderEvents($order);
                        } else {
                            $arr[$o['id']] = ['error' => true, 'message' => 'Erro ao definir status do pedido '.$o['id'].': Pedido não encontrado.'];
                        }
                    } else {
                        //Status não existe
                        $arr[$o['id']] = ['error' => true, 'message' => 'Erro ao definir status do pedido '.$o['id'].': status '.$o['status'].' não existe.'];
                    }
                }
            } catch (\Exception $e) {
                Log::info('Erro ao processar alteração de vários pedidos no estabelecimento '.$this->establishment->id.': '.$e->getMessage());
                $arr[$o['id']] = ['error' => true, 'message' => 'Erro ao definir status do pedido '.$o['id'].': Erro de processamento.'];
            }

            return response(['process' => 'Pedidos alterados: '.count($arr), 'payload' => $arr]);
        }

        return response(['error' => true, 'message' => 'Você deve enviar o campo "orders" com um array dos pedidos.']);
    }

    /**
     * Insere/Atualiza um determinado Produto.
     *
     * @param [type] $product_id [description]
     *
     * @return [type] [description]
     */
    public function updateProduct(Request $request, $product_id = null)
    {
        $arr = [];
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            $arr = ['success' => false, 'message' => $validator->errors()];
        } else {
            if ($product_id) {
                $product = Product::firstOrNew(['id' => $product_id, 'establishment_id' => $this->establishment->id]);
            } elseif ($request->code) {
                $product = Product::firstOrNew(['code' => $request->code, 'establishment_id' => $this->establishment->id]);
            } else {
                $product = new Product();
            }

            $product->establishment_id = $this->establishment->id;
            $product->name = ($request->name) ? Utils::normalize($request->name) : null;
            $product->code = ($request->code) ? $request->code : null;
            $product->weight = ($request->weight) ? $request->weight : null;
            $product->description = ($request->description) ? $request->description : null;
            $product->price = ($request->price) ? $request->price : null;
            $product->promo_price = ($request->promo_price) ? $request->promo_price : $request->price;
            $product->status = ($request->status) ? $request->status : 'disabled';
            $product->is_promo_price = ($request->is_promo_price) ? $request->is_promo_price : 0;
            $product->not_sale = ($request->not_sale) ? $request->not_sale : false;
            // $product->external_id = $request->external_id;

            if ($product_id == null) {
                $product->available_days = 'a:7:{i:0;s:6:"monday";i:1;s:7:"tuesday";i:2;s:9:"wednesday";i:3;s:8:"thursday";i:4;s:6:"friday";i:5;s:8:"saturday";i:6;s:6:"sunday";}';
                $product->first_turn_end = '23:59:59';
                $product->second_turn_end = '23:59:59';
            }

            $product->save();
            $product->updateIfPromoPrice();
            Product::clearCache($this->establishment->id);

            if ($request->groups) {
                $this->updateProductGroups($product, $request->groups);
            }

            if ($request->category) {
                $category = ProductCategory::where(['slug' => str_slug($request->category), 'establishment_id' => $this->establishment->id])->get()->first();
                if (!$category) {
                    $category = ProductCategory::insertCategory($request->category, 'enabled', $this->establishment->id);
                }
                $product->product_category_id = $category->id;
                $product->save();
            }

            if ($request->menu) {
                $menu = Menu::where(['establishment_id' => $this->establishment->id, 'slug' => str_slug($request->menu)])->get()->first();
                if (!$menu) {
                    $menu = Menu::insertMenu($request->menu, 'enabled', $this->establishment->id);
                }
                $this->deleteMenuProducts($product->id);
                $menu->products()->attach($product->id);
            }

            // Log::info('Produto editado. Id:'.$product->id.' e external id: '.$product->external_id);
            $arr = ['success' => true, 'message' => 'Produto inserido/editado com sucesso.', 'payload' => $product];
        }

        return response($arr);
    }

    /**
     * Iteração para inserir vários produtos.
     *
     * @param [type] $arrProducts [description]
     *
     * @return [type] [description]
     */
    public function insertManyProducts($arrProducts = null)
    {
        ini_set('max_execution_time', 180);
        //Serializa todos os registros enviados
        // $this->establishment->config->external_products_cache = serialize($arrProducts);
        // $this->establishment->config->save();

        $arr = [];
        if ($arrProducts) {
            DB::beginTransaction();
            try {
                Log::info('Inserindo produtos via importação.');

                $this->disableProducts($arrProducts);

                $config = $this->establishment->config;

                foreach ($arrProducts as $p) {
                    //Se produto for encontrado de alguma forma, pula a carga enviada
                    if (isset($p['product_id']) && !empty($p['product_id'])) {
                        $product = Product::where(['id' => $p['product_id'], 'establishment_id' => $this->establishment->id])->first();
                        if ($product) {
                            $product->status = (isset($p['status'])) ? $p['status'] : Product::STATUS_ENABLED;
                            $product->default_observations = (isset($p['observations'])) ? $p['observations'] : [];
                            if ($config->enable_price_update) {
                                $product->price = (isset($p['price'])) ? $p['price'] : $product->price;
                                $product->promo_price = (isset($p['promo_price'])) ? $p['promo_price'] : $product->price;
                            }
                            $product->save();
                            if (isset($p['groups'])) {
                                $this->updateProductGroups($product, $p['groups']);
                            }
                            $arr[$p['code']] = ['success' => true, 'message' => 'Produto atualizado com sucesso.', 'payload' => $product->id];
                            continue;
                        }
                    } elseif (isset($p['code']) && !empty($p['code'])) {
                        $product = Product::where(['code' => $p['code'], 'establishment_id' => $this->establishment->id])->first();
                        if ($product) {
                            $product->status = (isset($p['status'])) ? $p['status'] : Product::STATUS_ENABLED;
                            $product->default_observations = (isset($p['observations'])) ? $p['observations'] : [];
                            if ($config->enable_price_update) {
                                $product->price = (isset($p['price'])) ? $p['price'] : $product->price;
                                $product->promo_price = (isset($p['promo_price'])) ? $p['promo_price'] : $product->price;
                            }
                            $product->save();
                            if (isset($p['groups'])) {
                                $this->updateProductGroups($product, $p['groups']);
                            }
                            $arr[$p['code']] = ['success' => true, 'message' => 'Produto atualizado com sucesso.', 'payload' => $product->id];
                            continue;
                        }
                    }
                    //Se produto não for encontrado, insere um novo
                    $product = new Product();
                    $product->establishment_id = $this->establishment->id;
                    $product->name = (isset($p['name'])) ? Utils::normalize($p['name']) : null;
                    $product->code = (isset($p['code'])) ? $p['code'] : null;
                    $product->weight = (isset($p['weight'])) ? $p['weight'] : null;
                    $product->description = (isset($p['description'])) ? $p['description'] : null;
                    $product->price = (isset($p['price'])) ? $p['price'] : null;
                    $product->promo_price = (isset($p['promo_price'])) ? $p['promo_price'] : $p['price'];
                    $product->status = (isset($p['status'])) ? $p['status'] : Product::STATUS_DISABLED;
                    $product->is_promo_price = (isset($p['is_promo_price'])) ? $p['is_promo_price'] : 0;
                    $product->not_sale = (isset($p['not_sale'])) ? $p['not_sale'] : false;
                    $product->default_observations = (isset($p['observations'])) ? $p['observations'] : [];
                    $product->preparation_time = (isset($p['preparation_time'])) ? $p['preparation_time'] : '00:00:00';

                    //Apenas se o produto for novo
                    if (!isset($p['product_id']) && !isset($p['code'])) {
                        $product->available_days = 'a:7:{i:0;s:6:"monday";i:1;s:7:"tuesday";i:2;s:9:"wednesday";i:3;s:8:"thursday";i:4;s:6:"friday";i:5;s:8:"saturday";i:6;s:6:"sunday";}';
                        $product->first_turn_end = '23:59:59';
                        $product->second_turn_end = '23:59:59';
                    }

                    $product->save();
                    $product->updateIfPromoPrice();
                    // Product::clearCache($this->establishment->id);
                    if (isset($p['groups'])) {
                        $this->updateProductGroups($product, $p['groups']);
                    }

                    if (isset($p['category'])) {
                        $category = ProductCategory::where(['slug' => str_slug($p['category']), 'establishment_id' => $this->establishment->id])->get()->first();
                        if (!$category) {
                            $category = ProductCategory::insertCategory($p['category'], 'enabled', $this->establishment->id);
                        }
                        $product->product_category_id = $category->id;
                        $product->save();
                    }

                    if (isset($p['menu'])) {
                        $menu = Menu::where(['establishment_id' => $this->establishment->id, 'slug' => str_slug($p['menu'])])->get()->first();
                        if (!$menu) {
                            $menu = Menu::insertMenu($p['menu'], 'enabled', $this->establishment->id);
                        }
                        $this->deleteMenuProducts($product->id);
                        $menu->products()->attach($product->id);
                    }
                    // Log::info('Produto inserido. Id:'.$product->id.' de id externo: '.$product->code);
                    $arr[$p['code']] = ['success' => true, 'message' => 'Produto inserido com sucesso.', 'payload' => $product->id];
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                Log::info('Erro ao processar inserção de vários produtos no estabelecimento '.$this->establishment->id.': '.$e->getMessage());
                $arr[] = ['success' => false, 'message' => $e->getMessage()];
            }
        } else {
            $arr = ['success' => false, 'message' => 'Nenhum produto enviado.'];
        }

        return response($arr);
    }

    /**
     * Atualiza agrupamento de itens obrigatórios de um produto.
     */
    private function updateProductGroups($product, $groups)
    {
        //Apaga produtos que não estão mais relacionados
        $product->clearGroupItems($groups);

        //Insere/atualiza produtos relacionados
        foreach ($groups as $group) {
            $productGroup = ProductGroup::where(['establishment_id' => $this->establishment->id,
                'external_id' => $group['external_id'], 'product_id' => $product->id, ])->first();
            if (!$productGroup) {
                $productGroup = new ProductGroup();
                $productGroup->product_id = $product->id;
                $productGroup->external_id = (isset($group['external_id'])) ? $group['external_id'] : null;
                $productGroup->establishment_id = $this->establishment->id;
            }
            $productGroup->name = (isset($group['name'])) ? $group['name'] : '';
            $productGroup->description = (isset($group['description'])) ? $group['description'] : '';
            $productGroup->min = (isset($group['min'])) ? $group['min'] : 0;
            $productGroup->max = (isset($group['max'])) ? $group['max'] : 1;
            $productGroup->formula = (isset(ProductGroup::$formulaKeys[$group['formula']])) ? ProductGroup::$formulaKeys[$group['formula']] : 'sum_all';
            $productGroup->status = (isset($group['status'])) ? $group['status'] : 'disabled';
            $productGroup->save();

            foreach ($group['items'] as $item) {
                $groupItem = ProductGroupItem::where(['establishment_id' => $this->establishment->id,
                    'external_id' => $item['external_id'], 'product_group_id' => $productGroup->id, ])->first();
                if (!$groupItem) {
                    $groupItem = new ProductGroupItem();
                    $groupItem->product_group_id = $productGroup->id;
                    $groupItem->external_id = (isset($item['external_id'])) ? $item['external_id'] : null;
                    $groupItem->group_composition_id = (isset($item['group_composition_id'])) ? $item['group_composition_id'] : null;
                    $groupItem->establishment_id = $this->establishment->id;
                }
                $groupItem->name = (isset($item['name'])) ? $item['name'] : '';
                $groupItem->description = (isset($item['description'])) ? $item['description'] : null;
                $groupItem->price = (isset($item['price'])) ? $item['price'] : 0.0;
                $groupItem->status = (isset($item['status'])) ? $item['status'] : 'disabled';
                $groupItem->save();
            }
        }
    }

    /**
     * Remove produto de um menu.
     */
    private function deleteMenuProducts($id)
    {
        $menuProducts = MenuProduct::where(['product_id' => $id])->get();
        foreach ($menuProducts as $mp) {
            $mp->delete();
        }
    }

    /**
     * Desativa os produtos cadastrados que não vieram na carga da importação.
     */
    private function disableProducts($arrProducts)
    {
        $productsExternalIds = array_column($arrProducts, 'code');
        Product::where(['establishment_id' => $this->establishment->id])->whereNotIn('code', $productsExternalIds)
            ->update(['status' => Product::STATUS_DISABLED]);
    }

    /**
     * Dispara os eventos de socket relacionados a uma conta.
     *
     * @param Bill $bill
     *
     * @return void
     */
    private function broadcastBillEvents($bill)
    {
        broadcast(new BillCreated($bill));
        broadcast(new BillClosing($bill));
        broadcast(new HasClosedBill($bill));
    }

    /**
     * Dispara os eventos de socket relacionados a um pedido.
     *
     * @param Order $order
     *
     * @return void
     */
    private function broadcastOrderEvents($order)
    {
        broadcast(new OrderCreated($order));
        broadcast(new HasBillChanged($order->bill));
    }
}
