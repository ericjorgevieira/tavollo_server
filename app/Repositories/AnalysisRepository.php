<?php
/**
 * Created by PhpStorm.
 * User: ericoliveira
 * Date: 11/07/16
 * Time: 17:53
 */

namespace App\Repositories;

use App\Models\Establishment;
use App\Models\Product;
use App\Traits\SetEstablishmentTrait;
use App\Models\Table;
use App\Models\Bill;
use App\Models\User;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class AnalysisRepository
{

    use SetEstablishmentTrait;

    protected $bills;

    /**
     * Retorna o tempo médio de atendimento
     */
    public function averageService()
    {
        //Pega todas as contas abertas e encerradas no estabelecimento
        $bills = $this->getBills();

        $arrTime = array();

        foreach ($bills as $b) {
            $openedAt = new Carbon($b->getAttributes()['opened_at']);
            $closedAt = new Carbon($b->getAttributes()['closed_at']);
            $diff = $openedAt->diffInMinutes($closedAt);
            $arrTime[] = $diff;

        }
        $mediaTime = (count($arrTime)) ? (array_sum($arrTime)) / count($arrTime) : 0;
        $hours = intval($mediaTime / 60);
        $minutes = ($mediaTime % 60);

        return array(
            'hours' => $hours,
            'minutes' => $minutes
        );
    }

    /**
     * Retorna o consumo médio
     */
    public function getConsumption()
    {
        $totalBills = $this->totalBills();

        $orders = Order::query()
            ->select(DB::raw('orders.id, orders.product_id, orders.cache_price, products.calculate_baksheesh as calculate_baksheesh'))
            ->join('bills', function($join){
              $join->on('orders.bill_id', 'bills.id');
            })
            ->join('tables', function($join){
              $join->on('tables.id', 'bills.table_id');
            })
            ->join('products', function($join){
              $join->on('orders.product_id', 'products.id');
            })
            ->where('tables.establishment_id', '=', $this->establishment->id)
            ->where('orders.status', '!=', Order::STATUS_DENY)
            ->where('orders.status', '!=', Order::STATUS_TRASH)
            ->get();
        $total = 0.0;

        foreach ($orders as $order) {
          if($order->calculate_baksheesh === 1){
            $total += $order->cache_price;
          }
        }
        $total = floor($total*100)/100;
        $mediaConsumption = ($totalBills > 0) ? $total / $totalBills : 0;

        return array(
            'total' => number_format($total, 2, ',', '.'),
            'average' => number_format($mediaConsumption, 2, ',', '.')
        );
    }

    /**
     * Retorna o total de contas abertas em um determinado período
     */
    public function totalBills()
    {
        return count($this->getBills());
    }

    /**
     * Retorna o total de usuários em um determinado período
     */
    public function totalUsers()
    {
        $users = User::query()
            ->join('bills', function ($join) {
                $join->on('users.id', 'bills.user_id');
            })
            ->join('tables', function ($join) {
                $join->on('tables.id', 'bills.table_id');
            })
            ->groupBy('users.id')
            ->where('tables.establishment_id', '=', $this->establishment->id)
            ->get(['users.id', 'users.name', 'users.email']);

        return count($users);

    }

    /**
     * Retorna a lista de produtos mais vendidos
     */
    public function bestSellerProducts($category_id = null, $limit = 10)
    {
        $productsQuery = Product::query()
            ->select(DB::raw('COUNT(*) as count, products.*'))
            ->join('orders', function($join){
                $join->on('orders.product_id', 'products.id');
            })
            ->join('bills', function($join){
                $join->on('orders.bill_id', 'bills.id');
            })
            ->join('tables', function($join){
                $join->on('tables.id', 'bills.table_id');
            })
            ->where('tables.establishment_id', '=', $this->establishment->id)
            ->groupBy('products.id')
            ->orderBy('count', 'DESC')
            ->limit($limit);

        if($category_id){
            $productsQuery->where('products.category_id', '=', $category_id);
        }

        $products = $productsQuery->get();

        return $products;
    }

    public function topUsers($limit = 10){

        $users = User::query()
            ->select(DB::raw('COUNT(bills.id) as count, users.*'))
            ->join('bills', function($join){
                $join->on('bills.user_id', 'users.id');
            })
            ->join('tables', function($join){
                $join->on('tables.id', 'bills.table_id');
            })
            ->where('tables.establishment_id', '=', $this->establishment->id)
            ->groupBy('users.id')
            ->orderBy('count', 'DESC')
            ->limit($limit)
            ->get();

        return $users;

    }

    /**
     * Retorna as contas de acordo com um determinado período
     * @param null $begin_at
     * @param null $end_at
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function getBills($begin_at = null, $end_at = null)
    {
      if(!$this->bills){
        $billsQuery = Bill::with(['table'])
            ->join('tables', function ($join) {
                $join->on('bills.table_id', 'tables.id');
            })
            ->where(['tables.establishment_id' => $this->establishment->id])
            ->where('bills.closed_at', '!=', '0000-00-00 00:00:00')
            ->where('bills.status', '=', 'closed')
            ->groupBy('bills.id');

        if ($begin_at) {
            $billsQuery = $billsQuery->where('bills.opened_at', '>', new Carbon($begin_at));
        }
        if ($end_at) {
            $billsQuery = $billsQuery->where('bills.closed_at', '>', new Carbon($end_at));
        }

        $bills = $billsQuery->get(['bills.id', 'bills.opened_at', 'bills.closed_at', 'bills.status', 'bills.table_id']);
        $this->bills = $bills;

      }

      return $this->bills;
    }
}
