<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 11/07/16
* Time: 17:53.
*/

namespace App\Repositories;

use App\Models\Bill;
use App\Drivers\DriverRaffinato;
use App\Events\BillClosing;
use App\Events\BillCreated;
use App\Events\BillRenamed;
use App\Events\HasBillChanged;
use App\Models\LogActivity;
use App\Models\Order;
use App\Models\Product;
use App\Models\Table;
use App\Traits\SetEstablishmentTrait;
use App\Models\Waiter;
use Illuminate\Http\Response;

class BillRepository
{
    use SetEstablishmentTrait;

    /**
     * Get list of bills from table.
     *
     * @return mixed
     */
    public function forTable(Table $table)
    {
        return $table->bills()
        ->orderBy()
        ->get();
    }

    public function forEstablishment($trash = false)
    {
        $establishment = $this->establishment;
        $billQuery = Bill::with(['table' => function ($query) use ($establishment) {
            $query->where('establishment_id', $establishment->id);
        }, 'user' => function ($query) {
        }]);
        if ($trash) {
            $billQuery->where(['status' => 'trash']);
        } else {
            $billQuery->where(['status' => 'opened'])
            ->orWhere(['status' => 'closing'])
            ->orWhere(['status' => 'closed']);
        }

        return $billQuery->orderBy('id', 'DESC')->get();
    }

    /**
     * Lista todas as contas (Bills) ativas no Dashboard.
     *
     * @param string $status
     *
     * @return Response
     */
    public function listBills($status = 'opened')
    {
        $bills = Bill::with(['table', 'user'])
        ->join('tables', 'tables.id', '=', 'bills.table_id')
        ->where([
            'tables.establishment_id' => $this->establishment->id,
            'bills.status' => $status,
        ])
        ->orderBy('bills.status', 'ASC')
        ->orderBy('tables.name', 'ASC')
        ->get(['bills.*']);

        return response($bills);
    }

    /**
     * Cria uma conta temporária.
     *
     * @param [type] $user_name [description]
     *
     * @return [type] [description]
     */
    public function createBill($user_name, $table_id)
    {
        $table = Table::where(['establishment_id' => $this->establishment->id, 'id' => $table_id,
      'status' => Table::STATUS_ENABLED, ])->first();
        if ($table) {
            if (!$table->hasActiveBill()) {
                $bill = Bill::openBill($table, null, ['username' => $user_name]);
                LogActivity::registerActivity(LogActivity::ACTION_INSERT, 'Abertura de comanda '.$bill->id.' realizada.', $this->establishment->id);
                $arr = ['success' => true, 'message' => 'Conta criada com sucesso!', 'payload' => $bill];
            } else {
                $arr = ['success' => false, 'message' => 'Erro ao criar conta. Mesa/Cartão não disponível.'];
            }
        } else {
            $arr = ['success' => false, 'message' => 'Erro ao criar conta. Mesa não disponível.'];
        }

        return response($arr);
    }

    /**
     * Migra conta temporária para usuário.
     *
     * @param [type] $originBillId  [description]
     * @param [type] $destinyBillId [description]
     *
     * @return [type] [description]
     */
    public function migrateBill($originBillId, $destinyBillId)
    {
        if ($this->establishment->config->enable_advanced_functions == 1) {
            //Localiza a conta original
            $originBill = Bill::find($originBillId);
            $destinyBill = Bill::find($destinyBillId);

            if ($originBill->spot->establishment_id == $this->establishment->id &&
        $destinyBill->spot->establishment_id == $this->establishment->id) {
                $destinyBill = Bill::migrate($originBill, $destinyBill);

                return response(['success' => true, 'message' => 'Pedidos migrados com sucesso!', 'payload' => $destinyBill]);
            }

            return response(['success' => false, 'message' => 'Erro ao migrar pedidos. Conta(s) inexistente(s).']);
        }

        return response(['success' => false, 'message' => 'Função desabilitada.']);
    }

    /**
     * Resgata os dados da conta (Bill) para o Dashboard.
     *
     * @param $bill_id
     *
     * @return Response
     */
    public function getBill($bill_id)
    {
        //Consulta extrato da conta no PDV e sincroniza
        DriverRaffinato::loadExtract($bill_id);

        $bill = Bill::with(['table'])
        ->join('tables', function ($join) {
            $join->on('bills.table_id', 'tables.id');
        })
        ->where([
            'tables.establishment_id' => $this->establishment->id,
            'bills.id' => $bill_id, ])->get(['bills.*'])->first();

        return response($bill->toPublicArray());
    }

    /**
     * Modifica o nome da mesa para facilitar identificação.
     *
     * @param $bill_id
     * @param null $name
     *
     * @return Response
     */
    public function changeTableName($bill_id, $name = null)
    {
        $bill = Bill::find($bill_id);
        if ($bill && $bill->spot->establishment_id == $this->establishment->id) {
            $table = $bill->spot;
            $table->name = trim(strip_tags($name));
            $table->save();
            // $table->changeNumber($number, $this->establishment->id);

            broadcast(new BillRenamed($bill));
            broadcast(new HasBillChanged($bill));

            $arrResponse = ['success' => true, 'message' => 'Nome da mesa alterado com sucesso!'];
        } else {
            $arrResponse = ['error' => true, 'message' => 'Erro ao mudar nome da mesa'];
        }

        return response($arrResponse);
    }

    /**
     * Altera a mesa da conta.
     *
     * @param $bill_id
     * @param $table_id
     *
     * @return Response
     */
    public function changeBillTable($bill_id, $table_id)
    {
        $table = Table::find($table_id);
        $bill = Bill::find($bill_id);

        if ($table && $bill && $bill->spot->establishment_id == $this->establishment->id
        && $table->establishment_id = $this->establishment->id) {
            if (!$table->hasActiveBill()) {
                $bill->table_id = $table->id;
                $bill->save();
                $arrResponse = ['success' => true, 'message' => 'Mesa da conta alterada com sucesso!'];
            } else {
                $arrResponse = ['error' => true, 'message' => 'Mesa já está ocupada'];
            }
        } else {
            $arrResponse = ['error' => true, 'message' => 'Erro ao mudar mesa da conta.'];
        }

        return response($arrResponse);
    }

    /**
     * Adiciona um pedido (Order) a uma conta (Bill), calculando o valor do pedido por peso ou quantidade.
     *
     * @param $product_id
     * @param $quant
     *
     * @return Response
     */
    public function addOrderToBill(
        Bill $bill,
        $product_id,
        $quant,
        $obs = '',
        $weight = null,
        $options = null,
        $waiter_id = null
    ) {
        if ($bill && $bill->spot->establishment_id == $this->establishment->id && $bill->status == Bill::STATUS_OPENED) {
            $product = Product::find($product_id);
            $waiter = Waiter::find($waiter_id);
            $bill->addOrder($product, $quant, $obs, $weight, $options, null, $waiter_id, true);
            LogActivity::registerActivity(LogActivity::ACTION_INSERT, 'Adicionando produto '.$product_id.' na conta '.$bill->id.'.', $this->establishment->id);
            $arrResponse = ['success' => true, 'message' => 'Pedido adicionado com sucesso!'];
        } else {
            $arrResponse = ['error' => true, 'message' => $bill->status == Bill::STATUS_DISABLED ? 'Não é possível adicionar pedido. Conta bloqueada.' : 'Erro ao adicionar pedido'];
        }

        return response($arrResponse);
    }

    /**
     * Encerra uma conta (Bill) pelo Dashboard.
     *
     * @return Response
     */
    public function closeBill(Bill $bill)
    {
        if ($bill && $bill->spot->establishment_id == $this->establishment->id) {
            //Encerra todos os pedidos da conta
            $bill->close();
            LogActivity::registerActivity(LogActivity::ACTION_UPDATE, 'Encerrando comanda '.$bill->id.'.', $this->establishment->id);
            $arr = ['success' => true, 'message' => 'A conta foi encerrada com sucesso.'];
        } else {
            $arr = ['error' => true, 'message' => 'Erro ao encerrar conta.'];
        }

        return response($arr);
    }

    /**
     * Atualiza o status da conta.
     */
    public function updateBill(Bill $bill, $status = 'opened')
    {
        if ($bill && $bill->spot->establishment_id == $this->establishment->id) {
            //Encerra todos os pedidos da conta
            if ($status == Bill::STATUS_CLOSED) {
                $bill->close();
                $arr = ['success' => true, 'message' => 'O status da conta foi alterado com sucesso.'];
            }
            //Verifica se mesa da conta não possui outra conta em aberto
            if (($status == Bill::STATUS_OPENED && !$bill->spot->hasActiveBill()) || ($status == Bill::STATUS_OPENED && $bill->status == Bill::STATUS_CLOSING) ||
            ($status == Bill::STATUS_DISABLED && $bill->status == Bill::STATUS_OPENED)) {
                $bill->status = $status;
                $arr = ['success' => true, 'message' => 'O status da conta foi alterado com sucesso.'];
            } else {
                $arr = ['success' => false, 'message' => 'Mesa/Cartão já possui conta em aberto.'];
            }
            $bill->save();
            LogActivity::registerActivity(LogActivity::ACTION_UPDATE, 'Alterando status da comanda '.$bill->id.' para: '.$status.'.', $this->establishment->id);
            broadcast(new BillCreated($bill));
            broadcast(new BillClosing($bill));
            broadcast(new HasBillChanged($bill));
        } else {
            $arr = ['error' => true, 'message' => 'Erro ao alterar status da conta.'];
        }

        return response($arr);
    }
}
