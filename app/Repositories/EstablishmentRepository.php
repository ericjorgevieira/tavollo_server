<?php
/**
 * Created by PhpStorm.
 * User: ericoliveira
 * Date: 11/07/16
 * Time: 17:53.
 */

namespace App\Repositories;

use App\Models\Bill;
use App\Models\Establishment;
use App\Models\Marketplace;
use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\TableRequest;
use App\Models\User;
use App\Traits\SetEstablishmentTrait;
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Image;
use Storage;

class EstablishmentRepository
{
    use SetEstablishmentTrait;

    /**
     * Update establishment data and images.
     */
    public function updateEstablishment(Request $request, $establishment)
    {
        $validator = Validator::make($request->all(), [
          'name' => 'required|max:255',
          'email' => 'required|unique:establishments,email,'.$establishment->id,
          'cnpj' => 'required|unique:establishments,cnpj,'.$establishment->id,
          'image' => 'image|mimes:jpeg,png,jpg|max:2048',
          'logo' => 'image|mimes:jpeg,png,jpg|max:2048',
          'background' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

        if ($validator->fails()) {
            return redirect('/profile/establishment')->withInput()->withErrors($validator);
        }

        $establishment->name = $request->name;
        $establishment->slug = $establishment->slug ? $establishment->slug : str_slug($request->name.'-'.$establishment->id);
        $establishment->fantasy_name = $request->fantasy_name;
        $establishment->cnpj = $request->cnpj;
        $establishment->phone = $request->phone;
        $establishment->email = $request->email;
        $establishment->address = $request->address;
        $establishment->district = $request->district;
        $establishment->city = $request->city;
        $establishment->state = $request->state;
        $establishment->postal_code = $request->postal_code;
        $establishment->theme = $request->theme;
        $establishment->short_description = $request->short_description;
        $establishment->description = $request->description;
        $establishment->average_time = $request->average_time;

        if ($request->file('image')) {
            $path = Storage::putFile('public/establishments', $request->file('image'));
            $image = Image::make(Storage::get($path));
            $image->resize(400, 400)->stream('jpg', 30);
            Storage::put($path, $image);
            $env = \Config::get('app.env');
            $urlProduction = $env == 'homologation' ? 'https://test.admin.tavollo.com' : 'https://admin.tavollo.com';
            $establishment->image = ($env == 'local' || $env == 'testing') ? 'http://admin.tavollo.localhost:90'.Storage::url($path) : $urlProduction.Storage::url($path);
        }

        if ($request->file('logo')) {
            $path = Storage::putFile('public/establishments_logos', $request->file('logo'));
            $image = Image::make(Storage::get($path));
            $image->resize(668, 400)->stream('png', 30);
            Storage::put($path, $image);
            $env = \Config::get('app.env');
            $urlProduction = $env == 'homologation' ? 'https://test.admin.tavollo.com' : 'https://admin.tavollo.com';
            $establishment->logo = ($env == 'local' || $env == 'testing') ? 'http://admin.tavollo.localhost:90'.Storage::url($path) : $urlProduction.Storage::url($path);
        }

        if ($request->file('background')) {
            $path = Storage::putFile('public/establishments_backgrounds', $request->file('background'));
            $image = Image::make(Storage::get($path));
            $image->resize(600, 1212)->stream('jpg', 70);
            Storage::put($path, $image);
            $env = \Config::get('app.env');
            $urlProduction = $env == 'homologation' ? 'https://test.admin.tavollo.com' : 'https://admin.tavollo.com';
            $establishment->background = ($env == 'local' || $env == 'testing') ? 'http://admin.tavollo.localhost:90'.Storage::url($path) : $urlProduction.Storage::url($path);
        }

        $establishment->save();

        $config = [
          'primary_color' => $request->primary_color,
          'secondary_color' => $request->secondary_color,
          'title_color' => $request->title_color,
          'text_color' => $request->text_color,
          'button_color' => $request->button_color,
          'button_text_color' => $request->button_text_color,
          'category_text_color' => $request->category_text_color
        ];

        $establishment->saveThemeConfig($config);

        return redirect('/profile/establishment')->with('success', ['Dados alterados com sucesso!']);
    }

    /**
     * Return establishment config.
     */
    public function getEstablishmentConfig($establishment)
    {
        $config = $establishment->config;

        //Caso não tenha config definida, gera uma nova
        if (!$config) {
            $config = new \App\Models\EstablishmentConfig();
            $config->establishment_id = $establishment->id;
            $config->save();
        }

        if ($config->location_lat && $config->location_long) {
            Mapper::map($config->location_lat, $config->location_long, ['zoom' => 13]);
        } else {
            Mapper::map(-8.4267262, -44.6631426, ['zoom' => 4]);
        }

        return $config;
    }

    /**
     * Update establishment config.
     */
    public function updateEstablishmentConfig(Request $request, $establishment)
    {
        $config = $establishment->config;
        if ($establishment->hasOpenedBill()) {
            return redirect('/profile/config')
          ->with('warning', ['Existem contas abertas. Por favor, encerre todas para efetuar alterações.']);
        }

        if (!$this->validateTurns($request)) {
            return redirect('/profile/config')
          ->with('error', ['Os horários de turnos estão incompatíveis. Confira se o início possui valor menor que o fim nos dois turnos.']);
        }

        $config->generate_codes = ($request->generate_codes == '1') ? '1' : '0';
        $config->limit_table = 1;
        $config->request_alias = ($request->request_alias == '1') ? '1' : '0';
        $config->has_couvert = 0;
        $config->couvert_price = str_replace(',', '.', str_replace('.', '', $request->couvert_price));
        $config->has_baksheesh = ($request->has_baksheesh == '1' && $request->baksheesh_percentage > 0) ? '1' : '0';
        $config->baksheesh_percentage = str_replace(',', '.', str_replace('.', '', $request->baksheesh_percentage));
        $config->location_lat = $request->location_lat;
        $config->location_long = $request->location_long;
        $config->display_location = ($request->display_location == '1') ? '1' : '0';

        $config->setAvailableDays($request->available_days);
        $config->first_turn_begin = $request->first_turn_begin;
        $config->first_turn_end = $request->first_turn_end;
        $config->second_turn_begin = $request->second_turn_begin;
        $config->second_turn_end = $request->second_turn_end;

        $config->enable_price_update = ($request->enable_price_update == '1') ? '1' : '0';

        $config->save();

        return redirect('/profile/config')->with('success', ['Configurações alteradas com sucesso!']);
    }

    /**
     * Retorna o tema de um estabelecimento.
     */
    public function getEstablishmentTheme($establishment_id)
    {
        $establishment = Establishment::where(['id' => $establishment_id])->orWhere(['slug' => $establishment_id])->first();
        if ($establishment && $establishment->theme_config) {
            return response(['success' => true, 'theme' => $establishment->getThemeConfig()]);
        }

        return response(['success' => false]);
    }

    private function validateTurns(Request $request)
    {
        if (date('H:i', strtotime($request->first_turn_begin)) >= date('H:i', strtotime($request->first_turn_end)) ||
            date('H:i', strtotime($request->second_turn_begin)) >= date('H:i', strtotime($request->second_turn_end))) {
            return false;
        }

        return true;
    }

    /**
     * Check if Spot is available for use on establishment.
     *
     * @param $code
     *
     * @return array
     */
    public function checkSpot(Establishment $establishment = null, \App\Models\User $user, $code, $lat = null, $lng = null)
    {
        //Busca pela Spot
        $table = \App\Models\Table::where(['code' => strtolower($code), 'status' => 'enabled'])->first();
        if ($table) {
            //Caso estabelecimento seja definido, mas código não seja do mesmo, retorna erro
            if ($establishment && $table->establishment_id != $establishment->id) {
                return response(['error' => true, 'tmp_user' => false, 'attach' => false,
                  'title' => 'Erro ao abrir conta', 'message' => 'Este código não pertence a esse estabelecimento.', ]);
            }
            if ($establishment && !$establishment->config->allow_bills) {
                return response(['error' => true, 'tmp_user' => false, 'attach' => false,
              'title' => 'Função não disponível', 'message' => 'No momento, este estabelecimento não possui essa função.', ]);
            }
            if ($establishment && !$establishment->config->isAvailable()) {
                return response(['error' => true, 'tmp_user' => false, 'attach' => false,
                'title' => 'Estabelecimento está fechado', 'message' => 'No momento, este estabelecimento não se encontra disponível.', ]);
            }

            //Verifica se usuário está no raio do estabelecimento
            // Verifica se usuário não possui pedido pré-pago aberto
            if (!$user->getActivePreOrder()) {
                //Verifica se o Spot não possui conta ativa
                if ($table->hasActiveBill()) {
                    $bill = $table->getActiveBill();
                    if ($bill) {
                        //Se conta pertencer a usuário
                        if ($bill->user_id == $user->id) {
                            // Log::info('Usuário ' . $user->id . ' resgatando conta própria de id ' . $bill->id);
                            return response(['success' => true, 'attach' => false, 'tmp_user' => false, 'title' => 'Conta já pertence a usuário.', 'bill' => $bill]);
                        }
                        //Se conta não possui usuário
                        elseif ($bill->temp_user == 1) {
                            // Log::info('Conta ' . $bill->id . ' será assumida por usuário de webapp.');
                            return response(['success' => true, 'attach' => false, 'tmp_user' => true, 'title' => 'Spot em uso, mas sem usuário.', 'bill' => $bill]);
                        }
                        //Conta existe mas não pertence a usuário
                        else {
                            // $tableBill = $table->getPublicActiveBill();
                            // Log::info('Usuário não é responsável pela conta em questão. Gerando solicitação de anexação de usuário à conta ' . $tableBill->id);
                            // return response(array('success' => true, 'attach' => true, 'tmp_user' => false, 'title' => 'Spot em uso', 'bill' => $tableBill));
                            return response(['error' => true, 'attach' => false, 'tmp_user' => false, 'title' => 'Uma comanda já está aberta para este código.']);
                        }
                    }

                    return response(['error' => true, 'tmp_user' => false, 'title' => 'Erro', 'message' => 'Spot possui conta em aberto/a encerrar.', 'attach' => false]);
                }

                return response(['success' => true, 'tmp_user' => false, 'attach' => false, 'title' => 'Spot disponível', 'message' => 'Spot disponível!']);
            }

            return response(['error' => true, 'tmp_user' => false, 'attach' => false, 'title' => 'Pedido pendente', 'message' => 'Você possui um pedido pendente! Finalize-o e tente novamente.']);
        }

        return response(['error' => true, 'title' => 'Spot não encontrado',
          'message' => 'Código de cartão/mesa não identificado. Verifique com o garçom ou responsável.', ]);
    }

    /**
     * Lista de estabelecimentos disponíveis para listagem.
     *
     * @return [type] [description]
     */
    public function listEstablishments($lat, $lng, Marketplace $marketplace = null)
    {
        $establishments = Establishment::join('establishment_configs', 'establishments.id', '=', 'establishment_configs.establishment_id')
          ->where(['establishment_configs.display_location' => 1, 'establishments.status' => 'enabled'])->orderBy('establishments.name', 'ASC')
          ->get(['establishments.id', 'establishments.name', 'establishments.image', 'establishments.short_description', 'establishments.average_time',
          'establishment_configs.location_lat', 'establishment_configs.location_long', 'establishment_configs.fast2pay_account',
          'establishment_configs.cielo_account', 'establishment_configs.disable_orders', 'establishment_configs.allow_pre_bills', 'establishment_configs.allow_bills', ]);

        $arrEstablishments = [];
        $marketplaceEstablishments = ($marketplace) ? $marketplace->establishments->pluck('id')->toArray() : [];
        foreach ($establishments as $e) {
            //check if is available
            if ($marketplace) {
                if (in_array($e->id, $marketplaceEstablishments)) {
                    $arrEstablishments[] = $e;
                }
            } else {
                if ($e->checkIfIsNear($lat, $lng)) {
                    $arrEstablishments[] = $e;
                }
            }
        }
        if ($marketplace) {
            return response(['marketplace' => $marketplace, 'theme' => $marketplace->getThemeConfig(), 'establishments' => $arrEstablishments]);
        }

        return response($arrEstablishments);
    }

    /**
     * Abre uma solicitação de Token para o Estabelecimento.
     *
     * @param Establishment $establishment [description]
     * @param [type]        $tableNumber   [description]
     *
     * @return [type] [description]
     */
    public function requestToken(Establishment $establishment, $user, $tableNumber)
    {
        TableRequest::openRequest($user, $tableNumber, $establishment->id);
        Log::info('Solicitação de TOKEN para a mesa/cartão '.$tableNumber.' do estabelecimento '.$establishment->id);

        return response(['success' => true, 'message' => 'Solicitação efetuada! Aguarde a entrega do Token de acesso.']);
    }

    /**
     * Return a specific product from establishment.
     *
     * @param [type] $product_id [description]
     *
     * @return [type] [description]
     */
    public function getProduct($product_id, $establishment_id = null)
    {
        if (!$establishment_id && $this->establishment) {
            $establishment_id = $this->establishment->id;
        }
        if ($establishment_id) {
            $arr = [];
            $product = Product::where([
            'products.id' => $product_id,
            'products.establishment_id' => $establishment_id,
            'products.status' => 'enabled',
            ])->get(['products.*'])->first();

            if ($product) {
                if ($product->isAvailable()) {
                    $groups = ProductGroup::with(['items' => function ($query) {
                        $query->where('status', '=', 'enabled');
                    }])
                    ->where(['product_groups.product_id' => $product->id, 'product_groups.status' => 'enabled'])
                    ->groupBy('product_groups.id')->get(['product_groups.*']);
                    $arr = ['success' => true, 'product' => $product->toPublicArray(), 'groups' => $groups];
                } else {
                    $arr = ['success' => false, 'message' => 'Produto não disponível no momento.'];
                }
            } else {
                $arr = ['success' => false, 'message' => 'Produto não encontrado.'];
            }
        } else {
            $arr = ['success' => false, 'message' => 'Produto não encontrado no estabelecimento.'];
        }

        return response($arr);
    }
}
