<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 11/07/16
* Time: 17:53.
*/

namespace App\Repositories;

use App\Models\Establishment;
use App\Models\Island;
use App\Models\IslandTable;
use App\Models\Table;
use App\Traits\SetEstablishmentTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class IslandRepository
{
    use SetEstablishmentTrait;

    /**
     * Get list of islands from establishment.
     *
     * @param string $status
     *
     * @return mixed
     */
    public function forEstablishment($status = 'enabled')
    {
        return $this->establishment->islands()
        ->where('status', $status)
        ->orderBy('id', 'asc')
        ->get();
    }

    public function tables()
    {
        $tables = $this->establishment->tables()
        ->where('status', Table::STATUS_ENABLED)
        ->orderBy('number', 'asc')->get();

        return $tables;
    }

    public function tablesToArray(Island $island)
    {
        $tables = $island->tables()->orderBy('number', 'asc')->get();
        $arrTables = [];
        foreach ($tables as $p) {
            $arrTables[] = $p->id;
        }

        return $arrTables;
    }

    public function store(Request $request)
    {
        // if($this->establishment->hasOpenedBill()){
        //     return redirect('/profile/islands')
        //     ->with('warning', ['Existem contas abertas. Por favor, encerre todas para efetuar alterações.']);
        // }
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'waiter_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/profile/islands/create')
            ->withInput()
            ->withErrors($validator);
        }

        if ($request->input('opened_at') == $request->input('closed_at')) {
            return redirect('/profile/islands/create')
                ->withInput()
                ->with('error', ['Os horários de início e encerramento não podem ser iguais.']);
        }

        if (!$this->checkIslandConflict($request->tables, $request->available_days, $request->opened_at, $request->closed_at)) {
            $island = new Island();
            $island->name = $request->name;
            $island->establishment_id = $this->establishment->id;
            $island->opened_at = $request->opened_at;
            $island->closed_at = $request->closed_at;
            $island->waiter_id = $request->waiter_id;
            $island->setAvailableDays($request->available_days);
            $island->status = ($request->status == 'enabled') ? 'enabled' : 'disabled';
            $island->save();
            if ($request->tables && count($request->tables) > 0) {
                $island->tables()->sync($request->tables);
            }
            $island->setRawTableNumbers();

            return redirect('/profile/islands')
            ->with('success', ['Ilha cadastrada com sucesso!']);
        }

        return redirect('/profile/islands/create')
        ->with('error', ['Não foi possível inserir a Ilha. Houve um conflito de dias e mesas com uma Ilha existente.']);
    }

    public function update(Request $request, Island $island)
    {
        // if($this->establishment->hasOpenedBill()){
        //     return redirect('/profile/islands')
        //     ->with('warning', ['Existem contas abertas. Por favor, encerre todas para efetuar alterações.']);
        // }
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/profile/islands/'.$island->id.'/edit')
            ->withInput()
            ->withErrors($validator);
        }

        if ($request->input('opened_at') == $request->input('closed_at')) {
            return redirect('/profile/islands/'.$island->id.'/edit')
                ->withInput()
                ->with('error', ['Os horários de início e encerramento não podem ser iguais.']);
        }

        if (!$this->checkIslandConflict($request->tables, $request->available_days, $request->opened_at, $request->closed_at, $island->id)) {
            $island->name = $request->name;
            $island->establishment_id = $this->establishment->id;
            $island->opened_at = $request->opened_at;
            $island->closed_at = $request->closed_at;
            $island->waiter_id = $request->waiter_id;
            $island->setAvailableDays($request->available_days);
            $island->status = ($request->status == 'enabled') ? 'enabled' : 'disabled';
            $island->save();
            if ($request->tables && count($request->tables) > 0) {
                $island->tables()->sync($request->tables);
            } else {
                $island->tables()->detach();
            }
            $island->setRawTableNumbers();

            return redirect('/profile/islands/'.$island->id.'/edit')
            ->with('success', ['Ilha editada com sucesso!']);
        }

        return redirect('/profile/islands/'.$island->id.'/edit')
        ->with('error', ['Não foi possível alterar a Ilha. Houve um conflito de dias e mesas com uma Ilha existente.']);
    }

    public function enable(Island $island)
    {
        if ($this->establishment->hasOpenedBill()) {
            return redirect('/profile/islands')
            ->with('warning', ['Existem contas abertas. Por favor, encerre todas para efetuar alterações.']);
        }
        if ($island->enable() == Island::STATUS_DISABLED) {
            return response(['success' => true, 'message' => 'Ilha desabilitada com sucesso!']);
        } else {
            return response(['success' => true, 'message' => 'Ilha habilitada com sucesso!']);
        }
    }

    //Verifica se não existe nenhuma Ilha que conflite com a que está sendo cadastrada/editada
    public function checkIslandConflict($tables, $availableDays, $openedAt, $closedAt, $islandId = null)
    {
        if ($tables && count($tables) > 0) {
            $islandTables = IslandTable::whereIn('table_id', $tables)->groupBy('island_id')->get();
            //Verifica primeiro se as mesas também estão inseridas em outra Ilha
            if (count($islandTables) > 0) {
                foreach ($islandTables as $it) {
                    if ($islandId && $it->island_id == $islandId) {
                        continue;
                    }
                    //Verifica se a Ilha já possui disponibilidade nos mesmos dias
                    if ($it->island->hasActiveDays(array_keys($availableDays))) {
                        //Verificar se horário das ilhas choca
                        $today = Carbon::now();
                        $openedAt = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d').' '.$openedAt);
                        $closedAt = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d').' '.$closedAt);

                        $openedAtIsland = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d').' '.$it->island->opened_at);
                        $closedAtIsland = Carbon::createFromFormat('Y-m-d H:i:s', $today->format('Y-m-d').' '.$it->island->closed_at);
                        //Verifica se o horário de início começa hoje e termina amanhã
                        if (intval($openedAt->format('H')) > intval($closedAt->format('H'))) {
                            $closedAt->addDay();
                        }
                        if (intval($openedAtIsland->format('H')) > intval($closedAtIsland->format('H'))) {
                            $closedAtIsland->addDay();
                        }

                        if ($openedAt->between($openedAtIsland, $closedAtIsland) || $closedAt->between($openedAtIsland, $closedAtIsland)) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }
}
