<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 11/07/16
* Time: 17:53.
*/

namespace App\Repositories;

use App\Models\Establishment;
use App\Models\EstablishmentAccess;
use App\Models\Menu;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Traits\SetEstablishmentTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Image;
use Storage;

class MenuRepository
{
    use SetEstablishmentTrait;

    /**
     * Get list of menus from establishment.
     *
     * @param string $status
     *
     * @return mixed
     */
    public function forEstablishment($status = 'enabled')
    {
        return $this->establishment->menus()
        ->where('status', $status)
        ->orderBy('position', 'asc')
        ->get();
    }

    /**
     * Lista todos os Menus.
     *
     * @param string $status [description]
     *
     * @return [type] [description]
     */
    public function listMenus($status = 'enabled')
    {
        $menus = $this->forEstablishment($status);

        return response($menus);
    }

    /**
     * Return products grouped by categories.
     */
    public function productsByCategories()
    {
        $products = $this->establishment->products()
        ->where(['status' => 'enabled', 'not_sale' => false])->get();
        $arrProducts = [];
        foreach ($products as $p) {
            if ($p->productCategory) {
                $arrProducts[$p->product_category_id]['productCategory'] = $p->productCategory;
                $arrProducts[$p->product_category_id]['products'][] = $p;
            }
        }

        return $arrProducts;
    }

    /**
     * Store new menu.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/menus')
            ->withInput()
            ->withErrors($validator);
        }

        $menu = new Menu();
        $menu->name = $request->name;
        $menu->slug = str_slug($request->name);
        $menu->description = $request->description;
        $menu->establishment_id = $this->establishment->id;
        $menu->status = ($request->status == 'enabled') ? 'enabled' : 'disabled';
        $menu->save();
        if ($request->products && count($request->products) > 0) {
            $menu->products()->sync($request->products);
        }
        $menu->uploadPhoto($request);
        Menu::clearCache($this->establishment->id);

        return redirect('/menus')
        ->with('success', ['Menu cadastrado com sucesso!']);
    }

    /**
     * Update menu data.
     */
    public function update(Request $request, Menu $menu)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/menus')
            ->withInput()
            ->withErrors($validator);
        }

        $menu->name = $request->name;
        $menu->slug = str_slug($request->name);
        $menu->description = $request->description;
        $menu->establishment_id = $this->establishment->id;
        $menu->status = ($request->status == 'enabled') ? 'enabled' : 'disabled';
        $menu->save();

        if ($request->products && count($request->products) > 0) {
            $menu->products()->sync($request->products);
        } else {
            $menu->products()->detach();
        }
        $menu->uploadPhoto($request);
        Menu::clearCache($this->establishment->id);

        return redirect('/menus/'.$menu->id.'/edit')
        ->with('success', ['Menu editado com sucesso!']);
    }

    /**
     * Enable a menu on admin.
     */
    public function enable(Menu $menu)
    {
        $msg = '';
        if ($menu->status == 'disabled') {
            $menu->status = 'enabled';
            $menu->save();
            $msg = 'Cardápio habilitado com sucesso!';
        } else {
            $menu->status = 'disabled';
            $menu->save();
            $msg = 'Cardápio desativado com sucesso!';
        }
        Menu::clearCache($this->establishment->id);

        return response(['success' => true, 'message' => $msg]);
    }

    /**
     * Upload temporary photo for menu.
     */
    public function uploadTmpPhoto(Request $request)
    {
        $arrResponse = [];

        $validator = Validator::make($request->all(), [
            'avatar' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        if ($validator->fails()) {
            $arrResponse = ['success' => false, 'message' => $validator->errors()];
        } else {
            $path = Storage::putFile('public/tmp_photos', $request->file('avatar'));

            $image = Image::make(Storage::get($path));
            // if ($image->width() != $image->height()) {
            // $image->crop(500, 500, 25, 25)->stream('jpg', 30);
            // } else {
            $image->resize(500, 250)->stream('jpg', 30);
            // }
            Storage::put($path, $image);

            $arrResponse = ['success' => true, 'url' => Storage::url($path), 'path' => $path];
        }

        return response($arrResponse);
    }

    /**
     * Return menus from establishment.
     *
     * @return Response
     */
    public function getMenus($establishment_id = null, $slug = null, $latitude = null, $longitude = null)
    {
        $establishment = null;
        if ($slug) {
            $establishment = Establishment::where(['slug' => $slug])->first();
        } elseif ($establishment_id) {
            $establishment = Establishment::find($establishment_id);
        }

        if ($establishment) {
            $menus = Menu::where([
              'status' => 'enabled',
              'establishment_id' => $establishment->id,
            ])->orderBy('position', 'asc')->get();
            $establishment->theme = $establishment->getThemeConfig();
            $establishment->cielo_account = $establishment->getPaymentMethod();
            $establishment->is_near = $establishment->checkIfIsNear($latitude, $longitude);
            $establishment->is_available_now = $establishment->config->isAvailable();

            $menus->map(function ($menu) {
                $menu->__set('products_quantity', $menu->countProducts());
            });

            EstablishmentAccess::registerAccess($establishment->id);

            return response(['menus' => $menus, 'establishment' => $establishment]);
        } elseif ($this->establishment) {
            $menus = Menu::where([
              'status' => 'enabled',
              'establishment_id' => $this->establishment->id,
            ])->orderBy('position', 'asc')->get();

            $menus->map(function ($menu) {
                $menu->__set('products_quantity', $menu->countProducts());
            });

            EstablishmentAccess::registerAccess($this->establishment->id);

            return response($menus);
        }

        return response(['menus' => null, 'establishment' => null]);
    }

    /**
     * List products grouped by categories of establishment.
     *
     * @param [type] $menu_id [description]
     *
     * @return [type] [description]
     */
    public function getCategories($establishment_id = null, $menu_id)
    {
        if (!$establishment_id && $this->establishment) {
            $establishment_id = $this->establishment->id;
        }
        $categories = ProductCategory::join('products', function ($join) {
            $join->on('product_categories.id', 'products.product_category_id');
        })->join('menu_product', function ($join) {
            $join->on('menu_product.product_id', 'products.id');
        })->join('menus', function ($join) {
            $join->on('menu_product.menu_id', 'menus.id');
        })
        ->where('menus.establishment_id', '=', $establishment_id)
        ->where('menu_product.menu_id', '=', $menu_id)
        ->groupBy('product_categories.id')
        ->where('products.status', '=', 'enabled')
        ->orderBy('product_categories.name', 'ASC')
        ->orderBy('products.name', 'ASC')
        ->get(['product_categories.*']);

        $arrCategories = [];
        foreach ($categories as $k => $c) {
            $arrCategories[$k] = [
                'id' => $c->id,
                'name' => $c->name,
                'slug' => $c->slug,
                'products' => $this->getMenuProducts($establishment_id, $menu_id, $c->id),
            ];
        }

        return response([
            'menu' => Menu::find($menu_id),
            'categories' => $arrCategories,
        ]);
    }

    /**
     * Return products from a specific menu of establishment.
     *
     * @param $menu_id
     * @param $category_id
     *
     * @return Response
     */
    private function getMenuProducts($establishment_id, $menu_id, $category_id)
    {
        $products = Product::query()
        ->join('menu_product', function ($join) {
            $join->on('menu_product.product_id', 'products.id');
        })
        ->where('products.product_category_id', '=', $category_id)
        ->where('menu_product.menu_id', '=', $menu_id)
        ->where('products.establishment_id', '=', $establishment_id)
        ->where('products.status', '=', 'enabled')
        ->orderBy('products.name', 'ASC')
        ->get(['products.*']);

        $arrProducts = [];
        foreach ($products as $p) {
            if ($p->isAvailable()) {
                $p->updateIfPromoPrice();
                $arrProducts[] = $p;
            }
        }

        return $arrProducts;
    }
}
