<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 11/07/16
* Time: 17:53.
*/

namespace App\Repositories;

use App\Models\Bill;
use App\Models\Establishment;
use App\Models\LogActivity;
use App\Models\Order;
use App\Traits\SetEstablishmentTrait;
use Illuminate\Http\Response;

class OrderRepository
{
    use SetEstablishmentTrait;

    /**
     * Get list of orders from bill.
     *
     * @return mixed
     */
    public function forBill(Bill $bill)
    {
        return $bill->bills()
        ->orderBy('created_at', 'desc')
        ->get();
    }

    /**
     * Lista os pedidos de um estabelecimento de acordo com o status.
     *
     * @param string $status
     *
     * @return Response
     */
    public function listOrders($status = 'opened', $sector_id = null)
    {
        $ordersQuery = Order::with(['bill', 'bill.table', 'product', 'product.productCategory', 'items.item', 'items.group', 'product.sector'])
        ->join('bills', function ($join) {
            $join->on('bills.id', 'orders.bill_id');
        })
        ->join('tables', function ($join) {
            $join->on('tables.id', 'bills.table_id');
        })
        ->join('products', function ($join) {
            $join->on('orders.product_id', 'products.id');
        })
        ->whereHas('bill', function ($query) {
            $query->where('status', 'opened');
            $query->orWhere('status', 'closing');
        });

        if ($status == 'opened') {
            $ordersQuery->whereIn('orders.status', ['opened', 'deny'])->where([
              'tables.establishment_id' => $this->establishment->id,
            ]);
        } else {
            $ordersQuery->where([
              'orders.status' => $status,
              'tables.establishment_id' => $this->establishment->id,
            ]);
        }

        if ($sector_id) {
            $ordersQuery->where(['products.sector_id' => $sector_id]);
        }
        $orders = $ordersQuery->orderBy('orders.created_at', 'desc')
        ->get(['orders.*']);

        //Melhorar isso aqui depois
        foreach ($orders as $o) {
            $o->bill_waiter = $o->bill->waiter();
            // $o->updateCachePrice();
        }

        $orders = Order::getOrdersWithGroups($orders);

        // if($this->establishment->config && $this->establishment->config->enable_advanced_functions == 1){
        //   $arrOrders = array();
        //   foreach($orders as $o){
        //       // $o->updateCachePrice();
        //       $o->enable_advanced_functions = true;
        //       $arrOrders[] = $o;
        //   }
        //   $orders = $arrOrders;
        // }

        return response($orders);
    }

    /**
     * Adiciona cálculo de preço por peso ao pedido.
     *
     * @param $weight
     *
     * @return Response
     */
    public function addWeightToOrder(Order $order, $isWeight = true, $weight = null)
    {
        if ($order->bill->table->establishment_id == $this->establishment->id) {
            $order->addWeight($isWeight, $weight);

            return response(['success' => true, 'message' => 'Preço do pedido reajustado.']);
        }

        return response(['error' => true, 'message' => 'Erro ao processar pedido #'.$order->id]);
    }

    /**
     * Confirma um pedido (Order) de uma conta (Bill).
     *
     * @return Response
     */
    public function confirmOrder(Order $order)
    {
        if ($this->establishment->config->enable_advanced_functions == 1) {
            if ($order && $order->bill->table->establishment_id == $this->establishment->id) {
                $order->setStatus(Order::STATUS_PREPARATION);
                LogActivity::registerActivity(LogActivity::ACTION_UPDATE, 'Confirmando pedido '.$order->id.'.', $this->establishment->id);

                return response(['success' => true, 'message' => 'Pedido confirmado!']);
            }

            return response(['error' => true, 'message' => 'Erro ao confirmar pedido.']);
        }

        return response(['error' => true, 'message' => 'Função não disponível.']);
    }

    /**
     * Cancela pedido (Order) de uma conta (Bill).
     *
     * @return Response
     */
    public function cancelOrder(Order $order)
    {
        if ($this->establishment->config->enable_advanced_functions == 1 || $order->status == Order::STATUS_DENY) {
            if ($order && $order->bill->table->establishment_id == $this->establishment->id) {
                $order->setStatus(Order::STATUS_TRASH);
                LogActivity::registerActivity(LogActivity::ACTION_UPDATE, 'Cancelando pedido '.$order->id.' .', $this->establishment->id);

                return response(['success' => true, 'message' => 'Pedido cancelado com sucesso.']);
            }

            return response(['error' => true, 'message' => 'Erro ao cancelar pedido.']);
        }

        return response(['error' => true, 'message' => 'Função não disponível.']);
    }
}
