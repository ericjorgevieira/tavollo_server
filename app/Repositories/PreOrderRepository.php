<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 11/07/16
* Time: 17:53.
*/

namespace App\Repositories;

use App\Events\PreOrderCreated;
use App\Models\PreOrder;
use App\Traits\SetEstablishmentTrait;
use Illuminate\Http\Response;

class PreOrderRepository
{
    use SetEstablishmentTrait;

    /**
     * Retorna o pedido pré-pago.
     */
    public function getPreOrder($id)
    {
        $preOrder = PreOrder::with(['products', 'products.product', 'products.items', 'user'])->where(['establishment_id' => $this->establishment->id, 'id' => $id])->first();

        return $preOrder;
    }

    /**
     * Lista os pedidos pre pagos de um estabelecimento de acordo com o status.
     *
     * @param string $status
     *
     * @return Response
     */
    public function listPreOrders($status = 'opened')
    {
        $ordersQuery = PreOrder::with(['user', 'products.product', 'products.items', 'products.items.item', 'products.items.group', 'products.product.sector']);

        if ($status == 'opened') {
            $ordersQuery->whereIn('status', ['opened', 'preparation', 'deny'])->where([
            'establishment_id' => $this->establishment->id,
          ]);
        } else {
            $ordersQuery->where([
              'status' => $status,
              'establishment_id' => $this->establishment->id,
          ]);
        }

        $orders = $ordersQuery->orderBy('created_at', 'desc')->get();

        $orders = PreOrder::getPreOrdersWithGroups($orders);

        return response($orders);
    }

    /**
     * Inicia preparo de um pedido (PreOrder).
     *
     * @return Response
     */
    public function preparePreOrder(PreOrder $preOrder)
    {
        if ($preOrder && $preOrder->establishment_id == $this->establishment->id) {
            $preOrder->setStatus(PreOrder::STATUS_PREPARATION);
            $this->broadcastPreOrderEvents($preOrder);

            return response(['success' => true, 'message' => 'Pedido pré-pago em preparo!']);
        }

        return response(['error' => true, 'message' => 'Erro ao preparar pedido pré-pago.']);
    }

    /**
     * Confirma um pedido (PreOrder).
     *
     * @return Response
     */
    public function confirmPreOrder(PreOrder $preOrder)
    {
        if ($preOrder && $preOrder->establishment_id == $this->establishment->id) {
            $preOrder->setStatus(PreOrder::STATUS_FINISHED);
            $this->broadcastPreOrderEvents($preOrder);

            return response(['success' => true, 'message' => 'Pedido pré-pago finalizado! Entregue ao cliente.']);
        }

        return response(['error' => true, 'message' => 'Erro ao finalizar pedido pré-pago.']);
    }

    /**
     * Cancela pedido (PreOrder).
     *
     * @return Response
     */
    public function cancelPreOrder(PreOrder $preOrder)
    {
        if ($preOrder && $preOrder->establishment_id == $this->establishment->id) {
            $preOrder->setStatus(PreOrder::STATUS_TRASH);
            $this->broadcastPreOrderEvents($preOrder);

            return response(['success' => true, 'message' => 'Pedido cancelado com sucesso.']);
        }

        return response(['error' => true, 'message' => 'Erro ao cancelar pedido.']);
    }

    /**
     * Dispara os eventos de socket relacionados a um pedido.
     *
     * @param [type] $order [description]
     *
     * @return [type] [description]
     */
    private function broadcastPreOrderEvents(PreOrder $order)
    {
        broadcast(new PreOrderCreated($order));
    }
}
