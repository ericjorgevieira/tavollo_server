<?php
/**
 * Created by PhpStorm.
 * User: ericoliveira
 * Date: 11/07/16
 * Time: 17:53
 */

namespace App\Repositories;

use App\Models\Establishment;
use App\Models\EstablishmentConfig;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PrinterRepository
{
    public function updatePrinter(Request $request, $establishment)
    {
        $config = $establishment->config;

        //Caso não tenha config definida, gera uma nova
        if (!$config) {
            $config = new EstablishmentConfig();
            $config->establishment_id = $establishment->id;
        }

        $config->printer_name = $request->printer_name;
        $config->printer_ip = $request->printer_ip;
        $config->save();

        return redirect('/profile/printer')->with('success', ['Impressora editada com sucesso!']);
    }
}
