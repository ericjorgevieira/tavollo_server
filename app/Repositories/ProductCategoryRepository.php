<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 11/07/16
* Time: 17:53.
*/

namespace App\Repositories;

use App\Models\ProductCategory;
use App\Traits\SetEstablishmentTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ProductCategoryRepository
{
    use SetEstablishmentTrait;

    /**
     * Lista todas as categorias de produto do estabelecimento.
     *
     * @param string $status [description]
     *
     * @return [type] [description]
     */
    public function forEstablishment($status = 'enabled')
    {
        return $this->establishment->productCategories()
    ->orderBy('created_at', 'desc')
    ->where('status', $status)->get();
    }

    public function listCategories($status = 'enabled')
    {
        $products = $this->forEstablishment($status);
        response($products);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/categories/create')->withInput()->withErrors($validator);
        }

        $category = new ProductCategory();
        $category->populate($request, $this->establishment->id);
        if ($request->save_and_add == 1) {
            return redirect('/categories/create')
      ->with('success', ['Categoria cadastrada com sucesso!']);
        }

        return redirect('/categories')->with('success', ['Categoria cadastrada com sucesso!']);
    }

    public function update(Request $request, ProductCategory $category)
    {
        $validator = Validator::make($request->all(), [
      'name' => 'required|max:255',
    ]);

        if ($validator->fails()) {
            return redirect('/categories/'.$category->id.'/edit')
      ->withInput()
      ->withErrors($validator);
        }
        $category->populate($request);

        return redirect('/categories/'.$category->id.'/edit')
    ->with('success', ['Categoria editada com sucesso!']);
    }

    public function enable(ProductCategory $category)
    {
        $msg = '';
        if ($category->enable() == 'disabled') {
            $msg = 'Categoria desabilitada com sucesso!';
        } else {
            $msg = 'Categoria habilitada com sucesso!';
        }

        return response(['success' => true, 'message' => $msg]);
    }
}
