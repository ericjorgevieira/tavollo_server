<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 11/07/16
* Time: 17:53.
*/

namespace App\Repositories;

use App\Models\Bill;
use App\Models\LogActivity;
use App\Models\Menu;
use App\Models\MenuProduct;
use App\Miscelaneous\Utils;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductGroup;
use App\Models\ProductGroupItem;
use App\Models\Sector;
use App\Traits\SetEstablishmentTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Image;
use Storage;

class ProductRepository
{
    use SetEstablishmentTrait;

    /**
     * Get list of products from product category.
     *
     * @return mixed
     */
    public function forProductCategory(ProductCategory $productCategory)
    {
        return $productCategory->products()
          ->orderBy('created_at', 'desc')
          ->get();
    }

    public function forMenu(Menu $menu)
    {
    }

    public function forBill(Bill $bill)
    {
    }

    public function forEstablishment($status = 'enabled', $hasParent = false)
    {
        $query = $this->establishment->products()
        ->orderBy('created_at', 'desc')
        ->orderBy('not_sale', 'desc')
        ->where('status', $status);
        if ($hasParent) {
            $query->where('parent_id', $hasParent);
        }

        return $query->get();
    }

    public function listProductsForModal()
    {
        $queryProducts = $this->establishment->products()->with(['groups', 'groups.items'])->orderBy('name', 'asc')
        ->where(['status' => Product::STATUS_ENABLED, 'not_sale' => false]);

        return $queryProducts->get(['id', 'code', 'name', 'weight']);
    }

    public function listProducts($status = 'enabled')
    {
        $products = $this->forEstablishment($status);

        return response($products);
    }

    public function listCategories($status = 'enabled')
    {
        return ProductCategory::where(['status' => $status, 'establishment_id' => $this->establishment->id])
          ->orderBy('name', 'asc')
          ->get();
    }

    public function listSectors($status = 'enabled')
    {
        return Sector::where(['establishment_id' => $this->establishment->id, 'status' => $status])->get();
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'name' => 'required|max:255',
          'product_category_id' => 'required',
          'price' => 'required',
          'code' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/products/create')
              ->withInput()
              ->withErrors($validator);
        }

        $product = new Product();
        $product->populate($request, $this->establishment->id);
        Product::clearCache($this->establishment->id);
        LogActivity::registerActivity(LogActivity::ACTION_INSERT, 'Cadastro de produto '.$product->id.' realizado.', $this->establishment->id);
        if ($request->save_and_add == 1) {
            return redirect('/products/create')
              ->with('success', ['Produto cadastrado com sucesso!']);
        }

        return redirect('/products/'.$product->id.'/edit')->with('success', ['Produto cadastrado com sucesso!']);
    }

    public function update(Request $request, Product $product)
    {
        $validator = Validator::make($request->all(), [
          'name' => 'required|max:255',
          'product_category_id' => 'required',
          'price' => 'required',
          'code' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/products/'.$product->id.'/edit')
              ->withInput()
              ->withErrors($validator);
        }
        $product->populate($request, $this->establishment->id);
        Product::clearCache($this->establishment->id);
        LogActivity::registerActivity(LogActivity::ACTION_UPDATE, 'Edição de produto '.$product->id.' realizada.', $this->establishment->id);

        return redirect('/products/'.$product->id.'/edit')->with('success', ['Produto editado com sucesso!']);
    }

    public function checkIfExistsOnCache($code)
    {
        $importedProducts = $this->establishment->externalProducts();
        $foundProduct = null;
        $foundProduct = array_filter($importedProducts, function ($p) use ($code) {
            return $p['code'] == $code;
        });
        // foreach($importedProducts as $p){
        //   if($p['code'] == $code){
        //     $foundProduct = $p;
        //   }
        // }
        return $foundProduct;
    }

    public function uploadTmpPhoto(Request $request)
    {
        $arrResponse = [];
        $validator = Validator::make($request->all(), [
          'avatar' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        if ($validator->fails()) {
            $arrResponse = ['success' => false, 'message' => $validator->errors()];
        } else {
            $path = Storage::putFile('public/tmp_photos', $request->file('avatar'));

            $image = Image::make(Storage::get($path));

            // $width = $image->width();
            // $height = $image->height();

            // $dimension = 2362;

            // $vertical = (($width < $height) ? true : false);
            // $horizontal = (($width > $height) ? true : false);
            // $square = (($width = $height) ? true : false);

            // if ($vertical) {
            //     $top = $bottom = 245;
            //     $newHeight = ($dimension) - ($bottom + $top);
            //     $image->resize(null, $newHeight, function ($constraint) {
            //         $constraint->aspectRatio();
            //     });
            // } elseif ($horizontal) {
            //     $right = $left = 245;
            //     $newWidth = ($dimension) - ($right + $left);
            //     $image->resize($newWidth, null, function ($constraint) {
            //         $constraint->aspectRatio();
            //     });
            // } elseif ($square) {
            //     $right = $left = 245;
            //     $newWidth = ($dimension) - ($left + $right);
            //     $image->resize($newWidth, null, function ($constraint) {
            //         $constraint->aspectRatio();
            //     });
            // }

            // $image->resizeCanvas($dimension, $dimension, 'center', false, '#ffffff')->stream('jpg', 30);

            // if($image->width() != $image->height()){
            // $image->crop(500, 500, 25, 25)->stream('jpg', 60);
            // }else{
            // $image->resize(500, 500)->stream('jpg', 60);
            $image->stream('jpg', 60);
            // }
            Storage::put($path, $image);

            $arrResponse = ['success' => true, 'url' => Storage::url($path), 'path' => $path];
        }

        return response($arrResponse);
    }

    public function enable(Product $product)
    {
        Product::clearCache($this->establishment->id);
        $product->updateIfPromoPrice();
        $hasOtherProduct = false;
        if ($product->code) {
            $otherProduct = Product::where(['code' => $product->code, 'establishment_id' => $this->establishment->id, 'status' => 'enabled'])->where('id', '!=', $product->id)->first();
            if ($otherProduct) {
                $hasOtherProduct = true;
            }
        }

        if (!$hasOtherProduct) {
            if ($product->enable() == Product::STATUS_DISABLED) {
                return response(['success' => true, 'message' => 'Produto desabilitado com sucesso!']);
            } else {
                return response(['success' => true, 'message' => 'Produto habilitado com sucesso!']);
            }
        }

        return response(['success' => false, 'message' => 'Erro ao editar produto.']);
    }

    public function destroy(Product $product)
    {
        Product::clearCache($this->establishment->id);
        $groups = $product->groups;
        if ($groups) {
            foreach ($groups as $g) {
                $items = $g->items;
                if ($items) {
                    foreach ($items as $i) {
                        $i->delete();
                    }
                }
                $g->delete();
            }
        }
        $product->delete();

        return response(['success' => true, 'message' => 'Produto removido com sucesso!']);
    }

    public function updateCategory(Product $product, ProductCategory $category)
    {
        Product::clearCache($this->establishment->id);
        if ($this->establishment->id == $category->establishment_id) {
            $product->product_category_id = $category->id;
            $product->save();

            return response(['success' => true, 'message' => 'Produto alterado com sucesso.']);
        }

        return response(['success' => false, 'message' => 'Erro ao alterar produto.']);
    }

    public function listObservations(Product $product)
    {
        return response(['success' => true, 'observations' => $product->default_observations]);
    }

    public function saveObservation(Product $product, $observation)
    {
        $observations = $product->default_observations;
        if (!$observations) {
            $observations = [];
        }
        $observations[] = $observation;
        $product->default_observations = $observations;
        $product->save();

        return response(['success' => true, 'message' => 'Observação cadastrada com sucesso!', 'observations' => $observations]);
    }

    public function deleteObservation(Product $product, $key)
    {
        $observations = $product->default_observations;
        array_splice($observations, $key, 1);
        $product->default_observations = $observations;
        $product->save();

        return response(['success' => true, 'message' => 'Observação removida com sucesso!', 'observations' => $observations]);
    }

    /**
     * Adiciona grupo de itens no produto.
     *
     * @param Request $request [description]
     * @param Product $product [description]
     * @param [type]  $group   [description]
     *
     * @return [type] [description]
     */
    public function saveGroup(Request $request, Product $product, ProductGroup $group = null)
    {
        $arrResponse = [];
        $validator = Validator::make($request->all(), [
      'name' => 'required|max:255',
    ]);
        if ($validator->fails()) {
            $arrResponse = ['success' => false, 'message' => $validator->errors()];
        } else {
            if (!$group) {
                $group = new ProductGroup();
                $group->product_id = $product->id;
            }
            $group->name = $request->name;
            $group->description = $request->description;
            $group->prefix = str_slug($request->name);
            $group->min = ($request->min) ? $request->min : 0;
            $group->max = ($request->max) ? $request->max : 1;
            $group->external_id = $request->external_id;
            $group->formula = (isset(ProductGroup::$formulaKeys[$request->formula])) ? ProductGroup::$formulaKeys[$request->formula] : 'sum_all';
            $group->status = ($request->status) ? $request->status : 'disabled';
            $group->establishment_id = $this->establishment->id;
            $group->save();
            $arrResponse = ['success' => true, 'group' => $group];
        }

        return response($arrResponse);
    }

    /**
     * Deleta agrupamento de itens obrigatórios.
     */
    public function deleteGroup(ProductGroup $group)
    {
        Product::clearCache($this->establishment->id);
        $items = $group->items;
        if ($items) {
            foreach ($items as $i) {
                $i->delete();
            }
        }
        $group->delete();

        return response(['success' => true, 'message' => 'Agrupamento removido com sucesso!']);
    }

    /**
     * Adiciona item ao grup de itens do produto.
     *
     * @param Request      $request [description]
     * @param ProductGroup $group   [description]
     * @param [type]       $item    [description]
     *
     * @return [type] [description]
     */
    public function saveItem(Request $request, ProductGroup $group, ProductGroupItem $item = null)
    {
        $arrResponse = [];
        $validator = Validator::make($request->all(), [
      'name' => 'required|max:255',
    ]);
        if ($validator->fails()) {
            $arrResponse = ['success' => false, 'message' => $validator->errors()];
        } else {
            if (!$item) {
                $item = new ProductGroupItem();
                $item->product_group_id = $group->id;
            }
            $item->name = $request->name;
            $item->description = $request->description;
            $item->price = $request->price;
            $item->external_id = $request->external_id;
            $item->status = ($request->status) ? $request->status : 'disabled';
            $item->establishment_id = $this->establishment->id;
            $item->group_composition_id = $request->group_composition_id;
            $item->save();
            $arrResponse = ['success' => true, 'item' => $item];
        }

        return response($arrResponse);
    }

    /**
     * Deleta agrupamento de itens obrigatórios.
     */
    public function deleteItem(ProductGroupItem $item)
    {
        Product::clearCache($this->establishment->id);
        $item->delete();

        return response(['success' => true, 'message' => 'Item removido com sucesso!']);
    }

    /**
     * Processa importação de produtos em formato .csv.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function importProducts(Request $request)
    {
        ini_set('auto_detect_line_endings', true);
        header('Content-Type: text/html; charset=UTF-8');

        $validator = Validator::make($request->all(), [
      'import' => 'required|file|mimes:txt,csv',
    ]);
        if ($validator->fails()) {
            return redirect('/products/import')
      ->withInput()
      ->withErrors($validator);
        }

        try {
            $path = Storage::putFile('public/imports', $request->file('import'));
            $url = \Config::get('app.url').Storage::url($path);
            $file = fopen($url, 'r');
            $arrMessage = [];
            $i = 0;

            $arrMenus = [];
            $arrCategories = [];

            while (($r = fgetcsv($file, 10000, ';')) !== false) {
                if ($i == 0) {
                    ++$i;
                    continue;
                }
                $code = intval($r[0]);
                $name = Utils::normalize($r[1], false);
                $menuName = Utils::normalize($r[2], false);
                $categoryName = Utils::normalize($r[3], false);
                $menuslug = str_slug(Utils::normalize($r[2], false));
                $catslug = str_slug(Utils::normalize($r[3], false));
                $price = floatval($r[4]);
                $promo_price = floatval($r[5]);
                $description = Utils::normalize($r[6], false);

                if (!isset($arrMenus[$menuslug])) {
                    $menu = Menu::where(['establishment_id' => $this->establishment->id, 'slug' => $menuslug])->get()->first();
                    if (!$menu) {
                        $menu = new Menu();
                        $menu->name = $menuName;
                        $menu->slug = $menuslug;
                        $menu->status = 'enabled';
                        $menu->establishment_id = $this->establishment->id;
                        $menu->save();
                        $arrMessage[] = 'Menu '.$menu->name.' inserido.';
                    }
                    $arrMenus[$menuslug] = $menu;
                }

                if (!isset($arrCategories[$catslug])) {
                    $category = ProductCategory::where(['slug' => $catslug, 'establishment_id' => $this->establishment->id])->get()->first();
                    if (!$category) {
                        $category = new ProductCategory();
                        $category->name = $categoryName;
                        $category->slug = $catslug;
                        $category->status = 'enabled';
                        $category->establishment_id = $this->establishment->id;
                        $category->save();
                        $arrMessage[] = 'Categoria '.$category->name.' inserida.';
                    }
                    $arrCategories[$catslug] = $category;
                }

                $product = Product::where(['establishment_id' => $this->establishment->id, 'code' => $code])->get()->first();
                if (!$product) {
                    $product = new Product();
                    $product->code = $code;
                    $product->establishment_id = $this->establishment->id;
                    $arrMessage[] = 'Produto #'.$code.' - '.$name.' inserido.';
                }
                $product->name = $name;
                $product->price = $price;
                if ($promo_price && $promo_price > 0) {
                    $product->promo_price = $promo_price;
                } else {
                    $product->promo_price = $price;
                }
                $product->description = $description;
                $product->product_category_id = $arrCategories[$catslug]->id;
                $product->save();
                $arrMessage[] = 'Produto #'.$product->code.' - '.$product->name.' alterado.';

                $this->deleteMenuProducts($product->id);
                $arrMenus[$menuslug]->products()->attach($product->id);

                $arrMessage[] = 'Produto #'.$product->code.' - '.$product->name.' inserido no Menu "'.$menuName.'".';
            }

            Storage::delete($path);
            Product::clearCache($this->establishment->id);
            LogActivity::registerActivity(LogActivity::ACTION_INSERT, 'Importação de produtos realizada.', $this->establishment->id);

            return redirect('/products/import')
      ->with('imported', $arrMessage);
        } catch (Exception $e) {
            return redirect('/products/import')
      ->with('warning', ['Erro ao processar lista. Verifique se as colunas estão bem definidas e tente novamente.']);
        }
    }

    /**
     * Faz a conexão com a integração e importa os produtos do PDV.
     *
     * @return [type] [description]
     */
    public function importProductsFromIntegration()
    {
        $integration = $this->establishment->integration->getIntegration();
        if ($integration) {
            $products = $integration->importProducts();
            if ($products != false) {
                $arrMessage = [];
                if (count($products) > 0) {
                    foreach ($products as $product) {
                        $arrMessage[] = 'Produto #'.$product->code.' - '.$product->name.' importado.';
                    }
                }
                Product::clearCache($this->establishment->id);
                LogActivity::registerActivity(LogActivity::ACTION_INSERT, 'Importação de produtos via integração realizada.', $this->establishment->id);

                return redirect('/products/import')
        ->with('imported', $arrMessage);
            } else {
                return redirect('/products/import')
        ->with('error', ['Erro na conexão com o PDV. Entre em contato com o suporte.']);
            }
        }

        return redirect('/products/import')
    ->with('error', ['A integração está desativada.']);
    }

    private function deleteMenuProducts($id)
    {
        $menuProducts = MenuProduct::where(['product_id' => $id])->get();
        foreach ($menuProducts as $mp) {
            $mp->delete();
        }
    }
}
