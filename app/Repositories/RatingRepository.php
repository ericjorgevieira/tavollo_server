<?php

namespace App\Repositories;

use App\Traits\SetEstablishmentTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RatingRepository
{
    use SetEstablishmentTrait;

    public function configSave(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'question_max_rating' => 'required',
            'question_rating' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/analysis/ratings/config')
            ->withInput()
            ->withErrors($validator);
        }

        $config = $this->establishment->config;
        $arrPredefinedAnswers = [
            'question_max_rating' => $request->input('question_max_rating'),
            'answers_max_rating' => $request->input('answers_max_rating'),
            'question_rating' => $request->input('question_rating'),
            'answers_rating' => $request->input('answers_rating'),
        ];

        $config->predefined_answers = json_encode($arrPredefinedAnswers);
        $config->save();

        return redirect('/analysis/ratings/config')
        ->with('success', ['Configuração salva com sucesso.']);
    }
}
