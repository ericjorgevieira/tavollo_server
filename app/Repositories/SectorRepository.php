<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 11/07/16
* Time: 17:53.
*/

namespace App\Repositories;

use App\Models\Establishment;
use App\Models\Sector;
use App\Traits\SetEstablishmentTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class SectorRepository
{
    use SetEstablishmentTrait;

    /**
     * Get list of sectors from establishment.
     *
     * @return mixed
     */
    public function forEstablishment($status = 'enabled')
    {
        return $this->establishment->sectors()
        ->where('status', $status)
        ->orderBy('name', 'asc')
        ->get();
    }

    public function listSectors($status = 'enabled')
    {
        $sectors = $this->forEstablishment($status);

        return response($sectors);
    }

    public function productsByCategories($sector_id = null)
    {
        $productsQuery = $this->establishment->products()->select('id', 'name', 'product_category_id')
        ->where(['status' => 'enabled', 'sector_id' => '0', 'sector_id' => null]);

        if ($sector_id) {
            $productsQuery->orWhere('sector_id', $sector_id);
        }

        $products = $productsQuery->get();
        $arrProducts = [];
        foreach ($products as $p) {
            if ($p->product_category_id && !isset($arrProducts[$p->product_category_id])) {
                $category = $p->productCategory()->select('id', 'name')->first();
                if ($category) {
                    $arrProducts[$p->product_category_id] = [];
                    $arrProducts[$p->product_category_id]['productCategory'] = $category->toArray();
                }
            }
            if (isset($arrProducts[$p->product_category_id])) {
                $arrProducts[$p->product_category_id]['products'][] = $p->toArray();
            }
        }

        return $arrProducts;
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/profile/sectors/create')
            ->withInput()
            ->withErrors($validator);
        }

        $sector = new Sector();
        $sector->name = $request->name;
        $sector->establishment_id = $this->establishment->id;
        $sector->status = ($request->status == 'enabled') ? 'enabled' : 'disabled';
        $sector->use_default_printer = $request->use_default_printer == '1' ? true : false;
        $sector->printer_name = $request->printer_name;
        $sector->printer_ip = $request->printer_ip;
        $sector->save();
        if ($request->products && count($request->products) > 0) {
            $sector->syncProducts($request->products);
        }

        return redirect('/profile/sectors')
        ->with('success', ['Setor cadastrado com sucesso!']);
    }

    public function update(Request $request, Sector $sector)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return redirect('/profile/sectors')
            ->withInput()
            ->withErrors($validator);
        }
        $sector->name = $request->name;
        $sector->establishment_id = $this->establishment->id;
        $sector->status = ($request->status == 'enabled') ? 'enabled' : 'disabled';
        $sector->use_default_printer = $request->use_default_printer == '1' ? true : false;
        $sector->printer_name = $request->printer_name;
        $sector->printer_ip = $request->printer_ip;
        $sector->save();

        if ($request->products && count($request->products) > 0) {
            $sector->syncProducts($request->products);
        } else {
            $sector->detachProducts();
        }

        return redirect('/profile/sectors/'.$sector->id.'/edit')
        ->with('success', ['Setor editado com sucesso!']);
    }

    public function enable(Sector $sector)
    {
        if ($sector->enable() == Sector::STATUS_DISABLED) {
            return response(['success' => true, 'message' => 'Setor desabilitado com sucesso!']);
        } else {
            return response(['success' => true, 'message' => 'Setor habilitado com sucesso!']);
        }
    }
}
