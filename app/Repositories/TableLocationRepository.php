<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 11/07/16
* Time: 17:53.
*/

namespace App\Repositories;

use App\Models\TableLocation;
use App\Traits\SetEstablishmentTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class TableLocationRepository
{
    use SetEstablishmentTrait;

    /**
     * Lista todos os Locais do estabelecimento.
     *
     * @param string $status [description]
     *
     * @return [type] [description]
     */
    public function forEstablishment($status = 'enabled')
    {
        return $this->establishment->locations()
    ->orderBy('created_at', 'desc')
    ->where('status', $status)->get();
    }

    public function listLocations($status = 'enabled')
    {
        $locations = $this->forEstablishment($status);
        response($locations);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
      'name' => 'required|max:255',
    ]);

        if ($validator->fails()) {
            return redirect('/locations/create')
      ->withInput()
      ->withErrors($validator);
        }

        $location = new TableLocation();
        $location->populate($request, $this->establishment->id);
        if ($request->save_and_add == 1) {
            return redirect('/locations/create')
      ->with('success', ['Local cadastrado com sucesso!']);
        }

        return redirect('/locations')->with('success', ['Local cadastrado com sucesso!']);
    }

    public function update(Request $request, TableLocation $location)
    {
        $validator = Validator::make($request->all(), [
      'name' => 'required|max:255',
    ]);

        if ($validator->fails()) {
            return redirect('/locations/'.$location->id.'/edit')
      ->withInput()
      ->withErrors($validator);
        }
        $location->populate($request);

        return redirect('/locations/'.$location->id.'/edit')
    ->with('success', ['Local editado com sucesso!']);
    }

    public function enable(TableLocation $location)
    {
        $msg = '';
        if ($location->enable() == 'disabled') {
            $msg = 'Local desabilitada com sucesso!';
        } else {
            $msg = 'Local habilitada com sucesso!';
        }

        return response(['success' => true, 'message' => $msg]);
    }
}
