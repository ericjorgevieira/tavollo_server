<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 11/07/16
* Time: 17:53.
*/

namespace App\Repositories;

use App\Models\LogActivity;
use App\Miscelaneous\Utils;
use App\Models\Table;
use App\Traits\SetEstablishmentTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Storage;

class TableRepository
{
    use SetEstablishmentTrait;

    /**
     * Get list of tables from establishment.
     *
     * @return mixed
     */
    public function forEstablishment($status = 'enabled')
    {
        return $this->establishment->tables()
            ->where('status', $status)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    public function listTables($status = 'enabled')
    {
        $tables = $this->forEstablishment($status);

        return response($tables);
    }

    /**
     * Insert Spot.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function insert(Request $request)
    {
        $validator = Validator::make($request->all(), [
      'name' => 'required|max:255',
      'number' => 'required|max:10',
    ]);

        if ($validator->fails()) {
            return redirect('/tables/create')
      ->withInput()
      ->withErrors($validator);
        }

        $isConsumptionCard = ($request->is_consumption_card) ? true : false;

        if (!$this->establishment->config->isTableLimited($request->number, $isConsumptionCard)) {
            Log::info('Inserindo dados de Spot com nome '.$request->name);
            $table = new Table();
            $table->name = $request->name;
            $table->description = $request->description;
            $table->establishment_id = $this->establishment->id;
            $table->external_id = $request->external_id;
            $table->is_consumption_card = $isConsumptionCard;
            $table->status = ($request->status == 'enabled') ? 'enabled' : 'disabled';
            $table->save();

            $table->changeNumber($request->number, $this->establishment->id);
            LogActivity::registerActivity(LogActivity::ACTION_INSERT, 'Cadastro de Spot '.$table->id.' realizado.', $this->establishment->id);
            if ($request->save_and_add == 1) {
                return redirect('/tables/create')
        ->with('success', ['Spots cadastrado com sucesso!']);
            }

            return redirect('/tables')
      ->with('success', ['Spot cadastrado com sucesso!']);
        }

        return redirect('/tables/create')
    ->with('error', ['Número '.$request->number.' já existe. Não é possível cadastrar outro Spot com o mesmo número.']);
    }

    /**
     * Insert a sequence of Spots.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function insertSequence(Request $request)
    {
        $validator = Validator::make($request->all(), [
      'initial_number' => 'required|max:10',
      'final_number' => 'required|max:10',
    ]);

        if ($validator->fails()) {
            return redirect('/tables/create')
      ->withInput()
      ->withErrors($validator);
        }

        $preName = $request->pre_name;
        $initialNumber = $request->initial_number;
        $finalNumber = $request->final_number;
        $isConsumptionCard = ($request->is_consumption_card) ? true : false;
        $status = ($request->status == 'enabled') ? 'enabled' : 'disabled';

        if ($initialNumber >= $finalNumber) {
            return redirect('/tables/sequence')
      ->with('error', ['Número final deve ser maior que o inicial!']);
        }

        for ($i = $initialNumber; $i <= $finalNumber; ++$i) {
            $table = Table::where(['establishment_id' => $this->establishment->id, 'number' => $i,
       'is_consumption_card' => $isConsumptionCard, 'status' => Table::STATUS_ENABLED, ])->first();
            if (!$table) {
                $table = new Table();
                if ($i < 10) {
                    $i = '0'.$i;
                }
                $table->number = $i;
                $table->name = $preName.$i;
                $table->establishment_id = $this->establishment->id;
                $table->is_consumption_card = $isConsumptionCard;
                $table->status = $status;
                $table->generateNewCode();
            } else {
                return redirect('/tables/sequence')
        ->with('error', ['Spot de número '.$i.' já existe. Não é possível cadastrar outro Spot com o mesmo número.']);
            }
        }

        return redirect('/tables')
    ->with('success', ['Spots cadastrados com sucesso!']);
    }

    /**
     * Update Table.
     *
     * @param Request $request [description]
     * @param Table   $table   [description]
     *
     * @return [type] [description]
     */
    public function update(Request $request, Table $table)
    {
        $validator = Validator::make($request->all(), [
      'name' => 'required|max:255',
      'number' => 'required|max:10',
    ]);

        if ($validator->fails()) {
            return redirect('/tables/'.$table->id.'/edit')
      ->withInput()
      ->withErrors($validator);
        }

        $isConsumptionCard = ($request->is_consumption_card) ? true : false;
        $table->name = $request->name;
        $table->save();

        if (!$table->hasActiveBill()) {
            if (!$this->establishment->config->isTableLimited($request->number, $isConsumptionCard, $table->id)) {
                Log::info('Alterando dados de Spot de id:'.$table->id.' com nome '.$table->name.' para nome '.$request->name);
                $table->description = $request->description;
                $table->establishment_id = $this->establishment->id;
                $table->external_id = $request->external_id;
                $table->is_consumption_card = $isConsumptionCard;
                $table->status = ($request->status == 'enabled') ? 'enabled' : 'disabled';
                $table->save();

                $table->changeNumber($request->number, $this->establishment->id);
                LogActivity::registerActivity(LogActivity::ACTION_UPDATE, 'Edição de Spot '.$table->id.' realizada.', $this->establishment->id);

                return redirect('/tables/'.$table->id.'/edit')
        ->with('success', ['Spot editado com sucesso!']);
            }
        } else {
            return redirect('/tables/'.$table->id.'/edit')
      ->with('error', ['Spot está sendo usado no momento! Somente o nome foi alterado.']);
        }

        return redirect('/tables/'.$table->id.'/edit')
    ->with('error', ['Número '.$request->number.' já existe. Não é possível editar o Spot com o mesmo número.']);
    }

    /**
     * Verifica se mesa possui conta ativa.
     *
     * @return bool
     */
    public function hasActiveBill(Table $table)
    {
        return $table->hasActiveBill();
    }

    public function enable(Table $table)
    {
        $response = new Response();

        if (!$table->hasActiveBill()) {
            if ($table->status == 'disabled') {
                $table->status = 'enabled';
                $table->save();
                $msg = 'Spot habilitado com sucesso!';
            } else {
                $table->status = 'disabled';
                $table->save();
                $msg = 'Spot desabilitado com sucesso!';
            }
            $response->setContent(['success' => true, 'message' => $msg]);
            $response->setStatusCode(200);
        } else {
            $msg = 'Spot está sendo usado no momento e não pode ser removida';
            $response->setContent(['error' => true, 'message' => $msg]);
            $response->setStatusCode(200);
        }

        return $response;
    }

    /**
     * Gera novos códigos para todas as mesas do estabelecimento.
     *
     * @return bool
     */
    public function generateTokens()
    {
        $tables = $this->forEstablishment();

        foreach ($tables as $t) {
            $t->generateNewCode();
        }

        return true;
    }

    public function getTableCode(Table $table)
    {
        $response = new Response();

        $code = '';

        if ($table->establishment->id = $this->establishment->id) {
            if ($table->code == '') {
                $table->generateNewCode();
            }
            $code = $table->code;
        } else {
            $code = 'Não disponível';
        }

        $response->setContent($code);
        $response->setStatusCode(200);

        return $response;
    }

    /**
     * Processa importação de produtos em formato .csv.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function importTables(Request $request)
    {
        ini_set('auto_detect_line_endings', true);
        header('Content-Type: text/html; charset=UTF-8');

        $validator = Validator::make($request->all(), [
            'import' => 'required|file|mimes:txt,csv',
        ]);
        if ($validator->fails()) {
            return redirect('/tables/import')->withInput()->withErrors($validator);
        }

        try {
            $path = Storage::putFile('public/imports', $request->file('import'));
            $url = \Config::get('app.url').Storage::url($path);
            $file = fopen($url, 'r');
            $arrMessage = [];
            $i = 0;

            while (($r = fgetcsv($file, 10000, ';')) !== false) {
                if ($i == 0) {
                    ++$i;
                    continue;
                }
                $number = intval($r[0]);
                $name = Utils::normalize($r[1]);
                $is_consumption_card = (strtolower($r[2]) == 'true' || $r[2] == 1) ? 1 : 0;
                $external_id = (strtolower($r[3]) == 'true' || $r[3] == 1) ? 1 : 0;

                $table = Table::where(['establishment_id' => $this->establishment->id, 'number' => $number, 'status' => Table::STATUS_ENABLED])->first();

                if (!$table) {
                    $table = new Table();
                    $table->number = $number;
                    $table->is_consumption_card = $is_consumption_card;
                    $arrMessage[] = 'Spot #'.$number.' - '.$name.' inserido.';
                } else {
                    if ($this->establishment->config->limit_table == 0) {
                        $table->number = $number;
                        $table->is_consumption_card = $is_consumption_card;
                        $arrMessage[] = 'Spot #'.$number.' - '.$name.' alterado.';
                    } else {
                        $arrMessage[] = 'Spot #'.$number.' - '.$name.' alterado com limitações por número.';
                    }
                }
                $table->slug = str_slug($name);
                $table->name = $name;
                $table->external_id = $external_id;
                $table->establishment_id = $this->establishment->id;
                $table->status = Table::STATUS_ENABLED;
                $table->generateNewCode();
            }

            Storage::delete($path);
            LogActivity::registerActivity(LogActivity::ACTION_INSERT, 'Importação de Spots realizada.', $this->establishment->id);

            return redirect('/tables/import')
      ->with('imported', $arrMessage);
        } catch (Exception $e) {
            return redirect('/tables/import')
      ->with('warning', ['Erro ao processar lista. Verifique se as colunas/valores estão bem definidos e tente novamente.']);
        }
    }
}
