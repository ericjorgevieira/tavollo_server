<?php
/**
 * Created by PhpStorm.
 * User: ericoliveira
 * Date: 11/07/16
 * Time: 17:53.
 */

namespace App\Repositories;

use App\Mail\ConfirmUserAccount;
use App\Mail\ResetPassword;
use App\Models\Bill;
use App\Models\BillRating;
use App\Models\PasswordReset;
use App\Models\Rating;
use App\Models\User;
use App\Policies\UserGuard;
use App\Rules\Lowercase;
use App\Rules\UniqueUserEmail;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class UserRepository
{
    /**
     * Register user with credentials.
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'max:255', new Lowercase(), new UniqueUserEmail()],
            'name' => 'required|max:255',
            'cpf' => 'required|max:30|unique:users',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            return response(['success' => false, 'response' => 'Erro ao validar os dados.',
                'messages' => $validator->errors(), ], 422);
        } else {
            try {
                DB::beginTransaction();
                $guard = new UserGuard();
                $guard->newUser($request->all(), null);
                $user = $guard->user();

                //Dispara e-mail de boas vindas do Tavollo e link para ativar conta
                // Mail::to($user)->send(new ConfirmUserAccount($user));

                DB::commit();

                return response([
                    'success' => true,
                    'user' => $user->toPublicArray(),
                    'hash_user' => md5($user->id),
                ]);
            } catch (\Exception $e) {
                DB::rollback();

                return response(['success' => false, 'response' => 'Erro ao processar o cadastro',
                    'message' => $e->getMessage(), ], 500);
            }
        }
    }

    /**
     * Attempt to login user with credentials.
     */
    public function attemptLogin(array $fields = [])
    {
        $validator = Validator::make($fields, [
          'email' => 'bail|email|required|max:255',
          'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            return response(['success' => false, 'response' => 'Erro ao validar os dados.', 'messages' => $validator->errors()], 422);
        } else {
            $guard = new UserGuard();
            if ($guard->attempt($fields)) {
                $guard->user()->generateToken();
                $user = $guard->user();
                Log::info('Usuário logado pelo webapp: '.$user->id);

                return response(['user' => $user->toPublicArray(), 'hash_user' => md5($user->id)]);
            } else {
                return response(['success' => false, 'message' => 'Erro ao autenticar. Verifique seus dados e tente novamente.', 422]);
            }
        }
    }

    /**
     * Attempt to login user with Facebook or Google, or register if not exists.
     *
     * @param array $fields [string]string $fields
     *
     * @return \App\Models\User
     */
    public function attemptSocial(array $fields = [])
    {
        $guard = new UserGuard();
        if ($guard->attemptSocial($fields, false)) {
            if ($guard->user()) {
                $guard->user()->generateToken();
                $this->user = $guard->user();
                Log::info('Usuário logado pelo webapp: '.$this->user->id);

                return response(['user' => $this->user->toPublicArray(), 'hash_user' => md5($this->user->id)]);
            } else {
                return response(['error' => true, 'message' => 'Erro ao autenticar.']);
            }
        } else {
            return response(['error' => true, 'message' => 'Erro ao autenticar.']);
        }
    }

    /**
     * Process mail sending to redefine password.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function forgotPassword(Request $request)
    {
        $res = [];
        $validator = Validator::make($request->all(), ['email' => 'required|max:255']);

        if ($validator->fails()) {
            $res = ['success' => false, 'messages' => $validator->errors()];
        } else {
            $user = User::where(['email' => $request->email, 'status' => User::STATUS_ENABLED])->first();
            if ($user) {
                Mail::to($user)->send(new ResetPassword($user));
                $res = ['success' => true, 'message' => 'Um e-mail foi enviado para redefinir a senha.'];
            } else {
                $res = ['success' => false, 'message' => 'E-mail não consta na nossa base.'];
            }
        }

        return response($res);
    }

    /**
     * Process password redefinition.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function redefinePassword(Request $request)
    {
        $token = $request->input('token');
        $email = $request->input('email');
        $user = null;
        $error = false;
        $passwordReset = PasswordReset::where(['token' => $token])->first();

        if ($passwordReset && $passwordReset->email == $email) {
            $user = User::where(['email' => $passwordReset->email, 'status' => User::STATUS_ENABLED, 'profile' => User::PROFILE_CUSTOMER])->first();
            if ($user) {
                $user->password = bcrypt($request->input('password'));
                $user->save();

                $passwordReset->delete();
            } else {
                $error = true;
            }
        } else {
            $error = true;
        }

        return response(['success' => !$error, 'user' => $user]);
    }

    /**
     * Update profile on Admin.
     */
    public function updateProfile(Request $request, $user)
    {
        $validator = Validator::make($request->all(), [
          'name' => 'required|max:255',
          'email' => 'required|unique:users,email,'.$user->id,
        ]);

        if ($validator->fails()) {
            return redirect('/profile')->withInput()->withErrors($validator);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return redirect('/profile')->with('success', ['Perfil alterado com sucesso!']);
    }

    /**
     * Update profile on Webapp.
     *
     * @param User   $user  [description]
     * @param [type] $email [description]
     * @param [type] $cpf   [description]
     *
     * @return [type] [description]
     */
    public function updateProfileWebapp(User $user, $name = null, $email, $cpf)
    {
        if ($name) {
            $user->name = $name;
        }
        if ($email) {
            $user->email_account = $email;
        }
        // if ($cpf && User::where('cpf', $cpf)->first()) {
        // return response(array('error' => true, 'message' => 'CPF já existe na nossa base.'));
        // }
        if ($cpf && User::validaCPF($cpf)) {
            $user->cpf = $cpf;
        } else {
            return response(['error' => true, 'message' => 'CPF inválido.']);
        }
        $user->save();

        return response(['success' => true, 'message' => 'Perfil atualizado com sucesso.']);
    }

    /**
     * Update password on Admin.
     */
    public function updatePassword(Request $request, $user)
    {
        $validator = Validator::make($request->all(), [
          'password' => 'required|max:16|hash:'.$user->password,
          'new_password' => 'required|max:16|different:password|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('/profile/password')->withInput()->withErrors($validator);
        }

        $user->password = Hash::make($request->new_password);
        $user->save();

        return redirect('/profile/password')->with('success', ['Senha alterada com sucesso!']);
    }

    /**
     * Rating Tavollo.
     */
    public function rating(User $user, $ratingValue = 0, $comment = '')
    {
        $rating = new Rating();
        $rating->user_id = $user->id;
        $rating->rating = $ratingValue;
        $rating->comment = $comment;
        $rating->save();

        return response(['success' => 'Avaliação registrada com sucesso.']);
    }

    /**
     * Rating opened Bill.
     */
    public function ratingBill(Bill $bill, User $user, $ratingValue = 0, $answers, $comment = '')
    {
        if ($bill->user_id == $user->id) {
            $billRating = new BillRating();
            $billRating->bill_id = $bill->id;
            $billRating->user_id = $user->id;
            $billRating->establishment_id = $bill->spot->establishment_id;
            $billRating->rating = $ratingValue;
            $billRating->answers = json_encode($answers);
            $billRating->comment = $comment;
            $billRating->save();

            return response(['success' => 'Avaliação registrada com sucesso.']);
        }

        return response(['error' => 'Conta não pertence à usuário']);
    }
}
