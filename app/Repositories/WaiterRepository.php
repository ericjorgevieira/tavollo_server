<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 11/07/16
* Time: 17:53.
*/

namespace App\Repositories;

use App\Models\Establishment;
use App\Models\Waiter;
use App\Traits\SetEstablishmentTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class WaiterRepository
{
    use SetEstablishmentTrait;

    /**
     * Get list of waiters from establishment.
     *
     * @return mixed
     */
    public function forEstablishment($status = 'enabled')
    {
        return $this->establishment->waiters()
        ->where('status', $status)
        ->orderBy('name', 'asc')
        ->get();
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'register' => ['required', 'max:100', Rule::unique('waiters')->whereNull('deleted_at')->where('establishment_id', $this->establishment->id)],
            'new_password' => 'required|max:16|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('/profile/waiters/create')
            ->withInput()
            ->withErrors($validator);
        }

        if ($request->input('work_begin') == $request->input('work_end')) {
            return redirect('/profile/waiters')
                ->withInput()
                ->with('error', ['Os horários de início e encerramento não podem ser iguais.']);
        }

        $waiter = new Waiter();
        $waiter->populate($request, $this->establishment->id);

        return redirect('/profile/waiters')
        ->with('success', ['Garçom cadastrado com sucesso!']);
    }

    public function update(Request $request, Waiter $waiter)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'register' => ['required', 'max:100', Rule::unique('waiters')->ignore($waiter->id)->whereNull('deleted_at')->where('establishment_id', $this->establishment->id)],
            'new_password' => 'max:16|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('/profile/waiters/'.$waiter->id.'/edit')
            ->withInput()
            ->withErrors($validator);
        }

        if ($request->input('work_begin') == $request->input('work_end')) {
            return redirect('/profile/waiters/'.$waiter->id.'/edit')
                ->withInput()
                ->with('error', ['Os horários de início e encerramento não podem ser iguais.']);
        }

        $waiter->populate($request, $this->establishment->id);

        return redirect('/profile/waiters/'.$waiter->id.'/edit')
        ->with('success', ['Garçom editado com sucesso!']);
    }

    public function enable(Waiter $waiter)
    {
        if (!$this->checkIfHasDefaultWaiter($waiter) && $waiter->is_default) {
            return response(['error' => true,
              'message' => 'Você não pode desativar o garçom padrão! Defina um novo garçom padrão e tente novamente.', ]);
        } else {
            if ($waiter->enable() == Waiter::STATUS_DISABLED) {
                return response(['success' => true, 'message' => 'Garçom desabilitado com sucesso!']);
            } else {
                return response(['success' => true, 'message' => 'Garçom habilitado com sucesso!']);
            }
        }
    }

    /**
     * Confere se existe um garçom padrão cadastrado.
     *
     * @param [type] $waiter [description]
     *
     * @return [type] [description]
     */
    public function checkIfHasDefaultWaiter(Waiter $waiter = null)
    {
        $waiters = $this->establishment->waiters()->orderBy('status', 'asc')->get();
        $defaultWaiter = array_filter($waiters->toArray(), function ($w) {
            return $w['is_default'] == 1;
        });

        if (count($defaultWaiter)) {
            if ($waiter && array_shift($defaultWaiter)['id'] != $waiter->id) {
                return true;
            }
        }

        return false;
    }

    public function destroy(Waiter $waiter)
    {
        if (!$waiter->is_default) {
            $user = $waiter->user;
            $waiterDefault = Waiter::where(['establishment_id' => $waiter->establishment_id, 'is_default' => true, 'status' => Waiter::STATUS_ENABLED])->first();
            $waiter->orders()->withTrashed()->update(['waiter_id' => ($waiterDefault) ? $waiterDefault->id : null, 'by_waiter' => false, 'by_admin' => true]);
            $waiter->forceDelete();
            if ($user) {
                $user->forceDelete();
            }

            return response(['success' => true, 'message' => 'Garçom removido com sucesso!']);
        }

        return response(['success' => false, 'message' => 'Você não pode remover o garçom padrão!']);
    }
}
