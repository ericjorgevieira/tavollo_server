<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 14/08/16
* Time: 01:04.
*/

namespace App\Repositories\Webservice;

use App\Models\Establishment;
use App\Events\PreOrderCreated;
use App\Models\Order;
use App\Models\PreOrder;
use App\Models\Product;
use App\Models\User;
use Cache;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class WSPreOrderRepository
{
    private $user;

    public function getActive(User $user)
    {
        $preOrder = $user->getActivePreOrder();
        $arrResponse = [];
        if ($preOrder) {
            $arrResponse = ['success' => true, 'preorder' => $preOrder->toArray(), 'hash' => md5($preOrder->establishment_id)];
        } else {
            $arrResponse = ['success' => false, 'message' => 'Nenhum pedido encontrado.'];
        }

        return response($arrResponse);
    }

    public function listActive(User $user)
    {
        $preOrders = $user->listActivePreOrders();
        $arrResponse = [];
        if ($preOrders) {
            $arrResponse = ['success' => true, 'preorders' => $preOrders->toArray()];
        } else {
            $arrResponse = ['success' => false, 'message' => 'Nenhum pedido encontrado.'];
        }

        return response($arrResponse);
    }

    /**
     * Adiciona o pedido (Order) do usuário logado.
     *
     * @return Response
     */
    public function addPreOrder(
        User $user,
        $establishment_id,
        $products,
        $card_flag,
        $transaction_code,
        $merchant_order_id = null,
        $transaction_type = null,
        $alias = null,
        $payment_id = null
    ) {
        // $preOrder = $user->getActivePreOrder();
        $arrResponse = [];
        // if (!$preOrder) {
        //Se não tiver nenhuma preorder aberta, continua
        $establishment = Establishment::find($establishment_id);

        if ($establishment) {
            if($establishment->config->allow_pre_bills){
                Log::info('Pré-pedido será adicionado pelo usuário '.$user->id);
                Log::info('Estabelecimento: '.$establishment_id);
                Log::info('Transaction code da Cielo: '.$transaction_code);
    
                $preOrder = new PreOrder();
                $preOrder->establishment_id = $establishment->id;
                $preOrder->user_id = $user->id;
                $preOrder->card_flag = $card_flag;
                $preOrder->transaction_code = $transaction_code;
                $preOrder->transaction_type = $transaction_type;
                $preOrder->merchant_order_id = $merchant_order_id;
                $preOrder->alias = $alias;
                $preOrder->cielo_payment_id = $payment_id;
                $preOrder->save();
    
                foreach ($products as $p) {
                    $product = Product::where(['id' => $p['product']['id'], 'establishment_id' => $establishment->id])->first();
                    if ($product) {
                        $preOrder->addProduct($product, $p['quant'], $p['obs'], $p['options']);
                        Log::info('Produto '.$product->id.' adicionado pelo usuário '.$user->id.' no pré-pedido '.$preOrder->id);
                    } else {
                        Log::info('Produto '.$p['product']['id'].' não foi adicionado. Estabelecimento não possui produto ou ele foi excluído.');
                    }
                }
                //Calcula cache total
                $preOrder->updateCacheTotal();
                //Após adicionar pedidos, envia para a Raffinato e retorna o OK
                $this->broadcastPreOrderEvents($preOrder);
    
                $arrResponse = ['success' => true, 'message' => 'Seu pedido foi solicitado!', 'preorder' => $preOrder];
            } else {
                $arrResponse = ['success' => false, 'message' => 'Estabelecimento não aceita pedidos pre-pagos.'];
            }
        } else {
            $arrResponse = ['success' => false, 'message' => 'Estabelecimento não encontrado.'];
        }
        // } else {
        //     $arrResponse = array('success' => false, 'message' => 'Você já possui um pedido em aberto.');
        // }

        return response($arrResponse);
    }

    /**
     * Confirma a entrega de um preorder.
     *
     * @param [type] $id   [description]
     * @param User   $user [description]
     *
     * @return [type] [description]
     */
    public function confirmPreOrder($id, User $user)
    {
        $preOrder = PreOrder::where(['id' => $id, 'user_id' => $user->id])->first();
        if ($preOrder) {
            $preOrder->setStatus(PreOrder::STATUS_DELIVERED);

            return response(['success' => true, 'message' => 'Obrigado por confirmar o seu pedido. Boa refeição!']);
        }

        return response(['error' => true, 'message' => 'Erro ao confirmar entrega do pedido.']);
    }

    /**
     * Confirma a entrega de um preorder.
     *
     * @param [type] $id   [description]
     * @param User   $user [description]
     *
     * @return [type] [description]
     */
    public function cancelPreOrder($id, User $user)
    {
        $preOrder = PreOrder::where(['id' => $id, 'user_id' => $user->id])->first();
        if ($preOrder) {
            $preOrder->setStatus(PreOrder::STATUS_CANCELED);

            return response(['success' => true, 'message' => 'Seu pedido foi cancelado com sucesso. O estorno será processado em breve.']);
        }

        return response(['error' => true, 'message' => 'Erro ao cancelar o seu pedido.']);
    }

    /**
     * Verifica se Pedido (PreOrder) está encerrado.
     *
     * @param User $user
     *
     * @return Response
     */
    public function hasPreOrderClosed($id, User $user = null)
    {
        $bill = Bill::find($id);
        try {
            if ($bill) {
                if ($bill->status == Bill::STATUS_CLOSED) {
                    if ($bill->spot->establishment->config &&
          $bill->spot->establishment->config->generate_codes == '1') {
                        $bill->spot->generateNewCode();
                    }
                    Log::info('Encerramento da conta '.$bill->id.' pelo usuário '.$user->id.' concluida');
                    $arrResponse = ['success' => true, 'message' => 'A conta foi encerrada com sucesso!'];
                } else {
                    $arrResponse = ['error' => true, 'message' => 'A conta ainda não foi encerrada'];
                }
            } else {
                //Caso conta não seja encontrada, retorna success para encerrar sessão no APP
                $arrResponse = ['success' => true, 'message' => 'A conta não foi encontrada.'];
            }
        } catch (\Exception $e) {
            if ($user == null) {
                $arrResponse = ['success' => true, 'message' => 'Usuário não se encontra mais autenticado.'];
            }
        }

        return response($arrResponse);
    }

    /**
     * Dispara os eventos de socket relacionados a um pedido.
     *
     * @param [type] $order [description]
     *
     * @return [type] [description]
     */
    private function broadcastPreOrderEvents(PreOrder $order)
    {
        broadcast(new PreOrderCreated($order));
    }
}
