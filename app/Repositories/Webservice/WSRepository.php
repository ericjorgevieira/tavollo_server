<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 14/08/16
* Time: 01:04.
*/

namespace App\Repositories\Webservice;

use App\Models\Bill;
use App\Models\BillAttachedUser;
use App\Models\BillAttachmentRequest;
use App\Drivers\DriverRaffinato;
use App\Models\Establishment;
use App\Models\EstablishmentAccess;
use App\Events\BillAttachmentRequesting;
use App\Events\BillClosing;
use App\Events\BillCreated;
use App\Events\HasAttached;
use App\Events\HasBillChanged;
use App\Events\OrderCanceled;
use App\Models\Order;
use App\Models\Product;
use App\Models\Table;
use App\Traits\SetEstablishmentTrait;
use App\Models\User;
use Cache;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class WSRepository
{
    use SetEstablishmentTrait;

    private $user;

    /**
     * Gera nova conta (Bill) para o usuário.
     *
     * @param $code
     *
     * @return Response
     */
    public function generateBill(User $user, $code)
    {
        $bill = null;
        $arrReturn = [];

        try {
            if ($user) {
                $bill = $user->getActiveBill();
                $table = Table::where(['code' => strtolower($code), 'status' => 'enabled'])->first();

                if ($table) {
                    //Se a conta existir, efetua uma verificação
                    if ($bill) {
                        //Se a conta já estiver aberta no Spot, e pertencer ao usuário, retorna a conta aberta
                        if ($bill->user_id == $user->id && $bill->table_id == $table->id) {
                            $arrReturn = [
                            'bill' => $bill->toPublicArray(),
                          ];
                            Log::info('Conta '.$bill->id.' resgatada para usuário '.$user->id);
                        } else {
                            //Se não, informa que a conta aberta não é desse Spot, e o usuário possui conta aberta em outro local
                            Log::info('Erro ao criar conta para usuário '.$user->id);

                            return response(['error' => true, 'title' => 'Erro', 'message' => 'Você já possui uma conta aberta.']);
                        }
                    } else {
                        //Se não existir conta do usuário logado, faz outra verificação
                        if ($table->hasActiveBill()) {
                            //Caso o Spot possua conta em aberto, verifica se a conta está sem usuário
                            $bill = $table->getActiveBill();
                            if ($bill->temp_user == 1) {
                                //Se estiver sem usuário, libera o acesso do usuário à conta aberta
                                //Recupera Table
                                $bill = Bill::openBill($table, $user);

                                EstablishmentAccess::registerAccess($table->establishment_id, 'bill');

                                $arrReturn = [
                                    'bill' => $bill->toPublicArray(),
                                ];
                                Log::info('Conta '.$bill->id.' criada para usuário '.$user->id);
                            } else {
                                //Se não estiver sem usuário, barra o acesso
                                Log::info('Usuário '.$user->id.' tentou entrar em conta já existente com usuário e foi barrado.');
                                throw new \Exception('Erro na autenticação do usuário. Tente novamente.');
                            }
                        } else {
                            //Caso o Spot NÃO possua conta em aberto, libera o acesso do usuário
                            //Recupera Table
                            $bill = Bill::openBill($table, $user);

                            EstablishmentAccess::registerAccess($table->establishment_id, 'bill');

                            $arrReturn = [
                                'bill' => $bill->toPublicArray(),
                            ];
                            Log::info('Conta '.$bill->id.' criada para usuário '.$user->id);
                        }
                    }
                } else {
                    //Se não estiver sem usuário, barra o acesso
                    Log::info('Spot '.$code.' não encontrado.');
                    throw new \Exception('Erro na autenticação do usuário. Tente novamente.');
                }
            } else {
                Log::error('Erro de autenticação do usuário');
                throw new \Exception('Erro na autenticação do usuário. Tente novamente.');
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            if ($bill) {
                Log::info('Conta destruida: '.$bill->id);
                Bill::destroy($bill->id);
            }
            $arrReturn = ['error' => true, 'message' => $e->getMessage()];

            return response($arrReturn, 401);
        }

        return response($arrReturn);
    }

    /**
     * Retorna os dados da conta (Bill) do usuário.
     *
     * @return Response
     */
    public function getBill(User $user, $status = 'opened', $isAttached = false, $billId = null)
    {
        if ($billId) {
            //Consulta extrato da conta no PDV e retorna atualizada
            DriverRaffinato::loadExtract($billId);
            $bill = Bill::where(['id' => $billId])->first();
        } else {
            $bill = $user->getActiveBill($status, $isAttached);
        }
        if ($bill && $bill != null) {
            return response($bill->toPublicArray());
        }

        return response(['error' => true, 'message' => 'Conta não encontrada']);
    }

    public function changeAttachedUserStatus(Bill $bill, $attachedUserId, $status = 'closed')
    {
        $attached = BillAttachedUser::where(['bill_id' => $bill->id, 'user_id' => $attachedUserId])->first();
        if ($attached) {
            $attached->status = $status;
            if ($status == 'closed') {
                $attached->closed_at = Carbon::now();
            }
            $attached->save();
            // $bill->changeAttachedPermission($attached->user, $status);
            $arrResponse = ['success' => true, 'message' => 'Participação do usuário encerrada.'];
        } else {
            $arrResponse = ['success' => false, 'message' => 'Usuário não encontrado na conta.'];
        }

        return response($arrResponse);
    }

    public function getAttachedUserStatus(User $user, Bill $bill)
    {
        if (!$bill->attachedHasPermission($user)) {
            $arrResponse = ['success' => true, 'message' => 'Sua participação foi encerrada.'];
        } else {
            $arrResponse = ['success' => false, 'message' => 'Sua participação não foi encerrada.'];
        }

        return response($arrResponse);
    }

    /**
     * Adiciona o pedido (Order) na conta (Bill) do usuário logado.
     *
     * @param $quant
     * @param $obs
     *
     * @return Response
     */
    public function addOrderToBill(User $user, $productId, $quant, $obs, $options)
    {
        $bill = $user->getActiveBill('opened');
        $arrResponse = [];

        if ($bill) {
            $table = $bill->spot;
            if ($table && $table->establishment && !$table->establishment->config->isAvailable()) {
                $arrResponse = ['error' => true, 'message' => 'O estabelecimento já encerrou sua operação.'];
            } elseif ($bill->status != Bill::STATUS_OPENED) {
                $arrResponse = ['error' => true, 'message' => 'Sua conta não está habilitada para receber pedidos no momento.'];
            } else {
                $product = Product::where(['id' => $productId, 'establishment_id' => $this->establishment->id])->first();
                $order = $bill->addOrder($product, $quant, $obs, null, $options, Order::STATUS_OPENED, null, false, false, $user);
                Log::info('Pedido '.$order->id.' adicionado pelo usuário '.$user->id.' na conta '.$bill->id);
                $arrResponse = ['success' => true, 'message' => 'Seu pedido foi solicitado!'];
            }
        } else {
            $arrResponse = ['error' => true, 'message' => 'Erro ao processar o seu pedido.'];
        }

        return response($arrResponse);
    }

    /**
     * Adiciona vários pedidos do carrinho do App.
     *
     * @param User   $user   [description]
     * @param [type] $orders [description]
     */
    public function addOrdersToBill(User $user, $orders)
    {
        $bill = $user->getActiveBill('opened');
        if ($bill) {
            $table = $bill->spot;
            if ($table && $table->establishment && !$table->establishment->config->isAvailable()) {
                $arrResponse = ['error' => true, 'message' => 'O estabelecimento já encerrou sua operação.'];
            } else {
                try {
                    foreach ($orders as $o) {
                        $product = Product::where(['id' => $o['product']['id'], 'establishment_id' => $bill->spot->establishment->id])->first();
                        if ($product) {
                            $bill->addOrder($product, $o['quant'], $o['obs'], null, $o['options'], Order::STATUS_OPENED, null, false, false, $user);
                        } else {
                            throw new Exception('Produto de id '.$o['product']['id'].' não encontrado');
                        }
                    }
                    Log::info('Pedido(s) adicionado(s) pelo usuário '.$user->id.' na conta '.$bill->id);
                    $arrResponse = ['success' => true, 'message' => 'Seus pedidos foram enviados com sucesso!'];
                } catch (Exception $e) {
                    $arrResponse = ['error' => true, 'message' => 'Erro no processamento de pedidos. '.$e->getMessage()];
                }
            }
        } else {
            $arrResponse = ['error' => true, 'message' => 'Erro ao processar os seus pedidos.'];
        }

        return response($arrResponse);
    }

    /**
     * Processa solicitação de cancelamento do pedido (Order).
     *
     * @return Response
     */
    public function cancelOrder(User $user, Order $order)
    {
        $bill = $user->getActiveBill('opened', false, false);

        if ($order->bill_id == $bill->id) {
            $order->status = 'canceled';
            $order->save();
            Log::info('Pedido '.$order->id.' cancelado pelo usuário '.$user->id.' na conta '.$bill->id);
            broadcast(new OrderCanceled($order));
            broadcast(new HasBillChanged($order->bill));
            $arrResponse = ['success' => true, 'message' => 'Solicitação efetuada. Aguarde.'];
        } else {
            $arrResponse = ['error' => true, 'message' => 'Erro ao solicitar cancelamento.'];
        }

        return response($arrResponse);
    }

    /**
     * Processa solicitação de encerramento da conta (Bill).
     *
     * @return Response
     */
    public function requestCloseBill(User $user, $cardFlag = null, $transactionCode = null, $transactionType = null)
    {
        $bill = $user->getActiveBill('opened', false, false);
        if ($bill) {
            $bill->card_flag = $cardFlag;
            $bill->transaction_code = $transactionCode;
            $bill->transaction_type = $transactionType;
            $bill->status = Bill::STATUS_CLOSING;
            $bill->save();

            BillAttachedUser::where(['bill_id' => $bill->id])->update(['status' => 'closed']);

            $arrResponse = [
                'success' => true,
                'message' => 'Aguarde um momento. Sua conta será encerrada.',
                'bill' => $bill->toPublicArray(),
            ];
            Log::info('Solicitação de encerramento da conta '.$bill->id.' pelo usuário '.$user->id);

            broadcast(new BillClosing($bill));
        } else {
            $arrResponse = [
                'success' => false,
                'message' => 'Comanda não encontrada',
            ];
        }

        return response($arrResponse);
    }

    /**
     * Verifica se conta (Bill) está encerrada.
     *
     * @param User $user
     *
     * @return Response
     */
    public function hasBillClosed($id, User $user = null)
    {
        $bill = Bill::find($id);
        try {
            if ($bill) {
                if ($bill->status == Bill::STATUS_CLOSED) {
                    if ($bill->spot->establishment->config && $bill->spot->establishment->config->generate_codes == '1') {
                        $bill->spot->generateNewCode();
                    }
                    Log::info('Encerramento da conta '.$bill->id.' pelo usuário '.$user->id.' concluida');
                    $arrResponse = ['success' => true, 'message' => 'A conta foi encerrada com sucesso!'];
                } else {
                    $arrResponse = ['error' => true, 'message' => 'A conta ainda não foi encerrada'];
                }
            } else {
                //Caso conta não seja encontrada, retorna success para encerrar sessão no APP
                $arrResponse = ['success' => true, 'message' => 'A conta não foi encontrada.'];
            }
        } catch (\Exception $e) {
            if ($user == null) {
                $arrResponse = ['success' => true, 'message' => 'Usuário não se encontra mais autenticado.'];
            }
        }

        return response($arrResponse);
    }

    /**
     * Informa o alias do Spot para identificar localização.
     *
     * @param User   $user  [description]
     * @param [type] $alias [description]
     */
    public function setTableAlias(User $user, $alias)
    {
        $bill = $user->getActiveBill('opened', false, false);
        $bill->spot->alias = strip_tags(trim($alias));
        $bill->spot->save();
        broadcast(new BillCreated($bill));
        broadcast(new HasBillChanged($bill));

        $arrResponse = ['success' => true, 'message' => 'Identificação do local foi salva!'];

        return response($arrResponse);
    }

    public function requestBillAttachment(Bill $bill = null, User $user = null)
    {
        if ($bill && $user) {
            if ($bill->user_id != $user->id) {
                if ($bill->spot->is_consumption_card == 0) {
                    $request = BillAttachmentRequest::where(['bill_id' => $bill->id, 'user_id' => $user->id])->first();
                    if ($request && $request->status == BillAttachmentRequest::STATUS_OPENED) {
                        $request->delete();
                    } elseif ($request && $request->is_attached == 1) {
                        return response(['success' => true, 'request' => $request]);
                    } elseif ($request && $request->status == BillAttachmentRequest::STATUS_CLOSED) {
                        return response(['error' => true, 'request' => $request, 'message' => 'Sua solicitação já foi negada anteriormente.']);
                    }
                    $request = new BillAttachmentRequest();
                    $request->user_id = $user->id;
                    $request->bill_id = $bill->id;
                    $request->status = BillAttachmentRequest::STATUS_OPENED;
                    $request->save();

                    broadcast(new BillAttachmentRequesting($request, $user));

                    return response(['success' => true, 'request' => $request]);
                } else {
                    return response(['error' => true, 'message' => 'O código pertence a um cartão consumo. Solicite o seu e tente novamente.']);
                }
            } else {
                return response(['error' => true, 'message' => 'Você já possui conta em aberto.']);
            }
        } else {
            return response(['error' => true, 'message' => 'Essa conta já foi encerrada.']);
        }
    }

    /**
     * Retorna lista de requisições para entrar na conta do usuário.
     *
     * @param User $user [description]
     *
     * @return [type] [description]
     */
    public function listBillAttachmentRequests(User $user)
    {
        $bill = $user->getActiveBill('opened', false, false);
        if ($bill) {
            $attachmentRequests = BillAttachmentRequest::join('users', 'users.id', '=', 'bill_attachment_requests.user_id')
      ->where(['bill_attachment_requests.bill_id' => $bill->id, 'bill_attachment_requests.status' => 'opened'])
      ->first(['bill_attachment_requests.*', 'users.name as user_name', 'users.photo as user_photo']);
        } else {
            $attachmentRequests = [];
        }

        return response($attachmentRequests);
    }

    /**
     * Processa a inclusão ou não do usuário em uma determinada conta.
     *
     * @return [type] [description]
     */
    public function processAttach($requestId, $attach = true)
    {
        $billAttachmentRequest = BillAttachmentRequest::where(['id' => $requestId])->first();
        $billAttachmentRequest->status = BillAttachmentRequest::STATUS_CLOSED;
        $billAttachmentRequest->is_attached = $attach;
        $billAttachmentRequest->save();

        if ($attach) {
            $attachedUser = new BillAttachedUser();
            $attachedUser->user_id = $billAttachmentRequest->user_id;
            $attachedUser->bill_id = $billAttachmentRequest->bill_id;
            $attachedUser->status = BillAttachedUser::STATUS_OPENED;
            $attachedUser->opened_at = Carbon::now();
            $attachedUser->save();
        }
        broadcast(new HasAttached($billAttachmentRequest->user, $attach));
    }

    /**
     * Lista todos os Locais para sugerir ao usuário em caso de cartão consumo.
     */
    public function listLocations(Establishment $establishment = null)
    {
        try {
            if ($establishment) {
                $locationQuery = $establishment->locations()->where(['status' => 'enabled']);
                $locations = Cache::remember($establishment->id.'_locations_query', 120, function () use ($locationQuery) {
                    return $locationQuery->get(['id', 'name', 'slug']);
                });

                return response(['locations' => $locations]);
            } else {
                return response(['error' => 'No establishment defined.']);
            }
        } catch (\Exception $e) {
            return response(['error' => 'No establishment defined.']);
        }
    }
}
