<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 10/02/17
* Time: 14:01
*/

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

trait GetResponseTrait
{
    /**
    * Retorna a resposta em formato JSON com array de dados
    * @param  [type]  $data   [description]
    * @param  integer $status [description]
    * @return [type]          [description]
    */
    private function returnResponse($data, $status = 200){
        $response = new Response();

        $response->setContent($data);
        $response->setStatusCode($status);

        return $response;
    }
}
