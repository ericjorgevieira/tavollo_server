<?php
/**
* Created by PhpStorm.
* User: ericoliveira
* Date: 10/02/17
* Time: 14:01.
*/

namespace App\Traits;

use App\Models\Bill;
use App\Models\Establishment;
use App\Models\Product;
use Illuminate\Support\Facades\Log;

trait SetEstablishmentTrait
{
    protected $establishment;

    /**
     * Define o estabelecimento do Repository.
     */
    public function setEstablishment(Establishment $establishment)
    {
        $this->establishment = $establishment;
    }

    /**
     * Retorna conta ligada ao estabelecimento.
     *
     * @param int $bill_id
     *
     * @return Bill
     */
    public function getEstablishmentBill($bill_id)
    {
        $bill = Bill::join('tables', 'tables.id', '=', 'bills.table_id')
        ->where([
            'tables.establishment_id' => $this->establishment->id,
            'bills.id' => $bill_id,
        ])
        ->get(['bills.*'])->first();

        return $bill;
    }

    /**
     * Retorna produto ligado ao estabelecimento.
     *
     * @param int $product_id
     * @param int $code
     *
     * @return Product
     */
    public function getEstablishmentProduct($product_id = null, $code = null)
    {
        $arrWhere = ['establishment_id' => $this->establishment->id];
        if ($product_id == null && $code == null) {
            return false;
        }
        if ($product_id) {
            $arrWhere['id'] = $product_id;
        }
        if ($code) {
            $arrWhere['code'] = $code;
        }
        $product = Product::where($arrWhere)->first();
        Log::info('Produto do estabelecimento '.$this->establishment->id.' resgatado com o id:'.$product_id.' ou code:'.$code);

        return $product;
    }
}
