<?php

namespace App\Traits;

trait StatusEnableTrait{

  /**
  * Habilita ou desabilita objeto
  * @return [type] [description]
  */
  public function enable()
  {
    if ($this->status == self::STATUS_DISABLED) {
      $this->status = self::STATUS_ENABLED;
    } else {
      $this->status = self::STATUS_DISABLED;
    }
    $this->save();
    return $this->status;
  }
}
