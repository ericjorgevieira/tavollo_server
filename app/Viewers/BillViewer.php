<?php

namespace App\Viewers;

use App\Models\Bill;
use App\Models\Establishment;
use Carbon\Carbon;

class BillViewer
{
    public function listForTable(Establishment $establishment, $limit = 50, $status = '', $period = '')
    {
        $billsQuery = Bill::join('tables', 'tables.id', '=', 'bills.table_id')
        ->where(['tables.establishment_id' => $establishment->id]);
        if ($limit == '') {
            $limit = 50;
        }
        if ($status != '') {
            $billsQuery->where(['bills.status' => $status]);
        }
        if ($period != '') {
            $date = new Carbon();
            $date->subDays($period);
            $billsQuery->where('bills.opened_at', '>=', $date->toDateTimeString());
        }
        $bills = $billsQuery->orderBy('bills.status', 'asc')->orderBy('bills.opened_at', 'desc')->limit($limit)->get(['bills.*']);

        $arrBills = [];

        foreach ($bills as $p) {
            $total = Bill::getTotalValue($p);
            $baksheesh = $p->baksheesh_value;

            $raw_user = unserialize($p->raw_user);
            $user = $p->temp_user == 1 ? $raw_user['username'] : $p->user->name;

            $arrBills[] = [
                'id' => $p->id,
                'photo' => $p->temp_user == 1 ? 'https://via.placeholder.com/200x200&text=Sem+Foto' : $p->user->photo,
                'user' => $user,
                'spot' => $p->table->name,
                'total' => $total + $baksheesh,
                'baksheesh' => $baksheesh,
                'opened_at' => $p->opened_at,
                'status' => $p->status,
                'has_action_button' => true,
                'action_url' => 'bills/'.$p->id,
                'is_action_asynchronous' => false,
            ];
        }

        $arrData = [
            'model' => 'bills',
            'unsearchable_key' => 'photo',
            'columns' => [
                'photo' => [
                    'label' => 'Foto',
                    'hidden_xs' => true,
                    'type' => 'string',
                ],
                'spot' => [
                    'label' => 'Spot',
                    'hidden_xs' => false,
                    'type' => 'string',
                ],
                'user' => [
                    'label' => 'Cliente',
                    'hidden_xs' => true,
                    'type' => 'string',
                ],
                'opened_at' => [
                    'label' => 'Data de Abertura',
                    'hidden_xs' => false,
                    'type' => 'string',
                ],
                'total' => [
                    'label' => 'Total',
                    'hidden_xs' => false,
                    'type' => 'float',
                ],
            ],
            'objects' => $arrBills,
        ];

        return response($arrData);
    }
}
