<?php

namespace App\Viewers;

use App\Models\Establishment;

class IslandViewer
{
    public function listForTable(Establishment $establishment)
    {
        $islands = $establishment->islands()->orderBy('status', 'asc')->get();
        $arrIslands = [];
        foreach ($islands as $m) {
            $arrIslands[] = [
                'id' => $m->id,
                'name' => $m->name,
                'tables' => $m->getTablesToList(),
                'waiter' => $m->getWaiterToList(),
                'status' => $m->status,
                'has_action_button' => false,
                'action_url' => '',
                'is_action_asynchronous' => false,
            ];
        }
        $arrData = [
            'model' => 'profile/islands',
            'unsearchable_key' => 'tables',
            'columns' => [
                'name' => [
                    'label' => 'Ilhas',
                    'hidden_xs' => false,
                    'type' => 'string',
                ],
                'tables' => [
                    'label' => 'Spots',
                    'hidden_xs' => true,
                    'type' => 'array',
                ],
                'waiter' => [
                    'label' => 'Garçom',
                    'hidden_xs' => false,
                    'type' => 'string',
                ],
            ],
            'objects' => $arrIslands,
        ];

        return response($arrData);
    }
}
