<?php

namespace App\Viewers;

use App\Models\Establishment;

class MenuViewer
{
    public function listForTable(Establishment $establishment)
    {
        $menus = $establishment->menus()->orderBy('status', 'asc')->orderBy('position', 'asc')->get();
        $arrMenus = [];
        foreach ($menus as $m) {
            $arrMenus[] = [
              'id' => $m->id,
              'name' => $m->name,
              'products' => $m->getProductsToList(),
              'status' => $m->status,
              'has_action_button' => false,
              'action_url' => '',
              'is_action_asynchronous' => false,
          ];
        }
        $arrData = [
          'model' => 'menus',
          'unsearchable_key' => 'products',
          'columns' => [
              'name' => [
                  'label' => 'Cardápios',
                  'hidden_xs' => false,
                  'type' => 'string',
              ],
              'products' => [
                  'label' => 'Produtos',
                  'hidden_xs' => false,
                  'type' => 'array',
              ],
          ],
          'objects' => $arrMenus,
      ];

        return response($arrData);
    }
}
