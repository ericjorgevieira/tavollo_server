<?php

namespace App\Viewers;

use App\Models\Establishment;
use App\Models\PreOrder;
use Carbon\Carbon;

class PreOrderViewer
{
    public function listForTable(Establishment $establishment, $limit = 50, $status = '', $period = '')
    {
        $preOrdersQuery = PreOrder::where(['establishment_id' => $establishment->id]);
        if (!$limit || $limit == '') {
            $limit = 50;
        }
        if ($status && $status != '') {
            $preOrdersQuery->where(['status' => $status]);
        }
        if ($period && $period != '') {
            $date = Carbon::now()->subDays($period);
            $preOrdersQuery->where('created_at', '>=', $date->toDateTimeString());
        }
        $preOrders = $preOrdersQuery->orderBy('status', 'asc')->orderBy('created_at', 'desc')->limit($limit)->get();
        $arrPreOrders = [];

        foreach ($preOrders as $p) {
            $total = $p->cache_total;

            $user = $p->user->name;

            $arrPreOrders[] = [
                'id' => $p->id,
                'photo' => $p->user->photo ? $p->user->photo : 'https://via.placeholder.com/200x200&text=Sem+Foto',
                'user' => $user,
                'total' => $total,
                'opened_at' => $p->created_at,
                'status' => PreOrder::$statusList[$p->status],
                'has_action_button' => true,
                'action_url' => 'pre-orders/'.$p->id,
                'is_action_asynchronous' => false,
            ];
        }

        $arrData = [
            'model' => 'pre_order',
            'unsearchable_key' => 'photo',
            'columns' => [
                'photo' => [
                    'label' => 'Foto',
                    'hidden_xs' => true,
                    'type' => 'string',
                ],
                'user' => [
                    'label' => 'Cliente',
                    'hidden_xs' => true,
                    'type' => 'string',
                ],
                'total' => [
                    'label' => 'Total',
                    'hidden_xs' => false,
                    'type' => 'float',
                ],
                'opened_at' => [
                    'label' => 'Data de Pedido',
                    'hidden_xs' => false,
                    'type' => 'string',
                ],
                'status' => [
                    'label' => 'Status',
                    'hidden_xs' => false,
                    'type' => 'string',
                ],
            ],
            'objects' => $arrPreOrders,
        ];

        return response($arrData);
    }
}
