<?php

namespace App\Viewers;

use App\Models\Establishment;

class ProductCategoryViewer
{
    public function listForTable(Establishment $establishment)
    {
        $categories = $establishment->productCategories()
          ->orderBy('status', 'asc')->orderBy('name', 'asc')->get();

        $arrCategories = [];

        foreach ($categories as $p) {
            $arrCategories[] = [
              'id' => $p->id,
              'slug' => $p->slug,
              'name' => $p->name,
              'status' => $p->status,
              'has_action_button' => false,
              'action_url' => '',
              'is_action_asynchronous' => false,
            ];
        }

        $arrData = [
          'model' => 'categories',
          'unsearchable_key' => 'price',
          'columns' => [
            'name' => [
              'label' => 'Nome',
              'hidden_xs' => false,
              'type' => 'string',
            ],
            'slug' => [
              'label' => 'Slug',
              'hidden_xs' => false,
              'type' => 'string',
            ],
          ],
          'objects' => $arrCategories,
        ];

        return response($arrData);
    }
}
