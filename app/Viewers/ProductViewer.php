<?php

namespace App\Viewers;

use App\Models\Establishment;

class ProductViewer
{
    public function listForTable(Establishment $establishment, $limit = 0, $status = '', $category = null)
    {
        $productsQuery = $establishment->products()->getQuery();
        if ($status != '') {
            $productsQuery->where(['products.status' => $status]);
        }
        if ($limit == '') {
            $limit = 0;
        }
        if ($limit > 0) {
            $productsQuery->limit($limit);
        }
        if ($category && $category != '') {
            $productsQuery->where(['products.product_category_id' => $category]);
        }

        $products = $productsQuery->orderBy('not_sale', 'asc')
          ->orderBy('status', 'asc')->orderBy('name', 'asc')->get();
        $arrProducts = [];
        foreach ($products as $p) {
            $arrProducts[] = [
              'id' => $p->id,
              'photo' => $p->photo,
              'code' => $p->code,
              'name' => ($p->not_sale == 1) ? '[NÃO VENDIDO] '.$p->name : $p->name,
              'category' => ($p->ProductCategory) ? $p->ProductCategory->id : '',
              'price' => ($p->is_promo_price) ? '<b>Promo R$ '.$p->promo_price.'</b>' : '<b>R$ '.$p->price.'</b>',
              'status' => $p->status,
              'has_action_button' => false,
              'action_url' => '',
              'is_action_asynchronous' => false,
            ];
        }

        $arrData = [
          'model' => 'products',
          'unsearchable_key' => 'photo',
          'columns' => [
            'photo' => [
              'label' => 'Imagem',
              'hidden_xs' => true,
              'type' => 'image',
            ],
            'code' => [
              'label' => 'Código',
              'hidden_xs' => false,
              'type' => 'string',
            ],
            'name' => [
              'label' => 'Nome',
              'hidden_xs' => false,
              'type' => 'string',
            ],
            'category' => [
              'label' => 'Categoria',
              'hidden_xs' => true,
              'type' => 'select',
            ],
            'price' => [
              'label' => 'Preço',
              'hidden_xs' => false,
              'type' => 'string',
            ],
          ],
          'objects' => $arrProducts,
        ];

        return response($arrData);
    }
}
