<?php

namespace App\Viewers;

use App\Models\Establishment;

class RatingViewer
{
    public function listForTable(Establishment $establishment)
    {
        $billRatings = $establishment->billRatings;
        $preOrderRatings = $establishment->preOrderRatings;
        $billRatings->merge($preOrderRatings);
        $arrRatings = [];
        foreach ($billRatings as $m) {
            $arrRatings[] = [
                'id' => $m->id,
                'username' => $m->user->name.' ('.$m->user->email_account.')',
                'type' => $m->pre_order ? 'Pré-Pago' : 'Pós-Pago',
                'rating' => '<b>'.$m->rating.'</b>',
                'comment' => '"'.$m->comment.'"',
                'answers' => implode(',<br>', json_decode($m->answers, true)),
                'has_action_button' => false,
                'action_url' => '',
                'is_action_asynchronous' => false,
            ];
        }
        $arrData = [
            'model' => 'analysis/ratings',
            'unsearchable_key' => 'comment',
            'columns' => [
                'rating' => [
                    'label' => 'Avaliação',
                    'hidden_xs' => false,
                    'type' => 'string',
                ],
                'type' => [
                    'label' => 'Tipo',
                    'hidden_xs' => true,
                    'type' => 'string',
                ],
                'username' => [
                    'label' => 'Usuário',
                    'hidden_xs' => false,
                    'type' => 'string',
                ],
                'answers' => [
                    'label' => 'Respostas',
                    'hidden_xs' => true,
                    'type' => 'string',
                ],
                'comment' => [
                    'label' => 'Comentário',
                    'hidden_xs' => true,
                    'type' => 'string',
                ],
            ],
            'objects' => $arrRatings,
        ];

        return response($arrData);
    }
}
