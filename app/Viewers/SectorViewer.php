<?php

namespace App\Viewers;

use App\Models\Establishment;

class SectorViewer
{
    public function listForTable(Establishment $establishment)
    {
        $sectors = $establishment->sectors()->orderBy('status', 'asc')->get();
        $arrSectors = [];
        foreach ($sectors as $m) {
            $arrSectors[] = [
              'id' => $m->id,
              'name' => $m->name,
              'products' => $m->getProductsToList(),
              'status' => $m->status,
              'has_action_button' => false,
              'action_url' => '',
              'is_action_asynchronous' => false,
          ];
        }
        $arrData = [
          'model' => 'profile/sectors',
          'unsearchable_key' => 'products',
          'columns' => [
              'name' => [
                  'label' => 'Setores',
                  'hidden_xs' => false,
                  'type' => 'string',
              ],
              'products' => [
                  'label' => 'Produtos',
                  'hidden_xs' => false,
                  'type' => 'array',
              ],
          ],
          'objects' => $arrSectors,
      ];

        return response($arrData);
    }
}
