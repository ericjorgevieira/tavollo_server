<?php

namespace App\Viewers;

use App\Models\Establishment;

class TableLocationViewer
{
    public function listForTable(Establishment $establishment)
    {
        $locations = $establishment->locations()
        ->orderBy('status', 'asc')->orderBy('name', 'asc')->get();

        $arrLocations = [];

        foreach ($locations as $p) {
            $arrLocations[] = [
                'id' => $p->id,
                'slug' => $p->slug,
                'name' => $p->name,
                'status' => $p->status,
                'has_action_button' => false,
                'action_url' => '',
                'is_action_asynchronous' => false,
              ];
        }

        $arrData = [
            'model' => 'locations',
            'unsearchable_key' => 'price',
            'columns' => [
              'name' => [
                'label' => 'Nome',
                'hidden_xs' => false,
                'type' => 'string',
              ],
              'slug' => [
                'label' => 'Slug',
                'hidden_xs' => false,
                'type' => 'string',
              ],
            ],
            'objects' => $arrLocations,
          ];

        return response($arrData);
    }
}
