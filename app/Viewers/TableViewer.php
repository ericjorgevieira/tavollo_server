<?php

namespace App\Viewers;

use App\Models\Establishment;

class TableViewer
{
    public function listForTable(Establishment $establishment)
    {
        $tables = $establishment->tables()->orderBy('status', 'asc')->orderBy('number', 'asc')->get();
        $arrTables = [];

        foreach ($tables as $t) {
            $arrTables[] = [
              'id' => $t->id,
              'name' => $t->name,
              'number' => $t->number,
              'external_id' => $t->external_id,
              'code' => '<b>'.$t->code.'</b>',
              'status' => $t->status,
              'is_consumption_card' => $t->is_consumption_card == 1 ? 'Cartão' : 'Mesa',
              'has_action_button' => true,
              'action_url' => '/tables/'.$t->id.'/code',
              'is_action_asynchronous' => true,
            ];
        }

        $arrData = [
          'model' => 'tables',
          'unsearchable_key' => 'code',
          'columns' => [
            'is_consumption_card' => [
              'label' => 'Tipo',
              'hidden_xs' => false,
              'type' => 'string',
            ],
            'name' => [
              'label' => 'Nome',
              'hidden_xs' => true,
              'type' => 'string',
            ],
            'number' => [
              'label' => 'Número',
              'hidden_xs' => false,
              'type' => 'string',
            ],
            'external_id' => [
              'label' => 'Cód. Externo',
              'hidden_xs' => false,
              'type' => 'string',
            ],
            'code' => [
              'label' => 'Código',
              'hidden_xs' => false,
              'type' => 'string',
            ],
          ],
          'objects' => $arrTables,
        ];

        return response($arrData);
    }
}
