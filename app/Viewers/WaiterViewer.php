<?php

namespace App\Viewers;

use App\Models\Establishment;

class WaiterViewer
{
    public function listForTable(Establishment $establishment)
    {
        $waiters = $establishment->waiters()->orderBy('status', 'asc')->get();
        $arrWaiters = [];
        foreach ($waiters as $m) {
            $arrWaiters[] = [
                'id' => $m->id,
                'register' => $m->register,
                'name' => $m->name,
                'is_default' => $m->is_default ? 'Sim' : 'Não',
                'username' => ($m->user) ? $m->user->email : null,
                'status' => $m->status,
                'has_action_button' => false,
                'action_url' => '',
                'is_action_asynchronous' => false,
            ];
        }
        $arrData = [
            'model' => 'profile/waiters',
            'unsearchable_key' => '',
            'columns' => [
                'name' => [
                    'label' => 'Nome',
                    'hidden_xs' => false,
                    'type' => 'string',
                ],
                'is_default' => [
                    'label' => 'Padrão?',
                    'hidden_xs' => true,
                    'type' => 'string',
                ],
                'register' => [
                    'label' => 'Registro',
                    'hidden_xs' => true,
                    'type' => 'string',
                ],
                'username' => [
                    'label' => 'Login',
                    'hidden_xs' => false,
                    'type' => 'string',
                ],
            ],
            'objects' => $arrWaiters,
        ];

        return response($arrData);
    }
}
