<?php

use App\Models\ApiToken;
use App\Models\Establishment;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(ApiToken::class, function (Faker $faker) {
    $establishment = factory(Establishment::class)->create();
    $user = factory(User::class)->create(['is_admin' => true, 'is_establishment' => true, 'establishment_id' => $establishment->id]);

    return [
        'sequence' => Str::random(32),
        'establishment_id' => $establishment->id,
        'expires_at' => Carbon\Carbon::now()->addYears(2),
        'locale' => '192.168.1.1',
        'user_id' => $user->id,
    ];
});
