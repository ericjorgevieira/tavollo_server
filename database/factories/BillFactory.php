<?php

use App\Models\Bill;
use App\Models\Table;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Bill::class, function (Faker $faker) {
    $user = factory(User::class)->create();

    return [
        'table_id' => factory(Table::class)->create()->id,
        'user_id' => $user->id,
        'created_at' => Carbon\Carbon::now(),
        'opened_at' => Carbon\Carbon::now(),
        'closed_at' => Carbon\Carbon::now(),
        'status' => Bill::STATUS_OPENED,
    ];
});
