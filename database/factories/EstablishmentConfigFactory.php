<?php

use App\Models\Establishment;
use App\Models\EstablishmentConfig;
use Faker\Generator as Faker;

$factory->define(EstablishmentConfig::class, function (Faker $faker) {
    $establishment = factory(Establishment::class)->create();

    return [
        'establishment_id' => $establishment->id,
        'baksheesh_percentage' => 0,
        'allow_bills' => true,
        'allow_pre_bills' => true,
        'available_days' => 'a:7:{i:0;s:6:"monday";i:1;s:7:"tuesday";i:2;s:9:"wednesday";i:3;s:8:"thursday";i:4;s:6:"friday";i:5;s:8:"saturday";i:6;s:6:"sunday";}',
        'first_turn_begin' => '00:00:00',
        'first_turn_end' => '23:59:59',
        'second_turn_begin' => '00:00:00',
        'second_turn_end' => '23:59:59',
    ];
});
