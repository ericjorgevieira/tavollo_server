<?php

use App\Models\Establishment;
use Faker\Generator as Faker;

$factory->define(Establishment::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'fantasy_name' => $faker->name,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'cel' => $faker->phoneNumber,
        'address' => $faker->streetAddress,
        'street' => $faker->streetName,
        'city' => $faker->city,
        'district' => $faker->city,
        'state' => $faker->state,
        'country' => $faker->country,
        'postal_code' => $faker->postcode,
        'status' => 'enabled',
        'plan' => 'master',
        'cnpj' => $faker->numberBetween(0, 99).'.'.$faker->numberBetween(0, 999).'.'.$faker->numberBetween(0, 999).'/0001-00',
    ];
});
