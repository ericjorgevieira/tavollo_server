<?php

use App\Models\Menu;
use App\Models\Establishment;
use Faker\Generator as Faker;

$factory->define(Menu::class, function (Faker $faker) {

    $establishment = factory(Establishment::class)->create();
    $name = $faker->name;
    return [
        'establishment_id' => $establishment->id,
        'name' => $name,
        'status' => 'enabled',
        'description' => $faker->sentence,
        'position' => 1 ,
        'slug' => str_slug($name),
    ];
});
