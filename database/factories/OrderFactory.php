<?php

use App\Models\Bill;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    $bill = factory(Bill::class)->create();
    $user = factory(User::class)->create();
    $product = factory(Product::class)->create();

    return [
        'bill_id' => $bill->id,
        'user_id' => $user->id,
        'product_id' => $product->id,
        'quant' => 1,
        'cache_price' => $product->price,
        'observation' => $faker->text(100)
    ];
});
