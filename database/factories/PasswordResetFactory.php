<?php

use App\Models\PasswordReset;
use Faker\Generator as Faker;

$factory->define(PasswordReset::class, function (Faker $faker) {
    return [
        'email' => $faker->email,
        'token' => bcrypt(str_random(10)),
    ];
});