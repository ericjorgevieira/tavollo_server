<?php

use App\Models\Establishment;
use App\Models\PreOrder;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(PreOrder::class, function (Faker $faker) {
    $user = factory(User::class)->create();
    $establishment = factory(Establishment::class)->create();

    return [
        'user_id' => $user->id,
        'establishment_id' => $establishment->id,
    ];
});
