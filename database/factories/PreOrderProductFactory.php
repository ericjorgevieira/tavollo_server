<?php

use App\Models\PreOrder;
use App\Models\PreOrderProduct;
use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(PreOrderProduct::class, function (Faker $faker) {
    $product = factory(Product::class)->create();
    $preOrder = factory(PreOrder::class)->create();

    return [
        'pre_order_id' => $preOrder->id,
        'product_id' => $product->id,
        'quant' => 1,
        'cache_price' => $product->price,
        'observation' => $faker->text(100)
    ];
});
