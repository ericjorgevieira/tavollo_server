<?php

use App\Models\Establishment;
use App\Models\ProductCategory;
use Faker\Generator as Faker;

$factory->define(ProductCategory::class, function (Faker $faker) {
    $establishment = factory(Establishment::class)->create();
    $name = $faker->name();

    return [
        'establishment_id' => $establishment->id,
        'name' => $name,
        'slug' => str_slug($name),
        'status' => 'enabled'
    ];
});
