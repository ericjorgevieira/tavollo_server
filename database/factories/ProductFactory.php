<?php

use App\Models\Establishment;
use App\Models\ProductCategory;
use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $establishment = factory(Establishment::class)->create();
    $category = factory(ProductCategory::class)->create(['establishment_id' => $establishment->id]);

    return [
        'name' => $faker->name(),
        'code' => $faker->randomNumber(),
        'product_category_id' => $category->id,
        'establishment_id' => $establishment->id,
        'price' => $faker->randomFloat(2),
        'status' => 'enabled',
        'available_days' => 'a:7:{i:0;s:6:"monday";i:1;s:7:"tuesday";i:2;s:9:"wednesday";i:3;s:8:"thursday";i:4;s:6:"friday";i:5;s:8:"saturday";i:6;s:6:"sunday";}',
        'available_turn' => 'all',
        'first_turn_begin' => '00:00:00',
        'first_turn_end' => '23:59:59',
        'second_turn_begin' => '00:00:00',
        'second_turn_end' => '23:59:59',
        'preparation_time' => '00:10:00'
    ];
});
