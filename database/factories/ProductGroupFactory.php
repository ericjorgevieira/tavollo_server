<?php

use App\Models\Establishment;
use App\Models\Product;
use App\Models\ProductGroup;
use Faker\Generator as Faker;

$factory->define(ProductGroup::class, function (Faker $faker) {
    $establishment = factory(Establishment::class)->create();
    $product = factory(Product::class)->create(['establishment_id' => $establishment->id]);
    
    return [
        'product_id' => $product->id,
        'name' => $faker->name,
        'description' => $faker->sentence,
        'prefix' => '',
        'min' => random_int(0,5),
        'max' => random_int(5,10),
        'formula' => 'sum_all',
        'status' => 'enabled',
        'establishment_id' => $establishment->id,
    ];
});
