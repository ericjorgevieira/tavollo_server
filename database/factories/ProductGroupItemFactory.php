<?php

use App\Models\Establishment;
use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\ProductGroupItem;
use Faker\Generator as Faker;

$factory->define(ProductGroupItem::class, function (Faker $faker) {

    $establishment = factory(Establishment::class)->create();
    $product = factory(Product::class)->create(['establishment_id' => $establishment->id]);
    $productGroup = factory(ProductGroup::class)->create(['establishment_id' => $establishment->id,'product_id' => $product->id]);

    return [
        'product_group_id' => $productGroup->id,
        'name' => $faker->name,
        'description' => $faker->sentence,
        'price' => $faker->randomFloat(2),
        'status' => 'enabled',
        'establishment_id' => $establishment->id,
    ];
});
