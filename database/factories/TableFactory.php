<?php

use App\Models\Establishment;
use App\Models\Table;
use Faker\Generator as Faker;

$factory->define(Table::class, function (Faker $faker) {
    $establishment = factory(Establishment::class)->create();
    $number = $faker->randomDigit();

    return [
        'establishment_id' => $establishment->id,
        'name' => 'Mesa '.$number,
        'number' => $number,
        'is_consumption_card' => false,
        'status' => 'enabled',
        'code' => $faker->bothify('#?#?'),
    ];
});
