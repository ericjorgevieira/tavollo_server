<?php

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    $email = $faker->safeEmail;

    return [
        'name' => $faker->name,
        'email' => $email,
        'email_account' => $email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
        'establishment_id' => null,
        'is_establishment' => 0,
        'is_admin' => 0,
        'profile' => 'customer',
        'status' => 'enabled',
    ];
});
