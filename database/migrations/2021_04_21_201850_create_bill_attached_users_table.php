<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillAttachedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_attached_users', function (Blueprint $table) {
            $table->integer('bill_id')->index();
            $table->integer('user_id')->index();
            $table->dateTime('opened_at');
            $table->dateTime('closed_at');
            $table->timestamps();
            $table->enum('status', ['opened', 'closing', 'closed', 'trash']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_attached_users');
    }
}
