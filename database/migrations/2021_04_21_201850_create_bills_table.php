<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->integer('table_id')->index();
            $table->dateTime('opened_at');
            $table->dateTime('closed_at');
            $table->timestamps();
            $table->enum('status', ['opened', 'closing', 'closed', 'enabled', 'disabled', 'trash'])->nullable();
            $table->boolean('temp_user')->default(0);
            $table->text('raw_user')->nullable();
            $table->boolean('has_sync')->default(0);
            $table->double('cache_total', 8, 2)->nullable();
            $table->double('amount_paid', 8, 2)->nullable();
            $table->double('remaining_value', 8, 2)->nullable();
            $table->double('discount', 8, 2)->nullable();
            $table->integer('total_users')->default(1);
            $table->string('external_id')->nullable();
            $table->text('confirm_payment_url')->nullable();
            $table->text('raw_payload')->nullable();
            $table->string('card_flag')->nullable();
            $table->string('transaction_code')->nullable();
            $table->double('baksheesh_value', 8, 2)->nullable();
            $table->string('transaction_type')->nullable();
            $table->unsignedInteger('establishment_id')->nullable();
            $table->string('external_info')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
