<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstablishmentConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establishment_configs', function (Blueprint $table) {
            $table->unsignedInteger('establishment_id')->primary();
            $table->integer('has_couvert')->default(0);
            $table->double('couvert_price', 8, 2)->nullable();
            $table->integer('has_baksheesh')->default(0);
            $table->double('baksheesh_percentage', 8, 2);
            $table->integer('generate_codes')->default(0);
            $table->boolean('request_alias')->default(0);
            $table->decimal('location_lat', 10, 7)->nullable();
            $table->decimal('location_long', 10, 7)->nullable();
            $table->boolean('display_location')->default(0);
            $table->boolean('limit_table')->nullable()->default(0);
            $table->boolean('enable_advanced_functions')->default(0);
            $table->text('bill_extract_url')->nullable();
            $table->longText('external_products_cache')->nullable();
            $table->text('fast2pay_account')->nullable();
            $table->boolean('enable_payment')->default(0);
            $table->boolean('disable_location')->default(0);
            $table->boolean('allow_pre_bills')->default(0);
            $table->boolean('allow_bills')->default(1);
            $table->boolean('disable_orders')->default(0);
            $table->text('cielo_account')->nullable();
            $table->string('printer_name')->nullable();
            $table->string('printer_ip')->nullable();
            $table->json('available_days')->nullable();
            $table->time('first_turn_begin')->nullable();
            $table->time('first_turn_end')->nullable();
            $table->time('second_turn_begin')->nullable();
            $table->time('second_turn_end')->nullable();
            $table->boolean('enable_price_update')->default(0);
            $table->boolean('enable_rating')->default(1);
            $table->json('predefined_answers')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establishment_configs');
    }
}
