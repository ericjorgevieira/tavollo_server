<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstablishmentIntegrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establishment_integrations', function (Blueprint $table) {
            $table->integer('establishment_id')->index();
            $table->integer('integration_id')->index();
            $table->string('credential_user')->nullable();
            $table->string('credential_password')->nullable();
            $table->string('api_url')->nullable();
            $table->string('api_token')->nullable();
            $table->enum('status', ['enabled', 'disabled', 'trash']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establishment_integrations');
    }
}
