<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstablishmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establishments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('fantasy_name');
            $table->string('email');
            $table->string('cnpj')->unique();
            $table->string('phone');
            $table->string('cel');
            $table->string('address');
            $table->string('street');
            $table->string('city');
            $table->string('district');
            $table->string('state');
            $table->string('country');
            $table->string('postal_code');
            $table->string('photo')->nullable();
            $table->string('banner')->nullable();
            $table->enum('status', ['enabled', 'disabled', 'trash']);
            $table->timestamps();
            $table->rememberToken();
            $table->softDeletes();
            $table->enum('plan', ['test', 'lite', 'master'])->default('test');
            $table->string('image')->nullable();
            $table->enum('theme', ['light', 'dark'])->default('dark');
            $table->string('short_description')->nullable();
            $table->text('description')->nullable();
            $table->time('average_time')->nullable();
            $table->string('slug')->nullable()->unique();
            $table->text('theme_config')->nullable();
            $table->string('logo')->nullable();
            $table->string('background')->nullable();
            $table->date('payment_date')->nullable();
            $table->string('ie')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establishments');
    }
}
