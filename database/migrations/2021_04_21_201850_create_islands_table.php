<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIslandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('islands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('establishment_id')->index();
            $table->integer('waiter_id')->index();
            $table->time('opened_at');
            $table->time('closed_at');
            $table->enum('status', ['opened', 'closed', 'enabled', 'disabled', 'trash']);
            $table->timestamps();
            $table->softDeletes();
            $table->text('raw_table_numbers')->nullable();
            $table->string('available_days')->nullable()->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('islands');
    }
}
