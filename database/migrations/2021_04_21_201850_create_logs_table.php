<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('action');
            $table->string('description');
            $table->string('url');
            $table->string('ip', 45);
            $table->text('raw');
            $table->timestamps();
            $table->unsignedInteger('establishment_id')->nullable()->index('logs_establishment_id_foreign');
            $table->unsignedInteger('user_id')->nullable()->index('logs_user_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
