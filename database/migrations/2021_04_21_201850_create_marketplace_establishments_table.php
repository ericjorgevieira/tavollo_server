<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketplaceEstablishmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketplace_establishments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('marketplace_id')->index('marketplace_establishments_marketplace_id_foreign');
            $table->unsignedInteger('establishment_id')->index('marketplace_establishments_establishment_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketplace_establishments');
    }
}
