<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderProductGroupItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_product_group_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id')->index('order_id_order');
            $table->unsignedInteger('product_group_id')->index('order_product_group_items_product_group_id_foreign');
            $table->unsignedInteger('product_group_item_id')->index('order_product_group_items_product_group_item_id_foreign');
            $table->integer('group_external_id')->nullable();
            $table->integer('external_id')->nullable();
            $table->integer('quant')->default(1);
            $table->double('value', 8, 2)->nullable();
            $table->bigInteger('group_composition_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product_group_items');
    }
}
