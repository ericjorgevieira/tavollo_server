<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bill_id')->index();
            $table->integer('product_id')->index();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('quant');
            $table->string('observation');
            $table->enum('status', ['opened', 'preparation', 'finished', 'canceled', 'trash', 'deny'])->nullable();
            $table->boolean('is_weight_price')->default(0);
            $table->decimal('order_weight', 8, 3)->nullable();
            $table->decimal('cache_price');
            $table->boolean('is_promo')->default(0);
            $table->unsignedInteger('waiter_id')->nullable()->index('orders_waiter_id_foreign');
            $table->boolean('by_waiter')->nullable()->default(0);
            $table->boolean('by_admin')->nullable()->default(0);
            $table->unsignedInteger('user_id')->nullable()->index('orders_user_id_foreign');
            $table->boolean('is_defined_price')->default(0);
            $table->decimal('defined_price')->nullable();
            $table->integer('external_id')->nullable();
            $table->string('external_info')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
