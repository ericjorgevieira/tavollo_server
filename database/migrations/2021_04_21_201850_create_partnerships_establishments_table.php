<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnershipsEstablishmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partnerships_establishments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('partnership_id')->index('partnerships_establishments_partnership_id_foreign');
            $table->unsignedInteger('establishment_id')->index('partnerships_establishments_establishment_id_foreign');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partnerships_establishments');
    }
}
