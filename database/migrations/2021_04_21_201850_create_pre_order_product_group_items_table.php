<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreOrderProductGroupItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_order_product_group_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pre_order_product_id')->index('pre_order_product_group_items_pre_order_product_id_foreign');
            $table->unsignedInteger('product_group_id')->index('pre_order_product_group_items_product_group_id_foreign');
            $table->unsignedInteger('product_group_item_id')->index('pre_order_product_group_items_product_group_item_id_foreign');
            $table->integer('group_external_id')->nullable();
            $table->integer('external_id')->nullable();
            $table->integer('quant')->default(1);
            $table->double('value', 8, 2)->nullable();
            $table->bigInteger('group_composition_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_order_product_group_items');
    }
}
