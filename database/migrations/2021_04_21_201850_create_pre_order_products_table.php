<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_order_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pre_order_id')->index('pre_order_products_pre_order_id_foreign');
            $table->unsignedInteger('product_id')->index('pre_order_products_product_id_foreign');
            $table->integer('quant')->default(1);
            $table->string('observation')->nullable();
            $table->enum('status', ['opened', 'preparation', 'finished', 'canceled', 'trash'])->default('opened');
            $table->double('cache_price', 8, 2)->nullable();
            $table->boolean('is_promo')->nullable();
            $table->boolean('is_defined_price')->nullable();
            $table->double('defined_price', 8, 2)->nullable();
            $table->integer('external_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_order_products');
    }
}
