<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreOrderRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_order_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pre_order_id')->index('pre_order_ratings_pre_order_id_foreign');
            $table->unsignedInteger('user_id')->index('pre_order_ratings_user_id_foreign');
            $table->unsignedInteger('establishment_id')->index('pre_order_ratings_establishment_id_foreign');
            $table->integer('rating');
            $table->json('answers')->nullable();
            $table->string('comment')->nullable();
            $table->enum('status', ['enabled', 'disabled'])->default('disabled');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_order_ratings');
    }
}
