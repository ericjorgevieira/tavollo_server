<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->index('pre_orders_user_id_foreign');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->unsignedInteger('establishment_id')->index('pre_orders_establishment_id_foreign');
            $table->enum('status', ['opened', 'preparation', 'finished', 'delivered', 'canceled', 'trash', 'deny'])->default('opened');
            $table->double('cache_total', 8, 2)->nullable();
            $table->boolean('is_defined_total')->default(0);
            $table->double('defined_total', 8, 2)->nullable();
            $table->integer('external_id')->nullable();
            $table->text('raw_payload')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->string('card_flag')->nullable();
            $table->string('transaction_code')->nullable();
            $table->bigInteger('merchant_order_id')->nullable();
            $table->string('transaction_type')->nullable();
            $table->string('alias')->nullable();
            $table->string('external_info')->nullable();
            $table->string('cielo_payment_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_orders');
    }
}
