<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductGroupItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_group_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_group_id')->index('product_group_items_product_group_id_foreign');
            $table->string('name');
            $table->string('description')->nullable();
            $table->double('price', 8, 2)->nullable();
            $table->bigInteger('external_id')->nullable();
            $table->enum('status', ['enabled', 'disabled', 'trash']);
            $table->softDeletes();
            $table->timestamps();
            $table->unsignedInteger('establishment_id');
            $table->bigInteger('group_composition_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_group_items');
    }
}
