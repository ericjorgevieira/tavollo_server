<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->index('product_groups_product_id_foreign');
            $table->string('name', 120);
            $table->string('prefix', 50);
            $table->string('description')->nullable();
            $table->integer('min')->default(0);
            $table->integer('max')->default(1);
            $table->enum('formula', ['sum_all', 'sum_and_divide', 'sum_and_divide_price', 'item_highest_value', 'item_lower_value', 'item_highest_divide', 'item_lower_divide'])->default('sum_all');
            $table->enum('status', ['enabled', 'disabled', 'trash']);
            $table->softDeletes();
            $table->timestamps();
            $table->bigInteger('external_id')->nullable();
            $table->unsignedInteger('establishment_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_groups');
    }
}
