<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_category_id')->nullable()->index();
            $table->string('name');
            $table->string('photo')->nullable();
            $table->decimal('price');
            $table->decimal('promo_price')->nullable();
            $table->time('preparation_time');
            $table->enum('status', ['enabled', 'disabled', 'trash']);
            $table->timestamps();
            $table->softDeletes();
            $table->integer('establishment_id');
            $table->text('description')->nullable();
            $table->integer('parent_id')->nullable();
            $table->string('code');
            $table->decimal('weight', 8, 3)->nullable();
            $table->boolean('is_promo_price')->default(0);
            $table->integer('sector_id')->nullable()->index();
            $table->string('available_days')->default('a:7:{i:0;s:6:\"monday\";i:1;s:7:\"tuesday\";i:2;s:9:\"wednesday\";i:3;s:8:\"thursday\";i:4;s:6:\"friday\";i:5;s:8:\"saturday\";i:6;s:6:\"sunday\";}');
            $table->enum('available_turn', ['all', 'first', 'second'])->default('all');
            $table->time('first_turn_begin')->default('00:00:00');
            $table->time('first_turn_end')->default('23:59:59');
            $table->time('second_turn_begin')->default('00:00:00');
            $table->time('second_turn_end')->default('23:59:59');
            $table->time('promo_price_begin_at')->default('00:00:00');
            $table->time('promo_price_end_at')->default('23:59:59');
            $table->string('promo_price_available_days')->default('a:7:{i:0;s:6:\"monday\";i:1;s:7:\"tuesday\";i:2;s:9:\"wednesday\";i:3;s:8:\"thursday\";i:4;s:6:\"friday\";i:5;s:8:\"saturday\";i:6;s:6:\"sunday\";}');
            $table->boolean('enable_promo_schedule')->default(0);
            $table->boolean('not_sale')->default(0);
            $table->boolean('calculate_baksheesh')->default(1);
            $table->boolean('not_pre_sale')->default(0);
            $table->boolean('enable_automatic_order')->default(0);
            $table->json('default_observations')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
