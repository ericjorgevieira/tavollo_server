<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sectors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('establishment_id')->index();
            $table->string('name');
            $table->enum('status', ['enabled', 'disabled', 'trash']);
            $table->timestamps();
            $table->softDeletes();
            $table->boolean('use_default_printer')->default(1);
            $table->string('printer_name')->nullable();
            $table->string('printer_ip')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sectors');
    }
}
