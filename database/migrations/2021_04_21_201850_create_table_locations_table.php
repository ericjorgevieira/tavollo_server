<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('establishment_id')->index('table_locations_establishment_id_foreign');
            $table->string('name');
            $table->string('slug');
            $table->enum('status', ['enabled', 'disabled', 'trash']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_locations');
    }
}
