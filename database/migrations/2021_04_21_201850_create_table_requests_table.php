<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('establishment_id')->index();
            $table->text('table_number');
            $table->decimal('location_lat', 10, 7)->nullable();
            $table->decimal('location_long', 10, 7)->nullable();
            $table->string('ip', 45)->nullable()->default('');
            $table->timestamps();
            $table->enum('status', ['opened', 'preparation', 'closed', 'trash'])->default('opened');
            $table->unsignedInteger('user_id')->index('table_requests_user_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_requests');
    }
}
