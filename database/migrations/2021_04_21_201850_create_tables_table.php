<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('establishment_id')->index();
            $table->string('name');
            $table->integer('number');
            $table->text('description')->nullable();
            $table->string('code')->nullable()->unique('code');
            $table->string('external_id', 30)->nullable();
            $table->rememberToken();
            $table->dateTime('available_at')->nullable();
            $table->dateTime('unavailable_at')->nullable();
            $table->enum('status', ['enabled', 'disabled', 'trash']);
            $table->timestamps();
            $table->softDeletes();
            $table->string('alias')->nullable();
            $table->boolean('is_consumption_card')->default(0);
            $table->string('slug')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tables');
    }
}
