<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('facebook_id')->nullable();
            $table->string('google_id')->nullable();
            $table->string('password');
            $table->string('rg')->nullable();
            $table->string('cpf')->nullable();
            $table->string('sex')->nullable();
            $table->string('phone')->nullable();
            $table->string('cel')->nullable();
            $table->string('address')->nullable();
            $table->string('street')->nullable();
            $table->string('city')->nullable();
            $table->string('district')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('photo')->nullable();
            $table->boolean('is_establishment')->default(0);
            $table->integer('establishment_id')->index()->nullable();
            $table->enum('status', ['enabled', 'disabled', 'trash']);
            $table->timestamps();
            $table->rememberToken();
            $table->softDeletes();
            $table->integer('is_admin')->default(0);
            $table->enum('profile', ['customer', 'test', 'manager', 'waiter', 'partnership'])->default('customer');
            $table->boolean('first_access')->default(1);
            $table->string('email_account')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
