<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWaiterAvaliationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waiter_avaliations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('waiter_id')->index();
            $table->integer('bill_id')->index();
            $table->integer('rating');
            $table->text('comment')->nullable();
            $table->enum('status', ['enabled', 'disabled', 'trash']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waiter_avaliations');
    }
}
