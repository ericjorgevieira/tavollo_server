<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWaitersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waiters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('register');
            $table->time('work_begin');
            $table->time('work_end');
            $table->integer('establishment_id')->index();
            $table->enum('status', ['enabled', 'disabled', 'trash']);
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('user_id')->index('waiters_user_id_foreign');
            $table->string('code', 100)->nullable();
            $table->boolean('is_default')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waiters');
    }
}
