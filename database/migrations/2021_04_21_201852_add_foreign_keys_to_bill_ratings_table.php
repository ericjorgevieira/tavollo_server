<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToBillRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bill_ratings', function (Blueprint $table) {
            $table->foreign('bill_id')->references('id')->on('bills')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('establishment_id')->references('id')->on('establishments')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (DB::getDriverName() !== 'sqlite') {
            Schema::table('bill_ratings', function (Blueprint $table) {
                $table->dropForeign(['bill_ratings_bill_id_foreign']);
                $table->dropForeign('bill_ratings_establishment_id_foreign');
                $table->dropForeign('bill_ratings_user_id_foreign');
            });
        }
    }
}
