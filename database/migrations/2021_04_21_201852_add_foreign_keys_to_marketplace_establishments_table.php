<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToMarketplaceEstablishmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('marketplace_establishments', function (Blueprint $table) {
            $table->foreign('establishment_id')->references('id')->on('establishments')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('marketplace_id')->references('id')->on('marketplaces')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (DB::getDriverName() !== 'sqlite') {
            Schema::table('marketplace_establishments', function (Blueprint $table) {
                $table->dropForeign(['marketplace_establishments_establishment_id_foreign']);
                $table->dropForeign('marketplace_establishments_marketplace_id_foreign');
            });
        }
    }
}
