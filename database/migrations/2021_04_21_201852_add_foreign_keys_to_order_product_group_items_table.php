<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToOrderProductGroupItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_product_group_items', function (Blueprint $table) {
            $table->foreign('order_id', 'order_id_order')->references('id')->on('orders')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('product_group_id')->references('id')->on('product_groups')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('product_group_item_id')->references('id')->on('product_group_items')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (DB::getDriverName() !== 'sqlite') {
            Schema::table('order_product_group_items', function (Blueprint $table) {
                $table->dropForeign('order_id_order');
                $table->dropForeign('order_product_group_items_product_group_id_foreign');
                $table->dropForeign('order_product_group_items_product_group_item_id_foreign');
            });
        }
    }
}
