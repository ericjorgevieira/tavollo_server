<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToPartnershipsEstablishmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partnerships_establishments', function (Blueprint $table) {
            $table->foreign('establishment_id')->references('id')->on('establishments')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('partnership_id')->references('id')->on('partnerships')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (DB::getDriverName() !== 'sqlite') {
            Schema::table('partnerships_establishments', function (Blueprint $table) {
                $table->dropForeign('partnerships_establishments_establishment_id_foreign');
                $table->dropForeign('partnerships_establishments_partnership_id_foreign');
            });
        }
    }
}
