<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToPreOrderProductGroupItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pre_order_product_group_items', function (Blueprint $table) {
            $table->foreign('pre_order_product_id')->references('id')->on('pre_order_products')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('product_group_id')->references('id')->on('product_groups')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('product_group_item_id')->references('id')->on('product_group_items')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (DB::getDriverName() !== 'sqlite') {
            Schema::table('pre_order_product_group_items', function (Blueprint $table) {
                $table->dropForeign('pre_order_product_group_items_pre_order_product_id_foreign');
                $table->dropForeign('pre_order_product_group_items_product_group_id_foreign');
                $table->dropForeign('pre_order_product_group_items_product_group_item_id_foreign');
            });
        }
    }
}
