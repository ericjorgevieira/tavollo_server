<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToPreOrderRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pre_order_ratings', function (Blueprint $table) {
            $table->foreign('establishment_id')->references('id')->on('establishments')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('pre_order_id')->references('id')->on('pre_orders')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (DB::getDriverName() !== 'sqlite') {
            Schema::table('pre_order_ratings', function (Blueprint $table) {
                $table->dropForeign('pre_order_ratings_establishment_id_foreign');
                $table->dropForeign('pre_order_ratings_pre_order_id_foreign');
                $table->dropForeign('pre_order_ratings_user_id_foreign');
            });
        }
    }
}
