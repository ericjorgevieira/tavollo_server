<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstablishmentAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establishment_accesses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('establishment_id');
            $table->enum('type', ['bill', 'preorder'])->default('preorder');
            $table->string('ip_address')->nullable();
            $table->string('utm_source')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establishment_accesses');
    }
}
