<?php

use Illuminate\Database\Seeder;

class ProductCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_categories')->insert(
            [
                'name' => 'Caldos',
                'slug' => 'caldos',
                'status' => 'enabled'
            ]);
        DB::table('product_categories')->insert(
            [
                'name' => 'Porções',
                'slug' => 'porcoes',
                'status' => 'enabled'
            ]);
        DB::table('product_categories')->insert(
            [
                'name' => 'Espetos',
                'slug' => 'espetos',
                'status' => 'enabled'
            ]);
        DB::table('product_categories')->insert(
            [
                'name' => 'Whiskies',
                'slug' => 'whiskies',
                'status' => 'enabled'
            ]);
        DB::table('product_categories')->insert(
            [
                'name' => 'Vinhos',
                'slug' => 'vinhos',
                'status' => 'enabled'
            ]);
        DB::table('product_categories')->insert(
            [
                'name' => 'Cervejas',
                'slug' => 'cervejas',
                'status' => 'enabled'
            ]);
        DB::table('product_categories')->insert(
            [
                'name' => 'Saladas',
                'slug' => 'saladas',
                'status' => 'enabled'
            ]);
        DB::table('product_categories')->insert(
            [
                'name' => 'Carnes',
                'slug' => 'carnes',
                'status' => 'enabled'
            ]);
        DB::table('product_categories')->insert(
            [
                'name' => 'Frangos',
                'slug' => 'frangos',
                'status' => 'enabled'
            ]);
        DB::table('product_categories')->insert(
            [
                'name' => 'Peixes',
                'slug' => 'Peixes',
                'status' => 'enabled'
            ]);
        DB::table('product_categories')->insert(
            [
                'name' => 'Sucos',
                'slug' => 'sucos',
                'status' => 'enabled'
            ]);
        DB::table('product_categories')->insert(
            [
                'name' => 'Massas',
                'slug' => 'massas',
                'status' => 'enabled'
            ]);
        DB::table('product_categories')->insert(
            [
                'name' => 'Pizzas',
                'slug' => 'pizzas',
                'status' => 'enabled'
            ]);
        DB::table('product_categories')->insert(
            [
                'name' => 'Refrigerantes',
                'slug' => 'refrigerantes',
                'status' => 'enabled'
            ]);
        DB::table('product_categories')->insert(
            [
                'name' => 'Sanduíches',
                'slug' => 'sanduiches',
                'status' => 'enabled'
            ]
        );
    }
}
