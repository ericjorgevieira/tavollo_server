<?php

namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'Tavollo Server');

// Project repository
set('repository', 'git@bitbucket.org:ericjorgevieira/tavollo.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

set('default_stage', 'homologacao');

// Adicionar o storage folder
// Shared files/dirs between deploys
// set('shared_dirs', [
//     'src/storage',
// ]);

// Laravel shared file
// set('shared_files', [
//     'src/.env',
// ]);

// Writable dirs by web server
add('writable_dirs', []);

set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader --ignore-platform-reqs');

// Colocar no env
host('homologacao')
    ->stage('homologacacao')
    ->hostname('forge@test.admin.tavollo.com')
    ->identityFile('tavollo_homolog.pem')
    ->set('deploy_path', '/home/forge/test.admin.tavollo.com');

// Tasks
// task('build', function () {
    // run('cd {{release_path}} && build');
// });

// Start tests
task('test', function () {
    // runLocally('php vendor/bin/phpunit --stop-on-failure');
});

task('migrate', function () {
    run('cd {{release_path}} && php artisan migrate --force');
});

task('optimize', function () {
    // run('cd {{release_path}} && php artisan optimize');
});

task('queue_restart', function () {
    // run('cd {{release_path}} && php artisan queue:restart');
});

task('change_folder', function () {
    $link = get('release_path');
    set('release_path', function () use ($link) {
        return $link.'/';
    });
});

before('deploy', 'test');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
before('deploy:vendors', 'change_folder');
after('success', 'migrate');
after('migrate', 'optimize');
after('success', 'queue_restart');
