var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var path = './vendor/bower_resources/';

var scripts = [
    path + "jquery/jquery.min.js",
    path + "jquery-ui/ui/jquery-ui.js",
    path + "bootstrap/dist/js/bootstrap.min.js",
    path + "bootstrap-select/dist/js/bootstrap-select.min.js",
    // path + "datatables.net/js/jquery.dataTables.min.js",
    // path + "datatables.net-bs/js/dataTables.bootstrap.min.js",
    path + "multiselect/js/jquery.multi-select.js",
    path + "jquery-maskmoney/dist/jquery.maskMoney.min.js",
    path + "jquery.maskedinput/dist/jquery.maskedinput.min.js",
    path + "jquery.drag-n-crop/jquery.drag-n-crop.js"
];

// elixir.config.js.browserify.watchify = {
//     enabled: false,
//     options: {
//         poll: true
//     }
// }

elixir(function (mix) {

    /** Processa o SCSS dos templates **/
    mix.sass("plateau.scss");
    mix.sass("master.scss");

    /** Processa o JS dos templates **/
    mix.scripts(scripts, "public/js/plateau-scripts.js");

    mix.webpack('plateau.js');
    // mix.vueify('plateau.js', {insertGlobals: true, transform: "vueify", output: "public/js"});

    /** Copia imagens e fontes para o Public **/
    mix.copy('resources/assets/images', 'public/images')
        .copy('resources/assets/audios', 'public/audios')
        .copy('resources/assets/translates', 'public/translates')
        .copy('resources/assets/fonts', 'public/fonts')
        .copy('resources/assets/js/plateau', 'public/js/plateau')
        .copy('resources/assets/js/service_worker', 'public/js/service_worker')
        .copy('resources/assets/js/socket', 'public/js/socket')
        .copy('resources/assets/js/qz', 'public/js/qz')
        .copy('node_modules/animate.css/animate.min.css', 'public/css/animate.min.css')
        .copy('vendor/bower_resources/jquery.drag-n-crop/jquery.drag-n-crop.css', 'public/css/jquery.drag-n-crop.css')
        .copy('node_modules/font-awesome/fonts', 'public/fonts')
        .copy(path + 'bootstrap/dist/fonts', 'public/fonts/bootstrap');

    /** Adiciona Token aos scripts **/
    mix.version([
        'public/css/*.css',
        'public/js/*.js'
    ]);

    mix.browserSync({
      port: 3001,
      proxy: 'admin.tavollo.localhost'
    });

});
