/**
 * Created by ericoliveira on 09/12/16.
 */
$(function () {

    // $("#menusTable tbody").sortable({
    //     axis: 'y',
    //     cursor: "move",
    //     update: function (event, ui) {
    //
    //         $.ajax({
    //             data: {
    //                 order: $(this).sortable('serialize'),
    //                 _token: document.querySelector('#token').getAttribute('value')
    //             },
    //             type: 'POST',
    //             cache: false,
    //             url: '/menus/order',
    //             headers: {
    //                 'Authorization':'Basic YXBpOnBhc3N3b3Jk',
    //                 'X_CSRF_TOKEN': document.querySelector('#token').getAttribute('value')
    //             },
    //             success: function(data){
    //                 showSuccessToast('Ordem dos Menus alterada com sucesso!');
    //             },
    //             error: function(data){
    //                 showErrorToast('Erro ao alterar ordem dos Menus');
    //             }
    //         });
    //     }
    // });

    // $(".multi-select").multiSelect({selectableOptgroup: true});

    $("#uploadPhotoMenu").on('change', function(){

        var $this = $(this), $photoUpload = $('.photo-upload'), $loading = $photoUpload.find('.loading');
        $photoUpload.find('img').hide();
        var originalUrl = $photoUpload.find('img').attr('src');
        $photoUpload.find('img').attr('src', '');
        $loading.css('display', 'block');

        var form = new FormData($this.parents('form')[0]);
        form.set('_method', 'PUT');

        if($this.val() == ''){
            alert("Nenhuma imagem selecionada. Por favor, selecione uma imagem.");
        }else{
            $.ajax({
                url: '/menus/upload',
                type: 'POST',
                data: form,
                headers: {
                    'Authorization':'Basic YXBpOnBhc3N3b3Jk',
                    'X_CSRF_TOKEN': document.querySelector('#token').getAttribute('value')
                },
                success: function (data) {
                    $this.val('');
                    if(data.success == true){
                        $photoUpload.find('img').attr('src', data.url);
                        $photoUpload.find('img').show();
                        $('#photo').val(data.path);
                        $loading.hide();
                    }else{
                        $photoUpload.find('img').attr('src', originalUrl);
                        $photoUpload.find('img').show();
                        $loading.hide();
                        alert(data.message.tmp_photo[0]);
                    }
                },
                error: function(data){
                    $this.val('');
                    $('#photo').val('');
                    $photoUpload.find('img').attr('src', originalUrl);
                    $photoUpload.find('img').show();
                    $loading.hide();
                    alert("Erro no Upload. Contate o Suporte.");
                },
                cache: false,
                processData: false,
                contentType: false,
            });
        }

    });

});
