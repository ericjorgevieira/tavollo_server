var Vue = require('vue');
var VueResource = require('vue-resource');
var toastr = require('toastr');

Vue.use(VueResource);

Vue.http.options.root = '/tables';
Vue.http.headers.common['Authorization'] = 'Basic YXBpOnBhc3N3b3Jk';
Vue.http.headers.common['X-CSRF-Token'] = document.querySelector('#token').getAttribute('value');

var location = document.querySelector('#location').getAttribute('value');


/**
 * Created by ericoliveira on 09/12/16.
 */
$(function () {

    // $(".dataTableSmartFalse").DataTable({
    //     paging: false,
    //     search: {
    //         smart: false
    //     },
    //     language: {
    //         url: '/translates/datatables.pt_br.json'
    //     }
    // });

});

/**
 * ========================== TABLES =============================== *
 */
new Vue({
      el: '#tables',
      data: {
          listTables: []
      },
      ready: function(){
        this.getList()
      },
      methods: {
        getList: function() {
          this.$http.get('list')
          .then(function (response) {
              var data = response.data;
              this.$set('listTables', data);
          });
        }
      }
  });
