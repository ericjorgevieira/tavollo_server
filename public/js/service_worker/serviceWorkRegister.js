'use strict'

var swRegistration = null;
var sw = null;

function prepareServiceWorker(){
  if ('serviceWorker' in navigator && 'PushManager' in window) {

    navigator.serviceWorker.register('/js/service_worker/serviceWorker.js')
    .then(function(swReg) {

      swRegistration = swReg;
      sw = swReg.active;

      Notification.requestPermission().then(function(result) {
        if (result === 'denied') {
          console.log('Permission wasn\'t granted. Allow a retry.');
          return;
        }
        if (result === 'default') {
          console.log('The permission request was dismissed.');
          return;
        }
        // Do something with the granted permission.
      });

    })
    .catch(function(error) {
      console.error('Service Worker Error', error);
    });
  } else {
    console.warn('Push messaging is not supported');
  }
}

function showNotification(title, body){

  if(sw !== null){

    const options = {
      body: body,
      icon: '/images/tavollo-quadrada.jpg',
      vibrate: [200, 100, 200, 100, 200, 100, 200]
    };

    swRegistration.showNotification(title, options)

  }else{
    console.log('Service Worker not active');
    console.log(sw, swRegistration);
  }

}

module.exports = {
  prepareServiceWorker,
  showNotification
}
