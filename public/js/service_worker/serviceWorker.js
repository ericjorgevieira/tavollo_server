'use strict';

self.addEventListener('notificationclick', function(event) {
  event.notification.close();

  var appUrl = 'https://admin.tavollo.com/home';

  event.waitUntil(clients.matchAll({
    includeUncontrolled: true,
    type: 'window'
    }).then( activeClients => {
      if (activeClients.length > 0) {
        activeClients[0].navigate(appUrl);
        activeClients[0].focus();
      } else {
        clients.openWindow(appUrl);
      }
    })
  );

});
