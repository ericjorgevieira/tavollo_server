/**
 * Created by ericoliveira on 19/01/17.
 */

/**
 * Toastr de sucesso
 * @param message
 */

var toastr = require('toastr');

var showSuccessToast = function(message) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-left",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "3000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    toastr["success"](message);
};

/**
 * Toastr de erro
 * @param message
 */
var showErrorToast = function(message) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-left",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "3000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    toastr["error"](message);
};

var formatCurrency = function(num)
{
  num = num.toString().replace(/\$|\,/g, '');
  if (isNaN(num))
  {
    num = "0";
  }

  var sign = (num == (num = Math.abs(num)));
  num = Math.floor(num * 100);
  var cents = num % 100;
  num = Math.floor(num / 100).toString();

  if (cents < 10)
  {
    cents = "0" + cents;
  }
  for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
  {
    num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
  }

  return (((sign) ? '' : '-') + 'R$ ' + num + ',' + cents);
}

var prepareOrderToPrint = function(order)
{
    var groups = null;
    if(order.product.groups && Object.keys(order.product.groups).length > 0){
      groups = 'Adicionais: \n';
      Object.keys(order.product.groups).forEach((key) => {
        var g = order.product.groups[key];
        groups += '* ' + g.name + '\n';
        g.items.forEach((i) => {
          groups += '- ' + i.item.name + ' (' + i.quant + 'x) \n';
        });
        groups += '\n';
      })
    }else{
      groups = '';
    }

    var alias = order.bill.table.alias ? order.bill.table.alias : ' - ';
    var waiter = order.bill_waiter ? order.bill_waiter.name : ' - ';

    return [
      '\x1B\x21\x30 Pedido do Tavollo \x1B\x21\x00' +
      '\n\n' +
      '------------------------------------------' +
      '\n' +
      'Pedido #' + order.id +
      '\n' +
      'Criado em: ' + order.created_at +
      '\n' +
      '\x1B\x21\x08' + order.bill.table.name + '\x1B\x21\x00' +
      '\n' +
      'Ponto de Referência: ' + alias +
      '\n' +
      '\x1B\x21\x08' + 'Garçom: ' + waiter + '\x1B\x21\x00' +
      '\n\n' +
      '------------------------------------------' +
      '\n' +
      '\x1B\x21\x08' + order.product_id + ' - ' + order.product.name + '\x1B\x21\x00' +
      '\n' +
      'Quantidade: ' + order.quant +
      '\n' +
      groups +
      '\n' +
      'Observações: ' + order.observation +
      '\n\n\n\n' +
      '------------------------------------------' +
      '\n\n' +
      '\x1B' + '\x69'
    ];
}

var preparePreOrderToPrint = function(order, products)
{
    var productsString = '';
    products.map(function(p){
      var groups = null;
      if(p.product.groups){
        groups = 'Adicionais: \n';
        Object.keys(order.product.groups).forEach((key) => {
          var g = order.product.groups[key];
          groups += '* ' + g.name + '\n';
          g.items.forEach((i) => {
            groups += '- ' + i.item.name + ' (' + i.quant + 'x) \n';
          });
          groups += '\n';
        })
      }else{
        groups = '';
      }

      productsString +=
      '\x1B\x21\x08' + p.product_id + ' - ' + p.product.name + '\x1B\x21\x00' +
      '\n' +
      'Quantidade: ' + p.quant +
      '\n' +
      groups +
      '\n' +
      'Observações: ' + p.observation +
      '\n\n'
    })

    var alias = order.alias ? order.alias : ' - '

    return [
      '\x1B\x21\x30 Pedido do Tavollo \x1B\x21\x00' +
      '\n\n\n' +
      '------------------------------------------' +
      '\n' +
      'Pedido #' + order.id +
      '\n' +
      '\x1B\x21\x08Ponto de Referência: ' + alias + '\x1B\x21\x00' +
      '\n' +
      productsString +
      '\n\n' +
      '------------------------------------------' +
      '\n\n' +
      '\x1B' + '\x69'
    ];
}

var prepareClosingBillToPrint = function(bill)
{

  var isConsumptionCard = bill.table.is_consumption_card;
  var spotType = isConsumptionCard ? 'Cartão' : 'Mesa';

  var referencePoint = isConsumptionCard ? '\x1B\x21\x08Ponto de Referência: ' + bill.table.alias + '\x1B\x21\x00' : '\n';

  var isPaid = bill.transaction_code && bill.transaction_type ? '\x1B\x21\x08Pagamento já efetuado pelo cliente!\x1B\x21\x00' : '\n'; 

  var date = new Date();

  var month = date.getMonth() + 1;

  var closeTime = date.getDate() + '/' + month + '/' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getMinutes();

  return [
    '\x1B\x21\x30 Encerramento de \x1B\x21\x00' +
    '\x1B\x21\x30 Comanda Tavollo \x1B\x21\x00' +
    '\n\n' +
    '\x1B\x21\x08 ' + spotType + ': ' + bill.table.number + ' \x1B\x21\x00' +
    '\n' +
    referencePoint +
    '\n' +
    isPaid +
    '\n\n' +
    '\x1B\x21\x08 Data: ' + closeTime + ' \x1B\x21\x00' +
    '\x1B' + '\x69'
  ];
}

module.exports = {
    showSuccessToast,
    showErrorToast,
    formatCurrency,
    prepareOrderToPrint,
    preparePreOrderToPrint,
    prepareClosingBillToPrint
};
