/**
 * Created by ericoliveira on 06/07/16.
 */
'use strict';

import Vue from 'vue/dist/vue.js'
import VueResource from 'vue-resource'
Vue.use(VueResource)

Vue.http.options.root = '';
Vue.http.headers.common['Authorization'] = 'Basic YXBpOnBhc3N3b3Jk';
Vue.http.headers.common['X-CSRF-Token'] = document.querySelector('#token').getAttribute('value');

var functions = require('./functions.js');

import Dashboard from './master/components/Dashboard'

new Vue({
    el: '#app-layout',
    components: {
        Dashboard,
    }
})
