'use strict';

// import Vue from 'vue/dist/vue.esm.js'
import * as Vue from 'vue/dist/vue.common.js';
import VueResource from 'vue-resource'

Vue.use(VueResource)

Vue.http.options.root = '';
Vue.http.headers.common['Authorization'] = 'Basic YXBpOnBhc3N3b3Jk';
Vue.http.headers.common['X-CSRF-Token'] = document.querySelector('#token').getAttribute('value');

window.swRegister = require('./service_worker/serviceWorkRegister.js');
window.swRegister.prepareServiceWorker();

require('./qz/dependencies/rsvp-3.1.0.min.js');
require('./qz/dependencies/sha-256.min.js');
require('./qz/qz-tray.js');

var functions = require('./functions.js');

/**
 * Controllers
 */
// require('./plateau/scripts/history.js');
require('./plateau/scripts/default.js');
require('./plateau/scripts/home.js');
require('./plateau/scripts/menus.js');
require('./plateau/scripts/products.js');
require('./plateau/scripts/profile.js');
// require('./plateau/scripts/bills.js');
//
require('bootstrap-toggle');
require('bootstrap-tagsinput');

/**
 * Components
 */
import Sidebar from './plateau/components/Sidebar'
import Dashboard from './plateau/components/Dashboard'
import Smarttable from './plateau/components/SmartTable'
import Billtable from './plateau/components/BillTable'
import Ratingtable from './plateau/components/RatingTable'
import Preordertable from './plateau/components/PreOrderTable'
import Producttable from './plateau/components/ProductTable'
import Productgroups from './plateau/components/ProductGroups'
import Productobservations from './plateau/components/ProductObservations'
import Analysis from './plateau/components/Analysis'
import Tablerequesttoken from './plateau/components/TableRequestToken'
import Imageupload from './plateau/components/Imageupload';

new Vue({
    el: '#app-layout',
    components: {
        Dashboard,
        Sidebar,
        Smarttable,
        Billtable,
        Ratingtable,
        Preordertable,
        Producttable,
        Productgroups,
        Productobservations,
        Analysis,
        Tablerequesttoken,
        Imageupload
    }
})
