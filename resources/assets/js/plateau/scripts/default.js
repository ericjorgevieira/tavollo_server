/**
* ========================== Eventos Recorrentes =============================== *
*/

$(function () {

    $(document).ready(function(){
        $("#fake-loader").fadeOut();
    });

    $('body').tooltip({
        selector: '[rel=tooltip]',
        delay: {"show": 500, "hide": 100},
        track: true
    });

    $(".has-popover").popover({
        trigger: "hover",
        placement: "top"
    });

    $(".has-right-popover").popover({
        trigger: "hover",
        container: 'body',
        placement: "right"
    });

    $('.mask-money').maskMoney({
        thousands: '.',
        decimal: ','
    });

    $('.mask-cnpj').mask('99.999.999/9999-99');
    $('.mask-cpf').mask('999.999.999-99');
    $('.mask-date').mask('99/99/9999');
    $('.mask-phone').mask('(99)9999-9999?9');
    $('.mask-cep').mask('99.999-999');
    $('.mask-weight').mask('9.999');
    $('.mask-time').mask('99:99');

    /**
    * Dashboard Modal JS
    */
    $('#products').on('click', '.add-option', function (e) {

        e.preventDefault();

        var $products = $('#products'), $options = $products.find('.options'),
        $itemOption = $('#option-elem').find('.option-item');

        var $newItem = $itemOption.clone();
        $newItem.css('display', 'none');
        $newItem.appendTo($options).show('slow');

        $newItem.find('.mask-money').maskMoney({
            thousands: '.',
            decimal: ','
        });

        if ($options.find('.option-item').length > 0) {
            $('.remove-option').removeClass('hide');
        }
    }).on('click', '.remove-option', function (e) {

        e.preventDefault();

        var $products = $('#products'), $options = $products.find('.options'),
        $itemOption = $options.find('.option-item:last');

        $itemOption.hide('slow', function () {
            $(this).remove();
        });

        if ($options.find('.option-item').length <= 1) {
            $('.remove-option').addClass('hide');
        }
    });

    $(document).on('show.bs.collapse', '#modalBill', function () {
        $(this).find('.selectpicker').selectpicker('refresh');
    }).on('hide.bs.modal', function () {
        $(".collapse").collapse('hide');
    });
    /**
    * End Dashboard Modal JS
    */

    $('.btn-collapse-sidebar .hide-order-bar').on('click', function () {
        var $this = $(this);
        $('#sidebar-wrapper').removeClass('slideInRight');
        $('#sidebar-wrapper').addClass('slideOutRight');
        $this.hide();
        $('.show-order-bar').show();
    });

    $('.btn-collapse-sidebar .show-order-bar').on('click', function () {
        var $this = $(this);
        $('#sidebar-wrapper').removeClass('slideOutRight');
        $('#sidebar-wrapper').addClass('slideInRight');
        $('#sidebar-wrapper').show();
        $this.hide();
        $('.hide-order-bar').show();
    });
    /**
    * End Sidebar Orders JS
    */
});
