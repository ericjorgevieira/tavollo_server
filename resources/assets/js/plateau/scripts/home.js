var functions = require('../../functions.js');

var headers = {
  'Authorization':'Basic YXBpOnBhc3N3b3Jk',
  'X-CSRF-Token': document.querySelector('#token').getAttribute('value')
}

$(function () {
    /**
    * Dashboard Control JS
    */
    $('#control').on('changed.bs.select', '#viewToken', function(){
        var $option = $(this).find(':selected'), $modalToken = $("#modalToken"),
        billId = $option.val(), $viewToken = $("#viewToken");
        $modalToken.find('.modal-title').text('Código - ' + $option.text());
        $modalToken.find('#tableCode').text("Aguarde...");
        $modalToken.find('#tableQrCode').attr('src','../images/loading.gif');
        $modalToken.modal();
        $.ajax({
            url: '/home/table/getcode/' + billId,
            type: 'GET',
            headers: headers,
            success: function(retorno){
                $modalToken.find('#tableCode').text(retorno);
                $modalToken.find('#tableQrCode').attr('src','/tables/qr-code/' + billId);
                $viewToken.val('');
                $viewToken.selectpicker('refresh');
            },
            error: function(retorno){
                console.log("ERRO " + retorno);
            }
        });
    });

    $('#control').on('submit', '#newBillForm', function(e){
        e.preventDefault();
        var $this = $(this), $inputName = $this.find('input[name=user_name]'), userName = $inputName.val(),
        $inputTable = $('#selectTable'), tableId = $inputTable.val();

        if(tableId == ''){
            functions.showErrorToast('Deve selecionar a mesa/cartão!');
        }else{
            $.ajax({
                url: '/home/bill/create',
                type: 'POST',
                headers: headers,
                data: {'user_name': userName, 'table_id': tableId},
                dataType: 'json',
                success: function(retorno){
                    if(retorno.success == true){
                      $inputTable.val('')
                      $inputTable.selectpicker('refresh');
                      $inputName.val('');
                      functions.showSuccessToast(retorno.message);
                    }else{
                        functions.showErrorToast(retorno.message);
                    }
                },
                error: function(retorno){
                    functions.showErrorToast("Erro ao processar abertura de conta.");
                }
            });
        }
    });
});
