/**
 * Created by ericoliveira on 09/12/16.
 */
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var redis = new Redis();
redis.on('ready', function(){
    redis.psubscribe('*', function (err, count) {
    });
});

//redis.psubscribe('bill-created.?', function (err, count) {
//});
//redis.psubscribe('bill-closing.?', function (err, count) {
//});
redis.on('pmessage', function (channel, pattern, message) {
    console.log('Message Recieved: ' + message);
    console.log('Channel: ' + channel);
    console.log('Pattern: ' + pattern);
    message = JSON.parse(message);
    io.emit(pattern + ':' + message.event, message.data);
});
http.listen(3000, function () {
    console.log('Listening on Port 3000');
});
io.on('connection', function (socket) {
    console.log(socket.request.connection.remoteAddress);
});
