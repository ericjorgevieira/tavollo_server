/**
 * Created by ericoliveira on 14/12/16.
 */
'use strict'

var app = require('express')();
var https = require('https');
var fs = require('fs');
var Redis = require('ioredis');

var privateKey = fs.readFileSync('/etc/letsencrypt/live/ws.tavollo.com/privkey.pem').toString();
var certificate = fs.readFileSync('/etc/letsencrypt/live/ws.tavollo.com/cert.pem').toString();
var ca = fs.readFileSync('/etc/letsencrypt/live/ws.tavollo.com/fullchain.pem').toString();

var credentials = {
    ca: ca,
    key: privateKey,
    cert: certificate
};

var server = https.createServer(credentials, app);
server.listen(3000, function(){
    console.log("Listening to 3000");
});

var io = require('socket.io').listen(server);
io.sockets.on('connection', function(socket){
    console.log(socket);
});

var redis = new Redis();

redis.on('ready', function(){
    redis.psubscribe('*', function (err, count) {
    });
});

redis.on('error', function(error){
    console.log("ERROR REDIS");
    console.log(error)
});

redis.on('pmessage', function (channel, pattern, message) {
    console.log('Message Recieved: ' + message);
    console.log('Channel: ' + channel);
    console.log('Pattern: ' + pattern);
    message = JSON.parse(message);
    io.emit(pattern + ':' + message.event, message.data);
});
