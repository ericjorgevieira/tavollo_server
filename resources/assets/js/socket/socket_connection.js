/**
* ========================== Socket.IO =============================== *
*/

var pathArray = window.location.href.split('/');
var protocol = pathArray[0];
var host = pathArray[2];
host = host.split(':');
var url = protocol + '//' + host[0];
var socket = io(url + ':3000', {secure: true});

module.exports = {
  socket
}
