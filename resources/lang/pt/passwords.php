<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Senha precisa ter pelo menos 6 caracteres e estar confirmada.',
    'reset' => 'Sua senha foi resetada!',
    'sent' => 'Nós lhe enviamos um email com link para nova senha!',
    'token' => 'Este token de renovação de senha está inválido.',
    'user' => "Nós não encontramos um usuário com este e-mail.",

];
