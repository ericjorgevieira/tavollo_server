@extends('layouts.plateau-login')

@section('content')
  <div class="logo animated fadeInDown">
    <img src="/images/logo-2.png"/>
  </div>
  <div class="row animated fadeInRight">
    @if (session('success'))
      <div class="alert alert-success">
        {{ session('success') }}
      </div>
    @endif
    @if (session('warning'))
      <div class="alert alert-danger">
        {{ session('warning') }}
      </div>
    @endif
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
      {{ csrf_field() }}
      <input type="hidden" name="remember" value="true">

      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <div class="col-md-2"></div>

        <div class="col-md-8">
          <input placeholder="E-mail" id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

          @if ($errors->has('email'))
            <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
            </span>
          @endif
          @if ($errors->has('token_error'))
            <span class="help-block">
              <strong>{{ $errors->first('token_error') }}</strong>
            </span>
          @endif
        </div>
      </div>

      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <div class="col-md-2"></div>

        <div class="col-md-8">
          <input placeholder="Senha" id="password" type="password" class="form-control" name="password">

          @if ($errors->has('password'))
            <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
            </span>
          @endif
        </div>
      </div>

      <div class="form-group">
        <div class="col-md-2"></div>
        <div class="col-md-8 text-center">
          <button type="submit" class="btn btn-primary">
            Entrar
          </button>
        </div>
      </div>
    </form>
  </div>
@endsection
