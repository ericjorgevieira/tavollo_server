@extends('layouts.plateau-register')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Cadastre-se agora!</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        @if (session('warning'))
                          <div class="alert alert-danger">
                            {{ session('warning') }}
                          </div>
                        @endif

                        <input type="hidden" name="partnership_token" value="{{ $partnership->partnership_token }}" />
                        <input type="hidden" name="partnership_id" value="{{ $partnership->id }}" />

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Nome do Proprietário</label>

                            <div class="col-md-6">
                                <input required id="username" type="text" class="form-control" name="username" value="{{ old('username') }}">

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Celular</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control mask-phone" name="phone" value="{{ old('phone') }}">

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('fantasy_name') ? ' has-error' : '' }}">
                            <label for="fantasy_name" class="col-md-4 control-label">Razão Social</label>

                            <div class="col-md-6">
                                <input required id="fantasy_name" type="text" class="form-control" name="fantasy_name" value="{{ old('fantasy_name') }}">

                                @if ($errors->has('fantasy_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fantasy_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nome Fantasia</label>

                            <div class="col-md-6">
                                <input required id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('cnpj') ? ' has-error' : '' }}">
                            <label for="cnpj" class="col-md-4 control-label">CNPJ</label>

                            <div class="col-md-6">
                                <input required id="cnpj" type="text" class="form-control mask-cnpj" name="cnpj" value="{{ old('cnpj') }}">

                                @if ($errors->has('cnpj'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cnpj') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('ie') ? ' has-error' : '' }}">
                            <label for="ie" class="col-md-4 control-label">Inscrição Estadual</label>

                            <div class="col-md-6">
                                <input id="ie" type="text" class="form-control" name="ie" value="{{ old('ie') }}">

                                @if ($errors->has('ie'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('ie') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Endereço</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}">

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                            <label for="postal_code" class="col-md-4 control-label">CEP</label>

                            <div class="col-md-6">
                                <input id="postal_code" type="text" class="form-control mask-cep" name="postal_code" value="{{ old('postal_code') }}">

                                @if ($errors->has('postal_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('postal_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('district') ? ' has-error' : '' }}">
                            <label for="district" class="col-md-4 control-label">Bairro</label>

                            <div class="col-md-6">
                                <input id="district" type="text" class="form-control" name="district" value="{{ old('district') }}">

                                @if ($errors->has('district'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('district') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                            <label for="state" class="col-md-4 control-label">Estado</label>

                            <div class="col-md-6">
                              <select name="state" class="form-control" id="selectCategoria" title="Selecione..">
                                  @foreach(\App\Models\Establishment::estados() as $sigla => $e)
                                      <option {{ (old('discrict') == $sigla) ? 'selected' : '' }} value="{{ $sigla }}">{{ $e }}</option>
                                  @endforeach
                              </select>

                                @if ($errors->has('state'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <label for="city" class="col-md-4 control-label">Cidade</label>

                            <div class="col-md-6">
                                <input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}">

                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('integration') ? ' has-error' : '' }}">
                            <label for="integration" class="col-md-4 control-label">Possui integração com </label>

                            <div class="col-md-6">
                                <select name="enable_advanced_functions" class="form-control">
                                  <option value="1">
                                    Nenhum
                                  </option>
                                  <option value="0">
                                    Raffinato
                                  </option>
                                </select>

                                @if ($errors->has('enable_advanced_functions'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('enable_advanced_functions') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('product') ? ' has-error' : '' }}">
                            <label for="product" class="col-md-4 control-label">Produto adquirido </label>

                            <div class="col-md-6">
                                <select name="plan" class="form-control" id="product">
                                  @foreach(\App\Models\Establishment::$productsList as $k => $p)
                                    <option {{ old('product') && old('product') == $k ? 'selected' : '' }} value="{{ $k }}">
                                      {{ $p }}
                                    </option>
                                  @endforeach
                                </select>

                                @if ($errors->has('product'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('product') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail de Login</label>

                            <div class="col-md-6">
                                <input required id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Senha</label>

                            <div class="col-md-6">
                                <input required id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirmar Senha</label>

                            <div class="col-md-6">
                                <input required id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Cadastrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$('.mask-cnpj').mask('99.999.999/9999-99');
$('.mask-phone').mask('(99) 99999-9999');
$('.mask-cep').mask('99.999-999');
</script>
@endpush
