<!DOCTYPE html>
<html>
    <head>
        <title>Tavollo - Erro</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
                background-color: #014687;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #fff;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
            a{
                cursor: pointer;
                font-size: 2em;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Encontramos um problema. Nos perdoe!</div>
                <a onclick="window.history.back();">Voltar</a>
            </div>
        </div>
    </body>
</html>
