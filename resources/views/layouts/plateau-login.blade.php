<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Tavollo - Auto Atendimento para Bares e Restaurantes - Login</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link href="{{ mix('css/plateau.css') }}" rel="stylesheet">
    <link href="/css/animate.min.css" rel="stylesheet" />

</head>
<body id="app-layout">
<div id="login" class="container">
    <div class="row">
        <div class="col-xs-1 col-sm-3 col-md-3"></div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            @yield('content')
        </div>
        <div class="col-xs-1 col-sm-3 col-md-3"></div>
    </div>
</div>

<!-- JavaScripts -->
<script src="{{ mix('js/plateau-scripts.js') }}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
</body>
</html>
