<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta id="token" name="token" value="{{csrf_token()}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta id="location" name="location" value="{{ md5(Auth::user()->establishment_id) }}">
    <title>Tavollo - Controle de Atendimento</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link href="{{ mix('css/plateau.css') }}" rel="stylesheet">
    <link href="/css/animate.min.css" rel="stylesheet" />
    <link href="/css/bootstrap-toggle.min.css" rel="stylesheet" />

    <!-- Socket.IO -->
    {{-- <script src="https://cdn.socket.io/socket.io-1.4.5.js"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.4.5/socket.io.min.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8LffY9p9jgnGCjpv7ma9t3Mme4qGoAd8&libraries=places"></script>
</head>
<body class="plateau">
    <div id="app-layout">
        <div id="fake-loader">
            <span><i class="fa fa-cog fa-spin fa-4x fa-fw"></i></span>
        </div>
        @include('plateau.includes.topbar')
        <div id="wrapper">
            <Sidebar :config="{{ Auth::user()->establishment->config }}"></Sidebar>
            <div id="page-content-wrapper">
                <div class="page-content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <audio id="alertOrder">
                <source src="/audios/alert.mp3" type="audio/mp3"/>
            </audio>
        </div>
    </div>

    <!-- JavaScripts -->
    <script src="{{ mix('js/plateau-scripts.js') }}"></script>
    <script src="{{ mix('js/plateau.js') }}"></script>
    <script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    @stack('scripts')
</body>
</html>
