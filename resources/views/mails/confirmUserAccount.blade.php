@component('layouts.mail')
    {{-- Header --}}
    @slot('header')
        @component('mails.includes.header', ['url' => config('app.url')])
            <img src="{{ asset('images/top.jpg') }}" />
        @endcomponent
    @endslot

    {{-- Body --}}
    <div style="text-align: center; padding: 30px;">
      
      <b style="font-size: 28px;">{{ 'Olá, ' . $user->name . '!' }}</b><br/>

      Seja bem-vindo(a) ao Tavollo! Clique neste botão para ativar a sua conta:

      @slot('button')
        @component('vendor.mail.html.button', ['url' =>  $link . '/' . $token ])
        Confirmar Conta
        @endcomponent
      @endslot

    </div>

    {{-- Footer --}}
    @slot('footer')
        @component('mails.includes.footer')
            &nbsp;
        @endcomponent
    @endslot
@endcomponent
