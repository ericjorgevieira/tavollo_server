<tr>
    <td>
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" style="background-color: #014687; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
            <tr>
                <td class="content-cell" align="center">
                    {{ Illuminate\Mail\Markdown::parse($slot) }}
                </td>
            </tr>
        </table>
    </td>
</tr>
