<tr>
    <td>
      <table class="header" align="center" width="570" cellpadding="0" cellspacing="0" style="background-color: #014687;border-top-left-radius: 10px; border-top-right-radius: 10px;">
        <tr>
          <td class="content-cell" align="center">
            {{ $slot }}
          </td>
        </tr>
      </table>
    </td>
</tr>
