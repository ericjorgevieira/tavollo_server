@component('layouts.mail')
    {{-- Header --}}
    @slot('header')
        @component('mails.includes.header', ['url' => config('app.url')])
            <img src="{{ asset('images/top.png') }}" />
        @endcomponent
    @endslot

    {{-- Body --}}
    <div style="text-align: center; padding: 30px;">

      <b style="font-size: 28px;">{{ 'Olá, ' . $user->name . '!' }}</b><br/>

      Clique neste link para redefinir a sua senha:

      @slot('button')
        @component('vendor.mail.html.button', ['url' =>  $link . '?token=' . $token ])
        Redefinir Senha
        @endcomponent
      @endslot

    </div>

    {{-- Footer --}}
    @slot('footer')
        @component('mails.includes.footer')
            &nbsp;
        @endcomponent
    @endslot
@endcomponent
