@extends('layouts.master')

@section('content')
        <h2>Contas</h2>

            <div class="container-fluid">
                <div class="row">
                    @include('master.includes.return-message')
                </div>
                <div class="row">
                    <table id="establishmentsTable" class="dataTable table table-striped">
                        <thead>
                        <tr>
                            <th>Imagem</th>
                            <th>Usuário</th>
                            <th>Estabelecimento</th>
                            <th>Usuários Anexos</th>
                            <th>Status</th>
                            <th>Consumo</th>
                            <th>Hora de Abertura</th>
                            <th>Duração</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($bills as $e)
                            <tr>
                                <td>
                                    <img style="border-radius: 50%; width: 70px;" src="{{ $e->user->photo }}" />
                                </td>
                                <td>{{ $e->user->name }}</td>
                                <td>{{ ($e->table) ? $e->table->establishment->name : '-' }}</td>
                                <td>{{ count($e->getAttachedUsers()) }}</td>
                                <td>{{ $e->getStatusMessage() }}</td>
                                <td><b>R$ {{ App\Models\Bill::getTotalValue($e, true) }}</b></td>
                                <td>{{ $e->opened_at }}</td>
                                <td><b>{{ $e->duration()}}</b></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
@endsection
