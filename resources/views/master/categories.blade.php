@extends('layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Categorias de Produtos</div>

        <div class="panel-body">
            <div class="container-fluid">
                <div class="row">
                    @include('master.includes.return-message')
                    <?php if($trash){ ?>
                    <a class="btn btn-primary" href="{{ url('/categories') }}" role="button">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Categorias
                    </a>
                    <?php }else{ ?>
                    <a class="btn btn-success" href="{{ url('/categories/create') }}" role="button">
                        <i class="fa fa-plus-circle"></i> Cadastrar Categorias</a>
                    <a class="btn btn-danger hidden-xs" href="{{ url('/categories/trash') }}" role="button">
                        <i class="fa fa-close"></i> Categorias Desativadas</a>
                    <?php } ?>
                </div>
                <div class="row">
                    <table id="establishmentsTable" class="dataTable table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>Slug</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $e)
                            <tr>
                                <td>{{ $e->id }}</td>
                                <td>{{ $e->name }}</td>
                                <td>{{ $e->slug }}</td>
                                <td>
                                    <a class="btn btn-default pull-left has-popover"
                                       data-content="Editar '{{ $e->name }}'"
                                       href="{{ url('/categories/' . $e->id . '/edit') }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                    <form class="hidden-xs" action="{{ url('/categories/' . $e->id . '/enable') }}"
                                          method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}

                                        <?php if($e->status == 'enabled'){ ?>
                                        <button type="submit" class="btn btn-danger pull-left has-popover"
                                                data-content="Desativar '{{ $e->name }}'">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        <?php }else{ ?>
                                        <button type="submit" class="btn btn-success pull-left has-popover"
                                                data-content="Ativar '{{ $e->name }}'">
                                            <i class="fa fa-reply"></i>
                                        </button>
                                        <?php } ?>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
