@extends('layouts.master')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Cadastro de Categoria</div>

        <div class="panel-body">
            <div class="container-fluid">
                <div class="row">
                    <a class="btn btn-primary" href="{{ url('/categories') }}" role="button">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>Voltar
                    </a>
                </div>
                <div class="row">
                    @include('master.includes.return-message')
                </div>
                <div class="row">
                    <form method="post" action="{{ url('/categories/' . $category->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Nome *</label>
                                <input required class="form-control" type="text" name="name" value="{{ $category->name }}"/>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="status" checked value="enabled" type="checkbox"> Ativo?
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <hr/>
                            <button type="submit" class="btn btn-primary">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
