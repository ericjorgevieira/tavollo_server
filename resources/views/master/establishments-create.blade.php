@extends('layouts.master')

@section('content')

            <div class="container">
                <div class="row">
                  <div class="col-sm-12">
                    <h2>Cadastro de Estabelecimento</h2>
                    <a href="{{ url('/establishments') }}">
                        <span class="fa fa-arrow-left" aria-hidden="true"></span> Voltar
                    </a>
                  </div>

                </div>
                <div class="row">
                    @include('master.includes.return-message')
                </div>
                <div class="row">
                    <form method="post" action="{{ url('/establishments/store') }}">
                        {{ csrf_field() }}
                        {{ method_field('POST') }}
                        <div class="form-group col-sm-12">
                            <label>Nome *</label>
                            <input required class="form-control" type="text" name="name" value=""/>
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Nome Fantasia</label>
                            <input class="form-control" type="text" name="fantasy_name" value=""/>
                        </div>
                        <div class="form-group col-sm-4">
                            <label>CNPJ *</label>
                            <input required class="form-control mask-cnpj" type="text" name="cnpj" value=""/>
                        </div>
                        <div class="form-group col-sm-4">
                            <label>Telefone</label>
                            <input class="form-control mask-phone" type="text" name="phone" value=""/>
                        </div>
                        <div class="form-group col-sm-4">
                            <label>Data de Vencimento</label>
                            <input class="form-control mask-date" type="text" name="payment_date" value=""/>
                        </div>
                        <div class="form-group col-sm-12">
                            <label>E-mail *</label>
                            <input required class="form-control" type="text" name="email" value=""/>
                        </div>
                        <hr/>
                        <div class="form-group col-sm-12">
                            <hr/>
                            <label>Endereço</label>
                            <input class="form-control" type="text" name="address" value=""/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Bairro</label>
                            <input class="form-control" type="text" name="district" value=""/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Cidade</label>
                            <input class="form-control" type="text" name="city" value=""/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Estado</label>
                            <select name="state" class="form-control selectpicker"
                                    data-live-search="true" id="selectCategoria" title="Selecione.." data-size="10">
                                @foreach($estados as $sigla => $e)
                                    <option value="{{ $sigla }}">{{ $e }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>CEP</label>
                            <input class="form-control mask-cep" type="text" name="postal_code" value=""/>
                        </div>
                        <div class="col-sm-12">
                          <hr/>
                        </div>

                        <div class="form-group col-sm-4">
                            <label>Plano</label>
                            <select name="plan" class="form-control">
                              <option value="test">
                                Teste
                              </option>
                              <option value="master">
                                Produção
                              </option>
                            </select>
                        </div>
                        <div class="form-group col-sm-4">
                          <label>Utiliza integração?</label>
                          <select name="enable_advanced_functions" class="form-control">
                            <option value="0">
                              Sim (Oculta funções manuais)
                            </option>
                            <option value="1">
                              Não (Exibe funções manuais)
                            </option>
                          </select>
                        </div>
                        <div class="form-group col-sm-4">
                          <label>Remover controle de geolocalização?</label>
                          <select name="disable_location" class="form-control">
                            <option value="1">
                              Sim (Estabelecimento será exibido para todos)
                            </option>
                            <option value="0">
                              Não (Visível apenas para usuários próximos)
                            </option>
                          </select>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Possui função: Abertura de Comanda (Pós-Pago)?</label>
                            <select name="allow_bills" class="form-control">
                              <option value="1">
                                Sim
                              </option>
                              <option value="0">
                                Não
                              </option>
                            </select>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Possui função: Pedir e Pagar (Pré-Pago)?</label>
                            <select name="allow_pre_bills" class="form-control">
                              <option value="1">
                                Sim
                              </option>
                              <option value="0">
                                Não
                              </option>
                            </select>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Ocultar Estabelecimento?</label>
                            <select name="display_location" class="form-control">
                              <option value="0">
                                Sim (Oculta a visualização do Estabelecimento)
                              </option>
                              <option value="1">
                                Não (Libera a visualização do Estabelecimento)
                              </option>
                            </select>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Habilitar funções de Pedido (Pré-Pago)?</label>
                            <select name="disable_orders" class="form-control">
                              <option value="1">
                                Não (Apenas visualização de cardápio)
                              </option>
                              <option value="0">
                                Sim (É necessário ter uma conta de pagamento)
                              </option>
                            </select>
                        </div>

                        <div class="form-group col-sm-12">
                            <hr/>
                            <button type="submit" class="btn btn-success pull-right">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
@endsection

@push('scripts')
<script type="text/javascript">
$('.mask-cnpj').mask('99.999.999/9999-99');
$('.mask-phone').mask('(99) 99999-9999');
$('.mask-date').mask('99/99/9999');
</script>
@endpush
