@extends('layouts.master')

@section('content')
@php
$isPartnership = Auth::guard('master')->user()->partnership();
@endphp
            <div class="container">
                <div class="row">
                  <div class="col-sm-12">
                    <h2>Edição de Estabelecimento</h2>
                    <a class="" href="{{ url('/establishments/' . $establishment->id) }}">
                        <span class="fa fa-arrow-left" aria-hidden="true"></span> Voltar
                    </a>
                  </div>
                </div>
                <div class="row">
                    @include('master.includes.return-message')
                </div>

                <div class="row">
                    <form method="post" action="{{ url('/establishments/' . $establishment->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group col-sm-12">
                            <label>Nome Fantasia</label>
                            <input {{ $isPartnership ? 'readonly' : '' }} required class="form-control" type="text" name="name" value="{{ $establishment->name }}"/>
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Razão Social</label>
                            <input {{ $isPartnership ? 'readonly' : '' }} class="form-control" type="text" name="fantasy_name" value="{{ $establishment->fantasy_name }}"/>
                        </div>
                        <div class="form-group col-sm-4">
                            <label>CNPJ *</label>
                            <input {{ $isPartnership ? 'readonly' : '' }} required class="form-control mask-cnpj" type="text" name="cnpj" value="{{ $establishment->cnpj }}"/>
                        </div>
                        <div class="form-group col-sm-12">
                          <label>URL de Acesso {{ $isPartnership ? '' : '(Ao alterar, talvez seja necessário gerar novamente os QR codes)' }}</label>
                          <input {{ $isPartnership ? 'readonly' : '' }} required class="form-control" type="text" name="slug" value="{{ $establishment->slug }}"/>
                      </div>
                        <div class="form-group col-sm-4">
                            <label>Telefone</label>
                            <input {{ $isPartnership ? 'readonly' : '' }} class="form-control mask-phone" type="text" name="phone" value="{{ $establishment->phone }}"/>
                        </div>
                        <div class="form-group col-sm-4">
                            <label>Data de Vencimento</label>
                            <input {{ $isPartnership ? 'readonly' : '' }}
                              class="form-control mask-date" type="text" name="payment_date" value="{{ ($establishment->payment_date) ? $establishment->payment_date->format('d/m/Y') : null }}"/>
                        </div>
                        <div class="form-group col-sm-12">
                            <label>E-mail *</label>
                            <input {{ $isPartnership ? 'readonly' : '' }} required class="form-control" type="text" name="email" value="{{ $establishment->email }}"/>
                        </div>

                        <div class="form-group col-sm-12">
                            <hr/>
                            <label>Endereço</label>
                            <input {{ $isPartnership ? 'readonly' : '' }} class="form-control" type="text" name="address" value="{{ $establishment->address }}"/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Bairro</label>
                            <input {{ $isPartnership ? 'readonly' : '' }} class="form-control" type="text" name="district" value="{{ $establishment->district }}"/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Cidade</label>
                            <input {{ $isPartnership ? 'readonly' : '' }} class="form-control" type="text" name="city" value="{{ $establishment->city }}"/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Estado</label>
                            <select {{ $isPartnership ? 'readonly' : '' }} name="state" class="form-control selectpicker"
                                    data-live-search="true" id="selectCategoria" title="Selecione.." data-size="10">
                                @foreach($estados as $sigla => $e)
                                    <option <?= ($establishment->state == $sigla) ? 'selected' : '' ?> value="{{ $sigla }}">{{ $e }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>CEP</label>
                            <input {{ $isPartnership ? 'readonly' : '' }} class="form-control mask-cep" type="text" name="postal_code" value="{{ $establishment->postal_code }}"/>
                        </div>

                        <div class="col-sm-12">
                          <hr/>
                        </div>

                        <div class="form-group col-sm-4">
                            <label>Plano</label>
                            <select {{ $isPartnership ? 'readonly' : '' }} name="plan" class="form-control">
                              <option {{$establishment->plan == 'test' ? 'selected' : ''}} value="test">
                                Teste
                              </option>
                              <option {{$establishment->plan == 'master' ? 'selected' : ''}} value="master">
                                Produção
                              </option>
                            </select>
                        </div>
                        <div class="form-group col-sm-4">
                          <label>Utiliza integração?</label>
                          <select {{ $isPartnership ? 'readonly' : '' }} name="enable_advanced_functions" class="form-control">
                            <option {{($establishment->config) && !$establishment->config->enable_advanced_functions ? 'selected' : ''}} value="0">
                              Sim (Oculta funções manuais)
                            </option>
                            <option {{($establishment->config) && $establishment->config->enable_advanced_functions ? 'selected' : ''}} value="1">
                              Não (Exibe funções manuais)
                            </option>
                          </select>
                        </div>
                        <div class="form-group col-sm-4">
                          <label>Remover controle de geolocalização?</label>
                          <select {{ $isPartnership ? 'readonly' : '' }} name="disable_location" class="form-control">
                            <option {{($establishment->config) && $establishment->config->disable_location ? 'selected' : ''}} value="1">
                              Sim (Estabelecimento será exibido para todos)
                            </option>
                            <option {{($establishment->config) && !$establishment->config->disable_location ? 'selected' : ''}} value="0">
                              Não (Visível apenas para usuários próximos)
                            </option>
                          </select>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Possui função: Abertura de Comanda (Pós-Pago)?</label>
                            <select {{ $isPartnership ? 'readonly' : '' }} name="allow_bills" class="form-control">
                              <option {{($establishment->config) && $establishment->config->allow_bills ? 'selected' : ''}} value="1">
                                Sim
                              </option>
                              <option {{($establishment->config) && !$establishment->config->allow_bills ? 'selected' : ''}} value="0">
                                Não
                              </option>
                            </select>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Possui função: Pedir e Pagar (Pré-Pago)?</label>
                            <select {{ $isPartnership ? 'readonly' : '' }} name="allow_pre_bills" class="form-control">
                              <option {{($establishment->config) && $establishment->config->allow_pre_bills ? 'selected' : ''}} value="1">
                                Sim
                              </option>
                              <option {{($establishment->config) && !$establishment->config->allow_pre_bills ? 'selected' : ''}} value="0">
                                Não
                              </option>
                            </select>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Ocultar Estabelecimento?</label>
                            <select {{ $isPartnership ? 'readonly' : '' }} name="display_location" class="form-control">
                              <option {{($establishment->config) && !$establishment->config->display_location ? 'selected' : ''}} value="0">
                                Sim (Oculta a visualização do Estabelecimento)
                              </option>
                              <option {{($establishment->config) && $establishment->config->display_location ? 'selected' : ''}} value="1">
                                Não (Libera a visualização do Estabelecimento)
                              </option>
                            </select>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Habilitar funções de Pedido (Pré-Pago)?</label>
                            <select {{ $isPartnership ? 'readonly' : '' }} name="disable_orders" class="form-control">
                              <option {{($establishment->config) && $establishment->config->disable_orders ? 'selected' : ''}} value="1">
                                Não (Apenas visualização de cardápio)
                              </option>
                              <option {{($establishment->config) && !$establishment->config->disable_orders ? 'selected' : ''}} value="0">
                                Sim (É necessário ter uma conta de pagamento)
                              </option>
                            </select>
                        </div>

                        @if(!$isPartnership)
                        <div class="form-group col-sm-12">
                            <hr/>
                            <button type="submit" class="btn btn-success pull-right">Salvar</button>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
@endsection

@push('scripts')
<script type="text/javascript">
$('.mask-cnpj').mask('99.999.999/9999-99');
$('.mask-phone').mask('(99) 99999-9999');
$('.mask-date').mask('99/99/9999');
</script>
@endpush
