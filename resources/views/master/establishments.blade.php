@extends('layouts.master')

@section('content')
  @php
    $isPartnership = Auth::guard('master')->user()->partnership();
  @endphp
        <h2>Estabelecimentos</h2>

            <div class="container-fluid">
                @if(!$isPartnership)
                <div class="row">
                    @include('master.includes.return-message')
                    <a class="btn btn-success" href="{{ url('/establishments/create') }}" role="button">
                        <i class="fa fa-plus-circle"></i> Cadastrar Estabelecimento</a>
                </div>
                @endif
                <div class="row">
                    <table id="establishmentsTable" class="dataTable table table-striped">
                        <thead>
                        <tr>
                            <th>Razão Social</th>
                            <th>Nome Fantasia</th>
                            <th>CNPJ</th>
                            <th>E-mail</th>
                            <th>Status</th>
                            <th>Data de Faturamento</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($establishments as $e)
                            <tr>
                                <td>{{ $e->fantasy_name }}</td>
                                <td><b>{{ $e->name }}</b></td>
                                <td>{{ $e->cnpj }}</td>
                                <td>{{ $e->email }}</td>
                                <td>{{ \App\Models\Establishment::$statusList[$e->status] }}</td>
                                <td>
                                  {{ ($e->payment_date) ? $e->payment_date->format('d/m/Y') : null }}
                                </td>
                                <td>
                                    <div class="row">
                                      <a class="btn btn-primary pull-left has-popover"
                                           data-content="Ver informações"
                                           href="{{ url('/establishments/' . $e->id) }}">
                                            <i class="fa fa-search"></i>
                                      </a>
                                      <a target="_blank" class="btn btn-default pull-right has-popover"
                                         data-content="Logar em '{{ $e->name }}'"
                                         href="{{ url('/establishments/' . $e->id . '/login') }}">
                                          <i class="fa fa-sign-in"></i>
                                      </a>
                                    </div>

                                    <div class="row">
                                      @if(!$isPartnership)
                                      <a class="btn btn-default pull-left has-popover"
                                         data-content="Editar '{{ $e->name }}'"
                                         href="{{ url('/establishments/' . $e->id . '/edit') }}">
                                          <i class="fa fa-pencil"></i>
                                      </a>
                                      @endif

                                      <form class="hidden-xs" action="{{ url('/establishments/' . $e->id . '/disable') }}"
                                            method="GET">
                                          {{ csrf_field() }}

                                          <?php if($e->status == 'enabled'){ ?>
                                          <button type="submit" class="btn btn-danger pull-right has-popover"
                                                  data-content="Desativar '{{ $e->name }}'">
                                              <i class="fa fa-trash"></i>
                                          </button>
                                          <?php }else{ ?>
                                          <button type="submit" class="btn btn-success pull-right has-popover"
                                                  data-content="Ativar '{{ $e->name }}'">
                                              <i class="fa fa-reply"></i>
                                          </button>
                                          <?php } ?>
                                      </form>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready( function () {
    $('#establishmentsTable').DataTable({
      language: {
        search: "Faça uma busca:",
        paginate: {
            first:      "Prumeiro",
            previous:   "Anterior",
            next:       "Próximo",
            last:       "Último"
        },
        lengthMenu:    "Exibindo _MENU_ resultados",
        info: "Exibindo _START_ até _END_ de _TOTAL_ resultados",
      }
    });
} );
</script>
@endpush
