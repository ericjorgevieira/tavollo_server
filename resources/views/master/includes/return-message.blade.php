@if (count($errors) > 0)
    <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (session()->has('error'))
    <div class="alert alert-danger fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
        @foreach (session('error') as $warning)
            <li>{{ $warning }}</li>
        @endforeach
        </ul>
    </div>
@endif
@if (session()->has('warning'))
    <div class="alert alert-warning fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
        @foreach (session('warning') as $warning)
            <li>{{ $warning }}</li>
        @endforeach
        </ul>
    </div>
@endif
@if (session()->has('success'))
    <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
        @foreach (session('success') as $success)
            <li>{{ $success }}</li>
        @endforeach
        </ul>
    </div>
@endif
