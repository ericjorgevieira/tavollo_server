<?php
$route = explode('/', Route::current()->uri());
$route = $route[0];
?>
<ul class="nav nav-sidebar">
    <li class="<?= ($route == 'home') ? 'active' : '' ?>">
        <a href="{{ url('/home') }}">
            Dashboard
            <span class="sr-only">(current)</span>
        </a>
    </li>
    <li class="<?= ($route == 'establishments') ? 'active' : '' ?>">
        <a href="{{ url('/establishments') }}">
            Estabelecimentos
        </a>
    </li>
    <li class="<?= ($route == 'users') ? 'active' : '' ?>">
        <a href="{{ url('/users') }}">
            Usuários
        </a>
    </li>

</ul>
