@php
$route = explode('/', Route::current()->uri());
$route = $route[0];
$user = Auth::guard('master')->user();
@endphp
<nav id="navbar" class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">

          <button type="button" class="navbar-toggle left collapsed" data-toggle="collapse"
          data-target="#app-navbar-collapse">
          <span class="sr-only">Toggle Navigation</span>
          <span class="icon"><i class="fa fa-bars"></i></span>
        </button>
            <!-- Branding Image -->
            <a class="navbar-brand logo" href="{{ url('/') }}">
                <img src="/images/logo-2.png"/>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="app-navbar-collapse">

            <ul class="nav navbar-nav">
                <li>
                    <a class="<?= ($route == 'home') ? 'active' : '' ?>" href="{{ url('/') }}">Inicial</a>
                </li>
                @if($user->profile == \App\Models\User::PROFILE_MANAGER)
                <li>
                    <a class="<?= ($route == 'ratings') ? 'active' : '' ?>" href="{{ url('/ratings') }}">Avaliações</a>
                </li>
                <li>
                    <a class="<?= ($route == 'marketplaces') ? 'active' : '' ?>" href="{{ url('/marketplaces') }}">Marketplaces</a>
                </li>
                <li>
                    <a class="<?= ($route == 'partnerships') ? 'active' : '' ?>" href="{{ url('/partnerships') }}">Representações</a>
                </li>
                @endif
                <li>
                    <a class="<?= ($route == 'establishments') ? 'active' : '' ?>" href="{{ url('/establishments') }}">Estabelecimentos</a>
                </li>
                @if($user->profile == \App\Models\User::PROFILE_MANAGER)
                <li>
                    <a class="<?= ($route == 'bills') ? 'active' : '' ?>" href="{{ url('/bills') }}">Contas</a>
                </li>
                <li>
                    <a class="<?= ($route == 'users') ? 'active' : '' ?>" href="{{ url('/users') }}">Usuários</a>
                </li>
                {{-- <li>
                    <a class="{{ ($route == 'integrations') ? 'active' : '' }}" href="{{ url('/integrations') }}">Integrações</a>
                </li> --}}
                <li>
                    <a class="<?= ($route == 'integrations') ? 'active' : '' ?>" target="_blank" href="{{ url('/log-viewer') }}">Logs</a>
                </li>
                @endif
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::guard('master')->user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            @if($user->partnership())
                            <li><a href="{{ url('/profile/partnership') }}"><i class="fa fa-btn fa-money"></i>Representação</a></li>
                            @endif
                            <li><a href="{{ url('/profile') }}"><i class="fa fa-btn fa-user"></i>Editar Perfil</a></li>
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                        </ul>
                    </li>
            </ul>
        </div>
    </div>
</nav>
