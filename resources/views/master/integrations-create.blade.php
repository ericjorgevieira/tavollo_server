@extends('layouts.master')

@section('content')
        <h2>Cadastro de Integração</h2>

            <div class="container-fluid">
                <div class="row">
                    <a class="btn btn-primary" href="{{ url('/integrations') }}" role="button">
                        <span class="fa fa-arrow-left" aria-hidden="true"></span>Voltar
                    </a>
                </div>
                <div class="row">
                    @include('master.includes.return-message')
                </div>
                <div class="row">
                    <form method="post" action="{{ url('/integrations/store') }}">
                        {{ csrf_field() }}
                        {{ method_field('POST') }}
                        <div class="form-group col-sm-6">
                            <label>Nome *</label>
                            <input required class="form-control" type="text" name="name" value=""/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>E-mail</label>
                            <input class="form-control" type="text" name="email" value=""/>
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Descrição</label>
                            <textarea class="form-control" name="description"></textarea>
                        </div>
                        <hr/>
                        <div class="form-group col-sm-6">
                            <label>Empresa *</label>
                            <input required class="form-control" type="text" name="company" value=""/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Telefone</label>
                            <input class="form-control mask-phone" type="text" name="phone" value=""/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Cidade</label>
                            <input class="form-control" type="text" name="city" value=""/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Estado</label>
                            <select name="state" class="form-control selectpicker"
                                    data-live-search="true" id="selectCategoria" title="Selecione.." data-size="10">
                                @foreach($estados as $sigla => $e)
                                    <option value="{{ $sigla }}">{{ $e }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <hr/>
                            <button type="submit" class="btn btn-success pull-right">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
@endsection
