@extends('layouts.master')

@section('content')
        <h2>Edição de Integração</h2>

        <div class="container-fluid">
                <div class="row">
                        <a class="btn btn-primary" href="{{ url('/integrations') }}" role="button">
                                <span class="fa fa-arrow-left" aria-hidden="true"></span>Voltar
                        </a>
                </div>
                <div class="row">
                        @include('master.includes.return-message')
                </div>
                <div class="row">
                        <form method="post" action="{{ url('/integrations/' . $integration->id) }}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="form-group col-sm-6">
                                        <label>Nome *</label>
                                        <input required class="form-control" type="text" name="name" value="{{ $integration->name }}"/>
                                </div>
                                <div class="form-group col-sm-6">
                                        <label>E-mail</label>
                                        <input class="form-control" type="text" name="email" value="{{ $integration->email }}"/>
                                </div>
                                <div class="form-group col-sm-12">
                                        <label>Descrição</label>
                                        <textarea class="form-control" name="description">{{ $integration->description }}</textarea>
                                </div>
                                <hr/>
                                <div class="form-group col-sm-6">
                                        <label>Empresa *</label>
                                        <input required class="form-control" type="text" name="company" value="{{ $integration->company }}"/>
                                </div>
                                <div class="form-group col-sm-6">
                                        <label>Telefone</label>
                                        <input class="form-control mask-phone" type="text" name="phone" value="{{ $integration->phone }}"/>
                                </div>
                                <div class="form-group col-sm-6">
                                        <label>Cidade</label>
                                        <input class="form-control" type="text" name="city" value="{{ $integration->city }}"/>
                                </div>
                                <div class="form-group col-sm-6">
                                        <label>Estado</label>
                                        <select name="state" class="form-control selectpicker"
                                        data-live-search="true" id="selectCategoria" title="Selecione.." data-size="10">
                                        @foreach($estados as $sigla => $e)
                                                <option <?= ($integration->state == $sigla) ? 'selected' : '' ?> value="{{ $sigla }}">{{ $e }}</option>
                                        @endforeach
                                </select>
                        </div>
                        <hr/>
                        <div class="form-group col-sm-12">
                                <button type="submit" class="btn btn-success pull-right">Salvar</button>
                        </div>
                </form>
        </div>
</div>
@endsection
