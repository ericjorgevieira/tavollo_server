@extends('layouts.master')

@section('content')
        <h2>Integrações</h2>

            <div class="container">
                <div class="row">
                    @include('master.includes.return-message')
                    <a class="btn btn-success" href="{{ url('/integrations/create') }}" role="button">
                        <i class="fa fa-plus-circle"></i> Cadastrar Integração</a>
                </div>
                <div class="row">
                    <table id="establishmentsTable" class="dataTable table table-striped">
                        <thead>
                        <tr>
                            <th>Data de Cadastro</th>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Empresa</th>
                            <th>Status</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($integrations as $e)
                            <tr>
                                <td>{{ $e->created_at }}</td>
                                <td><b>{{ $e->name }}</b></td>
                                <td>{{ $e->email }}</td>
                                <td>{{ $e->company }}</td>
                                <td>{{ \App\Models\Integration::$statusList[$e->status] }}</td>
                                <td>
                                    <a class="btn btn-default pull-left has-popover"
                                       data-content="Editar '{{ $e->name }}'"
                                       href="{{ url('/integrations/' . $e->id . '/edit') }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                    <form class="hidden-xs" action="{{ url('/integrations/' . $e->id . '/enable') }}"
                                          method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}

                                        <?php if($e->status == 'enabled'){ ?>
                                        <button type="submit" class="btn btn-danger pull-left has-popover"
                                                data-content="Desativar '{{ $e->name }}'">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        <?php }else{ ?>
                                        <button type="submit" class="btn btn-success pull-left has-popover"
                                                data-content="Ativar '{{ $e->name }}'">
                                            <i class="fa fa-reply"></i>
                                        </button>
                                        <?php } ?>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
@endsection
