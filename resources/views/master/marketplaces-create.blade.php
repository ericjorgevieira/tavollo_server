@extends('layouts.master')

@section('content')

            <div class="container">
                <div class="row">
                  <div class="col-sm-12">
                    <h2>Cadastro de Marketplace</h2>
                    <a href="{{ url('/marketplaces') }}">
                        <span class="fa fa-arrow-left" aria-hidden="true"></span> Voltar
                    </a>
                  </div>

                </div>
                <div class="row">
                    @include('master.includes.return-message')
                </div>
                <div class="row">
                    <form method="post" action="{{ url('/marketplaces/store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('POST') }}
                        <div class="form-group col-sm-12">
                            <label>Nome *</label>
                            <input required class="form-control" type="text" name="name" value=""/>
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Descrição</label>
                            <textarea class="form-control" name="description"></textarea>
                        </div>

                        <hr/>
                        <div class="form-group col-sm-4">
                            <label>Logo (668x400)</label>
                            <input class="form-control" type="file" name="logo"/>
                        </div>
                        <div class="form-group col-sm-4">
                            <label>Imagem de Divulgação (1368x720)</label>
                            <input class="form-control" type="file" name="image"/>
                        </div>
                        <div class="form-group col-sm-4">
                            <label>Imagem do background do Login(600x1212)</label>
                            <input class="form-control" type="file" name="background"/>
                        </div>

                        <hr/>

                        <div class="form-group col-sm-6">
                            <label>Status</label>
                            <select name="status" class="form-control">
                              <option value="enabled">
                                Habilitado
                              </option>
                              <option value="disabled">
                                Desabilitado
                              </option>
                            </select>
                        </div>

                        <div class="form-group col-sm-12">
                            <hr/>
                            <button type="submit" class="btn btn-success pull-right">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
@endsection
