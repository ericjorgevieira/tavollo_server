@extends('layouts.master')

@section('content')
@php($marketplaceUrl = \Config::get('app.env') == 'homologation' ? 'testapp': 'app')
            <div class="container">
                <div class="row">
                  <div class="col-sm-12">
                    <h2>Edição de Marketplace</h2>
                    <a href="{{ url('/marketplaces/' . $marketplace->id) }}">
                        <span class="fa fa-arrow-left" aria-hidden="true"></span> Voltar
                    </a>
                  </div>

                </div>
                <div class="row">
                    @include('master.includes.return-message')
                </div>
                <div class="row">
                    <form method="post" action="{{ url('/marketplaces/' . $marketplace->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group col-sm-12">
                            <label>Nome *</label>
                            <input required class="form-control" type="text" name="name" value="{{ $marketplace->name }}"/>
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Slug (url)</label>
                            <input readonly class="form-control" type="text" name="slug" value="https://{{$marketplaceUrl}}.tavollo.com/#/home/marketplace/{{ $marketplace->slug }}"/>
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Descrição</label>
                            <textarea class="form-control" name="description">{{ $marketplace->description }}</textarea>
                        </div>

                        <hr/>
                        <div class="form-group col-sm-4">
                            <label>Logo (668x400)</label>
                            <input class="form-control" type="file" name="logo"/>
                            @if($marketplace->logo)
                              <img src="{{ $marketplace->logo }}" width="200" style="margin-top: 20px;" />
                            @endif
                        </div>
                        <div class="form-group col-sm-4">
                            <label>Imagem de Divulgação (1368x720)</label>
                            <input class="form-control" type="file" name="image"/>
                            @if($marketplace->image)
                              <img src="{{ $marketplace->image }}" width="100%" style="margin-top: 20px;" />
                            @endif
                        </div>
                        <div class="form-group col-sm-4">
                            <label>Imagem do background do Login(600x1212)</label>
                            <input class="form-control" type="file" name="background"/>
                            @if($marketplace->background)
                              <img src="{{ $marketplace->background }}" width="100%" style="margin-top: 20px;" />
                            @endif
                        </div>

                        <div class="col-md-12">
                          <hr/>
                        </div>
                        @php($theme = $marketplace->getThemeConfig())
                        <div class="form-group col-sm-3">
                            <label>Cor Primária</label>
                            <input required class="form-control" type="color" name="primary_color" value="{{ (isset($theme['primary_color'])) ? $theme['primary_color'] : '#3686FF' }}"/>
                        </div>

                        <div class="form-group col-sm-3">
                            <label>Cor Secundária</label>
                            <input required class="form-control" type="color" name="secondary_color" value="{{ (isset($theme['secondary_color'])) ? $theme['secondary_color'] : '#237BFF' }}"/>
                        </div>

                        <div class="form-group col-sm-3">
                            <label>Cor de Título</label>
                            <input required class="form-control" type="color" name="title_color" value="{{ (isset($theme['title_color'])) ? $theme['title_color'] : '#ffffff' }}"/>
                        </div>

                        <div class="form-group col-sm-3">
                            <label>Cor de Texto</label>
                            <input required class="form-control" type="color" name="text_color" value="{{ (isset($theme['text_color'])) ? $theme['text_color'] : '#646464' }}"/>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Status</label>
                            <select name="status" class="form-control">
                              <option value="enabled">
                                Habilitado
                              </option>
                              <option value="disabled">
                                Desabilitado
                              </option>
                            </select>
                        </div>

                        <div class="row">
                          <div class="col-md-12">
                            <h3>Estabelecimentos</h3>
                            <select class="form-control multi-select" name="establishments[]" multiple="multiple">
                              @foreach($establishments as $e)
                                <option {{ (in_array($e->id, $marketplaceEstablishments)) ? 'selected' : '' }}
                                  value="{{ $e->id }}">{{ $e->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="form-group col-sm-12">
                            <hr/>
                            <button type="submit" class="btn btn-success pull-right">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
@endsection


@push('scripts')
<script type="text/javascript">
$(".multi-select").multiSelect({selectableOptgroup: true});
</script>

<style>
.ms-container{
  width: 100% !important;
}
</style>
@endpush
