@extends('layouts.master')

@section('content')
        <h2>Marketplaces</h2>

            <div class="container-fluid">
                <div class="row">
                    @include('master.includes.return-message')
                    <a class="btn btn-success" href="{{ url('/marketplaces/create') }}" role="button">
                        <i class="fa fa-plus-circle"></i> Cadastrar Marketplace</a>
                </div>
                <div class="row">
                    <table id="establishmentsTable" class="dataTable table table-striped">
                        <thead>
                        <tr>
                            <th>Logo</th>
                            <th>Nome</th>
                            <th>Status</th>
                            <th>

                            </th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($marketplaces as $e)
                            <tr>
                                <td>
                                  <img src="{{ $e->logo }}" width="100"  />
                                </td>
                                <td><b>{{ $e->name }}</b></td>
                                <td>{{ \App\Models\Establishment::$statusList[$e->status] }}</td>
                                <td>

                                </td>
                                <td>
                                    <a class="btn btn-default pull-left has-popover"
                                       data-content="Editar '{{ $e->name }}'"
                                       href="{{ url('/marketplaces/' . $e->id . '/edit') }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                    <form class="hidden-xs" action="{{ url('/marketplaces/' . $e->id . '/enable') }}"
                                          method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}

                                        <?php if($e->status == 'enabled'){ ?>
                                        <button type="submit" class="btn btn-danger pull-left has-popover"
                                                data-content="Desativar '{{ $e->name }}'">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        <?php }else{ ?>
                                        <button type="submit" class="btn btn-success pull-left has-popover"
                                                data-content="Ativar '{{ $e->name }}'">
                                            <i class="fa fa-reply"></i>
                                        </button>
                                        <?php } ?>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
@endsection
