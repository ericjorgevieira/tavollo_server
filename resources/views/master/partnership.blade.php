@extends('layouts.master')
@section('content')
  <div class="container">
    <div class="row">
      <div id="establishmentInfo">
        <div class="row col-md-12 text-center">
          <h2><b>{{ $establishment->name }}</b>
            <br />
            <a href="{{ url('/establishments/' . $establishment->id . '/edit') }}"
              class="btn btn-success has-popover" data-content="Editar Estabelecimento">
              <i class="fa fa-pencil"></i> Editar
            </a>
          </h2>
        </div>
        <div class="row col-md-4">
          <span>Nome fantasia: {{ $establishment->fantasy_name }}</span><br/>
          <span>E-mail: {{ $establishment->email }}</span><br/>
          <span>CNPJ: {{ $establishment->cnpj }}</span><br/>
          <span>Criado em: {{ $establishment->created_at }}</span>
        </div>
        <div class="row col-md-4">
          <span>Telefone: {{ $establishment->phone }}</span><br/>
          <span>Celular: {{ $establishment->cel }}</span>
        </div>
        <div class="row col-md-4">
          <span>Endereço: {{ $establishment->address }}</span><br/>
          <span>Bairro: {{ $establishment->district }}</span><br/>
          <span>Cidade: {{ $establishment->city }}</span><br/>
          <span>Estado: {{ $establishment->state }}</span><br/>
          <span>CEP: {{ $establishment->postal_code }}</span>
        </div>
      </div>
    </div>
    <div class="row">
      <h3>Dados</h3>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua">
            <i class="fa fa-beer"></i>
          </span>
          <div class="info-box-content">
            <span class="info-box-text">
              Produtos
            </span>
            <span class="info-box-number">
              {{ count($products) }}
            </span>
          </div>
        </div>
      </div>

      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua">
            <i class="fa fa-users"></i>
          </span>
          <div class="info-box-content">
            <span class="info-box-text">
              Contas
            </span>
            <span class="info-box-number">
              {{ count($bills) }}
            </span>
          </div>
        </div>
      </div>

      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua">
            <i class="fa fa-money"></i>
          </span>
          <div class="info-box-content">
            <span class="info-box-text">
              Vendas
            </span>
            <span class="info-box-number">
              R$ {{ $sales }}
            </span>
          </div>
        </div>
      </div>

      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua">
            <i class="fa fa-line-chart"></i>
          </span>
          <div class="info-box-content">
            <span class="info-box-text">
              Comparação<br> com mês anterior
            </span>
            <span class="info-box-number">
              -
            </span>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <h3>Ações</h3>
      <div class="col-xs-12">
        <a class="btn btn-default" href="{{ url('/establishments/' . $establishment->id . '/generate-tokens') }}">
          <i class="fa fa-refresh"></i> Gerar novos Tokens
        </a>
        <a class="btn btn-warning" href="{{ url('/establishments/' . $establishment->id . '/clear') }}">
          <i class="fa fa-trash"></i> Limpar Registro de Contas
        </a>
        <a class="btn btn-warning" href="{{ url('/establishments/' . $establishment->id . '/clear-products') }}">
          <i class="fa fa-trash"></i> Limpar Produtos e Cardápios
        </a>
        <a class="btn btn-warning" href="{{ url('/establishments/' . $establishment->id . '/clear-tables') }}">
          <i class="fa fa-trash"></i> Limpar Mesas e Cartões
        </a>
        @if($establishment->status == \App\Models\Establishment::STATUS_DISABLED)
          <a class="btn btn-success" href="{{ url('/establishments/' . $establishment->id . '/disable') }}">
            <i class="fa fa-check"></i> Ativar Estabelecimento
          </a>
        @else
          <a class="btn btn-danger" href="{{ url('/establishments/' . $establishment->id . '/disable') }}">
            <i class="fa fa-trash"></i> Desativar Estabelecimento
          </a>
        @endif
      </div>
      {{-- <div class="col-xs-3">
      <button class="btn btn-default">Apagar Produtos</button>
    </div> --}}
  </div>
  <div class="row">
    <h3>Usuários</h3>
    @include('master.includes.return-message')
    <a class="btn btn-success"
    href="{{ url('/establishments/' . $establishment->id . '/users/create') }}" role="button">
    <i class="fa fa-plus-circle"></i> Cadastrar Usuário
  </a>
  <table id="establishmentsTable" class="dataTable table table-striped">
    <thead>
      <tr>
        <th>Data de Cadastro</th>
        <th>Nome</th>
        <th>E-mail</th>
        <th>Status</th>
        <th>Ações</th>
      </tr>
    </thead>
    <tbody>
      @foreach($users as $u)
        <tr>
          <td>{{ $u->created_at }}</td>
          <td>{{ $u->name }}</td>
          <td>{{ $u->email }}</td>
          <td>{{ \App\Models\User::$statusList[$u->status] }}</td>
          <td>
            <a class="btn btn-default pull-left has-popover"
            data-content="Editar '{{ $u->name }}'"
            href="{{ url('/establishments/' . $establishment->id . '/users/' . $u->id . '/edit') }}">
            <i class="fa fa-pencil"></i>
          </a>

          <form class="hidden-xs" action="{{ url('/establishments/' . $establishment->id . '/users/' . $u->id) }}"
            method="POST">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}

              <button type="submit" class="btn btn-danger pull-left has-popover"
                data-content="Excluir '{{ $u->name }}'">
                <i class="fa fa-trash"></i>
              </button>
          </form>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
</div>
</div>
@endsection
