@extends('layouts.master')

@section('content')

            <div class="container">
                <div class="row">
                  <div class="col-sm-12">
                    <h2>Cadastro de Parceiro</h2>
                    <a href="{{ url('/partnerships') }}">
                        <span class="fa fa-arrow-left" aria-hidden="true"></span> Voltar
                    </a>
                  </div>

                </div>
                <div class="row">
                    @include('master.includes.return-message')
                </div>
                <div class="row">
                    <form id="formPartner" method="post" action="{{ url('/partnerships/store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('POST') }}
                        <div class="form-group col-sm-12">
                            <label>Nome do Revendedor *</label>
                            <input required class="form-control" type="text" name="name" value=""/>
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Nome da Revenda</label>
                            <input class="form-control" type="text" name="company_name" value=""/>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Documento (CNPJ)</label>
                            <input class="form-control mask-cnpj" type="text" name="document" value=""/>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Telefone</label>
                            <input class="form-control mask-phone" type="text" name="phone" value=""/>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Cidade</label>
                            <input class="form-control" type="text" name="city" value=""/>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Estado (UF)</label>
                            <select name="state" class="form-control selectpicker"
                                    data-live-search="true" id="selectCategoria" title="Selecione.." data-size="10">
                                @foreach($estados as $sigla => $e)
                                    <option value="{{ $sigla }}">{{ $e }}</option>
                                @endforeach
                            </select>
                        </div>

                        <hr />

                        <div class="form-group col-sm-12">
                            <label>E-mail *</label>
                            <input required class="form-control" type="email" name="email" value=""/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Senha *</label>
                            <input required class="form-control" type="password" name="password" value=""/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Confirmar Senha *</label>
                            <input required class="form-control" type="password" name="password_confirmation" value=""/>
                        </div>

                        <hr/>

                        <div class="form-group col-sm-6">
                            <label>Status</label>
                            <select name="status" class="form-control">
                              <option value="enabled">
                                Habilitado
                              </option>
                              <option value="disabled">
                                Desabilitado
                              </option>
                            </select>
                        </div>

                        <div class="form-group col-sm-12">
                            <hr/>
                            <button type="submit" class="btn btn-success pull-right">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
@endsection

@push('scripts')
<script type="text/javascript">
$('.mask-cnpj').mask('99.999.999/9999-99');
$('.mask-phone').mask('(99) 99999-9999');

$('#formPartner').on('submit', function(e){
  e.preventDefault();
  var $this = $(this);
  var r = confirm('Deseja salvar o Parceiro? O e-mail cadastrado não poderá ser alterado.');
  if(r === true){
    e.currentTarget.submit();
  }
})
</script>
@endpush
