@extends('layouts.master')

@section('content')
@php($partnershipUrl = \Config::get('app.env') == 'homologation' ? 'test.admin': 'admin')
            <div class="container">
                <div class="row">
                  <div class="col-sm-12">
                    <h2>Edição de Parceiro</h2>
                    <a href="{{ url('/partnerships') }}">
                        <span class="fa fa-arrow-left" aria-hidden="true"></span> Voltar
                    </a>
                  </div>

                </div>
                <div class="row">
                    @include('master.includes.return-message')
                </div>
                <div class="row">
                    <form method="post" action="{{ url('/partnerships/' . $partnership->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group col-sm-12">
                            <label>Nome do Revendedor *</label>
                            <input required class="form-control" type="text" name="name" value="{{ $partnership->name }}"/>
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Nome da Revenda</label>
                            <input class="form-control" type="text" name="company_name" value="{{ $partnership->company_name }}"/>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Documento (CNPJ)</label>
                            <input class="form-control mask-cnpj" type="text" name="document" value="{{ $partnership->document }}"/>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Telefone</label>
                            <input class="form-control mask-phone" type="text" name="phone" value="{{ $partnership->phone }}"/>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Cidade</label>
                            <input class="form-control" type="text" name="city" value="{{ $partnership->city }}"/>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Estado (UF)</label>
                            <select name="state" class="form-control selectpicker"
                                    data-live-search="true" id="selectCategoria" title="Selecione.." data-size="10">
                                @foreach($estados as $sigla => $e)
                                    <option {{ $partnership->uf == $sigla ? 'selected' : '' }} value="{{ $sigla }}">{{ $e }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-sm-12">
                            <label>E-mail</label>
                            <input readonly class="form-control" type="email" name="email" value="{{ $partnership->user->email }}"/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Senha</label>
                            <input class="form-control" type="password" name="password" value=""/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Confirmar Senha</label>
                            <input class="form-control" type="password" name="password_confirmation" value=""/>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Status</label>
                            <select name="status" class="form-control">
                              <option {{ $partnership->status == 'enabled' ? 'selected' : '' }} value="enabled">
                                Habilitado
                              </option>
                              <option {{ $partnership->status == 'disabled' ? 'selected' : '' }} value="disabled">
                                Desabilitado
                              </option>
                            </select>
                        </div>

                        <hr/>

                        <div class="form-group col-sm-12">
                            <label>URL de Venda</label>
                            <input readonly class="form-control" type="text" name="slug" value="https://{{$partnershipUrl}}.tavollo.com/r/{{ $partnership->user->email }}"/>
                        </div>

                        <div class="row">
                          <div class="col-md-12">
                            <h3>Estabelecimentos</h3>
                            <select class="form-control multi-select" name="establishments[]" multiple="multiple">
                              @foreach($establishments as $e)
                                <option {{ (in_array($e->id, $partnershipEstablishments)) ? 'selected' : '' }}
                                  value="{{ $e->id }}">{{ $e->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="form-group col-sm-12">
                            <hr/>
                            <button type="submit" class="btn btn-success pull-right">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
@endsection


@push('scripts')
<script type="text/javascript">
$(".multi-select").multiSelect({selectableOptgroup: true});
$('.mask-cnpj').mask('99.999.999/9999-99');
$('.mask-phone').mask('(99) 99999-9999');
</script>

<style>
.ms-container{
  width: 100% !important;
}
</style>
@endpush
