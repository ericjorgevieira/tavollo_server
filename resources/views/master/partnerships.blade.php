@extends('layouts.master')

@section('content')
        <h2>Representações</h2>

            <div class="container-fluid">
                <div class="row">
                    @include('master.includes.return-message')
                    <a class="btn btn-success" href="{{ url('/partnerships/create') }}" role="button">
                        <i class="fa fa-plus-circle"></i> Cadastrar Parceiro</a>
                </div>
                <div class="row">
                    <table id="establishmentsTable" class="dataTable table table-striped">
                        <thead>
                        <tr>
                            <th>Data de Cadastro</th>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Status</th>
                            {{-- <th>Info</th> --}}
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($partnerships as $e)
                            <tr>
                                <td>{{ $e->created_at }}</td>
                                <td><b>{{ $e->name }}</b></td>
                                <td>{{ $e->user->email }}</td>
                                <td>{{ \App\Models\Partnership::$statusList[$e->status] }}</td>
                                {{-- <td>
                                  <a class="btn btn-primary has-popover"
                                       data-content="Ver informações"
                                       href="{{ url('/partnerships/' . $e->id) }}">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td> --}}
                                <td>
                                    <a class="btn btn-default pull-left has-popover"
                                       data-content="Editar '{{ $e->name }}'"
                                       href="{{ url('/partnerships/' . $e->id . '/edit') }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>


                                    <form class="hidden-xs" action="{{ url('/partnerships/' . $e->id . '/disable') }}"
                                          method="GET">
                                        {{ csrf_field() }}

                                        <?php if($e->status == 'enabled'){ ?>
                                        <button type="submit" class="btn btn-danger pull-left has-popover"
                                                data-content="Desativar '{{ $e->name }}'">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        <?php }else{ ?>
                                        <button type="submit" class="btn btn-success pull-left has-popover"
                                                data-content="Ativar '{{ $e->name }}'">
                                            <i class="fa fa-reply"></i>
                                        </button>
                                        <?php } ?>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
@endsection
