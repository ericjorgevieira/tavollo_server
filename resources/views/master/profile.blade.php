@php
$user = Auth::guard('master')->user();    
@endphp
@extends('layouts.master')

@section('content')
    <div id="profile">
        <div class="panel panel-default">
            <div class="panel-heading">Dados de Perfil</div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4>Menu de Perfil</h4>
                            </div>

                            <div class="panel-body">
                                <ul class="nav nav-pills nav-stacked">
                                    @if($user->partnership())
                                    <li role="presentation" <?= ($action == 'partnership') ? 'class="active"' : '' ?> >
                                        <a href="{{ url('/profile/partnership') }}"><i class="fa fa-money"></i> Representação</a>
                                    </li>
                                    @endif
                                    <li role="presentation" <?= ($action == 'user') ? 'class="active"' : '' ?> >
                                        <a href="{{ url('/profile') }}"><i class="fa fa-user"></i> Usuário</a>
                                    </li>
                                    <li role="presentation" <?= ($action == 'password') ? 'class="active"' : '' ?>>
                                        <a href="{{ url('/profile/password') }}"><i class="fa fa-key"></i> Mudar
                                            Senha</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        @include('master.profile.' . $action)
                    </div>
                </div>
                {{--<div class="row">--}}
                {{--<h3>Configurações</h3>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
@endsection
