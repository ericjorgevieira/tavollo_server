<h4><i class="fa fa-user"></i> Representação</h4>
<h5>Visualize informações relacionadas à sua revenda</h5>
<hr/>
@include('master.includes.return-message')
@php($partnershipUrl = \Config::get('app.env') == 'homologation' ? 'test.admin': 'admin')
<div class="form-group col-sm-12">
    <label>Link de parceiro para cadastro do cliente</label>
    <input readonly class="form-control" type="text" name="slug" value="https://{{$partnershipUrl}}.tavollo.com/r/{{ $partnership->user->email }}"/>
</div>
