<h4><i class="fa fa-key"></i> Mudança de Senha</h4>
<h5>Altere a sua senha</h5>
<hr/>
@include('master.includes.return-message')
<form method="post" action="{{ url('/profile/password/update') }}">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="form-group col-sm-12">
        <label>Sua Senha</label>
        <input required class="form-control" type="password" name="password" value=""/>
    </div>
    <div class="form-group col-sm-6">
        <label>Nova Senha</label>
        <input required class="form-control" type="password" name="new_password" value=""/>
    </div>
    <div class="form-group col-sm-6">
        <label>Confirme a Nova Senha</label>
        <input required class="form-control" type="password" name="new_password_confirmation" value=""/>
    </div>
    <div class="form-group col-sm-12">
        <hr/>
        <button type="submit" class="btn btn-primary">Salvar</button>
    </div>
</form>
