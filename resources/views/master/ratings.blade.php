@extends('layouts.master')

@section('content')
        <h2>Avaliações</h2>

            <div class="container-fluid">
                <div class="row">
                    @include('master.includes.return-message')
                </div>
                <div class="row">
                    <div class="col-sm-6 col-lg-6">
                        <!-- Widget -->
                        <span class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <div class="widget-icon pull-left background-spring animation-fadeIn">
                                    <i class="fa fa-star"></i>
                                </div>
                                <h3 class="widget-content text-right animation-pullDown">
                                    <strong>{{ $ratingAverage }}</strong><br>
                                    <small>Avaliação Média</small>
                                </h3>
                            </div>
                        </span>
                        <!-- END Widget -->
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <!-- Widget -->
                        <span class="widget widget-hover-effect1">
                            <div class="widget-simple">
                                <div class="widget-icon pull-left background-spring animation-fadeIn">
                                    <i class="fa fa-user"></i>
                                </div>
                                <h3 class="widget-content text-right animation-pullDown">
                                    <strong>{{ $ratingTotal }}</strong><br>
                                    <small>Total de Avaliações</small>
                                </h3>
                            </div>
                        </span>
                        <!-- END Widget -->
                    </div>
                </div>
                <div class="row">
                    <table id="establishmentsTable" class="dataTable table table-striped">
                        <thead>
                        <tr>
                            <th>Foto</th>
                            <th>Usuário</th>
                            <th>Nota</th>
                            <th>Comentário</th>
                            <th>Data de Avaliação</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ratings as $e)
                            <tr>
                                <td>
                                    <img style="border-radius: 50%; width: 70px;" src="{{ $e->user->photo }}" />
                                </td>
                                <td>{{ $e->user->name }}</td>
                                <td>{{ $e->rating }}</td>
                                <td>{{ $e->comment }}</td>
                                <td>{{ $e->created_at->format('d/m/Y H:i') }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
@endsection
