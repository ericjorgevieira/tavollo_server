@extends('layouts.master')

@section('content')

            <div class="container">
                <div class="row">
                  <div class="col-sm-12">
                    <h2>Cadastro de Usuários - <b>{{ $establishment->name }}</b></h2>

                    <a href="{{ url('/establishments/' . $establishment->id) }}">
                        <span class="fa fa-arrow-left" aria-hidden="true"></span> Voltar
                    </a>
                  </div>
                </div>
                <div class="row">
                    @include('master.includes.return-message')
                </div>
                <div class="row">
                    <form method="post" action="{{ url('/establishments/' . $establishment->id .'/users/store') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="establishment_id" value="{{ $establishment->id }}"/>
                        <div class="form-group col-sm-6">
                            <label>Nome</label>
                            <input class="form-control" type="text" name="name" value=""/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>E-mail</label>
                            <input class="form-control" type="text" name="email" value=""/>
                        </div>
                        <hr/>
                        <div class="form-group col-sm-6">
                            <label>Nova Senha</label>
                            <input required class="form-control" type="password" name="new_password" value=""/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Confirme a Nova Senha</label>
                            <input required class="form-control" type="password" name="new_password_confirmation" value=""/>
                        </div>
                        <div class="form-group col-sm-12">
                            <hr/>
                            <button type="submit" class="btn btn-success pull-right">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
@endsection
