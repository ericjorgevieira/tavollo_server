@extends('layouts.master')

@section('content')

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                      <h2>Edição de Usuários - <b>{{ $user->establishment->name }}</h2>

                      <a href="{{ url('/establishments/' . $user->establishment_id) }}">
                          <span class="fa fa-arrow-left" aria-hidden="true"></span> Voltar
                      </a>
                    </div>

                </div>
                <div class="row">
                    @include('master.includes.return-message')
                </div>

                <div class="row">
                    <form method="post" action="{{ url('/establishments/' . $user->establishment_id . '/users/' . $user->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group col-sm-6">
                            <label>Nome</label>
                            <input class="form-control" type="text" name="name" value="{{ $user->name }}"/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>E-mail</label>
                            <input class="form-control" type="text" name="email" value="{{ $user->email }}"/>
                        </div>
                        <hr/>
                        <div class="form-group col-sm-6">
                            <label>Nova Senha</label>
                            <input class="form-control" type="password" name="new_password" value=""/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label>Confirme a Nova Senha</label>
                            <input class="form-control" type="password" name="new_password_confirmation" value=""/>
                        </div>
                        <div class="form-group col-sm-12">
                            <hr/>
                            <button type="submit" class="btn btn-success pull-right">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
@endsection
