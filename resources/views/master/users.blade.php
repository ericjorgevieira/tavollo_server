@extends('layouts.master')

@section('content')
        <h2>Usuários</h2>

            <div class="container-fluid">
                <div class="row">
                    @include('master.includes.return-message')
                </div>
                <div class="row">
                    <table id="establishmentsTable" class="dataTable table table-striped">
                        <thead>
                        <tr>
                            <th>Imagem</th>
                            <th>Nome</th>
                            <th>Login</th>
                            <th>Tipo de Conta</th>
                            {{-- <th>Contas Abertas</th> --}}
                            {{-- <th>Total Gasto</th> --}}
                            <th>Última Atualização</th>
                            <th>Data de Cadastro</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $e)
                            <tr>
                                <td>
                                    <img style="border-radius: 50%; width: 70px;" src="{{ $e->photo }}" />
                                </td>
                                <td>{{ $e->name }}</td>
                                <td>{{ $e->email }}</td>
                                <td>{{ ($e->facebook_id) ? 'Facebook' : 'Google' }}</td>
                                {{-- <td><b>{{ count($e->bills) }}</b></td> --}}
                                {{-- <td><b>{{ $e->totalSpent()}}</b></td> --}}
                                <td>{{ $e->updated_at }}</td>
                                <td>{{ $e->created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
@endsection
