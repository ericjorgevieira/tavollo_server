@extends('layouts.plateau')

@section('content')
    <div id="profile">
      <h1>Sobre</h1>

      <div class="row">
          <div class="col-md-12">
              <?php if(Auth::user()->establishment->isTest()) { echo "<h3>Versão de Testes</h3>"; }?>
              <h4><b>Copyright © 2017 Tavollo.</b> Todos os direitos reservados.</h4>
              {{-- <h5><b><a target="_blank" href="/manual_api.pdf">Clique aqui</a> para ver o nosso manual de instruções e documentação da API.</b></h5> --}}
          </div>
      </div>
      <div class="row">
          <div class="col-md-12">
              <h4><b>Media Kit</b></h4>
              <h5><b><a target="_blank" href="/mediakit.zip">Clique aqui</a> para baixar nosso media kit!</b></h5>
          </div>
      </div>
    </div>
@endsection
