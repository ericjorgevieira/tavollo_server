@extends('layouts.plateau')

@section('content')
    <div id="profile">
        <h3>Análise de Dados</h3>

        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-primary">

                    <div class="panel-body">
                      
                        <ul class="nav nav-pills nav-stacked">
                            <li role="presentation" {{ ($action == 'index') ? 'class=active' : '' }}>
                                <a href="{{ url('/analysis') }}"><i class="fa fa-bar-chart"></i> <span>Inicial</span></a>
                            </li>
                            <li role="presentation" {{ ($action == 'ratings' || $action == 'ratings-config') ? 'class=active' : '' }}>
                                <a href="{{ url('/analysis/ratings') }}"><i class="fa fa-star"></i> <span>Avaliações</span></a>
                            </li>
                            <li role="presentation" {{ ($action == 'service') ? 'class=active' : '' }} >
                                <a href="{{ url('/analysis/service') }}"><i class="fa fa-institution"></i> <span>Atendimento</span></a>
                            </li>
                            <li role="presentation" {{ ($action == 'products') ? 'class=active' : '' }} >
                                <a href="{{ url('/analysis/products') }}"><i class="fa fa-shopping-cart"></i> <span>Produtos</span></a>
                            </li>
                            <li role="presentation" {{ ($action == 'users') ? 'class=active' : '' }}>
                                <a href="{{ url('/analysis/users') }}"><i class="fa fa-user"></i> <span>Usuários</span></a>
                            </li>
                        </ul>

                        </div>
                      </div>
                    </div>
                    <div class="col-md-9">
                        @include('plateau.analysis.' . $action)
                    </div>
                </div>
            </div>
        @endsection
