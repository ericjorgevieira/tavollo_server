<h4><i class="fa fa-star"></i> Avaliações</h4>
<span>Visualize aqui todas as avaliações realizadas pelos seus clientes</span>
<hr/>
<div class="container-fluid">
    <div class="row">
        <a href="{{ url('/analysis/ratings') }}" role="button">
            <i class="fa fa-arrow-left"></i> Voltar
        </a>
    </div>
    <div class="row">
        @include('plateau.includes.return-message')

        <div class="straight">

            <form method="post" action="{{ url('/analysis/ratings/config') }}">
                {{ csrf_field() }}

                <h4>No caso de avaliação máxima (5 estrelas)</h4>

                <div class="form-group">
                    <label>O que deverá ser questionado? (*)</label>
                    <input value="{{ ($predefinedAnswers) ? $predefinedAnswers['question_max_rating'] : 'Pode nos dizer o que mais gostou na sua experiência?' }}" 
                        required class="form-control" name="question_max_rating" placeholder="Pode nos dizer o que mais gostou na sua experiência?" />
                </div>

                <div class="form-group">
                    <label>Quais serão as respostas pré-definidas para essa pergunta?</label><br>
                    <input name="answers_max_rating" class="form-control" type="text" data-role="tagsinput" 
                        value="{{ ($predefinedAnswers) ? $predefinedAnswers['answers_max_rating'] : 'A comida, A bebida, O atendimento, O ambiente' }}">
                </div>

                <hr />

                <h4>No caso de avaliação 4 estrelas ou menos</h4>

                <div class="form-group">
                    <label>O que deverá ser questionado? (*)</label>
                    <input value="{{ ($predefinedAnswers) ? $predefinedAnswers['question_rating'] : 'O que podemos fazer para melhorar a sua experiência?' }}"
                     required class="form-control" name="question_rating" placeholder="O que podemos fazer para melhorar a sua experiência?" />
                </div>

                <div class="form-group">
                    <label>Quais serão as respostas pré-definidas para essa pergunta?</label><br>
                    <input name="answers_rating" class="form-control" type="text" data-role="tagsinput" 
                        value="{{ ($predefinedAnswers) ? $predefinedAnswers['answers_rating'] : 'Melhorar a comida, Melhorar a bebida, Melhorar o atendimento, Melhorar o ambiente' }}">
                </div>

                <button class="btn btn-primary">Salvar configuração</button>

            </form>
            
        </div>
    </div>
</div>
