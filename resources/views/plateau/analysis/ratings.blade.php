<h4><i class="fa fa-star"></i> Avaliações</h4>
<span>Visualize aqui todas as avaliações realizadas pelos seus clientes</span>
<hr/>
@include('plateau.includes.return-message')
<div class="container-fluid">
    <div class="row">
        <a class="btn btn-default" href="{{ url('/analysis/ratings/config') }}" role="button">
            <i class="fa fa-gear"></i> Configurar Avaliações
        </a>
    </div>
    <div class="row">
        <div class="col-sm-6 col-lg-6">
            <!-- Widget -->
            <span class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left background-spring animation-fadeIn">
                        <i class="fa fa-star"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong>{{ $ratingTotal['average'] }}</strong><br>
                        <small>Avaliação Média</small>
                    </h3>
                </div>
            </span>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-lg-6">
            <!-- Widget -->
            <span class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left background-spring animation-fadeIn">
                        <i class="fa fa-user"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong>{{ $ratingTotal['count'] }}</strong><br>
                        <small>Total de Avaliações</small>
                    </h3>
                </div>
            </span>
            <!-- END Widget -->
        </div>
    </div>
    <div class="row">
        <div class="straight">
            <Ratingtable :url='"{{ $url }}"' :has-edit-action='false' :has-disable-action='false' :has-delete-action='false'></Ratingtable>
        </div>
    </div>
</div>
