@extends('layouts.plateau')

@section('content')
  <h2>Histórico de Contas</h2>

  <div class="row">
    <div class="straight">
      @include('plateau.includes.return-message')
    </div>
  </div>
  <div class="row">
    <div class="straight">
      <Billtable
      :url='"<?= $url ?>"'
      :has-edit-action='true'
      :has-disable-action='false'>
    </Billtable>
  </div>
</div>

@endsection
