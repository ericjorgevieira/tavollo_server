@extends('layouts.plateau')

@section('content')
  <div id="tables">

    <h2>Cadastro de Categoria</h2>

      <div class="container-fluid">
        <div class="row">
          @include('plateau.includes.return-message')
        </div>
        <div class="row">
          <form method="post" action="{{ url('/categories/store') }}">
            {{ csrf_field() }}
            {{ method_field('POST') }}

            <div class="col-xs-12">
              <div class="form-group">
                <label for="inputNome">Nome</label>
                <input required type="text" name="name" id="inputNome" class="form-control"
                placeholder="Digite o nome da Categoria">
              </div>
              <div class="checkbox">
                <label>
                  <input name="status" checked value="enabled" type="checkbox" data-toggle="toggle" data-on="Ativado" data-off="Desativado"> Categoria Ativa?
                </label>
              </div>
            </div>

            <button type="submit" class="pull-right btn btn-default">Salvar</button>
            <button type="submit" name="save_and_add" value="1"
            class="pull-right btn btn-success">Salvar e Adicionar Novo</button>
          </form>
        </div>
      </div>
    </div>

  @endsection
