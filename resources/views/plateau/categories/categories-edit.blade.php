@extends('layouts.plateau')

@section('content')
  <div id="tables">
    <h2>Edição de Categorias</h2>

    <div class="container-fluid">
      <div class="row">
        @include('plateau.includes.return-message')
      </div>
      <div class="row">
        <form method="post" action="{{ url('/categories/' . $category->id . '/update') }}">
          {{ csrf_field() }}
          {{ method_field('PUT') }}

          <div class="col-xs-12">
            <div class="form-group">
              <label for="inputNome">Nome</label>
              <input required type="text" name="name" id="inputNome" class="form-control"
              placeholder="Digite o nome da Categoria" value="{{ $category->name }}">
            </div>
            <div class="checkbox">
              <label>
                <input <?= ($category->status == 'enabled') ? 'checked' : '' ?>
                name="status" value="enabled" type="checkbox" data-toggle="toggle" data-on="Ativado" data-off="Desativado"> Categoria Ativa?
              </label>
            </div>
          </div>

          <button type="submit" class="pull-right btn btn-success">Salvar</button>
        </form>
      </div>
    </div>
  </div>
@endsection
