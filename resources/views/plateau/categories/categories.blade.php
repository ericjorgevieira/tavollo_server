@extends('layouts.plateau')

@section('content')
    <h2>Categorias de Produtos</h2>

    <div class="row">
        <div class="straight">
            @include('plateau.includes.return-message')
            <a class="btn btn-success" href="{{ url('/categories/create') }}" role="button">
                <i class="fa fa-plus-circle"></i> Cadastrar Categoria
            </a>
        </div>
    </div>
    <div class="row">
        <div class="straight">
            <Smarttable
            :url='"<?= $url ?>"'
            :has-edit-action='true'
            :has-disable-action='true'>
        </Smarttable>
    </div>
</div>
@endsection
