@extends('layouts.plateau')
@section('content')
  @include('plateau.includes.token-modal')

  <div class="container-fluid">
    <div class="row" id="control">

      {{-- <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <TableRequestToken></TableRequestToken>
      </div> --}}

      <div class="col-lg-6 col-sm-12 col-md-6 col-xs-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <label for="selectTableToken">
              <i class="fa fa-search fa-2x"></i> Busca de Spot:
            </label>
          </div>
          <div class="panel-body">
            <form method="post" action="">
              <div class="form-group">

                <select id="viewToken" name="table_token" class="form-control selectpicker" data-live-search="true" id="selectTableToken" title="Selecione.." data-size="10">
                  @foreach($tables as $t)
                    <option value="{{ $t->id }}">{{ $t->name }}</option>
                  @endforeach
                </select>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="col-lg-6 col-sm-12 col-md-6 col-xs-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <label for="newBill">
              <i class="fa fa-plus-circle fa-2x"></i> Abrir nova Conta
            </label>
          </div>
          <div class="panel-body">
            <form class="form" id="newBillForm" method="POST">
              <div class="col-xs-12">
                <div class="form-group">
                  <input type="text" class="form-control" name="user_name" placeholder="Nome do Cliente"/>
                </div>
                <div class="form-group">
                  <select id="selectTable" name="table_id" class="form-control selectpicker" data-live-search="true" title="Mesa/Cartão" data-size="10">
                    @foreach($tables as $t)
                      <option value="{{ $t->id }}">{{ $t->name }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group pull-right">
                  <button class="btn btn-success"/>Abrir</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>

  <Dashboard :config="{{ Auth::user()->establishment->config }}"></Dashboard>

@endsection
