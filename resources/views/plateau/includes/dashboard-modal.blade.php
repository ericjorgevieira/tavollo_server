<!-- Modal -->

<div class="modal fade" id="modalBill" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

  <div class="modal-dialog" role="document">
    <div class="modal-content" v-if="modalBill" v-if="modalBill != false">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">@{{ modalBill.table.name }}</h4>
      </div>

      <div class="modal-body">

        <div class="row">

          <div class="col-xs-4 col-md-4">
            <div class="bill-user">
              <img width="100" height="100" v-bind:src="modalBill.user.photo"/>
            </div>
          </div>

          <div class="col-xs-8 col-md-8">
            <div class="info">
              <span>Nome: @{{ modalBill.user.name }}</span>
              <span>Consumo: R$ @{{ modalBill.total }}</span>

              <p>
                Nacionalidade: @{{ modalBill.user.country }}<br/>
                Horário de Entrada: @{{ modalBill.opened_at }}<br/>
                Duração: @{{ modalBill.duration }}
              </p>
            </div>
          </div>

        </div>

        <div class="row">

          <div class="col-md-12">

            <form method="post" action="">

              <div class="form-group">
                <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseProd"
                aria-expanded="false" aria-controls="collapseProd">
                <i class="fa fa-plus"></i> Adicionar Pedido
              </a>
            </div>

            <div class="collapse form-group" id="collapseProd">
              <input type="hidden" v-model="productQuantToOrder" value="1"/>
              <input type="hidden" v-model="productObsToOrder" value=""/>

              <div class="col-md-12">
                <div class="form-group">
                  <label for="selectProdOrder">Selecione o Produto:</label>
                  <select v-model="productToOrder" required name="product_id"
                  class="form-control selectpicker"
                  data-live-search="true" id="selectProdOrder" title="Selecione.."
                  data-size="10">
                  @foreach($productsToOrder as $prod)
                    <option data-weight="<?= ($prod->weight > 0) ? 'true' : 'false' ?>"
                      value="{{ $prod->id }}">{{ (($prod->code) ? $prod->code : $prod->id)
                        . ' - ' . $prod->name }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="col-md-6 is-weight-price">
                  <div class="checkbox">
                    <label>
                      <input v-model="productIsWeight" id="isWeightCheck" name="is_weight_price"
                      value="enabled"
                      type="checkbox"/> Calcular
                      Valor por Peso
                    </label>
                  </div>
                </div>

                <div class="col-md-6 order-weight">
                  <div class="form-group">
                    <label>Peso (g)</label>
                    <input v-model="productWeightToOrder" id="orderWeight"
                    placeholder="Informe o peso do pedido"
                    class="form-control mask-weight"
                    name="order_weight"/>
                  </div>
                </div>

                <div class="col-md-12 product-obs-to-order">
                  <div class="form-group">
                    <label>Observações</label>
                    <textarea class="form-control" v-model="productObsToOrder"
                    id="productObsToOrder"
                    name="product_obs_to_order">
                  </textarea>
                </div>
              </div>

              <button class="btn pull-right btn-success"
              v-on:click.stop.prevent="addOrderToBill(modalBill.id)">Adicionar
            </button>
          </div>

        </form>

      </div>

    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="bill-orders panel panel-default">
          <div class="panel-heading">Pedidos</div>
          <div class="panel-body">
            <ul class="list-group" v-if="modalBill.orders.length > 0">
              <li class="list-group-item" v-for="o in modalBill.orders">
                <div class="order-info-resume">
                  <span v-if="o.is_weight_price == 1">
                    <b>#@{{ o.id }}: </b>
                    <b v-if="o.product.code != ''">@{{ o.product.code }} -</b>
                    <b v-else>@{{ o.product.id }} -</b>
                    <b>@{{ o.product.name }} - (peso: @{{ o.order_weight }}g)</b>
                    - R$ @{{ o.cache_price }}</span>
                    <span v-else>
                      <b>#@{{ o.id }}: </b>
                      <b v-if="o.product.code != ''">@{{ o.product.code }} -</b>
                      <b v-else>@{{ o.product.id }} -</b>
                      <b>@{{ o.product.name }}</b>- R$ @{{ o.cache_price }}</span>
                    </div>
                    <div class="collapse order-info-complete" id="collapseInfo-@{{ o.id }}">
                      <b>Data do Pedido: @{{ o.created_at }}</b><br/>
                      <b>Quantidade solicitada pelo cliente: @{{ o.quant }}</b><br/>
                      <b v-if="o.observation">Observação: @{{ o.observation }}</b>

                      <div class="alter-weight" v-if="o.product.weight > 0">
                        <form method="post">
                          <div class="col-md-6">
                            <div class="form-group">
                              <div class="checkbox">
                                <label>
                                  <input v-model="productsIsWeight[o.id]"
                                  value="true"
                                  v-bind:checked="o.is_weight_price == 1"
                                  type="checkbox"/> Calcular Valor por Peso ?
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Peso (g)</label>
                              <input v-model="productsWeightToOrder[o.id]"
                              placeholder="Informe o peso do pedido"
                              class="form-control mask-weight"
                              value="@{{ o.order_weight }}"
                              name="order_weight"/>
                            </div>
                            <div class="form-group">
                              <button class="btn pull-right submit btn-success"
                              v-on:click.stop.prevent="addWeightToPriceOrder(modalBill.id, o.id)">
                              Salvar
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <button rel="tooltip" data-toggle="tooltip" data-placement="top"
                  title="Cancelar Pedido"
                  type="submit" v-on:click.stop.prevent="cancelOrder(o.id, modalBill.id)"
                  class="btn btn-danger cancel">
                  <i class="fa fa-close"></i>
                </button>
                <a rel="tooltip" data-toggle="collapse" href="#collapseInfo-@{{ o.id }}"
                aria-expanded="false" aria-controls="collapseInfo-@{{ o.id }}"
                class="btn btn-success open-info">
                <i class="fa fa-info"></i>
              </a>
            </li>
            <li class="list-group-item" v-if="modalBill.has_baksheesh > 0">
              <span><b>Acréscimo de {{modalBill.baksheesh_percentage}}% </b> - R$ @{{ modalBill.baksheesh }}</span>
            </li>
          </ul>
          <h4 class="text-center" v-if="modalBill.orders.length == 0">Cliente ainda não fez nenhum
            pedido.</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn pull-right btn-danger" data-dismiss="modal">Fechar</button>
    <button v-on:click="closeBill(modalBill.id)"
    type="button" class="btn pull-left btn-warning" data-dismiss="modal">Encerrar Conta
  </button>
</div>
</div>
<div class="modal-content" v-else>
  <div class="modal-header">
    <h4 class="modal-title" id="myModalLabel">Aguarde, carregando</h4>
  </div>
  <div class="modal-body">
    <div class="loading-modal">
      <i class="fa fa-spinner fa-spin fa-2x"></i>
    </div>
  </div>
</div>
</div>

</div>
