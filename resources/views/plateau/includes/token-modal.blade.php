<!-- Modal -->
<div class="modal fade" id="modalToken" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Código da mesa </h4>
            </div>
            <div class="modal-body">
                <div class="row text-center">
                    <h1 id="tableCode"> - </h1>
                </div>
                <div class="row text-center">
                    <img id="tableQrCode" alt="QrCode da mesa" width="250" height="250"/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn pull-right btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>