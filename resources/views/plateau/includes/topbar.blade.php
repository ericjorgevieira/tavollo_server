@php
$route = explode('/', Route::current()->uri());
$route = $route[0];
$config = Auth::user()->establishment->config;
@endphp
<nav id="navbar" class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">

      <!-- Collapsed Menu -->
      <button type="button" class="navbar-toggle left collapsed" data-toggle="collapse"
      data-target="#app-navbar-collapse">
      <span class="sr-only">Toggle Navigation</span>
      <span class="icon"><i class="fa fa-bars"></i></span>
    </button>

    <!-- Collapsed Pedidos -->
    {{-- <button type="button" class="navbar-toggle right collapsed" data-toggle="collapse"
    data-target="#sidebar-wrapper">
    <span class="sr-only">Toggle Navigation</span>
    <span class="icon"><i class="fa fa-user"></i></span>
  </button> --}}

  <!-- Branding Image -->
  <a class="navbar-brand logo" href="{{ url('/') }}">
    <img src="/images/logo-2.png"/>
  </a>
</div>
<div class="collapse navbar-collapse" id="app-navbar-collapse">
  <!-- Left Side Of Navbar -->
  <ul class="nav navbar-nav">
    <li class="hidden-sm">
      <a class="{{ ($route == 'home') ? 'active' : '' }}" href="{{ url('/home') }}">Inicial</a>
    </li>
    @if(Auth::user()->profile == \App\Models\User::PROFILE_MANAGER)
      <li class="dropdown">
        <a class="dropdown-toggle {{ ($route == 'tables') ? 'active' : '' }}" href="#" data-toggle="dropdown" role="button" aria-expanded="false">
          <i class="fa fa-map-marker"></i>
          Spots
        </a>
        <ul class="dropdown-menu" role="menu">
          <li>
            <a href="{{ url('/tables') }}"><i class="fa fa-btn fa-align-justify"></i>
              <span>Lista de Spots</span>
            </a>
          </li>
          @if($config->enable_advanced_functions == 1)
          <li>
            <a href="{{ url('/tables/create') }}"><i class="fa fa-btn fa-plus-circle"></i>
              <span>Cadastrar Spot</span>
            </a>
          </li>
          
          <li>
            <a href="{{ url('/tables/import') }}"><i class="fa fa-btn fa-upload"></i>
              <span>Importar Spots</span>
            </a>
          </li>
          @endif
          <li class="dropdown-submenu">
            <a class="dropdown-togle" href="#" data-toggle="dropdown" role="button" aria-expanded="false">
              <i class="fa fa-btn fa-map-pin"></i>
              <span>Referências</span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li>
                <a href="{{ url('/locations') }}"><i class="fa fa-btn fa-align-justify"></i>
                  <span>Lista de Locais</span>
                </a>
              </li>
              <li>
                <a href="{{ url('/locations/create') }}"><i class="fa fa-btn fa-plus-circle"></i>
                  <span>Cadastrar Local</span>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle {{ ($route == 'menus') ? 'active' : '' }}" href="#" data-toggle="dropdown" role="button" aria-expanded="false">
          <i class="fa fa-list-ul"></i>
          Cardápios
        </a>
        <ul class="dropdown-menu" role="menu">
          <li>
            <a href="{{ url('/menus') }}"><i class="fa fa-btn fa-align-justify"></i>
              <span>Lista de Cardápios</span>
            </a>
          </li>
          <li>
            <a href="{{ url('/menus/create') }}"><i class="fa fa-btn fa-plus-circle"></i>
              <span>Cadastrar Cardápio</span>
            </a>
          </li>
        </ul>
      </li>
      <li class="dropdown">
        <a class="dropdown-toggle {{ ($route == 'products') ? 'active' : '' }}" href="#" data-toggle="dropdown" role="button" aria-expanded="false">
          <i class="fa fa-beer"></i>
          Produtos
        </a>
        <ul class="dropdown-menu" role="menu">
          <li>
            <a href="{{ url('/products') }}"><i class="fa fa-btn fa-align-justify"></i>
              <span>Lista de Produtos</span>
            </a>
          </li>
          <li>
            <a href="{{ url('/products/create') }}"><i class="fa fa-btn fa-plus-circle"></i>
              <span>Cadastrar Produto</span>
            </a>
          </li>
          @if($config && $config->enable_advanced_functions)
          <li>
            <a href="{{ url('/products/import') }}"><i class="fa fa-btn fa-upload"></i>
              <span>Importar Produtos</span>
            </a>
          </li>
          @endif
          <li class="dropdown-submenu">
            <a class="dropdown-togle" href="#" data-toggle="dropdown" role="button" aria-expanded="false">
              <i class="fa fa-btn fa-map-pin"></i>
              <span>Categorias</span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li>
                <a href="{{ url('/categories') }}"><i class="fa fa-btn fa-align-justify"></i>
                  <span>Lista de Categorias</span>
                </a>
              </li>
              <li>
                <a href="{{ url('/categories/create') }}"><i class="fa fa-btn fa-plus-circle"></i>
                  <span>Cadastrar Categoria</span>
                </a>
              </li>
            </ul>
          </li>

          {{-- <li>
          <a href="{{ url('/products/create') }}"><i class="fa fa-btn fa-list"></i>
          <span>Lista de Categorias</span>
        </a>
      </li>
      <li>
      <a href="{{ url('/products/create') }}"><i class="fa fa-btn fa-plus"></i>
      <span>Cadastrar Categoria</span>
    </a>
  </li> --}}
</ul>
</li>
@endif
</ul>

<!-- Right Side Of Navbar -->
<ul class="nav navbar-nav navbar-right">
  <!-- Authentication Links -->
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
      {{ Auth::user()->establishment->name . " (" . Auth::user()->firstName() . ")" }} <span
      class="caret"></span>
    </a>

    <ul class="dropdown-menu" role="menu">
      <li>
        <a href="{{ url('/profile') }}"><i class="fa fa-btn fa-user"></i>
          <span>Editar Perfil</span>
        </a>
      </li>
      @if(Auth::user()->isManager())
        @if(!Auth::user()->establishment->isTest())
          <li>
            <a href="{{ url('/profile/islands') }}"><i class="fa fa-btn fa-sitemap"></i>
              <span>Ilhas</span>
            </a>
          </li>
          <li>
            <a href="{{ url('/profile/waiters') }}"><i class="fa fa-btn fa-users"></i>
              <span>Garçons</span>
            </a>
          </li>
          <hr/>
          @if($config && $config->allow_bills)
          <li>
            <a href="{{ url('/bills') }}"><i class="fa fa-btn fa-clock-o"></i>
              <span>Histórico de Contas</span>
            </a>
          </li>
          @endif
          @if($config && $config->allow_pre_bills)
          <li>
            <a href="{{ url('/pre-orders') }}"><i class="fa fa-btn fa-clock-o"></i>
              <span>Histórico de Pré-Pagos</span>
            </a>
          </li>
          @endif
          <li>
            <a href="{{ url('/analysis') }}"><i class="fa fa-btn fa-line-chart"></i>
              <span>Analise de Dados</span>
            </a>
          </li>
          <hr/>
        @endif
        <li>
          <a href="{{ url('/profile/config') }}"><i class="fa fa-btn fa-gears"></i>
            <span>Configurações</span>
          </a>
        </li>
      @endif
      <li>
        <a href="{{ url('/about') }}"><i class="fa fa-btn fa-info-circle"></i>
          <span>Sobre</span>
        </a>
      </li>
      <hr/>
      <li>
        <a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>
          <span>Sair</span>
        </a>
      </li>
    </ul>
  </li>
</ul>
</div>
</div>
</nav>
