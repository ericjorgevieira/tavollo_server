@extends('layouts.plateau')

@section('content')
  <div id="tables">
    <h2>Edição de Locais</h2>

    <div class="container-fluid">
      <div class="row">
        @include('plateau.includes.return-message')
      </div>
      <div class="row">
        <form method="post" action="{{ url('/locations/' . $location->id . '/update') }}">
          {{ csrf_field() }}
          {{ method_field('PUT') }}

          <div class="col-xs-12">
            <div class="form-group">
              <label for="inputNome">Nome</label>
              <input required type="text" name="name" id="inputNome" class="form-control"
              placeholder="Digite o nome do Local de Referência" value="{{ $location->name }}">
            </div>
            <div class="checkbox">
              <label>
                <input <?= ($location->status == 'enabled') ? 'checked' : '' ?>
                name="status" value="enabled" type="checkbox" data-toggle="toggle" data-on="Ativado" data-off="Desativado"> Local de Referência Ativo?
              </label>
            </div>
          </div>
          <div class="col-xs-12">
            <button type="submit" class="pull-right btn btn-success">Salvar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
