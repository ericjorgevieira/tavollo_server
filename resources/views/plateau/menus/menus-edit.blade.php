@extends('layouts.plateau')

@section('content')
  <div id="menus">
    <h2>Cadastro de Cardápio</h2>
    <p>
      Os produtos com categorias definidas serão listados abaixo.
    </p>

    <div class="container-fluid">
      <div class="row">
        @include('plateau.includes.return-message')
      </div>
      <div class="row">
        <form method="post" action="{{ url('/menus/' . $menu->id . '/update') }}">
          {{ csrf_field() }}
          {{ method_field('PUT') }}

          @if($menu->photo)
          <ImageUpload :image="'{{ $menu->photo }}'" :width="500" :height="250" :url="'/menus/upload'"></ImageUpload>
          @else
          <ImageUpload :width="500" :height="250" :url="'/menus/upload'"></ImageUpload>
          @endif

          <div class="col-xs-8">
            <div class="form-group">
              <label for="inputNome">Nome (*)</label>
              <input required type="text" name="name" id="inputNome" class="form-control"
              placeholder="Digite o nome do Cardápio" value="{{ $menu->name }}">
            </div>
            <div class="form-group">
              <label for="inputNome">Descrição (*)</label>
              <textarea required name="description"
              class="form-control">{{ $menu->description }}</textarea>
            </div>
            <div class="form-group">
              <label for="textareaDescricao">Produtos por Categoria / Inseridos no Cardápio</label>
              <select class="form-control multi-select" name="products[]" multiple="multiple">
                <?php foreach($listProducts as $cat){ ?>
                  <optgroup label="{{ $cat['productCategory']->name }}">
                    <?php foreach($cat['products'] as $p){ ?>
                      <option <?= (in_array($p->id, $menuProducts)) ? "selected" : "" ?>
                        value="{{ $p->id }}">{{ $p->name }}</option>
                        <?php } ?>
                      </optgroup>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="checkbox">
                    <label>
                      <input <?= ($menu->status == 'enabled') ? 'checked' : '' ?>
                      name="status" value="enabled" type="checkbox"> Ativo?
                    </label>
                  </div>
                </div>
                <button type="submit" class="pull-right btn btn-success">Salvar</button>
              </form>
            </div>
          </div>
        </div>
      @endsection
