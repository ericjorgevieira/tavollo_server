@extends('layouts.plateau')

@section('content')

  <h2>Cardápios</h2>

  <div class="row">
    <div class="straight">
      @include('plateau.includes.return-message')
      <a class="btn btn-success" href="{{ url('/menus/create') }}" role="button">
        <i class="fa fa-plus-circle"></i> Cadastrar Cardápio
      </a>
    </div>

  </div>
  <div class="row">
    <div class="straight">
      <Smarttable :url='"<?= $url ?>"' :has-edit-action='true' :has-disable-action='true'></Smarttable>
    </div>
  </div>

@endsection
