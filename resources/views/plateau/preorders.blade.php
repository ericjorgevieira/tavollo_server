@extends('layouts.plateau')

@section('content')
  <h2>Histórico de Pedidos Pré-Pagos</h2>

  <div class="row">
    <div class="straight">
      @include('plateau.includes.return-message')
    </div>
  </div>
  <div class="row">
    <div class="straight">
      <PreOrderTable
      :url='"{{ $url }}"'
      :has-edit-action='true'
      :has-disable-action='false'>
    </PreOrderTable>
  </div>
</div>

@endsection
