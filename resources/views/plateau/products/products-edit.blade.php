@extends('layouts.plateau')

@section('content')
  <div id="products">
    <h2>Edição de Produtos</h2>

    @include('plateau.includes.return-message')

    <div class="container-fluid">
      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#product">Dados do Produto</a></li>
        <li><a data-toggle="tab" href="#observations">Observações</a></li>
        <li><a data-toggle="tab" href="#available">Disponibilidade</a></li>
        <li><a data-toggle="tab" href="#extras">Opções Extras</a></li>
      </ul>

      <form method="post" action="{{ url('/products/' . $product->id . '/update') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <div class="tab-content">

          <div id="product" class="tab-pane fade in active">

          @if($product->photo)
          <ImageUpload :image="'{{ $product->photo }}'" :width="500" :height="500" :url="'/products/upload'"></ImageUpload>
          @else
          <ImageUpload :width="500" :height="500" :url="'/products/upload'"></ImageUpload>
          @endif

            <div class="col-xs-8">
              <div class="col-xs-8">
                <div class="form-group">
                  <label for="inputNome">Nome (*)</label>
                  <input required type="text" name="name" id="inputNome" class="form-control"
                  placeholder="Digite o nome do Produto" value="{{ $product->name }}">
                </div>
              </div>
              <div class="col-xs-4">
                <div class="form-group">
                  <label for="inputCode">Código</label>
                  <input {{ !$product->establishment->config->enable_advanced_functions ? 'readonly' : '' }} type="text" required name="code" id="inputCode" class="form-control"
                  placeholder="Digite o código do Produto" value="{{ $product->code }}">
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group">
                  <label for="exampleInputAmount">Valor (R$)</label>

                  <div class="input-group">
                    <div class="input-group-addon">R$</div>
                    <input required name="price" type="text" class="form-control mask-money"
                    id="exampleInputAmount"
                    placeholder="00,00"
                    value="{{ number_format($product->price, 2, ',', '.') }}">
                  </div>
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group">
                  <label for="exampleInputAmountPromo">Valor Promocional (R$)</label>

                  <div class="input-group">
                    <div class="input-group-addon">R$</div>
                    <input name="promo_price" type="text" class="form-control mask-money"
                    id="exampleInputAmountPromo"
                    placeholder="00,00"
                    value="{{ number_format($product->promo_price, 2, ',', '.') }}">
                  </div>
                </div>
              </div>

              <div class="col-xs-4">
                <div class="form-group">
                  <label for="promoBegin">Início do Período Promocional</label>
                  <input class="form-control mask-time" name="promo_price_begin_at"  value="{{ $product->promo_price_begin_at }}"/>
                  {{-- <select name="promo_price_begin_at" class="form-control" title="Selecione..">
                    @foreach(App\Models\Product::$hours as $t => $hour)
                      <option {{ ($product->promo_price_begin_at == $t) ? 'selected' : '' }} value="{{ $t }}">{{ $hour }}</option>
                    @endforeach
                  </select> --}}
                </div>
              </div>

              <div class="col-xs-4">
                <div class="form-group">
                  <label for="promoBegin">Fim do Período Promocional</label>
                  <input class="form-control mask-time" name="promo_price_end_at" value="{{ $product->promo_price_end_at }}" />
                  {{-- <select name="promo_price_end_at" class="form-control" title="Selecione..">
                    @foreach(App\Models\Product::$hours as $t => $hour)
                      <option {{ ($product->promo_price_end_at == $t) ? 'selected' : '' }} value="{{ $t }}">{{ $hour }}</option>
                    @endforeach
                  </select> --}}
                </div>
              </div>

              <div class="col-xs-4">
                <div class="form-group">
                  <label for="preparation_time">Tempo de Preparo</label>
                  <input type="text" class="form-control mask-time" value="{{ $product->preparation_time }}" name="preparation_time" id="preparation_time" />
                </div>
              </div>

              <div class="col-xs-12">
                <p><h4>Dias do Período Promocional</h4> <b>(Atenção: Nesse período, o Preço Promocional será aplicado automaticamente.)</b></p>
                @foreach(App\Models\Product::$days as $d => $day)
                  <div class="checkbox-inline">
                    <label>
                      <input name="promo_price_available_days[{{ $d }}]" <?= (in_array($d, $product->getPromoPriceAvailableDays())) ? 'checked' : '' ?> value="true" type="checkbox"> {{ $day }}
                    </label>
                  </div>
                @endforeach
                <hr />
              </div>

              <div class="col-xs-6">
                <div class="form-group">
                  <label for="selectCategoria">Categoria (*)</label>
                  <select required name="product_category_id" class="form-control selectpicker"
                  data-live-search="true" id="selectCategoria" title="Selecione.." data-size="10">
                  @foreach($productCategories as $cat)
                    <option <?= ($cat->id == $product->product_category_id) ? 'selected' : '' ?>
                      value="{{ $cat->id }}">{{ $cat->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="col-xs-6">
                <div class="form-group">
                  <label for="weight">Peso (Kg)</label>

                  <div class="input-group">
                    <div class="input-group-addon">Gramas</div>
                    <input type="text" name="weight" id="weight" class="form-control mask-weight"
                    placeholder="Digite o peso do Produto" value="{{ $product->weight }}">
                  </div>
                </div>
              </div>
              <div class="col-xs-12">
                <div class="form-group">
                  <label for="textareaDescricao">Descrição</label>
                  <textarea name="description" class="form-control"
                  rows="3">{{ $product->description }}</textarea>
                </div>
              </div>

              <div class="col-xs-12">
                <div class="checkbox">
                  <label>
                    <input <?= ($product->enable_promo_schedule == '1') ? 'checked' : '' ?>
                    name="enable_promo_schedule" value="1" type="checkbox" data-toggle="toggle" data-on="Ativado" data-off="Desativado" > <b>Ativar Período Promocional</b>
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input <?= ($product->is_promo_price == '1') ? 'checked' : '' ?>
                    name="is_promo_price" value="1" type="checkbox" data-toggle="toggle" data-on="Ativado" data-off="Desativado" > <b>Ativar Preço Promocional</b>
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input <?= ($product->status == 'enabled') ? 'checked' : '' ?>
                    name="status" value="enabled" type="checkbox" data-toggle="toggle" data-on="Ativado" data-off="Desativado" > Produto Ativo?
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input <?= ($product->not_sale == '1') ? 'checked' : '' ?>
                    name="not_sale" value="1" type="checkbox" data-toggle="toggle" data-on="Ativado" data-off="Desativado" > Não Vender
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input <?= ($product->calculate_baksheesh == '1') ? 'checked' : '' ?>
                    name="calculate_baksheesh" value="1" type="checkbox" data-toggle="toggle" data-on="Ativado" data-off="Desativado" > Cobrar <b>Taxa de Serviço</b>?
                  </label>
                </div>
                <div class="checkbox">
                  <label>
                    <input <?= ($product->enable_automatic_order == '1') ? 'checked' : '' ?>
                    name="enable_automatic_order" value="1" type="checkbox" data-toggle="toggle" data-on="Ativado" data-off="Desativado" > <b>Lançar automaticamente</b> nos pedidos?
                  </label>
                </div>
              </div>
            </div>
            <button type="submit" class="pull-right btn btn-success">Salvar</button>

          </div>

          <div id="observations" class="tab-pane fade in">
            <ProductObservations :product="{{ $product->id }}" :readonly="{{ $product->establishment->config->enable_advanced_functions ? 0 : 1 }}"></ProductObservations>
          </div>

          <div id="available" class="tab-pane fade in">
            <div class="col-xs-12">
              <p><h4>Dias em que o Produto estará disponível</h4></p>
              @foreach(App\Models\Product::$days as $d => $day)
                <div class="checkbox-inline">
                  <label>
                    <input name="available_days[{{ $d }}]" <?= (in_array($d, $product->getAvailableDays())) ? 'checked' : '' ?> value="true" type="checkbox"> {{ $day }}
                  </label>
                </div>
              @endforeach
              <div class="form-inline">
                <p><h4>Turno Disponível</h4></p>
                <div class="form-group">
                  <select name="available_turn" class="form-control" title="Selecione..">
                    @foreach(App\Models\Product::$turns as $t => $turn)
                      <option <?= ($product->available_turn == $t) ? 'selected' : '' ?> value="{{ $t }}">{{ $turn }}</option>
                    @endforeach
                  </select>
                </div>

                <p><h4>Início / Fim do 1&ordm; Turno</h4></p>
                <div class="form-group">
                  <select name="first_turn_begin" class="form-control" title="Selecione..">
                    <option value="">
                      Início do 1&ordm; Turno
                    </option>
                    @foreach(App\Models\Product::$hours as $h => $hour)
                      <option <?= ($product->first_turn_begin == $h) ? 'selected' : '' ?> value="{{ $h }}">
                        {{ $hour }}
                      </option>
                    @endforeach
                  </select>
                </div>

                <div class="form-group">
                  <select name="first_turn_end" class="form-control" title="Selecione..">
                    <option value="">
                      Fim do 1&ordm; Turno
                    </option>
                    @foreach(App\Models\Product::$hours as $h => $hour)
                      <option <?= ($product->first_turn_end == $h) ? 'selected' : '' ?> value="{{ $h }}">
                        {{ $hour }}
                      </option>
                    @endforeach
                  </select>
                </div>

                <p><h4>Início / Fim do 2&ordm; Turno</h4></p>
                <div class="form-group">
                  <select name="second_turn_begin" class="form-control" title="Selecione..">
                    <option value="">
                      Início do 2&ordm; Turno
                    </option>
                    @foreach(App\Models\Product::$hours as $h => $hour)
                      <option <?= ($product->second_turn_begin == $h) ? 'selected' : '' ?> value="{{ $h }}">
                        {{ $hour }}
                      </option>
                    @endforeach
                  </select>
                </div>

                <div class="form-group">
                  <select name="second_turn_end" class="form-control" title="Selecione..">
                    <option value="">
                      Fim do 2&ordm; Turno
                    </option>
                    @foreach(App\Models\Product::$hours as $h => $hour)
                      <option <?= ($product->second_turn_end == $h) ? 'selected' : '' ?> value="{{ $h }}">
                        {{ $hour }}
                      </option>
                    @endforeach
                  </select>
                </div>

              </div>
            </div>
            <button type="submit" class="pull-right btn btn-success">Salvar</button>
          </div>

          <div id="extras" class="tab-pane fade in">

              <ProductGroups :readonlyExternalCodes="{{ $product->establishment->config->enable_advanced_functions ? 0 : 1 }}" :product="{{ $product->id }}"></ProductGroups>

          </div>

        </div>

      </form>
    </div>
  </div>

  @endsection
