@extends('layouts.plateau')

@section('content')
  <div id="products">
    <h2>Importação de Produtos</h2>

    @include('plateau.includes.return-message')

    <div class="container-fluid">

      <form method="post" action="{{ url('/products/import-file') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('POST') }}

        <div class="col-xs-6">
          <h4>
            Importe vários produtos ao mesmo tempo! Utilize <a target="_blank" href="/tavollo-model.csv">esse modelo</a> de informações para fazer o upload.
            São aceitos apenas arquivos no formato .CSV.
          </h4>
          <div class="form-group">
            <label>Selecione o arquivo .CSV</label>
            <input class="form-control" type="file" name="import" />
          </div>
        </div>

        <div class="col-xs-12">
          {{-- @if($integration)
            <a href="{{ url('/products/import-integration') }}" type="submit" class="pull-right btn btn-warning">Importar via Integração</a>
          @endif --}}
          <button type="submit" name="save" value="1" class="pull-right btn btn-primary">Salvar</button>
        </div>

      </form>

      @if(session()->has('imported'))
        @foreach (session('imported') as $success)
          <div class="alert alert-success fade in">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ $success }}
          </div>
        @endforeach
      @endif
    </div>

  </div>

@endsection
