@extends('layouts.plateau')

@section('content')
  <div id="products">
    <h2>Importação de Produtos</h2>

    @include('plateau.includes.return-message')

    <div class="container-fluid">

        <div class="col-xs-12">
          <h4>
            Para cadastrar/importar produtos no Tavollo, utilize a integração.<br/>
            Se houver dúvidas quanto ao procedimento, entre em contato com o suporte da integração.
          </h4>
        </div>

    </div>

  </div>

@endsection
