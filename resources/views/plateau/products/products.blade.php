@extends('layouts.plateau')

@section('content')
    <h2>Produtos</h2>

    <div class="row">
        <div class="straight">
            @include('plateau.includes.return-message')
            <a class="btn btn-success" href="{{ url('/products/create') }}" role="button">
                <i class="fa fa-plus-circle"></i> Cadastrar Produto
            </a>
            <a class="btn btn-default" href="{{ url('/products/import') }}" role="button">
                <i class="fa fa-upload"></i> Importar Produtos
            </a>
        </div>
    </div>
    <div class="row">
        <div class="straight">
            <Producttable
                :url='"{{ $url }}"'
                :categories='{{ $categories }}'
                :has-edit-action='true'
                :has-disable-action='true'>
            </Producttable>
    </div>
</div>
@endsection
