@extends('layouts.plateau')

@section('content')
  <div id="profile">
    <h3>Perfil</h3>

    <div class="row">
      <div class="col-md-4 col-lg-3">
        <div class="panel panel-primary">

          <div class="panel-body">
            <ul class="nav nav-pills nav-stacked">
              <li role="presentation" {{ ($action == 'user') ? 'class="active"' : '' }} >
                <a href="{{ url('/profile') }}"><i class="fa fa-user"></i>
                  <span>Usuário</span>
                </a>
              </li>
              <li role="presentation" {{ ($action == 'password') ? 'class="active"' : '' }}>
                <a href="{{ url('/profile/password') }}"><i class="fa fa-key"></i>
                  <span>Mudar Senha</span>
                </a>
              </li>
              <li role="presentation" {{ ($action == 'establishment') ? 'class="active"' : '' }}>
                <a href="{{ url('/profile/establishment') }}"><i class="fa fa-institution"></i>
                  <span>Estabelecimento</span>
                </a>
              </li>
            </ul>
            @if(!Auth::user()->establishment->isTest())
              <hr/>
              <ul class="nav nav-pills nav-stacked">
                <li role="presentation" {{ ($action == 'sectors') ? 'class="active"' : '' }}>
                  <a href="{{ url('/profile/sectors') }}"><i class="fa fa-bookmark"></i>
                    <span>Setores</span>
                  </a>
                </li>
                <li role="presentation" {{ ($action == 'islands') ? 'class="active"' : '' }}>
                  <a href="{{ url('/profile/islands') }}"><i class="fa fa-sitemap"></i>
                    <span>Ilhas</span>
                  </a>
                </li>
                <li role="presentation" {{ ($action == 'waiters') ? 'class="active"' : '' }}>
                  <a href="{{ url('/profile/waiters') }}"><i class="fa fa-users"></i>
                    <span>Garçons</span>
                  </a>
                </li>
              </ul>
            @endif
            <hr/>
            <ul class="nav nav-pills nav-stacked">
              <li role="presentation" {{ ($action == 'config') ? 'class="active"' : '' }}>
                <a href="{{ url('/profile/config') }}"><i class="fa fa-gears"></i>
                  <span>Configurações</span>
                </a>
              </li>
              @if(!Auth::user()->establishment->isTest())
                <li role="presentation" {{ ($action == 'api') ? 'class="active"' : '' }}>
                  <a href="{{ url('/profile/api') }}"><i class="fa fa-link"></i>
                    <span>API</span>
                  </a>
                </li>
                <li role="presentation" {{ ($action == 'payment') ? 'class="active"' : '' }}>
                  <a href="{{ url('/profile/payment') }}"><i class="fa fa-money"></i>
                    <span>Pagamento</span>
                  </a>
                </li>
                <li role="presentation" {{ ($action == 'printer') ? 'class="active"' : '' }}>
                  <a href="{{ url('/profile/printer') }}"><i class="fa fa-print"></i>
                    <span>Impressora</span>
                  </a>
                </li>
              @endif
            </ul>
            <hr/>
            <ul class="nav nav-pills nav-stacked">
              <li role="presentation" {{ ($action == 'faq') ? 'class="active"' : '' }}>
                <a href="{{ url('/profile/faq') }}"><i class="fa fa-question-circle"></i>
                  <span>Tutoriais</span>
                </a>
              </li>
              <li role="presentation" {{ ($action == 'activities') ? 'class="active"' : '' }}>
                <a href="{{ url('/profile/activities') }}"><i class="fa fa-pencil"></i>
                  <span>Registro de Atividades</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-md-8 col-lg-9">
        @include('plateau.profile.' . $action)
      </div>
    </div>
  </div>
@endsection
