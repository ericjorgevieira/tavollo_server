<h4><i class="fa fa-pencil"></i> Registo de Atividades</h4>
<span>Confira aqui o registro de ações realizadas na plataforma</span>
<hr/>
@include('plateau.includes.return-message')
<table class="table" id="activitiesTable">
  <thead>
    <tr>
      <th scope="col">Data</th>
      <th scope="col">Usuário</th>
      <th scope="col">Ação</th>
      <th scope="col">Descrição</th>
    </tr>
  </thead>
  <tbody>
    @foreach($logs as $l)
    <tr>
      <th scope="row">{{ $l->created_at->format('d/m/Y H:i:s') }}</th>
      <td>{{ $l->user->name . ' (' . $l->user->email . ')' }}</td>
      <td>{{ $l->action }}</td>
      <td>{{ $l->description }}</td>
    </tr>
    @endforeach
  </tbody>
</table>

@push('scripts')
<script type="text/javascript">
$(document).ready( function () {
    $('#activitiesTable').DataTable({
      language: {
        search: "Faça uma busca:",
        paginate: {
            first:      "Prumeiro",
            previous:   "Anterior",
            next:       "Próximo",
            last:       "Último"
        },
        lengthMenu:    "Exibindo _MENU_ resultados",
        info: "Exibindo _START_ até _END_ de _TOTAL_ resultados",
      }
    });
} );
</script>
@endpush
