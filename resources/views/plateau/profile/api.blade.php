<h4><i class="fa fa-link"></i> Chave de API</h4>
<h5>
  Gere sua chave para efetuar requisições na API.
  Com isso, você poderá integrar com o seu PDV e facilitar ainda mais o seu trabalho!
  <b>Essa chave irá expirar em 2 anos.</b>
</h5>
<hr/>
@include('plateau.includes.return-message')
<div class="container-fluid">
  <div class="row">
    <form method="post" action="{{ url('/profile/api-generate') }}">
      {{ csrf_field() }}
      {{ method_field('PUT') }}
      <div class="form-group col-sm-12">
        <label>Chave de API</label>
        <input readonly class="form-control" type="text" name="api_key" value="{{ $token }}" placeholder="Nenhuma chave foi criada."/>
      </div>
      <div class="form-group col-sm-12">
        <hr/>
        <button type="submit" class="btn btn-primary">Gerar nova Chave</button>
      </div>
    </form>
  </div>
</div>
