<h4><i class="fa fa-gear"></i> Configurações</h4>
<span>Veja opções para gerenciamento</span>
<hr/>
@include('plateau.includes.return-message')
<form method="post" action="{{ url('/profile/config-update') }}">
  {{ csrf_field() }}
  {{ method_field('PUT') }}
  <input type="hidden" name="limit_table" value="1"/>
  <input type="hidden" name="has_couvert" value="0"/>
  @if($config->allow_bills)
  <div class="checkbox col-sm-12">
    <label>
      <input type="checkbox" {{ ($config->generate_codes == 1) ? "checked" : "" }} data-toggle="toggle" data-on="Ativado" data-off="Desativado" name="generate_codes"
      value="1"/>
      Gerar novo <b>TOKEN de Spot</b> ao encerrar uma Conta
    </label>
  </div>
  @endif
  <div class="checkbox col-sm-12">
    <label>
      <input type="checkbox" {{ ($config->enable_price_update == 1) ? "checked" : "" }} data-toggle="toggle" data-on="Ativado" data-off="Desativado" name="enable_price_update"
      value="1"/>
      Atualizar preços na <b>importação de produtos</b> (serão sobrescrevidos pelos valores importados)
    </label>
  </div>
  @if($config->allow_pre_bills)
  <div class="checkbox col-sm-12">
    <label>
      <input type="checkbox" {{ ($config->request_alias == 1) ? "checked" : "" }} data-toggle="toggle" data-on="Ativado" data-off="Desativado" name="request_alias"
      value="1"/>
      Solicitar <b>ponto de referência</b> ao cliente no "Pedir e Retirar"
    </label>
  </div>
  @endif
  @if($config->allow_bills)
  <div class="checkbox col-sm-12">
    <label>
      <input type="checkbox" {{ ($config->has_baksheesh == 1) ? "checked" : "" }} data-toggle="toggle" data-on="Ativado" data-off="Desativado" name="has_baksheesh"
      value="1"/>
      Inserir <b>cobrança de taxa de serviço</b> no valor final da Conta
    </label>
  </div>
  {{-- <div class="checkbox col-sm-12">
    <label>
      <input type="checkbox" {{ ($config->has_couvert == 1) ? "checked" : "" }} data-toggle="toggle" data-on="Ativado" data-off="Desativado" name="has_couvert" value="1"/>
      Informar cobrança de Couvert Artístico
    </label>
  </div> --}}
  {{-- <div class="checkbox col-sm-6">
    <div class="form-group">
      <div class="input-group">
        <div class="input-group-addon">Valor do Couvert R$</div>
        <input type="text" name="couvert_price" value="{{ number_format($config->couvert_price, 2, ',', '.') }}"
        class="form-control mask-money"
        id="couvert_price"
        placeholder="00,00" step="0.01"/>
      </div>
    </div>
  </div> --}}
  <div class="checkbox col-sm-6">
    <div class="form-group">
      <div class="input-group">
        <div class="input-group-addon">Valor da Taxa de Serviço %</div>
        <input type="text" name="baksheesh_percentage" value="{{ number_format($config->baksheesh_percentage, 2, ',', '.') }}"
        class="form-control mask-money"
        id="baksheesh_percentage"
        placeholder="00,00" step="0.01"/>
      </div>
    </div>
  </div>
  @endif

  <div class="row">
    <div class="form-group col-sm-12">

      <p><h4>Dias em que o Estabelecimento estará disponível</h4></p>
      @foreach(App\Models\EstablishmentConfig::$days as $d => $day)
        <div class="checkbox-inline">
          <label>
            <input name="available_days[{{ $d }}]" {{ (in_array($d, $config->getAvailableDays())) ? 'checked' : '' }} value="true" type="checkbox"> {{ $day }}
          </label>
        </div>
      @endforeach

    </div>

    <div class="form-row ">
      <div class="col-sm-3">
        <label>Horário de início do primeiro turno</label>
        <input type="text" name="first_turn_begin" class="form-control mask-time" value="{{ $config->first_turn_begin }}" />
      </div>

      <div class="col-sm-3">
        <label>Horário de fim do primeiro turno</label>
        <input type="text" name="first_turn_end" class="form-control mask-time" value="{{ $config->first_turn_end }}" />
      </div>

      <div class="col-sm-3">
        <label>Horário de início do segundo turno</label>
        <input type="text" name="second_turn_begin" class="form-control mask-time" value="{{ $config->second_turn_begin }}" />
      </div>

      <div class="col-sm-3">
        <label>Horário de fim do segundo turno</label>
        <input type="text" name="second_turn_end" class="form-control mask-time" value="{{ $config->second_turn_end }}" />
      </div>
    </div>
  </div>

  <div class="row">
    <div class="form-group col-sm-12" style="height: 380px; padding-bottom: 50px;">
      <h4>Localização</h4>
      <div class="form-group">
        <input id="autocomplete" type="text" class="form-control" placeholder="Busque o local do seu estabelecimento" />
      </div>
      <div id="maps"></div>
    </div>
  </div>

  <div class="row" style="margin-top: 60px;">
    <div class="form-group col-sm-6">
      <div class="form-group">
        <label>Latitude</label>
        <input readonly type="text" name="location_lat" value="{{ $config->location_lat }}" data-toggle="toggle" data-on="Ativado" data-off="Desativado" class="form-control" />
      </div>
    </div>
    <div class="form-group col-sm-6">
      <div class="form-group">
        <label>Longitude</label>
        <input readonly type="text" name="location_long" value="{{ $config->location_long }}" data-toggle="toggle" data-on="Ativado" data-off="Desativado" class="form-control" />
      </div>
    </div>

    <div class="checkbox col-sm-12">
      <label>
        <input type="checkbox" {{ ($config->display_location == 1) ? "checked" : "" }} data-toggle="toggle" data-on="Ativado" data-off="Desativado" name="display_location"
        value="1"/>
        <b>PUBLICAR</b> Estabelecimento
      </label>
    </div>
    {{-- <div class="checkbox col-sm-12">
      <label>
        <input type="checkbox" {{ ($config->allow_bills == 1) ? "checked" : "" }} data-toggle="toggle" data-on="Ativado" data-off="Desativado" name="allow_bills"
        value="1"/>
        Liberar <b>Abertura de Conta</b>
      </label>
    </div>
    <div class="checkbox col-sm-12">
      <label>
        <input type="checkbox" {{ ($config->allow_pre_bills == 1) ? "checked" : "" }} data-toggle="toggle" data-on="Ativado" data-off="Desativado" name="allow_pre_bills"
        value="1"/>
        Liberar venda via <b>Pré-Pagamento</b>
      </label>
    </div> --}}
    {{-- <div class="checkbox col-sm-12">
      <label>
        <input type="checkbox" {{ ($config->disable_orders == 1) ? "checked" : "" }} data-toggle="toggle" data-on="Ativado" data-off="Desativado" name="disable_orders"
        value="1"/>
        Desabilitar <b>Pedidos</b> (Clientes só poderão visualizar os produtos dos cardápios)
      </label>
    </div> --}}

    <div class="form-group col-sm-12">
      <hr/>
      <button type="submit" class="btn btn-primary pull-right">Salvar</button>
    </div>
  </div>

</form>

@push('scripts')
<script type='text/javascript'>

setTimeout(function () {

    var latLng = {lat: -27.1546489, lng: -52.6051635};

    @if($config->location_lat && $config->location_long)
    latLng = {lat: {{$config->location_lat}}, lng: {{$config->location_long}}};
    @endif

    var map = new google.maps.Map(document.getElementById('maps'), {
      center: latLng,
      zoom: 12
    });
    var marker = new google.maps.Marker({position: latLng, map: map});
    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var place = autocomplete.getPlace();
      marker.setPosition({lat: place.geometry.location.lat(), lng: place.geometry.location.lng()})
      $("input[name=location_lat]").val(place.geometry.location.lat())
      $("input[name=location_long]").val(place.geometry.location.lng())
      map.setCenter({lat: place.geometry.location.lat(), lng: place.geometry.location.lng()})
    });

    google.maps.event.addListener(map, 'click', function(event) {
        marker.setPosition(event.latLng);
        $("input[name=location_lat]").val(event.latLng.lat())
        $("input[name=location_long]").val(event.latLng.lng())
    });
}, 2000);

</script>
@endpush
