@php($url = \Config::get('app.env') == 'homologation' ? 'https://testapp.tavollo.com/#/establishments/': 'https://app.tavollo.com/#/establishments/')
<h4><i class="fa fa-institution"></i> Estabelecimento</h4>
<span>Altere os dados do seu estabelecimento</span>
<hr/>
@include('plateau.includes.return-message')
<form method="post" action="{{ url('/profile/establishment-update') }}" enctype="multipart/form-data">
  {{ csrf_field() }}
  {{ method_field('PUT') }}
  @if($establishment->config->allow_pre_bills || (!$establishment->config->allow_pre_bills && $establishment->config->disable_orders))
  <div class="form-group col-sm-12">
    <label>Url de acesso ao cardápio (Pré-Pago)</label>
    <input readonly class="form-control" placeholder="Informe o nome do seu estabelecimento" type="text" name="slug" value="{{ $url . $establishment->slug }}"/>
  </div>
  <div class="form-group col-sm-12">
    <label>QR Code</label>
    <a style="display: block; width: 200px;" href="/profile/establishment/qrcode" target="_blank" class="btn btn-default">Download</a>
  </div>
  @endif
  <div class="form-group col-sm-12">
    <label>Nome Fantasia</label>
    <input required class="form-control" placeholder="Informe o nome do seu estabelecimento" type="text" name="name" value="{{ $establishment->name }}"/>
  </div>
  <div class="form-group col-sm-12">
    <label>Razão Social</label>
    <input class="form-control" type="text" placeholder="Informe a razão social do seu estabelecimento" name="fantasy_name" value="{{ $establishment->fantasy_name }}"/>
  </div>
  <div class="form-group col-sm-6">
    <label>CNPJ *</label>
    <input required class="form-control mask-cnpj" type="text" name="cnpj" value="{{ $establishment->cnpj }}"/>
  </div>
  <div class="form-group col-sm-6">
    <label>Telefone</label>
    <input class="form-control mask-phone" type="text" name="phone" value="{{ $establishment->phone }}"/>
  </div>
  <div class="form-group col-sm-12">
    <label>E-mail *</label>
    <input required class="form-control" placeholder="Informe um e-mail para contato" type="text" name="email" value="{{ $establishment->email }}"/>
  </div>
  <hr/>
  <div class="form-group col-sm-12">
    <hr/>
    <label>Endereço</label>
    <input class="form-control" type="text" name="address" value="{{ $establishment->address }}"/>
  </div>
  <div class="form-group col-sm-6">
    <label>Bairro</label>
    <input class="form-control" type="text" name="district" value="{{ $establishment->district }}"/>
  </div>
  <div class="form-group col-sm-6">
    <label>Cidade</label>
    <input class="form-control" type="text" name="city" value="{{ $establishment->city }}"/>
  </div>
  <div class="form-group col-sm-6">
    <label>Estado</label>
    <select name="state" class="form-control selectpicker"
    data-live-search="true" id="state" title="Selecione.." data-size="10">
    @foreach($estados as $sigla => $e)
      <option <?= ($establishment->state == $sigla) ? 'selected' : '' ?> value="{{ $sigla }}">{{ $e }}</option>
    @endforeach
  </select>
</div>
<div class="form-group col-sm-6">
  <label>CEP</label>
  <input class="form-control mask-cep" type="text" name="postal_code" value="{{ $establishment->postal_code }}"/>
</div>
<div class="form-group col-sm-12">
  <hr />
  <label>Descrição Curta</label>
  <input class="form-control" placeholder="insira uam descrição curta do seu estabelecimento" type="text" name="short_description" value="{{$establishment->short_description}}" />
</div>
<div class="form-group col-sm-12">
  <label>Descrição Completa</label>
  <textarea class="form-control" placeholder="Insira uma descrição mais completa do seu estabelecimento" name="description">{{$establishment->description}}</textarea>
</div>
<div class="form-group col-sm-12">
  <label>Tempo médio de entrega de Pedidos</label>
  <input class="form-control mask-time" placeholder="hh:mm" type="text" name="average_time" value="{{$establishment->average_time}}" />
</div>

<div class="col-md-12">
  <hr/>
</div>

@php($theme = $establishment->getThemeConfig())

<div class="form-group col-sm-12">
  <label>Esquema de cores</label>
  <select name="theme" class="form-control">
    <option value="">Selecione qual o estilo do seu tema</option>
    <option {{ $establishment->theme == 'dark' ? 'selected' : '' }} value="dark">Escuro</option>
    <option {{ $establishment->theme == 'light' ? 'selected' : '' }} value="light">Claro</option>
  </select>
</div>

<div class="form-group col-sm-3">
    <label>Cor Primária</label>
    <input required class="form-control" type="color" name="primary_color" value="{{ (isset($theme['primary_color'])) ? $theme['primary_color'] : '#3686FF' }}"/>
</div>

<div class="form-group col-sm-3">
    <label>Cor Secundária</label>
    <input required class="form-control" type="color" name="secondary_color" value="{{ (isset($theme['secondary_color'])) ? $theme['secondary_color'] : '#237BFF' }}"/>
</div>

<div class="form-group col-sm-3">
    <label>Cor de Título</label>
    <input required class="form-control" type="color" name="title_color" value="{{ (isset($theme['title_color'])) ? $theme['title_color'] : '#ffffff' }}"/>
</div>

<div class="form-group col-sm-3">
    <label>Cor de Texto</label>
    <input required class="form-control" type="color" name="text_color" value="{{ (isset($theme['text_color'])) ? $theme['text_color'] : '#646464' }}"/>
</div>

<div class="form-group col-sm-3">
    <label>Cor do Botão</label>
    <input required class="form-control" type="color" name="button_color" value="{{ (isset($theme['button_color'])) ? $theme['button_color'] : '#237BFF' }}"/>
</div>

<div class="form-group col-sm-3">
    <label>Cor de Texto do Botão</label>
    <input required class="form-control" type="color" name="button_text_color" value="{{ (isset($theme['button_text_color'])) ? $theme['button_text_color'] : '#ffffff' }}"/>
</div>

<div class="form-group col-sm-3">
    <label>Cor da Categoria</label>
    <input required class="form-control" type="color" name="category_text_color" value="{{ (isset($theme['category_text_color'])) ? $theme['category_text_color'] : '#FFBD17' }}"/>
</div>

<div class="form-row col-sm-12">
  <button class="btn btn-default" id="define-default">Restaurar para o Padrão</button>
  <hr />
</div>

<div class="form-group col-sm-4">
  <label>Logo do Estabelecimento (668x400)</label>
  <input class="form-control" type="file" name="logo"/>
</div>

<div class="form-group col-sm-4">
  <label>Imagem da listagem do App (400x400)</label>
  <input class="form-control" type="file" name="image"/>
</div>

<div class="form-group col-sm-4">
  <label>Imagem do background do Login(600x1212)</label>
  <input class="form-control" type="file" name="background"/>
</div>

<div class="form-group col-sm-4">
  @if($establishment->logo)
    <label>Logo</label>
    <img src="{{ $establishment->logo }}"  style="display: block; width: 100%;  height: auto;"/>
  @endif
</div>

<div class="form-group col-sm-4">
  @if($establishment->image)
    <label>Imagem</label>
    <img src="{{ $establishment->image }}"  style="display: block; width: 100%;  height: auto;"/>
  @endif
</div>

<div class="form-group col-sm-4">
  @if($establishment->background)
    <label>Background</label>
    <img src="{{ $establishment->background }}"  style="display: block; width: 100%;  height: auto;"/>
  @endif
</div>

<div class="form-group col-sm-12">
  <hr/>
  <button type="submit" class="btn btn-primary pull-right">Salvar</button>
</div>
</form>

@push('scripts')
<script type="text/javascript">
  $('#define-default').on('click', function(e){
    e.preventDefault();
    $('input[name=primary_color]').val('#3686FF');
    $('input[name=secondary_color]').val('#237BFF');
    $('input[name=title_color]').val('#ffffff');
    $('input[name=text_color]').val('#646464');
    $('input[name=button_color]').val('#237BFF');
    $('input[name=button_text_color]').val('#ffffff');
    $('input[name=category_text_color]').val('#FFBD17')
  })
</script>
@endpush
