<h3><i class="fa fa-question-circle"></i> Tutoriais</h3>
<span>Assista aos tutoriais abaixo e tire suas dúvidas sobre o Tavollo.</span>
<hr/>
<div id="accordion">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h3 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Introdução
        </button>
      </h3>
    </div>

    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        <p>Neste vídeo você aprenderá como editar o nome de cadastro e usuário e como modificar a sua senha. Será explicado também como fazer as configurações de uso do token de mesa ou cartão.
          Como configurar a cobrança da taxa de serviço e couvert artístico e cadastrar a localização do seu estabelecimento.</p>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/_8iSsfk2MfE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingTwo">
      <h3 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Categorias
        </button>
      </h3>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        <p>Neste vídeo será ensinado como cadastrar a categoria de produtos que é a divisão responsável por separar produtos de um mesmo tipo. Nos próximos vídeos será ensinado como atribuir uma categoria a um cardápio.</p>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/uH7-8BRBRFw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingThree">
      <h3 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Produtos
        </button>
      </h3>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <p>Neste vídeo veremos como fazer o cadastro de produtos, como adicionar um preço promocional a um item, informar se o produto está ou não ativo,
          se este produtos faz parte do cálculo da gorjeta e a programar a disponibilidade dos itens e os turnos de trabalho.</p>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/2aYuU7HbU4w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingFour">
      <h3 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
          Cardápio
        </button>
      </h3>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
      <div class="card-body">
        <p>Aprenderemos com cadastrar um cardápio, que é a principal divisão de características dos produtos e como vincular um produto ou uma categoria a esse cardápio.</p>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/xfDN0FKpBLY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingFive">
      <h3 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
          Spots: Mesas e Cartões
        </button>
      </h3>
    </div>
    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
      <div class="card-body">
        <p>Neste vídeo será demonstrado como fazer o cadastro de um spot, ou seja, uma mesa ou cartão consumo. Como configurar a geração de token que é uma chave temporária ou permanente para o spot e como gerar um qrcode para acesso aos spots.</p>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/sv92YFxWkz8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingSix">
      <h3 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
          Pontos de Referência
        </button>
      </h3>
    </div>
    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
      <div class="card-body">
        <p>Neste vídeo aprenderemos como cadastrar pontos de referências para que quando o cliente faça o pedido possamos identificar o local no qual ele está e fazer a entrega.</p>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/pe1cX1jIcG0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header" id="headingSeven">
      <h3 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
          Ilhas: Como gerenciar as praças e garçons
        </button>
      </h3>
    </div>
    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
      <div class="card-body">
        <p>Ensinaremos como cadastrar ilhas, ou praças, e vincular essas praças as mesas e a garçons que sejam responsáveis por essas praças.</p>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/7IRrkhZMRFk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
h3 button{
  font-weight: bold !important;
}
</style>
