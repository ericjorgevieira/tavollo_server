<h4><i class="fa fa-link"></i> Integração</h4>
<h5>
  Configure os dados da integração com o seu PDV.
  <b>Consulte a empresa responsável para maiores detalhes.</b>
</h5>
<hr/>
@include('plateau.includes.return-message')
<div class="container-fluid">
  <div class="row">
    <form method="post" action="{{ url('/profile/integration/update') }}">
      {{ csrf_field() }}
      {{ method_field('PUT') }}
      <div class="form-group col-sm-12">
        <label>Software</label>
        <select name="integration_id" class="form-control selectpicker" data-live-search="true" id="selectCategoria" title="Selecione.." data-size="10">
          @foreach($integrations as $e)
            <option <?= ($establishmentIntegration->integration_id == $e->id) ? 'selected' : '' ?> value="{{ $e->id }}">
              {{ $e->name . ' - ' . $e->company }}
            </option>
          @endforeach
        </select>
      </div>
      <div class="form-group col-sm-6">
        <label>Usuário</label>
        <input class="form-control" type="text" name="credential_user" value="{{ $establishmentIntegration->credential_user }}"/>
      </div>
      <div class="form-group col-sm-6">
        <label>Senha</label>
        <input class="form-control" type="password" name="credential_password" value="{{ $establishmentIntegration->credential_password }}"/>
      </div>
      <div class="form-group col-sm-12">
        <label>URL padrão da API</label>
        <input class="form-control" type="text" name="api_url" value="{{ $establishmentIntegration->api_url }}"/>
      </div>
      <div class="form-group col-sm-6">
        <label>Token da API</label>
        <input class="form-control" type="text" name="api_token" value="{{ $establishmentIntegration->api_token }}"/>
      </div>
      <div class="checkbox col-sm-12">
        <label>
          <input type="checkbox" <?= ($establishmentIntegration->is_active == 1) ? "checked" : "" ?> name="is_active"
          value="1"/>
          Ativar Integração?
        </label>
      </div>
      <div class="form-group col-sm-12">
        <hr/>
        <button type="submit" class="btn btn-primary">Salvar</button>
        @if($establishmentIntegration->api_url)
          <a href="{{ url('/profile/integration/test') }}" class="btn btn-warning">Testar Conexão</a>
        @endif
      </div>
    </form>
  </div>
</div>
