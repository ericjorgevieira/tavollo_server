<h4><i class="fa fa-gear"></i> Ilhas</h4>
<span>Gerencie o agrupamento de mesas e defina os responsáveis</span>
<hr/>
<div class="container-fluid">
  @include('plateau.includes.return-message')
  <div class="row">
    <form method="post" action="{{ url('/profile/islands/' . $island->id . '/update') }}">
      {{ csrf_field() }}
      {{ method_field('PUT') }}

      <div class="col-xs-12">
        <div class="form-group">
          <label for="inputNome">Nome (*)</label>
          <input required type="text" name="name" id="inputNome" class="form-control"
          placeholder="Digite o nome da Ilha" value="{{ $island->name }}">
        </div>
        <div class="form-group">
          <label for="inputNome">Garçom Responsável (*)</label>
          <select required name="waiter_id" class="form-control selectpicker"
          data-live-search="true" id="selectCategoria" title="Selecione.." data-size="10">
          @foreach($waiters as $w)
            <option <?= ($w->id == $island->waiter_id) ? 'selected' : '' ?> value="{{ $w->id }}">{{ $w->register . " - " . $w->name }}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-xs-12">
      <p><h5><b>Dias em que a Ilha estará disponível</b></h5></p>
      @foreach(App\Models\Island::$days as $d => $day)
        <div class="checkbox-inline">
          <label>
            <input name="available_days[{{ $d }}]" <?= (in_array($d, $island->getAvailableDays())) ? 'checked' : '' ?> value="true" type="checkbox"> {{ $day }}
          </label>
        </div>
      @endforeach
    </div>
    <div class="col-xs-6">
      <div class="form-group">
        <label for="inputNome">Inicio</label>
        <input type="text" name="opened_at" id="inputNome" class="form-control mask-hour"
        placeholder="00:00:00" value="{{ $island->opened_at }}">
      </div>
    </div>
    <div class="col-xs-6">
      <div class="form-group">
        <label for="inputNome">Encerramento</label>
        <input type="text" name="closed_at" id="inputNome" class="form-control mask-hour"
        placeholder="00:00:00" value="{{ $island->closed_at }}">
      </div>
    </div>
    <div class="col-xs-12">
      <div class="form-group">
        <label for="textareaDescricao">Spots / Spots da Ilha (Todos os Spots com o mesmo número serão associados à Ilha)</label>
        <select class="form-control multi-select" name="tables[]" multiple="multiple">
          <?php foreach($listTables as $p){ ?>
            <option <?= (in_array($p->id, $islandTables)) ? "selected" : "" ?>
              value="{{ $p->id }}">{{ $p->name }}</option>
              <?php } ?>
            </select>
          </div>
          <div class="checkbox">
            <label>
              <input <?= ($island->status == 'enabled') ? 'checked' : '' ?>
              name="status" value="enabled" type="checkbox"> Ativo?
            </label>
          </div>
        </div>
        <button type="submit" class="pull-right btn btn-success">Salvar</button>
      </form>
    </div>
  </div>
