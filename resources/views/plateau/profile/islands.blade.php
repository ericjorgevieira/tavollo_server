<h4><i class="fa fa-gear"></i> Ilhas</h4>
<span>Gerencie o agrupamento de mesas e defina os responsáveis</span>
<hr/>
@include('plateau.includes.return-message')
<div class="container-fluid">
    <div class="row">
        <a class="btn btn-success" href="{{ url('/profile/islands/create') }}" role="button">
            <i class="fa fa-plus-circle"></i> Cadastrar Ilha
        </a>
    </div>
    <div class="row">
        <div class="straight">
            <Smarttable :url='"<?= $url ?>"' :has-edit-action='true' :has-disable-action='true'></Smarttable>
        </div>
    </div>
</div>
