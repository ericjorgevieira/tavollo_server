<h4><i class="fa fa-money"></i> Pagamento</h4>
<span>Configure sua conta da <a href="https://developercielo.github.io/" target="_blank">Cielo</a></span>
<hr/>
@include('plateau.includes.return-message')
<form method="post" action="{{ url('/profile/payment') }}" enctype="multipart/form-data">
  {{ csrf_field() }}

  <div class="form-group col-sm-6">
    <label>Merchant Id</label>
    <input {{ ($enable_payment == 1) ? 'required' : '' }} class="form-control" type="text" name="merchant_id" value="{{(isset($account)) ? $account['MerchantId'] : '' }}"/>
  </div>
  <div class="form-group col-sm-6">
    <label>Merchant Key</label>
    <input {{ ($enable_payment == 1) ? 'required' : '' }} class="form-control" type="text" name="merchant_key" value="{{(isset($account)) ? $account['MerchantKey'] : '' }}"/>
  </div>
  <div class="checkbox col-sm-6">
    <label>
      <input name="enable_payment" {{ ($enable_payment == 1) ? 'checked' : '' }} value="enabled" type="checkbox"> Habilitar Pagamento via Autoatendimento?
    </label>
  </div>

  <hr/>

  <div class="form-group col-sm-12">
    <div class="form-group">
      <label>Url de Extrato de Comandas</label>
      <textarea placeholder="https://url.com?token=12345&external_id=(?)&is_consumption_card=(?)"
        class="form-control" name="bill_extract_url">{{$establishment->config->bill_extract_url}}</textarea>
    </div>
  </div>

  <div class="form-group col-sm-12">
    <hr/>
    <button type="submit" class="btn btn-primary pull-right">Salvar</button>
  </div>
</form>

@push('scripts')
<script type="text/javascript">
  $('input[name=enable_payment]').on('change', function(e){
    var $this = $(this);
    if($this.is(':checked')){
      $('input[name=merchant_id]').attr('required', 'required');
      $('input[name=merchant_key]').attr('required', 'required');
    }else{
      $('input[name=merchant_id]').removeAttr('required');
      $('input[name=merchant_key]').removeAttr('required');
    }
  })
</script>
@endpush
