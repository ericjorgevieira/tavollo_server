<h4><i class="fa fa-print"></i> Impressora</h4>
<span>Veja opções para configuração de impressão. Você deve baixar e configurar o <a target="_blank" href="https://qz.io/download/">QZ Printer 2.0</a>.</span>
<hr/>
@include('plateau.includes.return-message')
<form method="post" action="{{ url('/profile/printer') }}">
  {{ csrf_field() }}

  <div class="row">

    <div class="col-sm-6">
      <div class="form-group">
        <label>Nome da Impressora</label>
        <input class="form-control" type="text" name="printer_name" value="{{ ($config) ? $config->printer_name : '' }}" />
      </div>
    </div>

    <div class="col-sm-6">
      <div class="form-group">
        <label>IP da Impressora</label>
        <input class="form-control" type="text" name="printer_ip" value="{{ ($config) ? $config->printer_ip : '' }}" />
      </div>
    </div>

    <div class="form-group col-sm-12">
      <hr/>
      <button type="submit" class="btn btn-primary pull-right">Salvar</button>
    </div>
  </div>

</form>
