<h4><i class="fa fa-bookmark"></i> Setores</h4>
<span>Gerencie os setores para filtragem de pedidos do seu estabelecimento</span>
<hr/>
<div class="container-fluid">
    @include('plateau.includes.return-message')
    <div class="row">
        <form method="post" action="{{ url('/profile/sectors/store') }}">
            {{ csrf_field() }}
            {{ method_field('POST') }}

            <div class="col-xs-12">
              <div class="form-group">
                  <label for="inputNome">Nome (*)</label>
                  <input required type="text" name="name" id="inputNome" class="form-control"
                  placeholder="Digite o nome do Setor">
              </div>
              <div class="form-group">
                  <label for="textareaDescricao">Produtos / Preparados no Setor</label>
                  <select class="form-control multi-select" name="products[]" multiple="multiple">
                    @foreach($listProducts as $cat)
                      <optgroup label="{{ $cat['productCategory']['name'] }}">
                          @foreach($cat['products'] as $p)
                            <option value="{{ $p['id'] }}">{{ $p['name'] }}</option>
                          @endforeach
                      </optgroup>
                    @endforeach
                  </select>
                </div>
                <div class="checkbox">
                    <label>
                        <input name="status" checked value="enabled" type="checkbox"> Ativo?
                    </label>
                </div>
            </div>
            @if(Auth::user()->establishment->config->enable_advanced_functions)
            <div class="col-xs-12">
              <hr />
              <div class="checkbox">
                  <label>
                      <input checked name="use_default_printer" value="1" type="checkbox"> User impressora padrão?
                  </label>
              </div>
            </div>

            <div class="col-xs-6">
              <div class="form-group">
                <label>Nome da Impressora</label>
                <input readonly type="text" class="form-control" name="printer_name" value="" />
              </div>
            </div>
            <div class="col-xs-6">
              <div class="form-group">
                <label>Ip da Impressora</label>
                <input readonly type="text" class="form-control" name="printer_ip" value="" />
              </div>
            </div>
            @endif
            <button type="submit" class="pull-right btn btn-success">Salvar</button>
        </form>
    </div>
</div>

@push('scripts')
  <script type="text/javascript">

    $('input[name=use_default_printer]').on('change', function(e){
        if($(this).is(':checked')){
          $('input[name=printer_name]').attr('readonly', 'readonly');
          $('input[name=printer_ip]').attr('readonly', 'readonly');
        }else{
          $('input[name=printer_name]').removeAttr('readonly');
          $('input[name=printer_ip]').removeAttr('readonly');
        }
    });

  </script>
@endpush
