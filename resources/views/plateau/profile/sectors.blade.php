<h4><i class="fa fa-bookmark"></i> Setores</h4>
<span>Gerencie os setores para filtragem de pedidos do seu estabelecimento</span>
<hr/>
@include('plateau.includes.return-message')
<div class="container-fluid">
    <div class="row">
        <a class="btn btn-success" href="{{ url('/profile/sectors/create') }}" role="button">
            <i class="fa fa-plus-circle"></i> Cadastrar Setor
        </a>
    </div>
    <div class="row">
        <div class="straight">
            <Smarttable :url='"<?= $url ?>"' :has-edit-action='true' :has-disable-action='true'></Smarttable>
        </div>
    </div>
</div>
