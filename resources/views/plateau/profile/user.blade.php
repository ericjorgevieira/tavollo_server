<h4><i class="fa fa-user"></i> Usuário</h4>
<h5>Altere os dados do seu perfil de usuário</h5>
<hr/>
@include('plateau.includes.return-message')
<div class="container-fluid">
  <div class="row">
    <form method="post" action="{{ url('/profile/update') }}">
      {{ csrf_field() }}
      {{ method_field('PUT') }}
      <div class="form-group col-sm-12">
        <label>Nome</label>
        <input class="form-control" type="text" name="name" value="{{ $profile->name }}"/>
      </div>
      <div class="form-group col-sm-12">
        <label>E-mail</label>
        <input class="form-control" type="text" name="email" value="{{ $profile->email }}"/>
      </div>
      <hr/>
      <div class="form-group col-sm-12">
        <hr/>
        <button type="submit" class="btn btn-primary pull-right">Salvar</button>
      </div>
    </form>
  </div>
</div>
