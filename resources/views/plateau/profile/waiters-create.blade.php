<h4><i class="fa fa-users"></i> Garçons</h4>
<span>Gerencie os profissionais de atendimento do seu estabelecimento</span>
<hr/>
<div class="container-fluid">
  @include('plateau.includes.return-message')
  <div class="row">
    <form method="post" action="{{ url('/profile/waiters/store') }}">
      {{ csrf_field() }}
      {{ method_field('POST') }}

      <div class="col-xs-12">
        <div class="form-group">
          <label for="inputNome">Nome (*)</label>
          <input required type="text" name="name" id="inputNome" class="form-control"
          placeholder="Informe o nome do Garçom" value="{{ old('name') }}">
        </div>
        <div class="form-group">
          <label for="inputNome">Matrícula/Registro (*)</label>
          <input required type="number" name="register" id="inputNome" class="form-control"
          placeholder="Informe o código de identificação do Garçom" value="{{ old('register') }}">
        </div>
      </div>
      <div class="form-group col-sm-6">
          <label>Nova Senha</label>
          <input required class="form-control" type="password" name="new_password" />
      </div>
      <div class="form-group col-sm-6">
          <label>Confirme a Nova Senha</label>
          <input required class="form-control" type="password" name="new_password_confirmation" value=""/>
      </div>
      <div class="col-xs-4">
        <div class="form-group">
          <label for="inputNome">Código de Identificação</label>
          <input type="text" name="code" id="inputNome" class="form-control" value="{{ old('code') }}"
          placeholder="Informe um código para identificar o Garçom">
        </div>
      </div>
      <div class="col-xs-4">
        <div class="form-group">
          <label for="inputNome">Inicio do Expediente</label>
          <input type="text" name="work_begin" id="inputNome" class="form-control mask-hour" value="{{ old('work_begin') }}"
          placeholder="00:00:00">
        </div>
      </div>
      <div class="col-xs-4">
        <div class="form-group">
          <label for="inputNome">Fim do Expediente</label>
          <input type="text" name="work_end" id="inputNome" class="form-control mask-hour" value="{{ old('work_end') }}"
          placeholder="00:00:00">
        </div>
      </div>
      <div class="col-xs-12">
        <div class="checkbox">
          <label>
            <input name="is_default" checked value="1" type="checkbox" data-toggle="toggle" data-on="Ativado" data-off="Desativado"> <b>Garçom Padrão?</b>
          </label>
        </div>
        <div class="checkbox">
          <label>
            <input name="status" checked value="enabled" type="checkbox" data-toggle="toggle" data-on="Ativado" data-off="Desativado"> Garçom Ativo?
          </label>
        </div>
      </div>
      <button type="submit" class="pull-right btn btn-success">Salvar</button>
    </form>
  </div>
</div>
