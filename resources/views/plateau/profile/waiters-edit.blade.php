<h4><i class="fa fa-users"></i> Garçons</h4>
<span>Gerencie os profissionais de atendimento do seu estabelecimento</span>
<hr/>
<div class="container-fluid">
  @include('plateau.includes.return-message')
  <div class="row">
    <form method="post" action="{{ url('/profile/waiters/' . $waiter->id . '/update') }}">
      {{ csrf_field() }}
      {{ method_field('PUT') }}

      <div class="col-xs-12">
        <div class="form-group">
          <label for="inputNome">Nome (*)</label>
          <input required type="text" name="name" id="inputNome" class="form-control"
          placeholder="Informe o nome do Garçom" value="{{ $waiter->name }}">
        </div>
        <div class="form-group">
          <label for="inputNome">Login</label>
          <input readonly type="text" name="email" id="inputNome" class="form-control"
           value="{{ ($waiter->user) ? $waiter->user->email : '' }}">
        </div>
        <div class="form-group">
          <label for="inputNome">Matrícula/Registro (*)</label>
          <input required type="number" name="register" id="inputNome" class="form-control"
          placeholder="Informe o código de identificação do Garçom" value="{{ $waiter->register }}">
        </div>
      </div>
      <div class="form-group col-sm-6">
          <label>Nova Senha</label>
          <input class="form-control" type="password" name="new_password" value=""/>
      </div>
      <div class="form-group col-sm-6">
          <label>Confirme a Nova Senha</label>
          <input class="form-control" type="password" name="new_password_confirmation" value=""/>
      </div>
      <div class="col-xs-4">
        <div class="form-group">
          <label for="inputNome">Código de Identificação</label>
          <input type="text" name="code" id="inputNome" class="form-control"
          placeholder="Informe um código para identificar o Garçom" value="{{ $waiter->code }}">
        </div>
      </div>
      <div class="col-xs-4">
        <div class="form-group">
          <label for="inputNome">Inicio do Expediente</label>
          <input type="text" name="work_begin" id="inputNome" class="form-control mask-hour"
          placeholder="00:00:00" value="{{ $waiter->work_begin }}">
        </div>
      </div>
      <div class="col-xs-4">
        <div class="form-group">
          <label for="inputNome">Fim do Expediente</label>
          <input type="text" name="work_end" id="inputNome" class="form-control mask-hour"
          placeholder="00:00:00" value="{{ $waiter->work_end }}">
        </div>
      </div>
      <div class="col-xs-12">
        <div class="checkbox">
          <label>
            <input <?= ($waiter->is_default == 1) ? 'checked' : '' ?>
            name="is_default" value="1" type="checkbox" data-toggle="toggle" data-on="Ativado" data-off="Desativado"> <b>Garçom Padrão?</b>
          </label>
        </div>
        <div class="checkbox">
          <label>
            <input <?= ($waiter->status == 'enabled') ? 'checked' : '' ?>
            name="status" value="enabled" type="checkbox" data-toggle="toggle" data-on="Ativado" data-off="Desativado"> Garçom Ativo?
          </label>
        </div>
      </div>
      <button type="submit" class="pull-right btn btn-success">Salvar</button>
    </form>
  </div>
</div>
