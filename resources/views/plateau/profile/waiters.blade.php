<h4><i class="fa fa-users"></i> Garçons</h4>
<span>Gerencie os profissionais de atendimento do seu estabelecimento</span>
<hr/>
@include('plateau.includes.return-message')
<div class="container-fluid">
    <div class="row">
        <a class="btn btn-success" href="{{ url('/profile/waiters/create') }}" role="button">
            <i class="fa fa-plus-circle"></i> Cadastrar Garçom
        </a>
    </div>
    <div class="row">
        <div class="straight">
            <Smarttable :url='"<?= $url ?>"' :has-edit-action='true' :has-disable-action='true' :has-delete-action='true'></Smarttable>
        </div>
    </div>
</div>
