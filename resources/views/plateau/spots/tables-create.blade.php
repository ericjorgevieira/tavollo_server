@extends('layouts.plateau')

@section('content')
  <div id="tables">

    <h2>Cadastro de Spots</h2>

      <div class="container-fluid">
        <div class="row">
          @include('plateau.includes.return-message')
        </div>
        <div class="row">
          <form method="post" action="{{ url('/tables/store') }}">
            {{ csrf_field() }}
            {{ method_field('POST') }}

            <div class="col-xs-6">
              <div class="form-group">
                <label for="inputNome">Nome</label>
                <input required type="text" name="name" id="inputNome" class="form-control"
                placeholder="Digite o nome da Mesa">
              </div>
            </div>
            <div class="col-xs-3">
              <div class="form-group">
                <label for="inputNumero">Número</label>

                <input required name="number" type="number" class="form-control"
                id="inputNumero"
                placeholder="10">
              </div>
            </div>
            <div class="col-xs-3">
              <div class="form-group">
                <label for="inputNumero">Código Externo</label>

                <input name="external_id" type="text" class="form-control"
                id="inputNumero"
                placeholder="10">
              </div>
            </div>
            <div class="col-xs-12">
              <div class="form-group">
                <label for="textareaDescricao">Descrição</label>
                <textarea name="description" class="form-control" rows="3"></textarea>
              </div>
              <div class="checkbox">
                <label>
                  <input name="is_consumption_card" checked value="1" type="checkbox" data-toggle="toggle" data-on="Cartão" data-off="Mesa">
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <input name="status" checked value="enabled" type="checkbox" data-toggle="toggle" data-on="Ativado" data-off="Desativado"> Spot Ativo?
                </label>
              </div>
            </div>
            <div class="col-xs-12">
              <button type="submit" class="pull-right btn btn-default">Salvar</button>
              <button type="submit" name="save_and_add" value="1"
              class="pull-right btn btn-success">Salvar e Adicionar Novo</button>
            </div>
            
          </form>
        </div>
      </div>
    </div>

  @endsection
