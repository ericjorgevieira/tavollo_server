@extends('layouts.plateau')

@section('content')
  <div id="tables">
    <h2>Edição de Spots</h2>

    <div class="container-fluid">
      <div class="row">
        @include('plateau.includes.return-message')
      </div>
      <div class="row">
        <form method="post" action="{{ url('/tables/' . $table->id . '/update') }}">
          {{ csrf_field() }}
          {{ method_field('PUT') }}

          <div class="col-xs-6">
            <div class="form-group">
              <label for="inputNome">Nome</label>
              <input required type="text" name="name" id="inputNome" class="form-control"
              placeholder="Digite o nome da Mesa" value="{{ $table->name }}">
            </div>
          </div>
          <div class="col-xs-3">
            <div class="form-group">
              <label for="inputNumero">Número</label>

              <input required name="number" type="number" class="form-control"
              id="inputNumero"
              placeholder="10" value="{{ $table->number }}">
            </div>
          </div>
          <div class="col-xs-3">
            <div class="form-group">
              <label for="inputNumero">Código Externo</label>

              <input name="external_id" type="text" class="form-control"
              id="inputNumero"
              placeholder="10" value="{{ $table->external_id }}">
            </div>
          </div>
          <div class="col-xs-12">
            <div class="form-group">
              <label for="textareaDescricao">Descrição</label>
              <textarea name="description" class="form-control"
              rows="3">{{ $table->description }}</textarea>
            </div>
            <div class="checkbox">
              <label>
                <input <?= ($table->is_consumption_card == '1') ? 'checked' : '' ?>
                name="is_consumption_card" value="1" type="checkbox" data-toggle="toggle" data-on="Cartão" data-off="Mesa">
              </label>
            </div>
            <div class="checkbox">
              <label>
                <input <?= ($table->status == 'enabled') ? 'checked' : '' ?>
                name="status" value="enabled" type="checkbox" data-toggle="toggle" data-on="Ativado" data-off="Desativado"> Spot Ativo?
              </label>
            </div>
          </div>
          <div class="col-xs-12">
            <button type="submit" class="pull-right btn btn-success">Salvar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
