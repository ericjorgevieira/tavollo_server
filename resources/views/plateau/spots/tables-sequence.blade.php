@extends('layouts.plateau')

@section('content')
  <div id="tables">

    <h2>Sequência de Spots</h2>

      <div class="container-fluid">
        <div class="row">
          @include('plateau.includes.return-message')
        </div>
        <div class="row">
          <form method="post" action="{{ url('/tables/sequence') }}">
            {{ csrf_field() }}
            {{ method_field('POST') }}

            <div class="col-xs-9">
              <div class="form-group">
                <label for="inputNome">Pré Nome:</label>
                <input type="text" name="pre_name" id="inputNome" class="form-control"
                placeholder="Digite o nome que antecederá os registros. Ex: MESA">
              </div>
            </div>
            <div class="col-xs-4">
              <div class="form-group">
                <label for="inputNumeroInicial">Número Inicial</label>

                <input required name="initial_number" type="number" class="form-control"
                id="inputNumeroInicial"
                placeholder="10">
              </div>
            </div>
              <div class="col-xs-4">
              <div class="form-group">
                <label for="inputNumeroFinal">Número Final</label>

                <input required name="final_number" type="number" class="form-control"
                id="inputNumeroFinal"
                placeholder="10">
              </div>
              </div>
            <div class="col-xs-12">
              <div class="checkbox">
                <label>
                  <input name="is_consumption_card" checked value="1" type="checkbox"> <b>São Cartões Consumo?</b>
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <input name="status" checked value="enabled" type="checkbox"> Ativos?
                </label>
              </div>
            </div>
            <button type="submit" class="pull-right btn btn-default">Salvar</button>
          </form>
        </div>
      </div>
    </div>

  @endsection
