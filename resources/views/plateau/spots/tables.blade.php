@extends('layouts.plateau')
@php
$config = Auth::user()->establishment->config;
@endphp
@section('content')
  <div id="tables">
    <h2>Spots</h2>

    <div class="row">
      <div class="straight">

        @include('plateau.includes.return-message')

          @if($config->enable_advanced_functions == 1)
          <a class="btn btn-success" href="{{ url('/tables/create') }}" role="button">
            <i class="fa fa-plus-circle"></i> Cadastrar Spot
          </a>

          <a class="btn btn-default" href="{{ url('/tables/sequence') }}" role="button">
            <i class="fa fa-plus-circle"></i> Cadastrar Sequência
          </a>
          @endif

          <a onclick="confirmGenerate(event)" class="btn btn-default" href="{{ url('/tables/generate') }}" role="button">
            <i class="fa fa-recycle"></i> Gerar TOKENS
          </a>

          <a class="btn btn-default" href="{{ url('/tables/qr-code') }}" role="button">
            <i class="fa fa-download"></i> Download de TOKENS
          </a>

        </div>
      </div>

      <div class="row">
        <div class="straight">
          <Smarttable
          :url='"<?= $url ?>"'
          :has-edit-action='true'
          :has-disable-action='true'>
        </Smarttable>
      </div>
    </div>
  </div>
@endsection

<script type="text/javascript">

function confirmGenerate(event)
{
  event = event || window.event;
  event.preventDefault();
  console.log(event);

  var confirm = window.confirm('Tem certeza que deseja gerar novos tokens? Os atuais ficarão inutilizados caso QR codes tenham sido gerados!');
  if(confirm === true){
    window.location.href = '{{ url('/tables/generate') }}';
  }
}

</script>
