<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$env = \Config::get('app.env');
$urlProduction = ($env == 'homologation') ? 'test.api.tavollo.com' : 'api.tavollo.com';
$url = ($env == 'local' || $env == 'testing') ? 'api.tavollo.localhost' : $urlProduction;

$app->group(['domain' => $url, 'namespace' => 'API', 'middleware' => 'api.cors'], function () use ($app) {
    $app->post('auth', 'AuthController@auth');
});

$app->group(['domain' => $url, 'namespace' => 'API', 'middleware' => ['api.cors', 'api.user']], function () use ($app) {
    $app->get('/', 'AuthController@index');

    //Retorna lista de requisições de Token
    $app->get('table-requests', 'SpotsController@spotRequests');

    //Atualiza status de requisição de Token
    $app->post('update-table-request/{tableRequest}', 'SpotsController@updateSpotRequest');

    //Retorna os Spots do estabelecimento
    $app->get('tables', 'SpotsController@listSpots');

    //Retorna o Spot pelo número da mesa
    $app->get('table', 'SpotsController@getSpot');

    //Atualiza os dados de um Spot
    $app->post('table/update/{table?}', 'SpotsController@updateSpot');

    $app->post('tables/update-many', 'SpotsController@updateManySpots')->middleware('throttle:1,1');

    //Atualiza apenas o alias de um Spot
    $app->post('table/alias/{table}', 'SpotsController@setSpotAlias');

    //Retorna a lista de Contas do Estabelecimento
    $app->get('bills/{bill?}', 'BillsController@bills');

    //Abre uma nova Conta sem usuário no Estabelecimento
    $app->post('bills/create', 'BillsController@createBill');

    //Migra os pedidos de uma Conta para outra
    $app->post('bills/migrate/{bill}/{newBill}', 'BillsController@migrateBill');

    //Atualiza o status de uma Conta
    $app->post('bills/update/{bill}', 'BillsController@updateBill');

    //Retorna a lista de Pedidos do Estabelecimento
    $app->get('orders/{bill?}', 'OrdersController@orders');

    //Cria um novo Pedido em uma determinada Conta
    $app->post('orders/create/{bill}', 'OrdersController@createOrder');

    //Altera o status de um Pedido em uma determinada Conta
    $app->post('orders/update/{order}', 'OrdersController@updateOrder')->middleware('throttle:20,1');

    //Altera o status de vários Pedidos
    $app->post('orders/update-many', 'OrdersController@updateManyOrders');

    //Retorna a lista de Pedidos Pre Pagos do Estabelecimento
    $app->get('pre-orders', 'PreOrdersController@preOrders');

    //Altera o status de um Pedido Pre Pago em um determinada Estabelecimento
    $app->post('pre-orders/update/{preorder}', 'PreOrdersController@updatePreOrder')->middleware('throttle:20,1');

    //Altera o status de vários Pedidos Pre Pagos
    $app->post('pre-orders/update-many', 'PreOrdersController@updateManyPreOrders');

    //Retorna a lista de Produtos do Estabelecimento
    $app->get('products/{product?}', 'ProductsController@products');

    //Atualiza um Produto específico
    $app->post('products/update/{product?}', 'ProductsController@updateProduct');

    $app->post('products/update-many', 'ProductsController@updateManyProducts')->middleware('throttle:1,1');

    //Lista os garçons cadastrados no Tavollo
    $app->get('waiters/{waiter?}', 'AuthController@waiters');
});
