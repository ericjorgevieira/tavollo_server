<?php

/**
 * Created by PhpStorm.
 * User: ericoliveira
 * Date: 12/12/16
 * Time: 22:56.
 */
$env = \Config::get('app.env');
$urlProduction = ($env == 'homologation') ? 'test.master.tavollo.com' : 'master.tavollo.com';
$url = ($env == 'local' || $env == 'testing') ? 'master.tavollo.localhost' : $urlProduction;

$app->group(['domain' => $url, 'namespace' => 'Master', 'middleware' => 'master.guest'], function () use ($app) {
    $app->get('/', ['uses' => 'AuthController@showLoginForm']);
    $app->post('login', ['uses' => 'AuthController@login']);
});

// Master panel namespace
$app->group(
    ['domain' => $url, 'namespace' => 'Master', 'middleware' => ['web', 'master.auth', 'is.admin']],
    function () use ($app) {
        $app->get('home', ['uses' => 'HomeController@index']);
        $app->get('home/dashboard', ['uses' => 'HomeController@dashboard']);

        $app->get('ratings', ['uses' => 'RatingsController@listRatings']);

        $app->get('categories', ['uses' => 'HomeController@categories']);
        $app->get('categories/trash', ['uses' => 'HomeController@categoriesTrash']);
        $app->get('categories/create', ['uses' => 'HomeController@categoriesCreate']);
        $app->get('categories/{product_category}/edit', ['uses' => 'HomeController@categoriesEdit']);
        $app->post('categories/store', ['uses' => 'HomeController@categoriesStore']);
        $app->put('categories/{product_category}/enable', ['uses' => 'HomeController@categoriesEnable']);
        $app->put('categories/{product_category}', ['uses' => 'HomeController@categoriesUpdate']);

        $app->get('establishments', ['uses' => 'EstablishmentsController@index']);
        $app->get('establishments/create', ['uses' => 'EstablishmentsController@create']);
        $app->get('establishments/{establishment}/generate-tokens', ['uses' => 'EstablishmentsController@generateTokens']);
        $app->get('establishments/{establishment}', ['uses' => 'EstablishmentsController@establishment']);
        $app->post('establishments/store', ['uses' => 'EstablishmentsController@store']);
        $app->get('establishments/{establishment}/edit', ['uses' => 'EstablishmentsController@edit']);
        $app->get('establishments/{establishment}/disable', ['uses' => 'EstablishmentsController@disable']);
        $app->get('establishments/{establishment}/clear', ['uses' => 'EstablishmentsController@clear']);
        $app->get('establishments/{establishment}/clear-products', ['uses' => 'EstablishmentsController@clearProducts']);
        $app->get('establishments/{establishment}/clear-tables', ['uses' => 'EstablishmentsController@clearTables']);
        $app->get('establishments/{establishment}/login', ['uses' => 'EstablishmentsController@loginAs']);
        $app->put('establishments/{establishment}', ['uses' => 'EstablishmentsController@update']);
        $app->get('establishments/{establishment}/users/create', ['uses' => 'EstablishmentsController@createUser']);
        $app->post('establishments/{establishment}/users/store', ['uses' => 'EstablishmentsController@storeUser']);
        $app->get('establishments/{establishment}/users/{user}/edit', ['uses' => 'EstablishmentsController@editUser']);
        $app->put('establishments/{establishment}/users/{user}', ['uses' => 'EstablishmentsController@updateUser']);
        $app->delete('establishments/{establishment}/users/{user}', ['uses' => 'EstablishmentsController@deleteUser']);

        $app->get('marketplaces', ['uses' => 'MarketplacesController@index']);
        $app->get('marketplaces/create', ['uses' => 'MarketplacesController@create']);
        $app->get('marketplaces/{marketplace}', ['uses' => 'MarketplacesController@marketplace']);
        $app->post('marketplaces/store', ['uses' => 'MarketplacesController@store']);
        $app->get('marketplaces/{marketplace}/edit', ['uses' => 'MarketplacesController@edit']);
        $app->get('marketplaces/{marketplace}/disable', ['uses' => 'MarketplacesController@disable']);
        $app->put('marketplaces/{marketplace}', ['uses' => 'MarketplacesController@update']);

        $app->get('bills', ['uses' => 'BillsController@index']);

        $app->get('users', ['uses' => 'UsersController@listUsers']);

        $app->get('partnerships', ['uses' => 'PartnershipsController@index']);
        $app->get('partnerships/create', ['uses' => 'PartnershipsController@create']);
        $app->post('partnerships/store', ['uses' => 'PartnershipsController@store']);
        $app->get('partnerships/{partnership}', ['uses' => 'PartnershipsController@partnership']);
        $app->get('partnerships/{partnership}/edit', ['uses' => 'PartnershipsController@edit']);
        $app->get('partnerships/{partnership}/disable', ['uses' => 'PartnershipsController@disable']);
        $app->put('partnerships/{partnership}', ['uses' => 'PartnershipsController@update']);

        $app->get('integrations', ['uses' => 'IntegrationsController@listIntegrations']);
        $app->get('integrations/create', ['uses' => 'IntegrationsController@create']);
        $app->post('integrations/store', ['uses' => 'IntegrationsController@store']);
        $app->get('integrations/{integration}/edit', ['uses' => 'IntegrationsController@edit']);
        $app->put('integrations/{integration}', ['uses' => 'IntegrationsController@update']);

        // Profile
        $app->get('profile', ['uses' => 'ProfileController@index']);
        $app->get('profile/partnership', ['uses' => 'ProfileController@partnership']);
        $app->put('profile/update', ['uses' => 'ProfileController@update']);
        $app->get('profile/password', ['uses' => 'ProfileController@password']);
        $app->put('profile/password/update', ['uses' => 'ProfileController@passwordUpdate']);
        $app->get('profile/config', ['uses' => 'ProfileController@config']);
        $app->put('profile/config/update', ['uses' => 'ProfileController@configUpdate']);

        $app->get('logout', ['uses' => 'AuthController@logout']);
    }
);
