<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$env = \Config::get('app.env');
$urlProduction = ($env == 'homologation') ? 'test.admin.tavollo.com' : 'admin.tavollo.com';
$url = ($env == 'local' || $env == 'testing') ? 'admin.tavollo.localhost' : $urlProduction;

$app->group(['domain' => $url, 'middleware' => ['guest']], function () use ($app) {
    $app->get('/', 'Auth\LoginController@index');

    Auth::routes();

    $app->get('/r/{partnershipEmail?}', 'Auth\RegisterController@showRegistrationForm');
});

$app->group(['domain' => $url, 'namespace' => 'Auth', 'middleware' => ['auth', 'is.establishment']], function () use ($app) {
    $app->get('/logout', 'LoginController@logout');
});

// Plateau namespace
$app->group(
    ['domain' => $url, 'namespace' => 'Plateau', 'middleware' => ['web', 'auth', 'is.establishment']],
    function () use ($app) {
        // Home
        $app->get('home', ['uses' => 'HomeController@index']);
        $app->get('home/request-tokens', ['uses' => 'HomeController@getRequestTokens']);
        $app->post('home/close-request-token', ['uses' => 'HomeController@closeRequestToken']);
        $app->get('home/sectors', ['uses' => 'HomeController@sectors']);
        $app->get('home/products', ['uses' => 'HomeController@products']);
        $app->get('home/pre-orders/{status}', ['uses' => 'HomeController@preOrders']);
        $app->get('home/orders/{status}/{sector?}', ['uses' => 'HomeController@orders']);
        $app->get('home/bills/{status}', ['uses' => 'HomeController@bills']);
        $app->get('home/bill/{bill}', ['uses' => 'HomeController@bill']);
        $app->post('home/bill/create', ['uses' => 'HomeController@createBill']);
        $app->post('home/bill/migrate', ['uses' => 'HomeController@migrateBill']);
        $app->post('home/bill/add-order/{bill}/{product}', ['uses' => 'HomeController@addOrder']);
        $app->post('home/bill/close/{bill}', ['uses' => 'HomeController@billClose']);
        $app->post('home/order/add-weight/{order}', ['uses' => 'HomeController@addWeight']);
        $app->post('home/order/close/{order}', ['uses' => 'HomeController@orderClose']);
        $app->post('home/pre-order/preparation/{preOrder}', ['uses' => 'HomeController@preOrderPreparation']);
        $app->post('home/pre-order/close/{preOrder}', ['uses' => 'HomeController@preOrderClose']);
        $app->post('home/order/cancel/{order}', ['uses' => 'HomeController@cancelOrder']);
        $app->post('home/table/changename/{bill}', ['uses' => 'HomeController@changeTableName']);
        $app->get('home/table/getcode/{table}', ['uses' => 'HomeController@tableCode']);

        // Profile
        $app->get('profile', ['uses' => 'ProfileController@index']);
        $app->put('profile/update', ['uses' => 'ProfileController@update']);
        $app->get('profile/password', ['uses' => 'ProfileController@password']);
        $app->put('profile/password-update', ['uses' => 'ProfileController@passwordUpdate']);
        $app->get('profile/establishment', ['uses' => 'ProfileController@establishment']);
        $app->get('profile/establishment/qrcode', ['uses' => 'ProfileController@generateEstablishmentQRCode']);
        $app->put('profile/establishment-update', ['uses' => 'ProfileController@establishmentUpdate']);
        $app->get('profile/config', ['uses' => 'ProfileController@config']);
        $app->put('profile/config-update', ['uses' => 'ProfileController@configUpdate']);
        $app->get('profile/api', ['uses' => 'ProfileController@api']);
        $app->put('profile/api-generate', ['uses' => 'ProfileController@generateApiKey']);
        $app->get('profile/faq', ['uses' => 'ProfileController@faq']);
        $app->get('profile/activities', ['uses' => 'ProfileController@activities']);

        //Integration
        $app->get('profile/integration', ['uses' => 'IntegrationsController@index']);
        $app->get('profile/integration/test', ['uses' => 'IntegrationsController@test']);
        $app->put('profile/integration/update', ['uses' => 'IntegrationsController@update']);

        //Payment
        $app->get('profile/payment', ['uses' => 'ProfileController@payment']);
        $app->post('profile/payment', ['uses' => 'ProfileController@savePayment']);

        //Printer
        $app->get('profile/printer', ['uses' => 'PrinterController@index']);
        $app->post('profile/printer', ['uses' => 'PrinterController@update']);

        // Sectors
        $app->get('profile/sectors', ['uses' => 'SectorsController@index']);
        $app->get('profile/sectors/list', ['uses' => 'SectorsController@listSectors']);
        $app->post('profile/sectors/store', ['uses' => 'SectorsController@store']);
        $app->get('profile/sectors/create', ['uses' => 'SectorsController@create']);
        $app->get('profile/sectors/{sector}/edit', ['uses' => 'SectorsController@edit']);
        $app->put('profile/sectors/{sector}/enable', ['uses' => 'SectorsController@enable']);
        $app->put('profile/sectors/{sector}/update', ['uses' => 'SectorsController@update']);

        // Islands
        $app->get('profile/islands', ['uses' => 'IslandsController@index']);
        $app->get('profile/islands/list', ['uses' => 'IslandsController@listIslands']);
        $app->post('profile/islands/store', ['uses' => 'IslandsController@store']);
        $app->get('profile/islands/create', ['uses' => 'IslandsController@create']);
        $app->get('profile/islands/{island}/edit', ['uses' => 'IslandsController@edit']);
        $app->put('profile/islands/{island}/enable', ['uses' => 'IslandsController@enable']);
        $app->put('profile/islands/{island}/update', ['uses' => 'IslandsController@update']);

        // Waiters
        $app->get('profile/waiters', ['uses' => 'WaitersController@index']);
        $app->get('profile/waiters/list', ['uses' => 'WaitersController@listWaiters']);
        $app->post('profile/waiters/store', ['uses' => 'WaitersController@store']);
        $app->get('profile/waiters/create', ['uses' => 'WaitersController@create']);
        $app->get('profile/waiters/{waiter}/edit', ['uses' => 'WaitersController@edit']);
        $app->put('profile/waiters/{waiter}/enable', ['uses' => 'WaitersController@enable']);
        $app->put('profile/waiters/{waiter}/update', ['uses' => 'WaitersController@update']);
        $app->delete('profile/waiters/{waiter}/destroy', ['uses' => 'WaitersController@destroy']);

        // Tables
        $app->get('tables', ['uses' => 'TablesController@index']);
        $app->get('tables/list', ['uses' => 'TablesController@listTables']);
        $app->get('tables/trash', ['uses' => 'TablesController@trash']);
        $app->get('tables/generate', ['uses' => 'TablesController@generate']);
        $app->get('tables/qr-code', ['uses' => 'TablesController@getAllQrCodes']);
        $app->get('tables/qr-code/{table}', ['uses' => 'TablesController@getQrCode']);
        $app->post('tables/store', ['uses' => 'TablesController@store']);
        $app->get('tables/create', ['uses' => 'TablesController@create']);
        $app->get('tables/sequence', ['uses' => 'TablesController@createSequence']);
        $app->post('tables/sequence', ['uses' => 'TablesController@storeSequence']);
        $app->get('tables/{table}/edit', ['uses' => 'TablesController@edit']);
        $app->delete('tables/{table}/destroy', ['uses' => 'TablesController@destroy']);
        $app->put('tables/{table}/enable', ['uses' => 'TablesController@enable']);
        $app->put('tables/{table}/code', ['uses' => 'TablesController@generateCode']);
        $app->put('tables/{table}/update', ['uses' => 'TablesController@update']);
        $app->get('tables/import', ['uses' => 'TablesController@showImport']);
        $app->post('tables/import-file', ['uses' => 'TablesController@import']);

        // Table Locations
        $app->get('locations', ['uses' => 'TableLocationsController@index']);
        $app->get('locations/list', ['uses' => 'TableLocationsController@listLocations']);
        $app->post('locations/store', ['uses' => 'TableLocationsController@store']);
        $app->get('locations/create', ['uses' => 'TableLocationsController@create']);
        $app->get('locations/{location}/edit', ['uses' => 'TableLocationsController@edit']);
        $app->put('locations/{location}/enable', ['uses' => 'TableLocationsController@enable']);
        $app->put('locations/{location}/update', ['uses' => 'TableLocationsController@update']);

        // Menus
        $app->get('menus', ['uses' => 'MenusController@index', 'as' => 'plateau.menus.index']);
        $app->get('menus/list', ['uses' => 'MenusController@listMenus', 'as' => 'plateau.menus.list']);
        $app->post('menus/order', ['uses' => 'MenusController@order', 'as' => 'plateau.menus.order']);
        $app->post('menus/store', ['uses' => 'MenusController@store', 'as' => 'plateau.menus.store']);
        $app->get('menus/create', ['uses' => 'MenusController@create', 'as' => 'plateau.menus.create']);
        $app->get('menus/{menu}/edit', ['uses' => 'MenusController@edit', 'as' => 'plateau.menus.edit']);
        $app->post('menus/upload', ['uses' => 'MenusController@uploadPhoto']);
        $app->delete('menus/{menu}/destroy', ['uses' => 'MenusController@destroy', 'as' => 'plateau.menus.destroy']);
        $app->put('menus/{menu}/enable', ['uses' => 'MenusController@enable', 'as' => 'plateau.menus.enable']);
        $app->put('menus/{menu}/update', ['uses' => 'MenusController@update', 'as' => 'plateau.menus.update']);

        // Products
        $app->get('products', ['uses' => 'ProductsController@index', 'as' => 'plateau.products.index']);
        $app->get('products/list', ['uses' => 'ProductsController@listProducts']);
        $app->get('products/trash', ['uses' => 'ProductsController@trash', 'as' => 'plateau.products.trash']);
        $app->post('products/store', ['uses' => 'ProductsController@store', 'as' => 'plateau.products.store']);
        $app->get('products/import', ['uses' => 'ProductsController@showImport']);
        $app->post('products/import-file', ['uses' => 'ProductsController@import']);
        // $app->get('products/import-integration', ['uses' => 'ProductsController@importIntegration']);
        $app->get('products/create', ['uses' => 'ProductsController@create', 'as' => 'plateau.products.create']);
        $app->get('products/{product}/edit', ['uses' => 'ProductsController@edit', 'as' => 'plateau.products.edit']);
        $app->post('products/upload', ['uses' => 'ProductsController@uploadPhoto']);
        $app->delete('products/{product}/destroy', ['uses' => 'ProductsController@destroy', 'as' => 'plateau.products.destroy']);
        $app->put('products/{product}/enable', ['uses' => 'ProductsController@enable', 'as' => 'plateau.products.enable']);
        $app->put('products/{product}/update', ['uses' => 'ProductsController@update', 'as' => 'plateau.products.update']);
        $app->put('products/{product}/update-category/{category}', ['uses' => 'ProductsController@updateCategory']);

        $app->get('products/observations/{product}', ['uses' => 'ProductsController@listObservations']);
        $app->post('products/observations/{product}', ['uses' => 'ProductsController@saveObservation']);
        $app->put('products/observations/{product}', ['uses' => 'ProductsController@deleteObservation']);

        $app->get('products/groups/{product}', ['uses' => 'ProductsController@listGroups']);
        $app->post('products/groups/{product}/{group?}', ['uses' => 'ProductsController@saveGroup']);
        $app->delete('products/groups/{product}/{group}/destroy', ['uses' => 'ProductsController@deleteGroup']);
        $app->get('products/groups/items/{product}/{group}', ['uses' => 'ProductsController@listGroupItems']);
        $app->post('products/groups/items/{product}/{group}/{item?}', ['uses' => 'ProductsController@saveItem']);
        $app->delete('products/groups/items/{product}/{group}/{item}/destroy', ['uses' => 'ProductsController@deleteItem']);

        // Product Categories
        $app->get('categories', ['uses' => 'ProductCategoriesController@index']);
        $app->get('categories/list', ['uses' => 'ProductCategoriesController@listCategories']);
        $app->post('categories/store', ['uses' => 'ProductCategoriesController@store']);
        $app->get('categories/create', ['uses' => 'ProductCategoriesController@create']);
        $app->get('categories/{category}/edit', ['uses' => 'ProductCategoriesController@edit']);
        $app->put('categories/{category}/update', ['uses' => 'ProductCategoriesController@update']);
        $app->put('categories/{category}/enable', ['uses' => 'ProductCategoriesController@enable']);

        // Bills
        $app->get('bills', ['uses' => 'BillsController@index']);
        $app->get('bills/list', ['uses' => 'BillsController@listBills']);
        $app->post('bills/{bill}/update', ['uses' => 'BillsController@updateBill']);

        //Pre Orders
        $app->get('pre-orders', ['uses' => 'PreOrdersController@index']);
        $app->get('pre-orders/list', ['uses' => 'PreOrdersController@listPreOrders']);
        $app->get('pre-orders/{id}', ['uses' => 'PreOrdersController@getPreOrder']);

        // History
        $app->get('analysis', ['uses' => 'AnalysisController@index']);
        $app->get('analysis/data', ['uses' => 'AnalysisController@loadIndex']);
        $app->get('analysis/service', ['uses' => 'AnalysisController@service']);
        $app->get('analysis/service/list', ['uses' => 'AnalysisController@serviceList']);
        $app->get('analysis/products', ['uses' => 'AnalysisController@products']);
        $app->get('analysis/products/list', ['uses' => 'AnalysisController@productsList']);
        $app->get('analysis/users', ['uses' => 'AnalysisController@users']);
        $app->get('analysis/users/list', ['uses' => 'AnalysisController@usersList']);
        $app->get('analysis/rating', ['uses' => 'AnalysisController@rating']);
        $app->get('analysis/rating/list', ['uses' => 'AnalysisController@ratingList']);

        //Rating
        $app->get('analysis/ratings', ['uses' => 'RatingController@index']);
        $app->get('analysis/ratings/list', ['uses' => 'RatingController@listRatings']);
        $app->get('analysis/ratings/config', ['uses' => 'RatingController@config']);
        $app->post('analysis/ratings/config', ['uses' => 'RatingController@configSave']);
        $app->put('analysis/ratings/enable/{rating}', ['uses' => 'RatingController@enable']);
        $app->delete('analysis/ratings/delete/{rating}', ['uses' => 'RatingController@delete']);

        // About
        $app->get('about', ['uses' => 'ProfileController@about']);
    }
);
