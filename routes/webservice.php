<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$env = \Config::get('app.env');
$urlProduction = ($env == 'homologation') ? 'test.ws.tavollo.com' : 'ws.tavollo.com';
$url = ($env == 'local' || $env == 'testing') ? 'ws.tavollo.localhost' : $urlProduction;

$app->group(
    ['domain' => $url, 'namespace' => 'Webservice', 'middleware' => ['webservice']],
    function () use ($app) {
        $app->get('/', 'AuthController@index');

        $app->post('auth', 'AuthController@authUser');

        $app->post('auth-social', 'AuthController@authSocial');

        $app->post('register', 'AuthController@register');

        $app->post('forgot-password', 'AuthController@forgotPassword');

        $app->post('redefine-password', 'AuthController@redefinePassword');

        $app->post('establishments', 'AuthController@establishments');

        $app->post('establishments/theme', 'AuthController@establishmentTheme');

        $app->get('marketplaces/{slug}', 'MarketplaceController@marketplace');

        //Retorna os menus do estabelecimento sem precisar de user
        $app->post('pre-order/menus/{slug?}', 'MenusController@listMenuPreOrder');

        //Retorna as categorias de um menu do estabelecimento sem precisas de user
        $app->post('pre-order/categories', 'MenusController@listCategoriesPreOrder');

        //Retorna um produto de uma categoria sem precisar de user
        $app->post('pre-order/product', 'MenusController@getProductPreOrder');

        //Realiza busca de produtos ativos por query string
        $app->post('establishments/search-products', 'MenusController@searchProducts');
    }
);

$app->group(
    ['domain' => $url, 'namespace' => 'Webservice', 'middleware' => ['webservice', 'webservice.user']],
    function () use ($app) {
        $app->post('request-token/{establishment}', 'AuthController@requestToken');

        //Verifica se Spot está disponível
        $app->post('check-table', 'AuthController@checkSpot');

        //Verifica token de autorização da API
        $app->get('check-token', 'DefaultController@show');

        /* Profile **/

        //Altera dados de perfil
        $app->post('profile/update', 'ProfileController@updateProfile');

        $app->post('profile/rating', 'ProfileController@rating');

        /* Menus **/

        //Retorna os menus do estabelecimento
        $app->post('menus', 'MenusController@index');

        //Retorna os menus do estabelecimento
        $app->post('categories', 'MenusController@categories');

        $app->post('product', 'MenusController@product');

        /* Bill **/

        //Abre uma nova conta
        $app->post('open-bill', 'BillsController@openBill');

        //Retorna os dados da conta (Bill) ativa
        $app->post('bill', 'BillsController@index');

        //Adiciona um novo pedido à conta (Bill)
        $app->post('bill/add', 'BillsController@add');

        //Adiciona vários pedidos do módulo de carrinhos
        $app->post('bill/add-orders', 'BillsController@addOrders');

        //Solicita a presença do Garçom através de notificação
        $app->post('bill/call', 'BillsController@call');

        //Solicita cancelamento de pedido (Order) da conta (Bill)
        $app->post('bill/cancel', 'BillsController@cancel');

        //Solicita finalização da conta (Bill)
        $app->post('bill/close', 'BillsController@close');

        //Altera o alias do Spot
        $app->post('bill/change-alias', 'BillsController@alias');

        //Verifica se ainda possui permissão como usuário anexo
        $app->post('bill/attach-status', 'BillsController@getAttachedStatus');

        $app->post('bill/change-attached-status', 'BillsController@changeAttachedStatus');

        //Verifica se conta (Bill) já foi encerrada
        $app->post('bill/has-closed', 'BillsController@hasClosed');

        //Avalia a conta
        $app->post('bill/rating', 'BillsController@rating');

        /* Attached Users **/
        $app->post('request-attachment', 'BillsController@requestAttachment');

        $app->get('attachment-requests', 'BillsController@listAttachmentRequests');

        $app->post('attach', 'BillsController@processAttach');

        $app->get('locations/{establishment_id?}', 'BillsController@locations');

        /* Pre Order **/
        $app->get('pre-order/active', 'PreOrderController@getActive');

        $app->get('pre-order/list', 'PreOrderController@list');

        $app->post('pre-order/add', 'PreOrderController@add');

        $app->post('pre-order/confirm', 'PreOrderController@confirm');

        $app->post('pre-order/cancel', 'PreOrderController@cancel');

        $app->post('pre-order/has-closed', 'PreOrderController@hasClosed');
    }
);

//Route::get('/user', function (Request $request) {
//    return $request->user();
//})->middleware('auth:api');
