<?php

namespace Tests\Feature\Api;

use App\Models\Bill;
use App\Models\Establishment;
use App\Models\EstablishmentConfig;
use App\Models\User;
use App\Models\Table;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class BillsControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected $establishment;

    protected $establishmentConfig;

    protected $user;

    protected $apiToken;

    public function setUp(): void
    {
        parent::setUp();
        $this->establishment = factory(Establishment::class)->create();
        $this->establishmentConfig = factory(EstablishmentConfig::class)->create(['establishment_id' => $this->establishment->id]);
        $this->user = factory(User::class)->create(['establishment_id' => $this->establishment->id, 'is_admin' => true, 'is_establishment' => true]);
        $this->apiToken = $this->user->generateApiToken();
    }

    /** @test */
    public function list_all_bills_from_establishment()
    {
        //Garante a listagem de todas as comandas de um estabelecimento filtradas pelo status
        $spots = factory(Table::class, 3)->create(['establishment_id' => $this->establishment->id]);
        foreach($spots as $s){
            factory(Bill::class)->create(['table_id' => $s->id, 'status' => Bill::STATUS_OPENED]);
        }

        $this->withHeaders($this->getApiHeaders())
            ->json('GET', $this->getUrl('api').'/bills?status=opened&_token='.$this->apiToken->sequence)
            ->assertStatus(200)
            ->assertJsonCount(3);
    }

    /** @test */
    public function list_one_bill_from_establishment()
    {
        //Garante a listagem de todas as comandas de um estabelecimento filtradas pelo status
        $spot = factory(Table::class)->create(['establishment_id' => $this->establishment->id]);
        $bill = factory(Bill::class)->create(['table_id' => $spot->id, 'status' => Bill::STATUS_OPENED]);

        $this->withHeaders($this->getApiHeaders())
            ->json('GET', $this->getUrl('api').'/bills/' . $bill->id . '?status=opened&_token='.$this->apiToken->sequence)
            ->assertStatus(200)
            ->assertSee('"id":' . $bill->id);
    }

    /** @test */
    public function create_bill_from_establishment()
    {
        $spot = factory(Table::class)->create(['establishment_id' => $this->establishment->id]);
        $user = factory(User::class)->create();
        //Garante a criação de uma comanda em um estabelecimento

        $this->withHeaders($this->getApiHeaders())
            ->json('POST', $this->getUrl('api').'/bills/create?_token='.$this->apiToken->sequence, [
                'table' => $spot->number,
                'username' => $user->name,
                'is_consumption_card' => $spot->is_consumption_card,
                'alias' => $spot->alias
            ])
            ->assertStatus(200)
            ->assertSee('"success":true');
    }

    /** @test */
    public function update_bill_from_establishment()
    {
        //Garante a atualização de uma comanda no estabelecimento
        $spot = factory(Table::class)->create(['establishment_id' => $this->establishment->id]);
        $bill = factory(Bill::class)->create(['table_id' => $spot->id, 'status' => Bill::STATUS_OPENED]);

        $this->withHeaders($this->getApiHeaders())
            ->json('POST', $this->getUrl('api').'/bills/update/' . $bill->id . '?_token='.$this->apiToken->sequence, [
                'status' => Bill::STATUS_CLOSED,
            ])
            ->assertStatus(200)
            ->assertSee('"success":true');
    }
}
