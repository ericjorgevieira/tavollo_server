<?php

namespace Tests\Feature\Api;

use App\Models\Bill;
use App\Models\Establishment;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\EstablishmentConfig;
use App\Models\User;
use App\Models\Table;
use App\Models\Order;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class OrdersControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected $establishment;

    protected $establishmentConfig;

    protected $products;

    protected $user;

    protected $apiToken;

    public function setUp(): void
    {
        parent::setUp();
        $this->establishment = factory(Establishment::class)->create();
        $this->establishmentConfig = factory(EstablishmentConfig::class)->create(['establishment_id' => $this->establishment->id]);
        $this->products = factory(Product::class, 5)->create(['establishment_id' => $this->establishment->id, 
            'product_category_id' => factory(ProductCategory::class)->create(['establishment_id' => $this->establishment->id])->id]);
        $this->user = factory(User::class)->create(['establishment_id' => $this->establishment->id, 'is_admin' => true, 'is_establishment' => true]);
        $this->apiToken = $this->user->generateApiToken();
    }

    /** @test */
    public function list_all_orders_from_establishment_by_status()
    {
        //Garante o retorno de todos os pedidos de um estabelecimento filtrados pelo status
        $spot = factory(Table::class)->create(['establishment_id' => $this->establishment->id]);
        $bill = factory(Bill::class)->create(['table_id' => $spot->id, 'status' => Bill::STATUS_OPENED, 'user_id' => $this->user->id ]);
        
        foreach($this->products as $p){
            factory(Order::class)->create(['bill_id' => $bill->id, 'product_id' => $p->id, 'user_id' => $this->user->id, "status" => Order::STATUS_OPENED]);
        }
        
        $this->withHeaders($this->getApiHeaders())
            ->json('GET', $this->getUrl('api').'/orders?status=' . Order::STATUS_OPENED . '&_token='.$this->apiToken->sequence)
            ->assertStatus(200)
            ->assertJsonCount(5);
    }

    /** @test */
    public function list_orders_from_bill_by_status()
    {
        //Garante o retorno de todos os pedidos de uma comanda filtrados pelo status
        $spotA = factory(Table::class)->create(['establishment_id' => $this->establishment->id]);
        $spotB = factory(Table::class)->create(['establishment_id' => $this->establishment->id]);
        $billA = factory(Bill::class)->create(['table_id' => $spotA->id, 'status' => Bill::STATUS_OPENED, 'user_id' => $this->user->id ]);
        $billB = factory(Bill::class)->create(['table_id' => $spotB->id, 'status' => Bill::STATUS_OPENED ]);
        
        foreach($this->products as $p){
            factory(Order::class)->create(['bill_id' => $billA->id, 'product_id' => $p->id, 'user_id' => $this->user->id, "status" => Order::STATUS_OPENED]);
        }

        factory(Order::class, 15)->create(["bill_id" => $billB->id, "status" => Order::STATUS_OPENED]);
        
        $this->withHeaders($this->getApiHeaders())
            ->json('GET', $this->getUrl('api').'/orders/' . $billA->id . '?status=' . Order::STATUS_OPENED . '&_token='.$this->apiToken->sequence)
            ->assertStatus(200)
            ->assertJsonCount(5);
    }

    /** @test */
    public function create_order_from_establishment()
    {
        //Garante a criação de um pedido no estabelecimento
        $spot = factory(Table::class)->create(['establishment_id' => $this->establishment->id]);
        $bill = factory(Bill::class)->create(['table_id' => $spot->id, 'status' => Bill::STATUS_OPENED, 'user_id' => $this->user->id ]);

        $product = $this->products[0];
        $this->withHeaders($this->getApiHeaders())
            ->json('POST', $this->getUrl('api').'/orders/create/' . $bill->id, [
                '_token' => $this->apiToken->sequence,
                'product_id' => $product->id,
                'code' => $product->code,
                'quant' => 1,
                'observation' => 'Observação de teste',
                'status' => 'opened'
            ])
            ->assertStatus(200)
            ->assertSee('"success":true');
    }

    /** @test */
    public function update_order_from_establishment()
    {
        //Garante a atualização de um pedido no estabelecimento
        $spot = factory(Table::class)->create(['establishment_id' => $this->establishment->id]);
        $bill = factory(Bill::class)->create(['table_id' => $spot->id, 'status' => Bill::STATUS_OPENED, 'user_id' => $this->user->id ]);

        $product = $this->products[0];
        $order = factory(Order::class)->create(['bill_id' => $bill->id, 'product_id' => $product->id]);
        $this->withHeaders($this->getApiHeaders())
            ->json('POST', $this->getUrl('api').'/orders/update/' . $order->id, [
                '_token' => $this->apiToken->sequence,
                'status' => Order::STATUS_PREPARATION,
            ])
            ->assertStatus(200)
            ->assertSee('"success":true');
    }

    /** @test */
    public function update_many_orders_from_establishment()
    {
        //Garante a atualização de vários pedidos no estabelecimento
        $spot = factory(Table::class)->create(['establishment_id' => $this->establishment->id]);
        $bill = factory(Bill::class)->create(['table_id' => $spot->id, 'status' => Bill::STATUS_OPENED, 'user_id' => $this->user->id ]);

        $product = $this->products[0];
        factory(Order::class, 2)->create(['bill_id' => $bill->id, 'product_id' => $product->id, 'status' => Order::STATUS_OPENED]);

        $this->withHeaders($this->getApiHeaders())
            ->json('POST', $this->getUrl('api').'/orders/update-many', [
                '_token' => $this->apiToken->sequence,
                'orders' => [
                    [
                        'id' => 1,
                        'status' => Order::STATUS_PREPARATION,
                    ],
                    [
                        'id' => 2,
                        'status' => Order::STATUS_PREPARATION,
                    ]
                ]
            ])
            ->assertStatus(200)
            ->assertSee('"success":true');
    }
}
