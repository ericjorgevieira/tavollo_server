<?php

namespace Tests\Feature\Api;

use App\Models\Establishment;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\EstablishmentConfig;
use App\Models\User;
use App\Models\PreOrder;
use App\Models\PreOrderProduct;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class PreOrdersControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected $establishment;

    protected $establishmentConfig;

    protected $products;

    protected $user;

    protected $apiToken;

    public function setUp(): void
    {
        parent::setUp();
        $this->establishment = factory(Establishment::class)->create();
        $this->establishmentConfig = factory(EstablishmentConfig::class)->create(['establishment_id' => $this->establishment->id]);
        $this->products = factory(Product::class, 5)->create(['establishment_id' => $this->establishment->id, 
            'product_category_id' => factory(ProductCategory::class)->create(['establishment_id' => $this->establishment->id])->id]);
        $this->user = factory(User::class)->create(['establishment_id' => $this->establishment->id, 'is_admin' => true, 'is_establishment' => true]);
        $this->apiToken = $this->user->generateApiToken();
    }

    /** @test */
    public function list_all_pre_orders_from_establishment_by_status()
    {
        //Garante o retorno de todos os pedidos pré-pagos de um estabelecimento filtrados pelo status
        $user = factory(User::class)->create();
        $preOrder = factory(PreOrder::class)->create(['user_id' => $user->id, 'establishment_id' => $this->establishment->id]);

        foreach($this->products as $p){
            factory(PreOrderProduct::class)->create(['product_id' => $p->id, 'pre_order_id' => $preOrder->id, 'status' => PreOrder::STATUS_OPENED]);
        }

        $this->withHeaders($this->getApiHeaders())
            ->json('GET', $this->getUrl('api').'/pre-orders?status=' . PreOrder::STATUS_OPENED . '&_token='.$this->apiToken->sequence)
            ->assertStatus(200)
            ->assertJsonCount(1);
    }

    /** @test */
    public function update_pre_order_from_establishment()
    {
        //Garante a atualização de um pedido pré-pago no estabelecimento
        $user = factory(User::class)->create();
        $preOrder = factory(PreOrder::class)->create(['user_id' => $user->id, 'establishment_id' => $this->establishment->id]);

        $this->withHeaders($this->getApiHeaders())
            ->json('POST', $this->getUrl('api').'/pre-orders/update/' . $preOrder->id, [
                '_token' => $this->apiToken->sequence,
                'status' => PreOrder::STATUS_PREPARATION,
            ])
            ->assertStatus(200)
            ->assertSee('"success":true');
    }

    /** @test */
    public function update_many_pre_orders_from_establishment()
    {
        //Garante a atualização de vários pedidos pré-pagos no estabelecimento
        $user = factory(User::class)->create();
        factory(PreOrder::class, 3)->create(['user_id' => $user->id, 'establishment_id' => $this->establishment->id]);

        $this->withHeaders($this->getApiHeaders())
            ->json('POST', $this->getUrl('api').'/pre-orders/update-many', [
                '_token' => $this->apiToken->sequence,
                'preorders' => [
                    [
                        'id' => 1,
                        'status' => PreOrder::STATUS_PREPARATION,
                    ],
                    [
                        'id' => 2,
                        'status' => PreOrder::STATUS_PREPARATION,
                    ],
                    [
                        'id' => 3,
                        'status' => PreOrder::STATUS_PREPARATION,
                    ]
                ]
            ])
            ->assertStatus(200)
            ->assertSee('"success":true');
    }
}
