<?php

namespace Tests\Feature\Api;

use App\Models\Establishment;
use App\Models\EstablishmentConfig;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ProductsControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected $establishment;

    protected $establishmentConfig;

    protected $user;

    protected $apiToken;

    public function setUp(): void
    {
        parent::setUp();
        $this->establishment = factory(Establishment::class)->create();
        $this->establishmentConfig = factory(EstablishmentConfig::class)->create(['establishment_id' => $this->establishment->id]);
        $this->user = factory(User::class)->create(['establishment_id' => $this->establishment->id, 'is_admin' => true, 'is_establishment' => true]);
        $this->apiToken = $this->user->generateApiToken();
    }

    /** @test */
    public function list_all_products_from_establishment()
    {
        //Garante o retorno da listagem de todos os produtos ativos do estabelecimento
        $productCategory = factory(ProductCategory::class)->create(['establishment_id' => $this->establishment->id]);
        factory(Product::class, 10)->create(['establishment_id' => $this->establishment->id, 'product_category_id' => $productCategory->id]);

        $this->withHeaders($this->getApiHeaders())
            ->json('GET', $this->getUrl('api').'/products/?status='.Product::STATUS_ENABLED.'&_token='.$this->apiToken->sequence)
            ->assertStatus(200)
            ->assertJsonCount(10);
    }

    /** @test */
    public function update_product_from_establishment()
    {
        //Garante a atualização de um único produto de um estabelecimento
        $productCategory = factory(ProductCategory::class)->create(['establishment_id' => $this->establishment->id]);
        $product = factory(Product::class)->create(['establishment_id' => $this->establishment->id, 'product_category_id' => $productCategory->id]);

        $this->withHeaders($this->getApiHeaders())
            ->json('POST', $this->getUrl('api').'/products/update/'.$product->id, [
                '_token' => $this->apiToken->sequence,
                'name' => 'Coca Cola 200ml',
                'price' => 2.99,
                'code' => $product->code
            ])
            ->assertStatus(200)
            ->assertSee('"success":true');
    }

    /** @test */
    public function update_many_products_from_establishment()
    {
        //Garante a atualização de vários produtos ao mesmo tempo em um estabelecimento
        $productCategory1 = factory(ProductCategory::class)->create(['establishment_id' => $this->establishment->id]);
        $productCategory2 = factory(ProductCategory::class)->create(['establishment_id' => $this->establishment->id]);

        $products = factory(Product::class, 3)->create(['establishment_id' => $this->establishment->id, 'product_category_id' => $productCategory1->id]);

        $this->withHeaders($this->getApiHeaders())
            ->json('POST', $this->getUrl('api').'/products/update-many',
            [
                '_token' => $this->apiToken->sequence,
                'products' => [
                    [
                        'name' => $products[0]->name,
                        'price' => $products[0]->price,
                        'code' => $products[0]->code,
                        'category' => $productCategory2->slug
                    ],
                    [
                        'name' => $products[1]->name,
                        'price' => $products[1]->price,
                        'code' => $products[1]->code,
                        'category' => $productCategory2->slug
                    ],
                    [
                        'name' => 'New Product',
                        'price' => 2.99,
                        'code' => rand(1, 2000),
                        'category' => 'new-category'
                    ]
                ]
                
            ])
            ->assertStatus(200)
            ->assertSee('"success":true')
            ->assertSee('Produto atualizado com sucesso.')
            ->assertSee('Produto inserido com sucesso');
    }
}
