<?php

namespace Tests\Feature\Api;

use App\Models\Establishment;
use App\Models\EstablishmentConfig;
use App\Models\Table;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class SpotsControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected $establishment;

    protected $establishmentConfig;

    protected $user;

    protected $apiToken;

    public function setUp(): void
    {
        parent::setUp();
        $this->establishment = factory(Establishment::class)->create();
        $this->establishmentConfig = factory(EstablishmentConfig::class)->create(['establishment_id' => $this->establishment->id]);
        $this->user = factory(User::class)->create(['establishment_id' => $this->establishment->id, 'is_admin' => true, 'is_establishment' => true]);
        $this->apiToken = $this->user->generateApiToken();
    }

    /** @test */
    public function list_spots_from_establishment()
    {
        //Garante o retorno da listagem de todos os cartões e mesas do estabelecimento
        factory(Table::class, 5)->create(['establishment_id' => $this->establishment->id]);

        $this->withHeaders($this->getApiHeaders())
            ->json('GET', $this->getUrl('api').'/tables/?status='.Table::STATUS_ENABLED.'&_token='.$this->apiToken->sequence)
            ->assertStatus(200)
            ->assertJsonCount(5);
    }

    /** @test */
    public function get_spot_from_establishment()
    {
        //Garante o retorno de um spot específico do estabelecimento
        $spot = factory(Table::class)->create(['establishment_id' => $this->establishment->id, 'is_consumption_card' => true]);

        $this->withHeaders($this->getApiHeaders())
            ->json('GET', $this->getUrl('api').'/table?is_consumption_card=1&table_number='.$spot->number.
                '&status='.Table::STATUS_ENABLED.'&_token='.$this->apiToken->sequence)
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'payload' => [
                    'name' => $spot->name,
                ],
            ]);
    }

    /** @test */
    public function update_spot_from_establishment()
    {
        //Garante a atualização de um spot específico do estabelecimento
        $spot = factory(Table::class)->create(['establishment_id' => $this->establishment->id, 'is_consumption_card' => true]);

        $this->withHeaders($this->getApiHeaders())
            ->json('POST', $this->getUrl('api').'/table/update/'.$spot->id,
            [
                '_token' => $this->apiToken->sequence,
                'name' => 'Mesa de Teste',
                'number' => $spot->number,
                'alias' => 'Ao lado do balcão',
                'is_consumption_card' => false,
                'status' => $spot->status,
            ])
            ->assertStatus(200)
            ->assertJson(['success' => true]);
    }

    /** @test */
    public function update_many_spots_from_establishment()
    {
        //Garante a atualização de vários spots de um estabelecimento
        $spots = factory(Table::class, 3)->create(['establishment_id' => $this->establishment->id, 'is_consumption_card' => true]);

        $this->withHeaders($this->getApiHeaders())
            ->json('POST', $this->getUrl('api').'/tables/update-many',
            [
                '_token' => $this->apiToken->sequence,
                'tables' => [
                    [
                        'id' => $spots[0]->id,
                        'name' => $spots[0]->name,
                        'number' => $spots[0]->number,
                        'alias' => 'Alias de teste 1',
                        'is_consumption_card' => false,
                        'status' => $spots[0]->status,
                    ],
                    [
                        'id' => $spots[1]->id,
                        'name' => $spots[1]->name,
                        'number' => $spots[1]->number,
                        'alias' => 'Alias de teste 2',
                        'is_consumption_card' => false,
                        'status' => $spots[1]->status,
                    ],
                    [
                        'code' => 55,
                        'name' => 'Cartão 55',
                        'number' => 55,
                        'alias' => 'Alias de teste 3',
                        'status' => Table::STATUS_ENABLED,
                    ],
                ],
            ])
            ->assertStatus(200)
            ->assertDontSee('"success":false');
    }

    /** @test */
    public function set_spot_alias()
    {
        //Garante a atualização do alias do spot
        //Garante a atualização de um spot específico do estabelecimento
        $spot = factory(Table::class)->create(['establishment_id' => $this->establishment->id, 'is_consumption_card' => true]);

        $this->withHeaders($this->getApiHeaders())
            ->json('POST', $this->getUrl('api').'/table/alias/'.$spot->id,
            [
                '_token' => $this->apiToken->sequence,
                'alias' => 'Ao lado do balcão',
            ])
            ->assertStatus(200)
            ->assertJson(['success' => true]);
    }
}
