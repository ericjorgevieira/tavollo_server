<?php

namespace Tests\Feature\Webservice;

use App\Models\EstablishmentConfig;
use App\Models\User;
use App\Models\PasswordReset;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function register_user()
    {
        //Esse teste precisa garantir que um usuário pode se cadastrar
        //No formulário de cadastro de usuário do webapp

        $attributes = [
            'name' => 'Test User',
            'email' => 'unittest@gmail.com',
            'password' => 'senha123',
            'password_confirm' => 'senha123',
            'cpf' => '101.202.303-44',
        ];

        $this->withHeaders($this->getWebserviceHeaders())
            ->json('POST', $this->getUrl('ws').'/register', $attributes)
            ->assertStatus(200);
    }

    /** @test */
    public function auth_user()
    {
        //Esse teste precisa garantir que um usuário cadastrado com login e senha
        //possa se autenticar no webapp

        $email = 'unittest@gmail.com';
        $password = 'senha123';

        factory(User::class)->create([
            'email' => $email,
            'password' => bcrypt($password),
        ]);

        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/auth', [
            'user' => [
                'email' => $email,
                'password' => $password,
            ],
        ])
        ->assertStatus(200)
        ->assertSee('hash_user');
    }

    /** @test */
    public function forgot_password()
    {
        //Esse teste precisa garantir que o usuário terá o seu e-mail de
        //redefinição de senha enviado

        $email = 'unittest@gmail.com';

        factory(User::class)->create([
            'email' => $email,
        ]);

        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/forgot-password', [
            'email' => $email,
        ])
        ->assertStatus(200)
        ->assertSee('"success":true');
    }

    /** @test */
    public function redefine_password()
    {
        //Esse teste precisa garantir que o usuário, com o token enviado no e-mail,
        //Possa redefinir sua senha
        
        $email = 'unittest@gmail.com';
        $token = bcrypt('12345');
        $password = 'senha123';

        factory(User::class)->create([
            'email' => $email,
        ]);

        factory(PasswordReset::class)->create([
            'email' => $email,
            'token' => $token,
        ]);
        
        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/redefine-password', [
            'email' => $email,
            'token' => $token,
            'password' => $password
        ])

        ->assertStatus(200)
        ->assertSee('"success":true');
    }

    /** @test */
    public function list_establishments_from_defined_lat_lng()
    {
        //Esse teste precisa garantir que estabelecimentos longe de uma determinada area
        //não serão listados caso possuam controle de geolocalização

        $lat1 = '-5.888565';
        $lng1 = '-35.213913';

        $lat2 = '-5.744785';
        $lng2 = '-35.259258';

        $config1 = factory(EstablishmentConfig::class)->create([
            'location_lat' => $lat1,
            'location_long' => $lng1,
            'display_location' => true,
        ]);
        $config2 = factory(EstablishmentConfig::class)->create([
            'location_lat' => $lat2,
            'location_long' => $lng2,
            'display_location' => true,
        ]);

        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/establishments', [
            'latitude' => $lat1,
            'longitude' => $lng1,
        ])
        ->assertStatus(200)
        ->assertSee($config1->establishment->name)
        ->assertDontSee($config2->establishment->name);
    }
}
