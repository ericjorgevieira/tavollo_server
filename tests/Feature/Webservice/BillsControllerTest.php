<?php

namespace Tests\Feature;

use App\Models\Bill;
use App\Models\Establishment;
use App\Models\EstablishmentConfig;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Table;
Use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class BillsControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected $establishment;

    protected $establishmentConfig;

    protected $table;

    protected $user;

    protected $userToken;

    protected $availableDays;

    public function setUp(): void
    {
        parent::setUp();
        $this->availableDays = json_encode(['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']);
        $this->establishment = factory(Establishment::class)->create();
        $this->establishmentConfig = factory(EstablishmentConfig::class)->create(['establishment_id' => $this->establishment->id, 'available_days' => $this->availableDays]);
        $this->table = factory(Table::class)->create(['establishment_id' => $this->establishment->id]);
        $this->user = factory(User::class)->create();
        $this->userToken = $this->user->generateToken();
    }

    /** @test **/
    public function open_bill_with_spot_token()
    {
        //Esse teste precisa garantir que uma comanda nova
        //vai ser aberta enviando um token (que será lido via QR code)

        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/open-bill', [
            '_token' => $this->userToken->sequence,
            'code' => $this->table->code,
        ])
        ->assertStatus(200)
        ->assertSee('bill');

    }

    /** @test **/
    public function get_opened_bill()
    {
        //Esse teste precisa garantir que uma comanda aberta
        //Possa ser retornada

        factory(Bill::class)->create([
            'table_id' => $this->table->id,
            'user_id' => $this->user->id,
            'status' => Bill::STATUS_OPENED,
        ]);

        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/bill', [
            '_token' => $this->userToken->sequence,
        ])
        ->assertStatus(200)
        ->assertSee('bill');

    }

    /** @test **/
    public function add_orders_to_bill()
    {
        //Esse teste precisa garantir que um pedido enviado será adicionado
        // à uma comanda aberta (com e sem itens obrigatórios)
        
        factory(Bill::class)->create([
            'table_id' => $this->table->id,
            'user_id' => $this->user->id,
            'status' => Bill::STATUS_OPENED,
        ]);

        $products = factory(Product::class,4)->create([
            'establishment_id' => $this->establishment->id,
            'product_category_id' => factory(ProductCategory::class)->create(['establishment_id' => $this->establishment->id]),
        ]);

        $orders = [];
        foreach($products as $k => $p){
            $orders[$k] = [
                'product' => $p,
                'quant' => 1,
                'obs' => 'Observação do teste de adição de pedidos',
                'options' => [],
            ];
        }

        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/bill/add-orders', [
            '_token' => $this->userToken->sequence,
            'orders' => $orders,
        ])
        ->assertStatus(200)
        ->assertSee('"success":true');
    }

    /** @test **/
    public function check_if_bill_has_closed()
    {
        //Esse teste precisa garantir que vai confirmar o encerramento
        //de uma comanda que já foi encerrada

        $bill = factory(Bill::class)->create([
            'table_id' => $this->table->id,
            'user_id' => $this->user->id,
            'status' => Bill::STATUS_CLOSED,
        ]);

        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/bill/has-closed', [
            '_token' => $this->userToken->sequence,
            '_bill' => $bill->id
        ])
        ->assertStatus(200)
        ->assertSee('"success":true');
    }
}
