<?php

namespace Tests\Feature;

use App\Models\Establishment;
use App\Models\EstablishmentConfig;
use App\Models\Menu;
Use App\Models\Table;
Use App\Models\User;
Use App\Models\Bill;
use App\Models\PreOrderProduct;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductGroup;
use App\Models\ProductGroupItem;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class MenusControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected $establishment;

    protected $establishmentConfig;

    protected $table;

    protected $user;

    protected $userToken;

    public function setUp(): void
    {
        parent::setUp();
        $this->establishment = factory(Establishment::class)->create();
        $this->establishmentConfig = factory(EstablishmentConfig::class)->create(['establishment_id' => $this->establishment->id,'location_lat' => '-5.744785','location_long' => '-35.259258',]);
        $this->table = factory(Table::class)->create(['establishment_id' => $this->establishment->id]);
        $this->user = factory(User::class)->create();
        $this->userToken = $this->user->generateToken();
    }

    /** @test **/
    public function get_establishment_menus()
    {
        //Esse teste precisa garantir que vai listar os menus (cardápios)
        //de um estabelecimento pré-definido

        factory(Menu::class,2)->create([
            'establishment_id' => $this->establishment->id,
        ]);

        //pre-order
        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/pre-order/menus', [
            'establishment_id' => $this->establishment->id,
            'latitude' => $this->establishmentConfig->location_lat,
            'longitude' => $this->establishmentConfig->location_long,
        ])
        ->assertStatus(200)
        ->assertJsonCount(2,'menus');

        //order
        factory(Bill::class)->create([ 'table_id'=>$this->table->id , 'user_id' => $this->user->id ]);
        
        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'menus', [
            '_token' => $this->userToken->sequence
        ])
        ->assertStatus(200)
        ->assertJsonCount(2);
        
    }

    /** @test **/
    public function get_establishment_menu_categories()
    {
        //Esse teste precisa garantir que as categorias de um determinado menu
        //serão retornadas com seus produtos

        $categories = factory(ProductCategory::class,2)->create([
            'establishment_id' => $this->establishment->id,
        ]);

        $menu = factory(Menu::class)->create([
            'establishment_id' => $this->establishment->id,
        ]);        
            
        $menu_products = $menu->products();
            
        $products = [];

        foreach($categories as $k => $c){
            $products[$k] = factory(Product::class)->create([
            'establishment_id' => $this->establishment->id,
            'product_category_id' => $c->id,
            ]);
            $menu_products->attach($products[$k]->id);
        }

        //pre-order
        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/pre-order/categories', [
            'menu' => $menu->id,
            'establishment_id' => $this->establishment->id
        ])
        ->assertStatus(200)
        ->assertJsonCount(2,'categories');

        //order 
        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/pre-order/categories', [
            'menu' => $menu->id,
            'establishment_id' => $this->establishment->id,
            '_token' => $this->userToken->sequence,
        ])
        ->assertStatus(200)
        ->assertJsonCount(2,'categories');

    }

    /** @test **/
    public function get_establishment_product()
    {
        //Esse teste precisa garantir que um produto pré-determinado será retornado
        //Bem como suas relações (itens obrigatórios)
        factory(Bill::class)->create([ 'table_id'=>$this->table->id , 'user_id' => $this->user->id ]);
        $product = factory(Product::class)->create([
            'establishment_id' => $this->establishment->id
        ]);
        $groups = factory(ProductGroup::class,2)->create([
            'establishment_id' => $this->establishment->id,
            'product_id' => $product->id,
            'min' => '1', 
            'max' => '1'
        ]);
        foreach($groups as $g){
            factory(ProductGroupItem::class,10)->create([
                'product_group_id' => $g->id,
                'establishment_id' => $this->establishment->id,
            ]);
        }

        //pre-order
        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/pre-order/product', [
            'product' => $product->id,
            'establishment_id' => $this->establishment->id,
        ])
        ->assertStatus(200)
        ->assertSee('product')
        ->assertJsonCount(2,'groups');


        //order
        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/product', [
            'product' => $product->id,
            '_token' => $this->userToken->sequence,
        ])
        ->assertStatus(200)
        ->assertSee('product')
        ->assertJsonCount(2,'groups');;
    }

    /** @test **/
    public function check_if_establishment_is_enabled_to_pre_order()
    {
        //Essa rota precisa garantir que um estabelecimento sem permissão de receber pedidos pre-order
        //não possa receber tais pedidos

        //gerar pedido preOrder? Estabelecimento sem preOrder; transações?

        $establishment =  factory(Establishment::class)->create();
        factory(EstablishmentConfig::class)->create([
            'establishment_id' => $establishment->id,
            'allow_pre_bills' => false,
        ]);

        $products = factory(Product::class,2)->create([
            'establishment_id' => $establishment->id,
            'product_category_id' => factory(ProductCategory::class)->create(['establishment_id' => $establishment->id])->id,
        ]);
        
        $preOrderProducts = [];

        foreach($products as $k => $p){
            $preOrderProducts[$k] = [
                'product' => $p,
                'quant' => 1,
                'obs' => factory(PreOrderProduct::class)->make()->observation,
                'options' => [],
            ];
        }

        $userToken = factory(User::class)->create()->generateToken();

        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/pre-order/add', [
            '_token' => $userToken->sequence,
            'products' => $preOrderProducts,
            'establishment_id' => $establishment->id,
            
        ])
        ->assertStatus(200)
        ->assertSee('"success":false')
        ->assertSee('"message":"Estabelecimento n\u00e3o aceita pedidos pre-pagos."');
    }
}
