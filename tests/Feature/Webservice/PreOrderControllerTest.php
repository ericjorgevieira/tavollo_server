<?php

namespace Tests\Feature;

use App\Models\Establishment;
use App\Models\EstablishmentConfig;
use App\Models\PreOrder;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class PreOrderControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected $establishment;

    protected $establishmentConfig;

    protected $user;

    protected $userToken;

    public function setUp(): void
    {
        parent::setUp();
        $this->establishment = factory(Establishment::class)->create();
        $this->establishmentConfig = factory(EstablishmentConfig::class)->create(['establishment_id' => $this->establishment->id,]);
        $this->user = factory(User::class)->create();
        $this->userToken = $this->user->generateToken();
    }

    /** @test **/
    public function create_pre_order()
    {
        //Esse teste precisa garantir que um pedido pre-order possa
        //ser criado, teoricamente após o processo de pagamento

        $products = factory(Product::class,2)->create([
            'establishment_id' => $this->establishment->id,
            'product_category_id' => factory(ProductCategory::class)->create(['establishment_id' => $this->establishment->id])->id,
        ]);
        
        $preOrderProducts = [];

        foreach($products as $k => $p){
            $preOrderProducts[$k] = [
                'product' => $p,
                'quant' => 1,
                'obs' => 'Observação de criação de pedido pre-order',
                'options' => [],
            ];
        }

        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/pre-order/add', [
            '_token' => $this->userToken->sequence,
            'products' => $preOrderProducts,
            'establishment_id' => $this->establishment->id,
            
        ])
        ->assertStatus(200)
        ->assertSee('"success":true');
    }

    /** @test **/
    public function list_user_pre_orders()
    {
        //Esse teste precisa garantir o retorno dos pedidos pre-order
        //de um determinado usuário

        factory(PreOrder::class,3)->create([
            'user_id' => $this->user->id,
            'establishment_id' => $this->establishment->id,
        ]);

        $this->withHeaders($this->getWebserviceHeaders())
        ->json('GET', $this->getUrl('ws').'/pre-order/list?_token='.$this->userToken->sequence)
        ->assertStatus(200)
        ->assertSee('"success":true')
        ->assertJsonCount(3,'preorders');
    }

    /** @test **/
    public function user_confirm_pre_order()
    {
        //Esse teste precisa garantir que, ao confirmar a entrega de um pedido
        //retorne a resposta correta para o usuário

        $preOrder = factory(PreOrder::class)->create([
            'user_id' => $this->user->id,
            'establishment_id' => $this->establishment->id,
        ]);
        
        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/pre-order/confirm', [
            '_token' => $this->userToken->sequence,
            'preorder_id' => $preOrder->id,
            
        ])
        ->assertStatus(200)
        ->assertSee('"success":true');

    }

}
