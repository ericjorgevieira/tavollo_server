<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ProfileControllerTest extends TestCase
{
    use DatabaseMigrations;

    /** @test **/
    public function update_profile()
    {
        //Essa rota precisa garantir que um usuário, ao alterar seus dados de perfil
        //tenha seus dados persistidos corretamente

        $user = factory(User::class)->create();

        $userToken = $user->generateToken();

        $email = 'unittest@gmail.com';
        $cpf = '474.535.930-69';
        
        $this->withHeaders($this->getWebserviceHeaders())
        ->json('POST', $this->getUrl('ws').'/profile/update', [
            'name' => $user->name,
            'email' => $email,
            'cpf' => $cpf,
            '_token' => $userToken->sequence,
        ])
        ->assertStatus(200)
        ->assertSee('"success":true');
    }
}
