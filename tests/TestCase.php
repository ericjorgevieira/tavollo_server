<?php

namespace Tests;

use App\Models\Establishment;
use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function signInAsAdmin($user = null, $establishment = null)
    {
        if (!$establishment) {
            $establishment = create(Establishment::class);
        }
        if (!$user) {
            $user = create(User::class, [
                'establishment_id' => $establishment->id,
                'is_establishment' => true,
                'is_admin' => true,
            ]);
        }

        auth('admin')->login($user);
    }

    public function getWebserviceHeaders()
    {
        return [
            'Authorization' => 'Bearer 771h0jij0d1h9dh9127d7u19hd1n98g9gdh0182eh0128g98ghd1',
            'content-type' => 'application/json; charset=UTF-8',
            'x-requested-with' => 'XMLHttpRequest',
        ];
    }

    public function getApiHeaders()
    {
        return [
            'content-type' => 'application/json; charset=UTF-8',
            'x-requested-with' => 'XMLHttpRequest',
        ];
    }

    public function getUrl($module = 'admin', bool $secure = false)
    {
        $url = parse_url(env('APP_URL'));
        if (empty($url)) {
            $this->fail('You should setup your APP_URL env configuration');
        }

        $port = empty($url['port']) || $url['port'] == 80 ? '' : ':'.$url['port'];

        $protocol = $secure ? 'https://' : 'http://';

        return $protocol.$module.'.tavollo.localhost'.$port.'/';
    }
}
