let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 var path = './vendor/bower_resources/';

 var scripts = [
    path + "jquery/jquery.min.js",
     path + "jquery-ui/ui/jquery-ui.js",
     path + "bootstrap/dist/js/bootstrap.min.js",
     path + "bootstrap-select/dist/js/bootstrap-select.min.js",
     // path + "datatables.net/js/jquery.dataTables.min.js",
     // path + "datatables.net-bs/js/dataTables.bootstrap.min.js",
     path + "multiselect/js/jquery.multi-select.js",
     path + "jquery-maskmoney/dist/jquery.maskMoney.min.js",
     path + "jquery.maskedinput/dist/jquery.maskedinput.min.js",
     path + "imagesloaded/imagesloaded.pkgd.min.js"
 ];

  mix.sass('resources/assets/sass/plateau.scss', 'public/css')
      .sass('resources/assets/sass/master.scss', 'public/css');
  /** Processa o JS dos templates **/
  mix.scripts(scripts, "public/js/plateau-scripts.js");

  /** Copia imagens e fontes para o Public **/
  mix.copy('resources/assets/images', 'public/images')
      .copy('resources/assets/audios', 'public/audios')
      .copy('resources/assets/translates', 'public/translates')
      .copy('resources/assets/fonts', 'public/fonts')
      .copy('resources/assets/js/plateau', 'public/js/plateau')
      .copy('resources/assets/js/service_worker', 'public/js/service_worker')
      .copy('resources/assets/js/socket', 'public/js/socket')
      .copy('node_modules/animate.css/animate.min.css', 'public/css/animate.min.css')
      .copy('node_modules/bootstrap-toggle/css/bootstrap-toggle.min.css', 'public/css/bootstrap-toggle.min.css')
      .copy('node_modules/font-awesome/fonts', 'public/fonts')
      .copy(path + 'bootstrap/dist/fonts', 'public/fonts/bootstrap');

  mix.js('resources/assets/js/plateau.js', 'public/js');
  mix.js('resources/assets/js/master.js', 'public/js');

  /** Adiciona Token aos scripts **/
  if (mix.inProduction()) {
    mix.version([
       'public/css/*.css',
       'public/js/*.js'
    ]);
  }
